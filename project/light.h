#pragma once

#include <scene.h>
#include <glm/glm.hpp>
#include <utils/color_helpers.h>

class Light : public SceneObject
{
public:
	Light( const std::string& name );
	~Light() {}

	void _update(float dT) override;

	glm::vec4 ortho_bounds;

	glm::mat4 view_mat;
	glm::mat4 proj_mat;

	color_helpers::color_RGB_t<> color = {1.f, 1.f, 1.f};
	float intensity = 1.f;
};


