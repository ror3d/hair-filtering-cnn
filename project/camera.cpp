#include "camera.h"
#include <utils/gl_helpers.h>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <utils/math_helpers.h>
#include <imgui.h>

using namespace glm;


Camera::Camera( const std::string& name )
	: SceneObject(name)
	, view_mat(1)
	, proj_mat(1)
{
}

void Camera::setFoV( float _fov )
{
	fov = _fov;
	updateProjMat();
}

void Camera::setClipPlanes( float near, float far )
{
	near_plane = near;
	far_plane = far;
	updateProjMat();
}

void Camera::setDirection( vec3 dir )
{
	vec3 up = vec3(rotate( roll, Scene::worldFwd ) * vec4(Scene::worldUp, 0));
	orientation = math_helpers::qLookAt( dir, Scene::worldFwd, up );
}

void Camera::setRoll( float angle )
{
	roll = angle;
	vec3 dir = rotate( orientation, Scene::worldFwd );
	setDirection( dir );
}

void Camera::updateWindowSize( float width, float height )
{
	viewportWidth = width;
	viewportHeight = height;
	updateProjMat();
}

void Camera::updateProjMat()
{
	if ( fov > 0 )
	{
		proj_mat = perspective( radians( fov ), viewportWidth / viewportHeight, near_plane, far_plane );
	}
	else
	{
		proj_mat = ortho( -viewportWidth/2, viewportWidth/2, -viewportHeight/2, viewportHeight/2, 0.1f, 100.f );
	}
}

void Camera::_update( float dT )
{
	vec3 direction = rotate( orientation, Scene::worldFwd );
	if ( control_enabled )
	{
		ImGuiIO& io = ImGui::GetIO();

		vec3 cameraRight = cross( direction, Scene::worldUp );
		if ( io.MouseDown[0] && !io.WantCaptureMouse )
		{
			quat yaw = angleAxis( rotationSpeed * -io.MouseDelta.x, Scene::worldUp );
			quat pitch = angleAxis( rotationSpeed * -io.MouseDelta.y, cameraRight );
			orientation = pitch * yaw * orientation;
		}

		if ( !io.WantCaptureKeyboard )
		{
			if ( io.KeysDown[SDL_SCANCODE_W] )
			{
				position += speed * dT * direction;
			}
			if ( io.KeysDown[SDL_SCANCODE_S] )
			{
				position -= speed * dT * direction;
			}
			if ( io.KeysDown[SDL_SCANCODE_A] )
			{
				position -= speed * dT * cameraRight;
			}
			if ( io.KeysDown[SDL_SCANCODE_D] )
			{
				position += speed * dT * cameraRight;
			}
			if ( io.KeysDown[SDL_SCANCODE_Q] )
			{
				position -= speed * dT * Scene::worldUp;
			}
			if ( io.KeysDown[SDL_SCANCODE_E] )
			{
				position += speed * dT * Scene::worldUp;
			}
		}
	}

	vec3 up = vec3(rotate( roll, Scene::worldFwd ) * vec4(Scene::worldUp, 0));
	view_mat = lookAt(position, position + direction, up);
}

void Camera::_render( uint32_t shaderProgram ) const
{
	gl_helpers::setUniformSlow(shaderProgram, "viewMatrix", view_mat);
	gl_helpers::setUniformSlow(shaderProgram, "projectionMatrix", proj_mat);
	gl_helpers::setUniformSlow(shaderProgram, "viewProjectionMatrix", proj_mat * view_mat);
	gl_helpers::setUniformSlow(shaderProgram, "invViewProjectionMatrix", inverse(proj_mat * view_mat));
	gl_helpers::setUniformSlow(shaderProgram, "invProjectionMatrix", inverse(proj_mat));
}

glm::mat4 Camera::getViewMatrix() const
{
	return view_mat;
}

glm::mat4 Camera::getProjMatrix() const
{
	return proj_mat;
}


