#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

in vec2 v_position;

layout(location = 0) out vec4 fragmentColor;

layout(binding = 0) uniform sampler2D colorTex;
uniform vec3 color; 

float f(float x)
{
	float xx = (1-(x-1)*(x-1));	
	return xx*xx;
}

void main() 
{
	// Shape for this is now 1 channel for each of {color, specular, alpha, 0}
	vec4 col = texture(colorTex, (v_position + 1)/2);

	float alpha = clamp(col.z, 0.0, 1.0);

	float specular = max(0.0, col.y);

	float color_factor = col.x;

	// This is the activation function used in the network for the color factor
	color_factor = pow(color_factor, 3) / 32.0 + color_factor / 5.0 + 1.0;

	vec3 lit = (color_factor * color + specular * vec3(1, 1, 1));

	//float alpha = min(exp(col.z) - 1, 1);
	//float alpha = min(col.z, 1);
	//float alpha = min(pow(3.2, col.z) - 1, 1);
	//float alpha = smoothstep(0.0, 0.5, min(col.z, 1));

	//fragmentColor = vec4(lit.xyz, col.z);
	//fragmentColor = vec4(lit.xyz, max(col.z, 1));

	//fragmentColor = vec4(lit.xyz, sqrt(min(col.z, 1)));
	//fragmentColor = vec4(lit.xyz, (tanh((col.z - 0.5) * 4) +1)/2);
	//fragmentColor = vec4(lit.xyz, 1 - (col.z-1)*(col.z-1));

	//float x = col.z-0.5;
	//float c = pow(sqrt(1296*x*x + 1) - 36*x, 1/3.0);
	//fragmentColor = vec4(lit.xyz, (1/c - c)/6 + 0.5);

	//fragmentColor = vec4(vec3(alpha), 1);
	fragmentColor = vec4(lit.xyz, alpha);
}
