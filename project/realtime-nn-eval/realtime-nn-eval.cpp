#include <GL/glew.h>
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <chrono>

#include <utils/gl_helpers.h>
#include <utils/math_helpers.h>
#include <imgui/imgui.h>
#include <utils/image.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
using namespace glm;

#include <camera.h>
#include <light.h>
#include "utils/fbo.h"

#include <scene.h>
#include <hair_model.h>
#include <module.h>
#include <utils/settings.h>

#include <fstream>

#include <utils/log.h>
#include <utils/perf.h>

#include <array>

#include <realtime-nn-eval/realtime-nn-eval-model.h>


using std::min;
using std::max;

using namespace glm;

extern uint32_t frame_number;

class RealtimeNNEvalRenderer : public Module
{
	int windowWidth, windowHeight;

	float hair_thickness_world_space = 0.00007; // 70um

	uint frames_till_next_save = 3;

	///////////////////////////////////////////////////////////////////////////////
	// Shader programs
	///////////////////////////////////////////////////////////////////////////////
	GLuint shadowMapShader = 0;
	GLuint fxShaderProgram = 0;
	GLuint multisampleStockAlphaShader = 0;
	GLuint colorLightShader = 0;
	GLuint separateMsShader = 0;

	GLuint objShaderProgram = 0;
	GLuint objShadowShaderProgram = 0;

	GLuint envShaderProgram;

	///////////////////////////////////////////////////////////////////////////////
	// Color stuff
	///////////////////////////////////////////////////////////////////////////////
	Framebuffer filteredFB;
	Framebuffer screenFB;

	Framebuffer sceneFB;

	Framebuffer multisampleFB;
	Framebuffer separateMsFB;

	Framebuffer shadowMapFB;

	CnnModel* model;

	CnnModel::Shape model_shape = CnnModel::Shape_3step_medium_4spp;
	uint32_t model_shape_spp = 4;


	float line_width = 1.0;

public:
	virtual const char* getModuleName() override
	{
		return "Realtime NN Eval";
	}

	void loadShaders(bool is_reload)
	{
		GLuint shader;

		gl_helpers::setCompileDefinition( "NETWORK_INPUT_SPP", std::to_string( model_shape_spp ) );
		
		gl_helpers::tryLoadShaderProgram(&fxShaderProgram, "fx.vert", "fx.frag", is_reload);

#if USE_QUADS_FOR_LINES
		gl_helpers::tryLoadShaderProgram( &shadowMapShader,
		                                  "stoch-alpha-rendering/shadowmap.vert",
		                                  "hair_shadow_geometry_shader.geom",
		                                  "stoch-alpha-rendering/shadowmap.frag",
		                                  is_reload );
#else
		gl_helpers::tryLoadShaderProgram(
		        &shadowMapShader, "stoch-alpha-rendering/shadowmap.vert", "stoch-alpha-rendering/shadowmap.frag", is_reload );
#endif


		gl_helpers::setCompileDefinition( "RENDER_EXTRA_TRAINING_DATA", "1" );

		gl_helpers::setCompileDefinition( "CHECK_AGAINST_DEPTH_TEXTURE", "1" );
#if USE_QUADS_FOR_LINES
		gl_helpers::tryLoadShaderProgram( &multisampleStockAlphaShader,
		                                  "stoch-alpha-rendering/shading.vert",
		                                  "hair_geometry_shader.geom",
		                                  "msaa-sa-rendering/msaa-shading.frag",
		                                  is_reload );
#else
		gl_helpers::tryLoadShaderProgram( &multisampleStockAlphaShader,
		                                  "stoch-alpha-rendering/shading.vert",
		                                  "msaa-sa-rendering/msaa-shading.frag",
		                                  is_reload );
#endif
		gl_helpers::clearCompileDefinition( "CHECK_AGAINST_DEPTH_TEXTURE" );

		gl_helpers::tryLoadShaderProgram( &objShaderProgram, "model-shading.vert", "model-shading.frag", is_reload );
		gl_helpers::tryLoadShaderProgram( &objShadowShaderProgram, "model-shadow.vert", "model-shadow.frag", is_reload );

		gl_helpers::tryLoadShaderProgram( &colorLightShader, "fx.vert", "realtime-nn-eval/color-light.frag", is_reload );

		gl_helpers::tryLoadShaderProgram( &separateMsShader, "fx.vert", "realtime-nn-eval/msaa-samples-to-buffers.frag", is_reload );

		gl_helpers::tryLoadShaderProgram( &envShaderProgram, "fx.vert", "environment.frag", is_reload);
	}

	virtual void reloadShaders() override
	{
		loadShaders( true );
	}

	virtual bool setup( uint32_t width, uint32_t height ) override
	{
		windowWidth = width;
		windowHeight = height;

		model = new CnnModel();

		std::string model_shape_str = chag::Settings::getGlobalInstance().get( "model-shape", "" );
		
		// No longer passing the model as a parameter, it's hardcoded in the loadFromFile function now,
		// since the model and the loaded parameters are too interdependent.
		//chag::Settings::getGlobalInstance().bindCommandArgument( "--model", "model-file" );
		//std::string model_file = chag::Settings::getGlobalInstance().get( "model-file", "" );
		model_shape_spp = 4;
		if ( model_shape_str == "1spp"s )
		{
			model_shape = CnnModel::Shape_1spp;
			model_shape_spp = 1;
		}
		else if ( model_shape_str == "2spp"s )
		{
			model_shape = CnnModel::Shape_2spp;
			model_shape_spp = 2;
		}
		else if ( model_shape_str == "2step"s )
		{
			model_shape = CnnModel::Shape_2step;
		}
		else if ( model_shape_str == "4step"s )
		{
			model_shape = CnnModel::Shape_4step;
		}
		else if ( model_shape_str == "tiny"s )
		{
			model_shape = CnnModel::Shape_tiny;
		}
		else if ( model_shape_str == "large"s )
		{
			model_shape = CnnModel::Shape_large;
		}
		else
		{
			model_shape = CnnModel::Shape_3step_medium_4spp;
		}

		loadShaders( false );

		model->loadFromFile( model_shape );

		multisampleFB.init( 2, true );
		multisampleFB.setMultisample( model_shape_spp );
		multisampleFB.resize( width, height );

		sceneFB.init( 1, true );
		sceneFB.resize( width, height );

		glBindTexture( GL_TEXTURE_2D, sceneFB.depthBuffer );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER );
		vec4 border( 1.f );
		glTexParameterfv( GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &border.x );
		glBindTexture( GL_TEXTURE_2D, 0 );


		if ( model_shape == CnnModel::Shape_1spp )
		{
			separateMsFB.init(2);
		}
		else if ( model_shape == CnnModel::Shape_2spp )
		{
			separateMsFB.init(3);
		}
		else
		{
			separateMsFB.init(6);
		}
		separateMsFB.resize( width, height );

		filteredFB.init();
		filteredFB.resize( width, height );

		screenFB.init();
		screenFB.resize( width, height );

		model->setOutputTextures( width, height, filteredFB.colorTextureTargets[0] );

		///////////////////////////////////////////////////////////////////////
		// Setup Framebuffer for shadow map rendering
		///////////////////////////////////////////////////////////////////////
		shadowMapFB.init(0, true);
		shadowMapFB.resize( 1024, 1024 );

		glBindTexture( GL_TEXTURE_2D, shadowMapFB.depthBuffer );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE );

		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER );
		border = vec4( 1.f );
		glTexParameterfv( GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &border.x );

		glBindTexture( GL_TEXTURE_2D, 0 );

		glEnable(GL_DEPTH_TEST); // enable Z-buffering
		glDisable(GL_CULL_FACE);  // enables backface culling

		frames_till_next_save = 2;

		return true;
	}

	virtual void teardown() override
	{
		screenFB.free();

		filteredFB.free();
		shadowMapFB.free();

		sceneFB.free();
		multisampleFB.free();
		separateMsFB.free();

		delete model;

		gl_helpers::unloadShaderProgram( shadowMapShader );
		gl_helpers::unloadShaderProgram( fxShaderProgram );
		gl_helpers::unloadShaderProgram( multisampleStockAlphaShader );
		gl_helpers::unloadShaderProgram( colorLightShader );
		gl_helpers::unloadShaderProgram( separateMsShader );
		gl_helpers::unloadShaderProgram( objShaderProgram );
		gl_helpers::unloadShaderProgram( objShadowShaderProgram );
		gl_helpers::unloadShaderProgram( envShaderProgram );
	}

	virtual void update( Scene* scene, float deltaTime ) override
	{
		if ( ImGui::Button( "Dump" ) )
		{
			model->getOutput();
		}

		ImGui::DragFloat( "Hair Thickness", &hair_thickness_world_space, 0.001, 0, 10, "%.5f", ImGuiSliderFlags_Logarithmic );

		ImGui::DragFloat( "Line Width", &line_width, 0.5, 0.01, 20, "%.3f", ImGuiSliderFlags_Logarithmic );
	}

	virtual void windowSizeChanged( uint32_t width, uint32_t height ) override
	{
		windowWidth = width;
		windowHeight = height;

		screenFB.resize( width, height );
		filteredFB.resize( width, height );
		sceneFB.resize( width, height );
		multisampleFB.resize( width, height );
		separateMsFB.resize( width, height );
	}

	virtual void render( const Scene* scene, Framebuffer& target_framebuffer ) override
	{
		if ( shadowMapFB.width != scene->shadowmap.size || shadowMapFB.height != scene->shadowmap.size )
		{
			shadowMapFB.resize( scene->shadowmap.size, scene->shadowmap.size );
		}

		glLineWidth( 1.0 );

		///////////////////////////////////////////////////////////////////////////
		// setup matrices
		///////////////////////////////////////////////////////////////////////////
		mat4 projMatrix = scene->active_camera->as<Camera>()->getProjMatrix();
		mat4 viewMatrix = scene->active_camera->as<Camera>()->getViewMatrix();

		mat4 lightProjMatrix = scene->lights[0]->as<Light>()->proj_mat;
		mat4 lightViewMatrix = scene->lights[0]->as<Light>()->view_mat;
		mat4 lightViewProjMatrix = lightProjMatrix * lightViewMatrix;

		mat4 lightMatrix = translate( vec3( 0.5f ) ) * scale( vec3( 0.5f ) ) * lightProjMatrix * lightViewMatrix * inverse( viewMatrix );

		///////////////////////////////////////////////////////////////////////////
		// Draw from light
		///////////////////////////////////////////////////////////////////////////
		{
			chag::perf::Scope s( "Shadow Map" );

			shadowMapFB.bind();
			shadowMapFB.clear( { 1, 1, 1, 1 }, 1 );

			if ( chag::Settings::getGlobalInstance().get( "display_head_model", false ) )
			{
				glUseProgram( objShadowShaderProgram );
				gl_helpers::setUniformSlow( objShadowShaderProgram, "viewProjectionMatrix", lightViewProjMatrix );
				for ( auto& obj : scene->objects )
				{
					if ( !obj->hasAlpha() )
					{
						obj->render( objShadowShaderProgram );
					}
				}
			}

			glUseProgram( shadowMapShader );

			gl_helpers::setUniformSlow( shadowMapShader, "frame_number", frame_number );

			gl_helpers::setUniformSlow( shadowMapShader, "line_width", line_width );

			gl_helpers::setUniformSlow(shadowMapShader, "viewProjectionMatrix", lightViewProjMatrix );
			gl_helpers::setUniformSlow(shadowMapShader, "projectionMatrix", lightProjMatrix);
			gl_helpers::setUniformSlow(shadowMapShader, "invViewProjectionMatrix", inverse( lightViewProjMatrix ));
			gl_helpers::setUniformSlow(shadowMapShader, "invProjectionMatrix", inverse(lightProjMatrix));


			gl_helpers::setUniformSlow( shadowMapShader, "line_offset_factor", scene->shadowmap.polygon_offset_factor );
			gl_helpers::setUniformSlow( shadowMapShader, "line_offset_units", scene->shadowmap.polygon_offset_units );

			gl_helpers::setUniformSlow( shadowMapShader, "hair_thickness_world_space", hair_thickness_world_space );

			gl_helpers::setUniformSlow( shadowMapShader, "screen_size", glm::vec2( shadowMapFB.width, shadowMapFB.height ) );

			{
				chag::perf::Scope s( "Render" );
				for ( auto& obj : scene->objects )
				{
					if ( obj->as<HairModel>() )
					{
						obj->render( shadowMapShader );
					}
				}
			}

			glUseProgram( 0 );
		}

	
		///////////////////////////////////////////////////////////////////////////
		// Draw from camera
		///////////////////////////////////////////////////////////////////////////


		///////////////////////////////////////////////////////////////////////////
		// Draw multisampled buffer
		///////////////////////////////////////////////////////////////////////////
		{
			chag::perf::Scope s( "Camera View MSAA" );

			////////////////////////////////////
			// Draw the scene with the head
			sceneFB.bind();
			sceneFB.clear_colors( { { 0, 0, 0, 1 } } );
			sceneFB.clear_depth();

			if ( scene->environment.texture && chag::Settings::getGlobalInstance().get( "display_envmap", false ) )
			{
				mat4 projInvMatrix = glm::inverse( projMatrix );
				mat4 viewInvMatrix = glm::inverse( viewMatrix );

				glUseProgram( envShaderProgram );
				glActiveTexture( GL_TEXTURE0 );
				glBindTexture( GL_TEXTURE_2D, scene->environment.texture );
				glDepthMask( false );
				gl_helpers::setUniformSlow( envShaderProgram, "ViewInverse", viewInvMatrix );
				gl_helpers::setUniformSlow( envShaderProgram, "ProjInverse", projInvMatrix );
				gl_helpers::setUniformSlow( envShaderProgram, "EnvRot", scene->environment.rotate );
				gl_helpers::drawFullScreenQuad();
				glDepthMask( true );
			}


			if ( chag::Settings::getGlobalInstance().get( "display_head_model", false ) )
			{ 
				vec3 light_position_viewspace = vec3( viewMatrix * vec4( scene->lights[0]->getPosition(), 1.0f ) );
				float light_intensity = scene->lights[0]->as<Light>()->intensity;

				glUseProgram(objShaderProgram);
				scene->active_camera->render( objShaderProgram );

				gl_helpers::setUniformSlow( objShaderProgram, "light_position_viewspace", light_position_viewspace);
				gl_helpers::setUniformSlow( objShaderProgram, "light_intensity", vec3( light_intensity ) ); // scene->lights[0]->as<Light>()->color ) * 
				gl_helpers::setUniformSlow( objShaderProgram, "frame_number", frame_number );

				for ( auto& obj : scene->objects )
				{
					if ( !obj->hasAlpha() )
					{
						obj->render( objShaderProgram );
					}
				}
			}

			////////////////////////////////////
			// Draw the hair

			multisampleFB.bind();
			multisampleFB.clear_colors( { { 0, 0, 0, 0 }, {-1, -1, -1, -1} } );
			multisampleFB.clear_depth();

			glUseProgram( multisampleStockAlphaShader );

			glActiveTexture( GL_TEXTURE16 );
			glBindTexture( GL_TEXTURE_2D, sceneFB.depthBuffer );

			gl_helpers::setUniformSlow( multisampleStockAlphaShader, "scene_depth_texture_present", 1 );

			glActiveTexture( GL_TEXTURE10 );
			glBindTexture( GL_TEXTURE_2D, shadowMapFB.depthBuffer );

			gl_helpers::setUniformSlow( multisampleStockAlphaShader, "frame_number", frame_number );

			gl_helpers::setUniformSlow( multisampleStockAlphaShader, "line_width", line_width );

			gl_helpers::setUniformSlow( multisampleStockAlphaShader, "light_position_viewspace", vec3( viewMatrix * vec4( scene->lights[0]->getPosition(), 1.0f ) ) );
			gl_helpers::setUniformSlow( multisampleStockAlphaShader, "lightMatrix", lightMatrix );
			gl_helpers::setUniformSlow( multisampleStockAlphaShader, "light_intensity", scene->lights[0]->as<Light>()->intensity );

			gl_helpers::setUniformSlow( multisampleStockAlphaShader, "hair_thickness_world_space", hair_thickness_world_space );

			gl_helpers::setUniformSlow( multisampleStockAlphaShader, "screen_size", glm::vec2( multisampleFB.width, multisampleFB.height ) );

			gl_helpers::setUniformSlow( multisampleStockAlphaShader, "user_display_mode", 0u );
			gl_helpers::setUniformSlow( multisampleStockAlphaShader, "generate_train_data_mode", 0u );
			gl_helpers::setUniformSlow( multisampleStockAlphaShader, "eval_mode", 1u );

			scene->active_camera->render( multisampleStockAlphaShader );

			{
				chag::perf::Scope s( "Render" );
				for ( auto& obj : scene->objects )
				{
					if ( obj->as<HairModel>() )
					{
						obj->render( multisampleStockAlphaShader );
					}
				}
			}

			glActiveTexture( GL_TEXTURE10 );
			glBindTexture( GL_TEXTURE_2D, 0 );
			glActiveTexture( GL_TEXTURE11 );
			glBindTexture( GL_TEXTURE_2D, 0 );
			glUseProgram( 0 );
		}

		{
			chag::perf::Scope s( "Separate multisampled buffers" );
			separateMsFB.bind();
			separateMsFB.clear_color();

			glUseProgram( separateMsShader );

			glActiveTexture( GL_TEXTURE0 );
			glBindTexture( GL_TEXTURE_2D_MULTISAMPLE, multisampleFB.colorTextureTargets[0] );

			glActiveTexture( GL_TEXTURE1 );
			glBindTexture( GL_TEXTURE_2D_MULTISAMPLE, multisampleFB.colorTextureTargets[1] );

			gl_helpers::drawFullScreenQuad();
		}


		///////////////////////////////////////////////////////////////////////////
		// Filter hair
		///////////////////////////////////////////////////////////////////////////

		{
			// TODO: Only do this on reshape!
			if (!model->isInputSet())
			{
				chag::perf::Scope s("Model->setInput");
				if ( model_shape == CnnModel::Shape_1spp )
				{
					model->setInput1spp( separateMsFB.width,
					                     separateMsFB.height,
					                     separateMsFB.colorTextureTargets[0],
					                     separateMsFB.colorTextureTargets[1] );
				}
				else if ( model_shape == CnnModel::Shape_2spp )
				{
					model->setInput2spp( separateMsFB.width,
					                     separateMsFB.height,
					                     separateMsFB.colorTextureTargets[0],
					                     separateMsFB.colorTextureTargets[1],
					                     separateMsFB.colorTextureTargets[2] );
				}
				else
				{
					model->setInput( separateMsFB.width,
									 separateMsFB.height,
									 separateMsFB.colorTextureTargets[0],
									 separateMsFB.colorTextureTargets[1],
									 separateMsFB.colorTextureTargets[2],
									 separateMsFB.colorTextureTargets[3],
									 separateMsFB.colorTextureTargets[4],
									 separateMsFB.colorTextureTargets[5] );
				}
			}
		}

		model->evaluate();



		{
			chag::perf::Scope s( "Merge Light Components" );
			screenFB.bind();
			screenFB.clear();


			glUseProgram( colorLightShader );

			glActiveTexture( GL_TEXTURE0 );
			glBindTexture( GL_TEXTURE_2D, filteredFB.colorTextureTargets[0] );

			for ( auto& obj : scene->objects )
			{
				HairModel* hair = obj->as<HairModel>();
				if ( hair )
				{
					gl_helpers::setUniformSlow( colorLightShader, "color", hair->getRenderingColor() );
					break;
				}
			}

			glDisable( GL_BLEND );

			gl_helpers::drawFullScreenQuad();
		}

		{
			chag::perf::Scope s( "Copy to Target" );
			target_framebuffer.bind();

			sceneFB.blit( target_framebuffer );

			glUseProgram( fxShaderProgram );

			glActiveTexture( GL_TEXTURE0 );

			if ( model_shape == CnnModel::Shape::Shape_3step_medium_4spp )
			{
				static int selected = 0;
				ImGui::RadioButton( "Filtered", &selected, 0 );
				ImGui::RadioButton( "Color", &selected, 1 );
				ImGui::RadioButton( "Specular", &selected, 2 );
				ImGui::RadioButton( "Alpha", &selected, 3 );
				ImGui::RadioButton( "Depth", &selected, 4 );
				ImGui::RadioButton( "Tangent 1", &selected, 5 );
				ImGui::RadioButton( "Tangent 2", &selected, 6 );
				ImGui::RadioButton( "Filtered color", &selected, 9 );

				switch ( selected )
				{
					case 0:
						glBindTexture( GL_TEXTURE_2D, screenFB.colorTextureTargets[0] );
						break;
					case 1:
						glBindTexture( GL_TEXTURE_2D, separateMsFB.colorTextureTargets[0] );
						break;
					case 2:
						glBindTexture( GL_TEXTURE_2D, separateMsFB.colorTextureTargets[1] );
						break;
					case 3:
						glBindTexture( GL_TEXTURE_2D, separateMsFB.colorTextureTargets[2] );
						break;
					case 4:
						glBindTexture( GL_TEXTURE_2D, separateMsFB.colorTextureTargets[3] );
						break;
					case 5:
						glBindTexture( GL_TEXTURE_2D, separateMsFB.colorTextureTargets[4] );
						break;
					case 6:
						glBindTexture( GL_TEXTURE_2D, separateMsFB.colorTextureTargets[5] );
						break;
					case 9:
						glBindTexture( GL_TEXTURE_2D, filteredFB.colorTextureTargets[0] );
						break;
				}
			}
			else
			{
				glBindTexture( GL_TEXTURE_2D, screenFB.colorTextureTargets[0] );
			}

			glEnable( GL_BLEND );
			glBlendEquation( GL_FUNC_ADD );
			glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
			//glBlendFuncSeparate( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ZERO, GL_ONE );

			gl_helpers::drawFullScreenQuad();

			glBindTexture( GL_TEXTURE_2D, 0 );

			glUseProgram( 0 );

			glDisable( GL_BLEND );
		}

		if ( frames_till_next_save > 0 )
		{
			frames_till_next_save -= 1;
		}
	}

	bool readyToSaveFrame() const override
	{
		if ( frames_till_next_save == 0 )
		{
			return true;
		}
		return false;
	}

	void saveFrameToFile( const std::filesystem::path& base_file_name, Framebuffer& target_framebuffer ) override
	{
		bool exporting_teaser = chag::Settings::getGlobalInstance().get( "exporting_teaser", false );

		std::filesystem::path base_name = base_file_name.stem();

		std::filesystem::path filtered_name = base_file_name.parent_path() / "filtered_";

		switch ( model_shape )
		{
			case CnnModel::Shape::Shape_1spp:
				filtered_name += "1spp";
				break;

			case CnnModel::Shape::Shape_2spp:
				filtered_name += "2spp";
				break;

			case CnnModel::Shape::Shape_3step_medium_4spp:
				filtered_name += "medium";
				break;

			case CnnModel::Shape::Shape_2step:
				filtered_name += "2step";
				break;

			case CnnModel::Shape::Shape_4step:
				filtered_name += "4step";
				break;

			case CnnModel::Shape::Shape_tiny:
				filtered_name += "tiny";
				break;

			case CnnModel::Shape::Shape_large:
				filtered_name += "large";
				break;

			default:
				LOG_ERROR( "Not implemented!" );
		}

		filtered_name += ".png";

		if ( exporting_teaser )
		{
			target_framebuffer.bind();
			sceneFB.blit( target_framebuffer );

			glUseProgram( fxShaderProgram );
			glActiveTexture( GL_TEXTURE0 );
			glBindTexture( GL_TEXTURE_2D, screenFB.colorTextureTargets[0] );
			glEnable( GL_BLEND );
			glBlendEquation( GL_FUNC_ADD );
			glBlendFuncSeparate( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ZERO, GL_ONE );

			gl_helpers::drawFullScreenQuad();

			glBindTexture( GL_TEXTURE_2D, 0 );
			glUseProgram( 0 );
			glDisable( GL_BLEND );

			target_framebuffer.saveToFile( filtered_name );
		}
		else
		{
			// Save shaded color
			target_framebuffer.bind();
			target_framebuffer.clear();
			glUseProgram( fxShaderProgram );
			glActiveTexture( GL_TEXTURE0 );
			glBindTexture( GL_TEXTURE_2D, screenFB.colorTextureTargets[0] );

			gl_helpers::drawFullScreenQuad();

			glActiveTexture( GL_TEXTURE0 );
			glBindTexture( GL_TEXTURE_2D, 0 );

			target_framebuffer.saveToFile( filtered_name );
		}

		frames_till_next_save = 2;
	}
};

std::shared_ptr<Module> makeRealtimeNNEvalRenderer()
{
	return std::make_shared<RealtimeNNEvalRenderer>();
}


