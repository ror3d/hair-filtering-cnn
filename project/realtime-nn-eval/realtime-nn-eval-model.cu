
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
using namespace glm;

#include <GL/glew.h>

#include "realtime-nn-eval-model.h"
#include <cublas_v2.h>
#include <cuda_fp16.h>
#include <cuda_gl_interop.h>
#include <cuda_runtime.h>
#include <cudnn.h>
#include <surface_functions.h>

#include <utils/log.h>

#include <cuda-utils/error_handling.cuh>
#include <cuda-utils/layers.cuh>
#include <utils/perf.h>
#include <cuda-utils/tensor.cuh>

#include <fstream>
#include <utils/base64.h>
#include <utils/json.hpp>

#include <stb/stb_image_write.h>
#include <fstream>



#define COPY_TENSOR_TO_IMAGE_USE_SURFACE 1


#define MODEL_PARAMS_FILE_1SPP "../nn/trained_networks/1spp/epoch_250_model.json"
#define MODEL_PARAMS_FILE_2SPP "../nn/trained_networks/2spp/epoch_250_model.json"
#define MODEL_PARAMS_FILE_3STEP_MEDIUM_4SPP "../nn/trained_networks/3step-medium-4spp/epoch_250_model.json"
#define MODEL_PARAMS_FILE_2STEP "../nn/trained_networks/2step/epoch_250_model.json"
#define MODEL_PARAMS_FILE_4STEP "../nn/trained_networks/4step/epoch_250_model.json"
#define MODEL_PARAMS_FILE_LARGE "../nn/trained_networks/large/epoch_250_model.json"
#define MODEL_PARAMS_FILE_TINY "../nn/trained_networks/tiny/epoch_250_model.json"


CnnModel::CnnModel()
{
	dnn::initializeCUDA();
}

CnnModel::~CnnModel()
{
	for ( auto l : layers )
	{
		delete l.second;
	}
	layers.clear();
}

static std::vector<float> getFloatDataAndCheckSize( const std::string& b64, size_t expected_size )
{
	auto data = b64decode_floats( b64 );
	if ( data.size() != expected_size )
	{
		LOG_ERROR( "Input data size doesn't match target tensor size in the device!" );
		return std::vector<float>();
	}
	return data;
}

static void setTensorDataFromJSON( dnn::tensor4D_base* t,
                                   nlohmann::json& field,
                                   bool transpose_to_nhwc = false,
                                   int pad_n_dimension = 0,
                                   int pad_c_dimension = 0 )
{
	dnn::tensor_dimension read_filter_shape;

	if ( field["shape"].size() == 4 )
	{
		read_filter_shape.n = field["shape"][0];
		read_filter_shape.c = field["shape"][1];
		read_filter_shape.h = field["shape"][2];
		read_filter_shape.w = field["shape"][3];
	}
	else if ( field["shape"].size() == 1 )
	{
		read_filter_shape.n = 1;
		read_filter_shape.c = field["shape"][0];
		read_filter_shape.h = 1;
		read_filter_shape.w = 1;
	}
	else
	{
		LOG_ERROR( "Unrecognised input shape in JSON! Expected 4 dimensions for NCHW or 1 for C, but got {}", field["shape"].size() );
		return;
	}

	dnn::tensor_dimension padded_filter_shape = t->dim; 

	dnn::tensor_dimension unpadded_expected_shape = padded_filter_shape;
	unpadded_expected_shape.n -= pad_n_dimension; 
	unpadded_expected_shape.c -= pad_c_dimension; 

	if ( unpadded_expected_shape != read_filter_shape )
	{
		LOG_ERROR( "Tensor didn't have the expected shape" );
		return;
	}

	std::string values = field.value( "values", "" );
	auto d = getFloatDataAndCheckSize( values, read_filter_shape.size() );


	if (pad_n_dimension || pad_c_dimension)
	{
		dnn::tensor_dimension read_filter_stride;
		read_filter_stride.w = 1;
		read_filter_stride.h = read_filter_stride.w * read_filter_shape.w;
		read_filter_stride.c = read_filter_stride.h * read_filter_shape.h;
		read_filter_stride.n = read_filter_stride.c * read_filter_shape.c;

		dnn::tensor_dimension padded_filter_stride;
		padded_filter_stride.w = 1;
		padded_filter_stride.h = padded_filter_stride.w * padded_filter_shape.w;
		padded_filter_stride.c = padded_filter_stride.h * padded_filter_shape.h;
		padded_filter_stride.n = padded_filter_stride.c * padded_filter_shape.c;

		std::vector<float> padded(padded_filter_shape.size(), 0.0f);
		for ( int n = 0; n < read_filter_shape.n; n++ )
		{
			for ( int c = 0; c < read_filter_shape.c; c++ )
			{
				for ( int h = 0; h < read_filter_shape.h; h++ )
				{
					for ( int w = 0; w < read_filter_shape.w; w++ )
					{
						padded[n * padded_filter_stride.n
							   + c * padded_filter_stride.c
							   + h * padded_filter_stride.h
						       + w * padded_filter_stride.w] =
									d[n * read_filter_stride.n
										+ c * read_filter_stride.c
										+ h * read_filter_stride.h
										+ w * read_filter_stride.w];
					}
				}
			}
		}
		d = padded;
	}
	
	if ( transpose_to_nhwc )
	{
		d = dnn::nchw2nhwc( d, padded_filter_shape );
	}


	if ( d.empty() )
	{
		LOG_FATAL( "Error loading input tensor" );
	}
	t->SetData( d.data() );
}

template<typename T>
static void loadConvFromJson(
        T* conv, nlohmann::json& js, const std::string& layer_name, int pad_out_channels = 0, int pad_in_channels = 0, bool transposed = false )
{
	auto weight = js[layer_name + ".weight"];
	auto bias = js[layer_name + ".bias"];

	if ( transposed )
	{
		setTensorDataFromJSON( &conv->f, weight, true, pad_in_channels, pad_out_channels );
	}
	else
	{
		setTensorDataFromJSON( &conv->f, weight, true, pad_out_channels, pad_in_channels );
	}

	setTensorDataFromJSON( &conv->bias, bias, false, 0, pad_out_channels );
}

template<typename T>
static void loadBatchNormFromJson( T* conv, nlohmann::json& js, const std::string& layer_name )
{
	auto& weight = js[layer_name + ".weight"];
	auto& bias = js[layer_name + ".bias"];
	auto& mean = js[layer_name + ".running_mean"];
	auto& var = js[layer_name + ".running_var"];

	dnn::tensor_dimension shape;
	shape.n = 1;
	shape.c = conv->bnBias.dim.c;
	shape.h = 1;
	shape.w = 1;

	if ( !(shape.c == weight["shape"][0] && shape.c == bias["shape"][0] && shape.c == mean["shape"][0] && shape.c == var["shape"][0]) )
	{
		LOG_ERROR( "Tensors didn't have the expected shape, expecting all to be {} but they were {}, {}, {}, {}.",
		           shape.c,
		           weight["shape"][0],
		           bias["shape"][0],
		           mean["shape"][0],
		           var["shape"][0] );
		return;
	}

	setTensorDataFromJSON( &conv->bnScale, weight );
	setTensorDataFromJSON( &conv->bnBias, bias );
	setTensorDataFromJSON( &conv->bnMean, mean );
	setTensorDataFromJSON( &conv->bnVar, var );
}


void CnnModel::insertLayersBefore( std::initializer_list<std::pair<std::string, dnn::smart_layer_t*>> to_insert,
								 const dnn::smart_layer_t* insert_before_this )
{
	auto slice_iter = layers.begin();
	for ( ; slice_iter != layers.end() && slice_iter->second != insert_before_this; ++slice_iter )
	{
		// Nothing, just finding the current position
	}

	layers.insert( slice_iter, to_insert );
}

void CnnModel::loadFromFile( Shape shape )
{
	if ( !layers.empty() )
	{
		throw std::runtime_error( "A file is already loaded" );
	}

	images_input = new dnn::network_input_image_layer();

	concat_2_2 = new dnn::concat_layer( 1 );

	image_output = new dnn::network_output_image_layer();


#define LAYER( name ) { #name, name }

	layers.push_back( LAYER( images_input ) );

	const char* model_file_name = nullptr;

	uint32_t inchannels = 6;
	uint32_t spp = 4;
	uint32_t step1c = 32;
	uint32_t step2c = 64;
	uint32_t step3c = 128;
	uint32_t step4c = 256;
	uint32_t input_padding = 0;

	switch ( shape )
	{
		case Shape_1spp:
			model_file_name = MODEL_PARAMS_FILE_1SPP;
			spp = 1;
			input_padding = 2;
			break;
		case Shape_2spp:
			model_file_name = MODEL_PARAMS_FILE_2SPP;
			spp = 2;
			images_input->addPaddingImages( 1 ); // There are 3 input images, we need 4 to get a multiple of 8 channels
			input_padding = 4; // The 4th image is padding that we need to add to the filter
			break;
		case Shape_2step:
			model_file_name = MODEL_PARAMS_FILE_2STEP;
			break;
		case Shape_4step:
			model_file_name = MODEL_PARAMS_FILE_4STEP;
			break;
		case Shape_tiny:
			model_file_name = MODEL_PARAMS_FILE_TINY;
			step1c = 16;
			step2c = 32;
			step3c = 64;
			break;
		case Shape_large:
			model_file_name = MODEL_PARAMS_FILE_LARGE;
			step1c = 64;
			step2c = 128;
			step3c = 256;
			break;

		case Shape_3step_medium_4spp:
			// Fallthrough
		default:
			model_file_name = MODEL_PARAMS_FILE_3STEP_MEDIUM_4SPP;
			break;
	}

	if ( shape == Shape_4step )
	{
		conv4 = new dnn::convbiasrelu_layer( step3c, step4c, 3, 2, 1 );
		deconv4 = new dnn::transpose_convolution_layer( step4c, step3c, 3, 2, 1, 1 );

		conv3 = new dnn::convbiasrelu_layer( step2c, step3c, 3, 2, 1 );
		deconv3 = new dnn::transpose_convolution_layer( 2 * step3c, step2c, 3, 2, 1, 1 );

		conv2 = new dnn::convbiasrelu_layer( step1c, step2c, 3, 2, 1 );
		deconv2 = new dnn::transpose_convolution_layer( step2c, step1c, 3, 2, 1, 1 );

		concat_4_4 = new dnn::concat_layer( 1 );
	}
	else if (shape != Shape_2step )
	{
		conv3 = new dnn::convbiasrelu_layer( step2c, step3c, 3, 2, 1 );
		deconv3 = new dnn::transpose_convolution_layer( step3c, step2c, 3, 2, 1, 1 );
	}

	if ( shape == Shape_2step )
	{
		conv2 = new dnn::convbiasrelu_layer( step1c, step2c, 3, 2, 1 );
		deconv2 = new dnn::transpose_convolution_layer( step2c, step1c, 3, 2, 1, 1 );
	}
	else
	{
		conv2 = new dnn::convbiasrelu_layer( step1c, step2c, 3, 2, 1 );
		deconv2 = new dnn::transpose_convolution_layer( 2 * step2c, step1c, 3, 2, 1, 1 );

		concat_3_3 = new dnn::concat_layer( 1 );
	}

	conv1 = new dnn::convbiasrelu_layer( inchannels * spp + input_padding, step1c, 3, 2, 1 );
	deconv1 = new dnn::transpose_convolution_layer( 2 * step1c, 8, 3, 2, 1, 1, false );


	layers.push_back( LAYER( conv1 ) );
	layers.push_back( LAYER( conv2 ) );

	if ( shape != Shape_2step )
	{
		layers.push_back( LAYER( conv3 ) );

		if ( shape == Shape_4step )
		{
			layers.push_back( LAYER( conv4 ) );
			layers.push_back( LAYER( deconv4 ) );
			layers.push_back( LAYER( concat_4_4 ) );
		}

		layers.push_back( LAYER( deconv3 ) );
		layers.push_back( LAYER( concat_3_3 ) );
	}

	layers.push_back( LAYER( deconv2 ) );
	layers.push_back( LAYER( concat_2_2 ) );
	layers.push_back( LAYER( deconv1 ) );


	layers.push_back( LAYER( image_output ) );


	for ( size_t i = 1; i < layers.size(); ++i )
	{
		layers[i].second->connectInput( layers[i - 1].second );
	}

	if ( shape == Shape_4step )
	{
		concat_4_4->addConcatInput( conv3 );
	}

	if ( shape != Shape_2step )
	{
		concat_3_3->addConcatInput( conv2 );
	}

	concat_2_2->addConcatInput( conv1 );


	using namespace nlohmann;
	json js;
	try
	{
		std::ifstream file( model_file_name );
		if ( file )
		{
			js = json::parse( file );
		}
		else
		{
			LOG_ERROR( "Failed to load dnn file." );
			return;
		}
	} catch ( std::exception& e )
	{
		LOG_ERROR( "Error when loading dnn file." );
		// DebugError( e.what() );
		return;
	}

	// Add padding to the input so it's multiple of 8 (for 1spp it's 6 and for 2spp it's 12)
	loadConvFromJson( conv1, js, "conv1", 0, input_padding );

	// Add padding to the output so it's multiple of 8 (3 + 5)
	loadConvFromJson( deconv1, js, "deconv1", 5, 0, true );

	loadConvFromJson( conv2, js, "conv2" );
	loadConvFromJson( deconv2, js, "deconv2" );

	if ( shape == Shape_4step )
	{
		loadConvFromJson( conv4, js, "conv4" );
		loadConvFromJson( deconv4, js, "deconv4" );
	}

	if ( shape != Shape_2step )
	{
		loadConvFromJson( conv3, js, "conv3" );
		loadConvFromJson( deconv3, js, "deconv3" );
	}

}

void CnnModel::setInput1spp( int width, int height, GLuint color_spec_alpha_depth, GLuint tangents )
{
	images_input->setInputImagesSize( width, height );
	images_input->setInputImages( { color_spec_alpha_depth, tangents } );

	updateOutputs( width, height );
}

void CnnModel::setInput2spp( int width, int height, GLuint color_spec, GLuint alpha_depth, GLuint tangents )
{
	images_input->setInputImagesSize( width, height );
	images_input->setInputImages( { color_spec, alpha_depth, tangents } );

	updateOutputs( width, height );
}

void CnnModel::setInput( int width, int height, GLuint color, GLuint specular, GLuint alpha, GLuint depth, GLuint tangents1, GLuint tangents2 )
{
	images_input->setInputImagesSize( width, height );
	images_input->setInputImages( { color, specular, alpha, depth, tangents1, tangents2 } );

	updateOutputs( width, height );
}

bool CnnModel::isInputSet() const
{
	return !images_input->gl_textures.empty();
}

void CnnModel::updateOutputs(int width, int height)
{
	for (size_t i = 0; i < layers.size(); ++i)
	{
		layers[i].second->updateInputAndOutputDims();
	}

	for (size_t i = 0; i < layers.size(); ++i)
	{
		layers[layers.size() - i - 1].second->updateInputAndOutputTensors();
	}
}

void CnnModel::evaluate()
{
	chag::perf::Scope s( "Network Evaluation" );
	for ( size_t i = 0; i < layers.size(); ++i )
	{
		chag::perf::Scope s( fmt::format( "Layer {} [{}]", layers[i].first, layers[i].second->info() ) );
		layers[i].second->forward();
	}
}

dnn::tensor_t* CnnModel::getOutput()
{
	if ( !std::filesystem::exists( "channels" ) )
	{
		std::filesystem::create_directories( "channels" );
	}

	int ctr = 0; 
	std::ofstream os("channels/norm.txt");

	for ( size_t i = 0; i < layers.size(); ++i )
	{

		chag::perf::Scope s( fmt::format( "Layer {} [{}]", layers[i].first, layers[i].second->info() ) );
		layers[i].second->forward();

		dnn::convbiasrelu_layer* l1 = dynamic_cast<dnn::convbiasrelu_layer*>(layers[i].second);
		dnn::transpose_convolution_layer* l2 = dynamic_cast<dnn::transpose_convolution_layer*>(layers[i].second);
		dnn::convolution_layer* l3 = dynamic_cast<dnn::convolution_layer*>(layers[i].second);
		dnn::network_input_image_layer* li = dynamic_cast<dnn::network_input_image_layer*>(layers[i].second);
		dnn::network_output_image_layer* lo = dynamic_cast<dnn::network_output_image_layer*>(layers[i].second);

		if (l1 == nullptr && l2 == nullptr && l3 == nullptr && li == nullptr && lo == nullptr) continue; 
		ctr += 1;

		auto tensor = layers[i].second->output_tensor_view.get_underlying_tensor(); 
		auto dim = tensor->dim; 

		auto d = tensor->DownloadFlatData();
		std::vector<std::vector<float>> channel_images(dim.c);
		for (int c = 0; c < dim.c; c++) channel_images[c].resize(dim.w * dim.h);
		for (int y = 0; y < dim.h; y++)
		{
			for (int x = 0; x < dim.w; x++)
			{
				for (int c = 0; c < dim.c; c++)
				{
					channel_images[c][y * dim.w + x] = d[y * dim.w * dim.c + x * dim.c + c];
				}
			}
		}

		std::vector<std::vector<uint8_t>> u_channel_images(dim.c);
		for (int c = 0; c < dim.c; c++) u_channel_images[c].resize(dim.w * dim.h);
		for (int c = 0; c < dim.c; c++)
		{
			float minval = FLT_MAX; 
			float maxval = -FLT_MAX; 
			for (int i = 0; i < dim.w * dim.h; i++) {
				minval = min(minval, channel_images[c][i]);
				maxval = max(maxval, channel_images[c][i]);
			}
			for (int i = 0; i < dim.w * dim.h; i++) {
				u_channel_images[c][i] = ((channel_images[c][i] - minval) / (maxval - minval)) * 255.0f; 
			}

			os << "Layer: " << i << " Channel: " << c << ": " << minval << ", " << maxval << std::endl; 
		}

		for (int c = 0; c < dim.c; c++)
		{
			std::string filename = fmt::format( "channels/{}_{}_{}.png", i, layers[i].first, c );
			stbi_write_png(filename.c_str(), dim.w, dim.h, 1, u_channel_images[c].data(), 0);
		}
	}

	exit(0);
	return nullptr;
}

void CnnModel::setOutputTextures( int width, int height, GLuint out_texture_1 )
{
	image_output->setOutputImagesSize( width, height );
	image_output->setOutputImages( { out_texture_1 } );
}

void CnnModel::resetRecurrentLayers()
{
	for ( auto l : layers )
	{
		if ( dynamic_cast<dnn::convbiasrelu_recurrent_layer*>(l.second) != nullptr )
		{
			dynamic_cast<dnn::convbiasrelu_recurrent_layer*>(l.second)->resetRecurrentValues();
		}
	}
}

