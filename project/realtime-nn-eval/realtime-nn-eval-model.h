#pragma once

#include <filesystem>
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include <cuda_gl_interop.h>

typedef uint32_t GLuint;

struct cudaGraphicsResource;

namespace dnn
{
struct tensor4D;
typedef tensor4D tensor_t;
struct smart_layer_t;
struct activation_layer;
struct concat_layer;
struct average_layer;
struct convolution_layer;
struct convbiasrelu_layer;
struct convbiasrelu_recurrent_layer;
struct network_input_image_layer;
//struct upsample_and_skip_layer; 
struct insert_channels_layer; 
struct transpose_convolution_layer; 
struct network_output_image_layer;
}

class CnnModel
{
public:
	enum Shape
	{
		Shape_1spp,
		Shape_2spp,
		Shape_3step_medium_4spp,
		Shape_tiny,
		Shape_large,
		Shape_2step,
		Shape_4step
	};

private:
	std::vector<std::pair<std::string, dnn::smart_layer_t*>> layers;

	dnn::network_input_image_layer* images_input = nullptr;

	dnn::convbiasrelu_layer* conv1 = nullptr;
	dnn::convbiasrelu_layer* conv2 = nullptr;
	dnn::convbiasrelu_layer* conv3 = nullptr;
	dnn::convbiasrelu_layer* conv4 = nullptr;
	dnn::transpose_convolution_layer* deconv4 = nullptr;
	dnn::transpose_convolution_layer* deconv3 = nullptr;
	dnn::transpose_convolution_layer* deconv2 = nullptr;
	dnn::transpose_convolution_layer* deconv1 = nullptr;
	dnn::convolution_layer* autoconv = nullptr;

	dnn::smart_layer_t* slice_skip_0_color = nullptr;
	dnn::smart_layer_t* slice_skip_0_specular = nullptr;
	dnn::smart_layer_t* slice_skip_0_alpha = nullptr;

	dnn::concat_layer* concat_4_4 = nullptr;
	dnn::concat_layer* concat_3_3 = nullptr;
	dnn::concat_layer* concat_2_2 = nullptr;
	dnn::concat_layer* concat_1_1 = nullptr;

	dnn::network_output_image_layer* image_output = nullptr;


public:
	CnnModel();
	~CnnModel();

	// File is hardcoded in the function since the model and the data are too closely related to have it as a parameter
	void loadFromFile(Shape shape);

	void setInput1spp( int width, int height, GLuint color_spec_alpha_depth, GLuint tangents );

	void setInput2spp( int width, int height, GLuint color_spec, GLuint alpha_depth, GLuint tangents );

	void setInput( int width, int height, GLuint color, GLuint specular, GLuint alpha, GLuint depth, GLuint tangents1, GLuint tangents2 );

	bool isInputSet() const;

	void evaluate();

	dnn::tensor_t* getOutput();

	void setOutputTextures( int width, int height, GLuint out_texture_1 );

	void resetRecurrentLayers();

private:
	void updateOutputs( int width, int height );

	void insertLayersBefore( std::initializer_list<std::pair<std::string, dnn::smart_layer_t*>> to_insert,
								 const dnn::smart_layer_t* insert_before_this );
};
