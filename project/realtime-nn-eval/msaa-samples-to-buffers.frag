#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

layout(binding = 0) uniform sampler2DMS tex_colors;
layout(binding = 1) uniform sampler2DMS tex_tangents;

#if NETWORK_INPUT_SPP == 1
layout(location = 0) out vec4 out_color_spec_alpha_depth;
layout(location = 1) out vec4 out_tangents;

#elif NETWORK_INPUT_SPP == 2
layout(location = 0) out vec4 out_color_spec;
layout(location = 1) out vec4 out_alpha_depth;
layout(location = 2) out vec4 out_tangents;

#else //if NETWORK_INPUT_SPP == 4
layout(location = 0) out vec4 out_color;
layout(location = 1) out vec4 out_specular;
layout(location = 2) out vec4 out_alpha;
layout(location = 3) out vec4 out_depth;
layout(location = 4) out vec4 out_tangents1;
layout(location = 5) out vec4 out_tangents2;
#endif


void main() 
{
#if NETWORK_INPUT_SPP == 1 
	vec4 c = texelFetch(tex_colors, ivec2(gl_FragCoord.xy), 0);
	out_color_spec_alpha_depth.xyzw = c.xywz; // w is alpha, z is depth, so we need to swap them
	vec4 t = texelFetch(tex_tangents, ivec2(gl_FragCoord.xy), 0);
	out_tangents.xy = t.xy;

#elif NETWORK_INPUT_SPP == 2
	vec4 c0 = texelFetch(tex_colors, ivec2(gl_FragCoord.xy), 0);
	vec4 c1 = texelFetch(tex_colors, ivec2(gl_FragCoord.xy), 0);

	out_color_spec.xz = c0.xy;
	out_color_spec.yw = c1.xy;

	out_alpha_depth.xz = c0.wz;
	out_alpha_depth.yw = c1.wz;

	vec4 t0 = texelFetch(tex_tangents, ivec2(gl_FragCoord.xy), 0);
	vec4 t1 = texelFetch(tex_tangents, ivec2(gl_FragCoord.xy), 0);

	out_tangents.xy = t0.xy;
	out_tangents.zw = t1.xy;

#else //if NETWORK_INPUT_SPP == 4
	float tangents[8];

	for (int i = 0; i < 4; ++i)
	{
		vec4 c = texelFetch(tex_colors, ivec2(gl_FragCoord.xy), i);
		out_color[i] = clamp(c.x, 0, 2);
		out_specular[i] = clamp(c.y, 0, 1);
		out_depth[i] = clamp(c.z, 0, 1);
		out_alpha[i] = c.w;

		vec4 t = texelFetch(tex_tangents, ivec2(gl_FragCoord.xy), i);
		tangents[2*i] = t.x;
		tangents[2*i + 1] = t.y;
	}

	for(int i = 0; i < 4; ++i)
	{
		out_tangents1[i] = tangents[i];
		out_tangents2[i] = tangents[4+i];
	}
#endif
}

