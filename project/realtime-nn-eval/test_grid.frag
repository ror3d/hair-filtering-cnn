#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

layout(location = 0) out vec4 fragmentColor;
layout(location = 1) out vec4 fragmentTangent;

in vec2 v_position;


void main() 
{
	vec2 uv = (v_position + 1)/2;
	vec3 color = vec3(0, 0, 0);
	color = vec3(1 - dot(uv.xy, uv.xy), uv.xy);

	fragmentColor = vec4(color, 1);
}

