#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

in vec2 v_position;

layout(location = 0) out vec4 output0;
layout(location = 1) out vec4 output1;

layout(binding = 0) uniform sampler2D stoch_color_tex;
layout(binding = 1) uniform sampler2D stoch_tan_tex;
layout(binding = 2) uniform sampler2D ms_tex;


void main() 
{
	vec2 uv = (v_position + 1)/2;

	// Channels are:
	// {color, color, color, specular}
	vec4 s_color = max(texture(stoch_color_tex, uv), 0.0);
	// {tangent_x, tangent_y, tangent_z, alpha}
	vec4 s_tgs = texture(stoch_tan_tex, uv);
	// {color, color, alpha, specular}
	vec4 ms = max(texture(ms_tex, uv), 0.0);

	// Inputs to the network are:
	// {color, specular, ms_color, ms_alpha}
	// {ms_specular, tangent_x, tangent_y, alpha}
	output0 = vec4(s_color.z, s_color.w, ms.x, ms.z);
	output1 = vec4(ms.w, (s_tgs.xy * 2.0) - vec2(1.0), s_tgs.a);
}
