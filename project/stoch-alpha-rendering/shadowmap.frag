#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

uniform vec3 material_color;

uniform float line_offset_factor = 0;
uniform float line_offset_units = 0;

uniform uint frame_number = 1;

uniform vec2 screen_size;

#if !USE_QUADS_FOR_LINES // If not using geometry shader
in float v_transparency;
in vec3 v_model_space_position; 
in float v_hair_thickness;
float	transparency			= v_transparency;
vec3	model_space_position	= v_model_space_position; 
float	hair_thickness			= v_hair_thickness;
#else 
in float g_transparency;
in vec3 g_model_space_position; 
in float g_hair_thickness;
float	transparency			= g_transparency;
vec3	model_space_position	= g_model_space_position; 
float	hair_thickness			= g_hair_thickness;
#endif


#include "../rnd_sample.glslh"

void main() 
{
	float thickness = length(screen_size) * hair_thickness * gl_FragCoord.w / 2;

	thickness = 0.5;

	//if (hash(vec4(model_space_position, float(frame_number + 13))) > thickness * transparency)
	if (hashed_randf(model_space_position, float(frame_number + 13)) > thickness * transparency)
	{
		discard;
	}

	// TODO: Modifying gl_FragDepth is usually a bad idea, because it invalidates 
	//       early z cull and hierarchical z. Probably doesn't matter here, though, 
	//       as we also discard. 
	float df = length(vec2(dFdx(gl_FragCoord.z), dFdy(gl_FragCoord.z)));
	gl_FragDepth = gl_FragCoord.z + line_offset_factor * df + line_offset_units * 0.0001f;
}

