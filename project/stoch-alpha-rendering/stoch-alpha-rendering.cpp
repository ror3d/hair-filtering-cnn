#include <GL/glew.h>
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <chrono>

#include <utils/gl_helpers.h>
#include <utils/math_helpers.h>
#include <imgui/imgui.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
using namespace glm;

#include <camera.h>
#include <light.h>
#include "utils/fbo.h"

#include <scene.h>
#include <hair_model.h>
#include <module.h>

#include <fstream>

#include <utils/log.h>
#include <utils/settings.h>
#include <utils/perf.h>

#include <array>

using std::min;
using std::max;

extern uint32_t frame_number;

class StochasticAlphaRenderer : public Module
{
	int windowWidth, windowHeight;

	///////////////////////////////////////////////////////////////////////////////
	// Shader programs
	///////////////////////////////////////////////////////////////////////////////
	GLuint shaderProgram;       // Shader for rendering the final image
	GLuint shadowMapShaderProgram;
	GLuint fxShaderProgram;

	///////////////////////////////////////////////////////////////////////////////
	// Shadowmap
	///////////////////////////////////////////////////////////////////////////////

	Framebuffer shadowMapFB;


	///////////////////////////////////////////////////////////////////////////////
	// FBO
	///////////////////////////////////////////////////////////////////////////////

	Framebuffer mainFB;

	///////////////////////////////////////////////////////////////////////////////
	// Models
	///////////////////////////////////////////////////////////////////////////////
	float hair_thickness_world_space = 0.00007; // 70um

	float line_width = 1.0;

public:
	virtual const char* getModuleName() override
	{
		return "Stochastic Alpha";
	}

	void loadShaders(bool is_reload)
	{
#if USE_QUADS_FOR_LINES
		GLuint shader = gl_helpers::loadShaderProgram("stoch-alpha-rendering/shading.vert", "hair_geometry_shader.geom", "stoch-alpha-rendering/shading.frag", is_reload);
#else
		GLuint shader = gl_helpers::loadShaderProgram("stoch-alpha-rendering/shading.vert", "stoch-alpha-rendering/shading.frag", is_reload);
#endif
		if(shader != 0)
		{
			shaderProgram = shader;
		}

#if USE_QUADS_FOR_LINES
		shader = gl_helpers::loadShaderProgram("stoch-alpha-rendering/shadowmap.vert", "hair_shadow_geometry_shader.geom", "stoch-alpha-rendering/shadowmap.frag", is_reload);
#else 
		shader = gl_helpers::loadShaderProgram("stoch-alpha-rendering/shadowmap.vert", "stoch-alpha-rendering/shadowmap.frag", is_reload);
#endif
		if(shader != 0)
		{
			shadowMapShaderProgram = shader;
		}

		shader = gl_helpers::loadShaderProgram("fx.vert", "fx.frag", is_reload);
		if(shader != 0)
		{
			fxShaderProgram = shader;
		}
	}

	virtual void reloadShaders() override
	{
		loadShaders( true );
	}

	virtual bool setup( uint32_t width, uint32_t height ) override
	{
		loadShaders( false );

		windowWidth = width;
		windowHeight = height;

		mainFB.init( 2, true );
		mainFB.resize( width, height );

		///////////////////////////////////////////////////////////////////////
		// Setup Framebuffer for shadow map rendering
		///////////////////////////////////////////////////////////////////////
		shadowMapFB.init( 0, true );
		shadowMapFB.resize( 1024, 1024 );

		glBindTexture( GL_TEXTURE_2D, shadowMapFB.depthBuffer );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE );

		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER );
		vec4 border( 1.f );
		glTexParameterfv( GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &border.x );

		glBindTexture( GL_TEXTURE_2D, 0 );

		glEnable(GL_DEPTH_TEST); // enable Z-buffering
		glDisable(GL_CULL_FACE);  // enables backface culling

		return true;
	}

	virtual void teardown() override
	{
		gl_helpers::unloadShaderProgram( shaderProgram );
		gl_helpers::unloadShaderProgram( shadowMapShaderProgram );
		gl_helpers::unloadShaderProgram( fxShaderProgram );

		mainFB.free();
		shadowMapFB.free();
	}

	virtual void update( Scene* scene, float deltaTime ) override
	{
		bool exporting = chag::Settings::getGlobalInstance().get( "exporting_data", false );

		ImGui::DragFloat( "Hair Thickness", &hair_thickness_world_space, 0.001, 0, 10, "%.5f", ImGuiSliderFlags_Logarithmic );

		ImGui::DragFloat( "Line Width", &line_width, 0.5, 0.01, 20, "%.3f", ImGuiSliderFlags_Logarithmic );
	}

	virtual void windowSizeChanged( uint32_t width, uint32_t height ) override
	{
		windowWidth = width;
		windowHeight = height;

		mainFB.resize( width, height );
	}

	virtual void render( const Scene* scene, Framebuffer& target_framebuffer ) override
	{
		if ( shadowMapFB.width != scene->shadowmap.size || shadowMapFB.height != scene->shadowmap.size )
		{
			shadowMapFB.resize( scene->shadowmap.size, scene->shadowmap.size );
		}

		glLineWidth( 1.0 );

		///////////////////////////////////////////////////////////////////////////
		// setup matrices
		///////////////////////////////////////////////////////////////////////////

		mat4 lightProjMatrix = scene->lights[0]->as<Light>()->proj_mat;
		mat4 lightViewMatrix = scene->lights[0]->as<Light>()->view_mat;

		mat4 projMatrix = scene->active_camera->as<Camera>()->getProjMatrix();
		mat4 viewMatrix = scene->active_camera->as<Camera>()->getViewMatrix();

		///////////////////////////////////////////////////////////////////////////
		// Draw from light
		///////////////////////////////////////////////////////////////////////////
		{
			chag::perf::Scope s( "Shadow Map" );
			shadowMapFB.bind();
			shadowMapFB.clear_depth();

			glUseProgram( shadowMapShaderProgram );

			gl_helpers::setUniformSlow( shadowMapShaderProgram, "frame_number", frame_number );

			gl_helpers::setUniformSlow( shadowMapShaderProgram, "line_width", line_width );

			gl_helpers::setUniformSlow( shadowMapShaderProgram, "viewProjectionMatrix", lightProjMatrix * lightViewMatrix );

			gl_helpers::setUniformSlow( shadowMapShaderProgram, "line_offset_factor", scene->shadowmap.polygon_offset_factor );
			gl_helpers::setUniformSlow( shadowMapShaderProgram, "line_offset_units", scene->shadowmap.polygon_offset_units );

			gl_helpers::setUniformSlow( shadowMapShaderProgram, "hair_thickness_world_space", hair_thickness_world_space );

			gl_helpers::setUniformSlow( shadowMapShaderProgram, "screen_size", glm::vec2( shadowMapFB.width, shadowMapFB.height ) );

			{
				chag::perf::Scope s( "Render" );
				for ( auto& obj : scene->objects )
				{
					if ( obj->as<HairModel>() )
					{
						obj->render( shadowMapShaderProgram );
					}
				}
			}

			glUseProgram( 0 );
		}


		///////////////////////////////////////////////////////////////////////////
		// Draw from camera
		///////////////////////////////////////////////////////////////////////////
		{
			chag::perf::Scope s( "Camera View" );
			glBindFramebuffer( GL_FRAMEBUFFER, mainFB.framebufferId );
			glViewport( 0, 0, mainFB.width, mainFB.height );
			glClearColor( 0.0, 0.0, 0.0, 0.0 );
			glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
			glClearBufferfv( GL_COLOR, 1, std::array<float, 4>{ 0.f, 0.f, 0.f, 0.f }.data() );

			glUseProgram( shaderProgram );

			glActiveTexture( GL_TEXTURE10 );
			glBindTexture( GL_TEXTURE_2D, shadowMapFB.depthBuffer );

			mat4 lightMatrix = translate( vec3( 0.5f ) ) * scale( vec3( 0.5f ) ) * lightProjMatrix * lightViewMatrix * inverse( viewMatrix );

			gl_helpers::setUniformSlow( shaderProgram, "frame_number", frame_number );

			gl_helpers::setUniformSlow( shaderProgram, "line_width", line_width );

			gl_helpers::setUniformSlow( shaderProgram, "light_position_viewspace", vec3( viewMatrix * vec4( scene->lights[0]->getPosition(), 1.0f ) ) );
			gl_helpers::setUniformSlow( shaderProgram, "lightMatrix", lightMatrix );
			gl_helpers::setUniformSlow( shaderProgram, "light_intensity", scene->lights[0]->as<Light>()->intensity );

			bool exporting = chag::Settings::getGlobalInstance().get( "exporting_data", false );
			gl_helpers::setUniformSlow( shaderProgram, "user_display_mode", (exporting ? 0u : 1u) );
			gl_helpers::setUniformSlow( shaderProgram, "generate_train_data_mode", (exporting ? 1u : 0u) );

			gl_helpers::setUniformSlow( shaderProgram, "hair_thickness_world_space", hair_thickness_world_space );

			gl_helpers::setUniformSlow( shaderProgram, "screen_size", glm::vec2( mainFB.width, mainFB.height ) );

			scene->active_camera->render( shaderProgram );

			{
				chag::perf::Scope s( "Render" );
				for ( auto& obj : scene->objects )
				{
					if ( obj->as<HairModel>() )
					{
						obj->render( shaderProgram );
					}
				}
			}

			glUseProgram( 0 );
			//glUseProgram(fxShaderProgram);
			//gl_helpers::drawFullScreenQuad();

			glActiveTexture( GL_TEXTURE10 );
			//glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE );
			glBindTexture( GL_TEXTURE_2D, 0 );
			glUseProgram( 0 );
		}

		{
			chag::perf::Scope s( "Copy to Target" );

			target_framebuffer.bind();

			glUseProgram( fxShaderProgram );

			glActiveTexture( GL_TEXTURE0 );
			static bool showTangent = false;
			ImGui::Checkbox( "Show Albedo", &showTangent );
			if ( showTangent )
			{
				glBindTexture( GL_TEXTURE_2D, mainFB.colorTextureTargets[1] );
			}
			else
			{
				glBindTexture( GL_TEXTURE_2D, mainFB.colorTextureTargets[0] );
			}

			glEnable( GL_BLEND );
			glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

			gl_helpers::drawFullScreenQuad();

			glDisable( GL_BLEND );

			glActiveTexture( GL_TEXTURE0 );
			glBindTexture( GL_TEXTURE_2D, 0 );


			glUseProgram( 0 );
		}
	}

	void saveFrameToFile( const std::filesystem::path& base_file_name, Framebuffer& target_framebuffer ) override
	{
		std::filesystem::path base_name = base_file_name.stem();

		// Save shaded color
		glBindFramebuffer(GL_FRAMEBUFFER, target_framebuffer.framebufferId);
		glViewport(0, 0, windowWidth, windowHeight);
		glClearColor(0, 0, 0, 0.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glUseProgram(fxShaderProgram);
		glActiveTexture( GL_TEXTURE0 );

		glBindTexture( GL_TEXTURE_2D, mainFB.colorTextureTargets[0] );

		gl_helpers::drawFullScreenQuad();

		target_framebuffer.saveToFile( base_file_name );

		// Save tangents
		glClearColor(0, 0, 0, 0.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glBindTexture( GL_TEXTURE_2D, mainFB.colorTextureTargets[1] );

		gl_helpers::drawFullScreenQuad();

		std::filesystem::path tan_file_name = base_file_name;
		tan_file_name.replace_filename( base_name.string() + "_tan" + base_file_name.extension().string() );
		target_framebuffer.saveToFile( tan_file_name );

		glActiveTexture( GL_TEXTURE0 );
		glBindTexture( GL_TEXTURE_2D, 0 );
	}
};

std::shared_ptr<Module> makeStochasticAlphaRender()
{
	return std::make_shared<StochasticAlphaRenderer>();
}


