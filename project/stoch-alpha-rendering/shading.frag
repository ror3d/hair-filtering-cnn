#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

uniform vec2 screen_size;

uniform mat4 lightMatrix;

uniform uint frame_number = 1;

uniform vec3 light_position_viewspace = vec3(1,1,1); 
uniform float light_intensity = 1000;

uniform vec3 ambient_w = vec3(0.2, 0.2, 0.2);

uniform uint should_override_color = 0;
uniform vec3 override_color = vec3(0, 0, 0);

uniform uint user_display_mode = 1;
uniform uint generate_train_data_mode = 0;
uniform uint eval_mode = 0;

layout(binding = 10) uniform sampler2DShadow shadowMapTex;

#if !USE_QUADS_FOR_LINES // If not using geometry shader
in vec4 v_view_space_position;
in vec3 v_color;
in float v_transparency;
in vec3 v_tangent; 
in vec3 v_model_space_position; 
in float v_hair_thickness;
vec4	view_space_position		= v_view_space_position;
vec3	color					= v_color;
float	transparency			= v_transparency;
vec3	tangent					= v_tangent; 
vec3	model_space_position	= v_model_space_position; 
float	hair_thickness			= v_hair_thickness;
#else 
in vec4 g_view_space_position;
in vec3 g_color;
in float g_transparency;
in vec3 g_tangent; 
in vec3 g_model_space_position; 
in float g_hair_thickness;
vec4	view_space_position		= g_view_space_position;
vec3	color					= g_color;
float	transparency			= g_transparency;
vec3	tangent					= g_tangent; 
vec3	model_space_position	= g_model_space_position; 
float	hair_thickness			= g_hair_thickness;
#endif


layout(location = 0) out vec4 out_shadedColor;
layout(location = 1) out vec4 out_tangent;


#include "../rnd_sample.glslh"
#include "../hair_shading.glslh"


void main() 
{
	float ss_hair_thickness = length(screen_size) * hair_thickness * gl_FragCoord.w / 2;

	ss_hair_thickness = 0.25;

	if (hashed_randf(model_space_position.xyz, float(frame_number)) > ss_hair_thickness * transparency)
	{
		discard;
	}

	vec3 T = normalize(tangent);

	vec4 shadowMapCoord = lightMatrix * vec4(view_space_position.xyz, 1.0);
	float light_visibility = textureProj(shadowMapTex, shadowMapCoord);

	vec3 light_d = view_space_position.xyz - light_position_viewspace.xyz;
	float light_radiance = light_visibility * light_intensity / dot(light_d, light_d);

	hair_shading hs = get_hair_shading(view_space_position.xyz, T, light_position_viewspace.xyz);

	vec3 hair_color;
	if (user_display_mode == 1)
	{
		hair_color = (should_override_color == 1 ? override_color : color);
	}
	else if(generate_train_data_mode == 1)
	{
		hair_color = vec3(0.5);
	}
	else // eval_mode
	{
		hair_color = vec3(1.0);
	}

	vec3 diffuse_contribution = light_radiance * (hs.diffuse * hair_color);
	float specular_contribution_R = light_radiance * (hs.specularR);
	vec3 specular_contribution_TRT = light_radiance * (hs.specularTRT * hair_color);

	if (user_display_mode == 1)
	{
		out_shadedColor.xyz = ambient_w * hair_color + diffuse_contribution + specular_contribution_TRT + specular_contribution_R;
		out_shadedColor.w = 1.0;
		out_tangent.xyz = (T + vec3(1))/2;
		out_tangent.w = 1.0;
	}
	else if(generate_train_data_mode == 1)
	{
		/*
		out_shadedColor.xyz = (ambient_w * hair_color + diffuse_contribution + specular_contribution_TRT).xyz;
		out_shadedColor.w = specular_contribution_R;
		/*/
		out_shadedColor.x = (ambient_w * hair_color + diffuse_contribution + specular_contribution_TRT).x;
		out_shadedColor.y = specular_contribution_R;
		out_shadedColor.z = gl_FragCoord.z;
		out_shadedColor.w = 1.0;
		//*/
		out_tangent.xyz = (T + vec3(1))/2;
		out_tangent.w = 1.0;
	}
	else // eval_mode
	{
		out_shadedColor.xyz = ambient_w * hair_color + diffuse_contribution + specular_contribution_TRT;
		out_shadedColor.w = specular_contribution_R;
		out_tangent.xyz = (T + vec3(1))/2;
		out_tangent.w = 1.0;
	}
}
