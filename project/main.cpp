#include <GL/glew.h>
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <chrono>

#include <utils/gl_helpers.h>
#include <utils/math_helpers.h>
#include <imgui/imgui.h>
#include <ImFileDialog/ImFileDialog.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
using namespace glm;

#include <scene.h>
#include <camera.h>
#include <light.h>

#include "utils/fbo.h"

#include "hair_model.h"
#include "obj_model.h"

#include <fstream>

#include "module.h"

#include <utils/log.h>

#include <utils/perf.h>

#include <utils/str.h>

#include <array>

#include <map>

#include <windows.h>

#include <high-res-rendering/high-res-rendering.h>
#include <stoch-alpha-rendering/stoch-alpha-rendering.h>
#include <msaa-sa-rendering/msaa-stoch-alpha-rendering.h>
#include <realtime-nn-eval/realtime-nn-eval.h>
#include <obj-rendering/obj-rendering.h>

#include <utils/settings.h>

#include <utils/color_helpers.h>
#include <utils/hdr.h>

#include <string>

extern "C" {
	_declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
}

#undef near
#undef far

using std::min;
using std::max;

///////////////////////////////////////////////////////////////////////////////
// Various globals
///////////////////////////////////////////////////////////////////////////////
SDL_Window* g_window = nullptr;
std::chrono::system_clock::time_point startTime;
float currentTime = 0.0f;
float previousTime = 0.0f;
float deltaTime = 0.0f;
bool showUI = true;
int windowWidth, windowHeight;


std::filesystem::path saving_to_dir = "";

bool generate_train_data = true;
bool generate_validation_data = true;
bool generate_eval_data = true;

int n_train_frames = 400;
int n_validation_frames = 40;

int batch_number = 1;

int train_seed = 1;
int validation_seed = 2;

int n_samples_in_input = 2;

int n_train_frames_between_random_light = 5;
int n_validation_frames_between_random_light = 2;


glm::vec2 start_light_attitude = {0, 0.25};
glm::vec3 start_camera_position = {0, 0.53, 0.848};
float start_camera_roll = 0;

glm::vec2 eval_frame_light_attitude = {0.806, 0.304};
glm::vec3 eval_frame_camera_position = {0.84, -0.454, -0.275};
float eval_frame_camera_roll = 0.475;

glm::vec2 teaser_light_attitude = {0.39, 0.25};
glm::vec3 teaser_camera_position = {-0.079, 0.952, 0.046};
glm::quat teaser_camera_orientation = glm::quat(-0.020, 0.001, -0.028, 0.999);
float teaser_camera_roll = 0;

// This is an actual global used in other places
uint32_t frame_number = 1;
bool update_frame_number = false;
bool dont_force_update_frame_number_on_export = false;

enum class SavingStage : int
{
	None,
	Start,
	InputTrain,
	InputVal,
	InputEval,
	TargetTrain,
	TargetVal,
	TargetEval
};

int n_frames_remaining = 0;
int n_samples_per_frame_remaining = 0;
SavingStage saving_stage = SavingStage::None;
bool create_input_sets = true;
bool create_target_sets = true;

bool saving_stage_changed_last_frame = false;


GLuint fxShaderProgram;
GLuint debugShaderProgram;

bool showDebugGeometry = false;

bool showGUI = true;

///////////////////////////////////////////////////////////////////////////////
// Scene
///////////////////////////////////////////////////////////////////////////////

Scene scene;

color_helpers::color_RGB_t background_color;

GLuint gl_origin_axes_vao = 0;
GLuint gl_origin_axes_buf = 0;

///////////////////////////////////////////////////////////////////////////////
// Framebuffer
///////////////////////////////////////////////////////////////////////////////

Framebuffer mainFB;

///////////////////////////////////////////////////////////////////////////////
// Camera parameters.
///////////////////////////////////////////////////////////////////////////////

float lightDistance = 30.f;
vec2 lightAttitude( 0, 0.25 );
vec4 lightOrthoBounds( -.3, .3, -.35, .35 );

float cameraNearPlane = 0.4;
float cameraFarPlane = 2;

float cameraStartDistance = 1;

float cameraMinDistance = 0.75;
float cameraMaxDistance = 1.3;

///////////////////////////////////////////////////////////////////////////////
// Models
///////////////////////////////////////////////////////////////////////////////
std::shared_ptr<HairModel> hair;
std::shared_ptr<HairModel> hair_simplified;
color_helpers::color_RGB_t original_hair_color;
color_helpers::color_RGB_t hair_color(.486f, .431f, .353f);
bool override_hair_color = true;

std::shared_ptr<ObjModel> head_model;


///////////////////////////////////////////////////////////////////////////////
// Animation
///////////////////////////////////////////////////////////////////////////////

bool animating = false;
int animation_frame = 0;
glm::vec3 animation_start_pos = { 0.188, 0.782, 0.055 };
glm::vec3 animation_end_pos = { 0.379, -0.729, 0.055 };
glm::vec3 animation_look_at = { -0.02, 0.0, 0.01 };
float animation_duration = 7;
float animation_time = 0.f;
bool export_animation = false;

///////////////////////////////////////////////////////////////////////////////
// Modules
///////////////////////////////////////////////////////////////////////////////

ModuleManager modules;

bool animation_window = false;
bool performance_window = false;


void loadHairFile( std::filesystem::path file, bool update_stored )
{
	if ( !hair->loadFromFile( file ) )
	{
		LOG_FATAL( "Error!\n" );
	}
	else
	{
		if ( update_stored )
		{
			chag::Settings::getGlobalInstance().set( "hair-file", file.u8string() );
			chag::Settings::getGlobalInstance().saveToFile();
		}

		/////////////////////////////////////////////////////////////////////////////
		// This is the _transparency_ of the hair. Nothing to do with its thickness. 
		/////////////////////////////////////////////////////////////////////////////
		hair->default_transparency = 0.6;

		original_hair_color = color_helpers::color_RGB_t(hair->color.empty() ? hair->default_color : hair->color[0]);
		if ( !override_hair_color )
		{
			hair_color = original_hair_color;
		}

		hair_simplified = std::make_shared<HairModel>( HairModel::generateRandomSubsample( *hair, 0.2 ) );

		head_model->loadFromFile( "../data/woman.obj" );
	}
}

void initDebugGeom()
{
	glCreateBuffers( 1, &gl_origin_axes_buf );
	float origin_axes[] = {
		0, 0, 0,
		1, 0, 0,
		0, 0, 0,
		0, 1, 0,
		0, 0, 0,
		0, 0, 1,
		1, 0, 0,
		1, 0, 0,
		0, 1, 0,
		0, 1, 0,
		0, 0, 1,
		0, 0, 1,
	};
	glNamedBufferData( gl_origin_axes_buf, sizeof( origin_axes ), origin_axes, GL_STATIC_DRAW );
	glCreateVertexArrays( 1, &gl_origin_axes_vao );
	glBindVertexArray( gl_origin_axes_vao );
	glBindBuffer( GL_ARRAY_BUFFER, gl_origin_axes_buf );
	glVertexAttribPointer( 0, 3, GL_FLOAT, false, 0, 0 );
	glVertexAttribPointer( 1, 3, GL_FLOAT, false, 0, (void*)(3*2*3*sizeof(float)) );
	glEnableVertexAttribArray( 0 );
	glEnableVertexAttribArray( 1 );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindVertexArray( 0 );
}

void randomiseLightPosition()
{
	float theta = math_helpers::randf();
	float phi = math_helpers::randf();
	lightAttitude.x = theta;
	lightAttitude.y = acosf( 2*phi - 1.f ) / M_PI;
	//lightAttitude.y = -fabsf((acosf( 2*phi - 1.f ) / M_PI) - 0.5f) + 0.5f;
}

void randomiseCameraPosition(bool rand_dist = true)
{
	glm::vec3 dir = math_helpers::sampleSphereSurface();
	float roll = math_helpers::randf();
	float d = cameraStartDistance;
	if ( rand_dist )
	{
		math_helpers::uniform_randf( cameraMinDistance, cameraMaxDistance );
	}

	scene.active_camera->as<Camera>()->setPosition(dir * d);
	scene.active_camera->as<Camera>()->setRoll( (roll * 2 - 1) * M_PI );
	scene.active_camera->as<Camera>()->setDirection( -scene.active_camera->getPosition() );
}

void setupEvalScene()
{
	scene.active_camera->as<Camera>()->setPosition( eval_frame_camera_position );
	scene.active_camera->as<Camera>()->setRoll( eval_frame_camera_roll );
	scene.active_camera->as<Camera>()->setDirection( -scene.active_camera->getPosition() );
	lightAttitude = eval_frame_light_attitude;
}

void setupTeaserScene()
{
	scene.active_camera->as<Camera>()->setPosition( teaser_camera_position );
	scene.active_camera->as<Camera>()->setOrientation( teaser_camera_orientation );
	lightAttitude = teaser_light_attitude;
}

void initialize()
{
	///////////////////////////////////////////////////////////////////////
	// Load models and set up model matrices
	///////////////////////////////////////////////////////////////////////
	std::string fname;
	fname = chag::Settings::getGlobalInstance().get( "hair-file", "" );
	hair = std::make_shared<HairModel>();
	head_model = std::make_shared<ObjModel>();
	if ( !fname.empty() && std::filesystem::exists( fname ) )
	{
		loadHairFile( fname, false );
	}
	hair->setScale( glm::vec3( 0.5f, 0.5f, 0.5f ) / 100.f );

	//hair_simplified->setScale( hair->getScale() );

	head_model->setScale( hair->getScale() * 0.96f );

	scene.objects.push_back( hair );
	scene.objects.push_back( head_model );
	//scene.root_object = hair_simplified;

	scene.environment.texture = gl_helpers::loadHdrTexture( "../data/schelde.hdr" );
	scene.environment.rotate = 0.75;

	mainFB.init();
	mainFB.resize( windowWidth, windowHeight );

	fxShaderProgram = gl_helpers::loadShaderProgram("fx.vert", "fx.frag", false);
	debugShaderProgram = gl_helpers::loadShaderProgram("debug.vert", "debug.frag", false);

	initDebugGeom();

	glBindTexture( GL_TEXTURE_2D, 0 );

	glEnable(GL_DEPTH_TEST); // enable Z-buffering
	glEnable(GL_CULL_FACE);  // enables backface culling

	std::shared_ptr<Camera> camera = std::make_shared<Camera>( "camera" );
	scene.active_camera = camera;
	camera->setClipPlanes( cameraNearPlane, cameraFarPlane );
	camera->setPosition( glm::normalize( glm::vec3( 0.0f, 50.0f, 80.0f ) ) * cameraStartDistance );;
	camera->setDirection( -camera->getPosition() );

	auto light = std::make_shared<Light>( "light" );
	light->ortho_bounds = lightOrthoBounds;
	light->intensity = 800;
	scene.lights.push_back( light );

	modules.addModule( makeObjRender() );
	modules.addModule( makeHighResRenderer() );
	modules.addModule( makeStochasticAlphaRender() );
	modules.addModule(makeRealtimeNNEvalRenderer());
	modules.addModule(makeMSAAStochasticAlphaRender());

	modules.sceneChanged( &scene );
	
	//////////////////////////////////////////////////////////////////////////////
	// Pass along defines to shaders
	//////////////////////////////////////////////////////////////////////////////
#if USE_QUADS_FOR_LINES
	gl_helpers::setCompileDefinition("USE_QUADS_FOR_LINES", "1");
#else
	gl_helpers::setCompileDefinition("USE_QUADS_FOR_LINES", "0");
#endif

}

namespace ImGui
{
void HelpMarker( const char* desc )
{
	ImGui::SameLine();
	ImGui::TextDisabled( "(?)" );
	if ( ImGui::IsItemHovered() )
	{
		ImGui::BeginTooltip();
		ImGui::PushTextWrapPos( ImGui::GetFontSize() * 35.0f );
		ImGui::TextUnformatted( desc );
		ImGui::PopTextWrapPos();
		ImGui::EndTooltip();
	}
}
}

void shutdown()
{
	glDeleteVertexArrays( 1, &gl_origin_axes_vao );
	glDeleteBuffers( 1, &gl_origin_axes_buf );
}

void display(void)
{
	// Start of frame
	chag::perf::Scope s("Display");

	///////////////////////////////////////////////////////////////////////////
	// Check if window size has changed and resize buffers as needed
	///////////////////////////////////////////////////////////////////////////
	auto camera = scene.active_camera->as<Camera>();
	{
		int w, h;
		SDL_GetWindowSize(g_window, &w, &h);
		if(w != windowWidth || h != windowHeight)
		{
			windowWidth = w;
			windowHeight = h;

			camera->updateWindowSize( w, h );

			mainFB.resize( windowWidth, windowHeight );

			// Update modules sizes
			modules.windowSizeChanged( w, h );
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// setup matrices
	///////////////////////////////////////////////////////////////////////////

	glBindFramebuffer(GL_FRAMEBUFFER, mainFB.framebufferId);
	glViewport(0, 0, windowWidth, windowHeight);
	glClearColor(background_color.r, background_color.g, background_color.b, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	{
		chag::perf::Scope s( fmt::format( "Rendering {} Module", modules.getActiveModuleName() ) );
		modules.render(&scene, mainFB);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, windowWidth, windowHeight);
	glClearColor(0.2, 0.2, 0.8, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	glBlitNamedFramebuffer(
	        mainFB.framebufferId, 0, 0, 0, windowWidth, windowHeight, 0, 0, windowWidth, windowHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST );

	if ( showDebugGeometry )
	{
		glUseProgram( debugShaderProgram );
		scene.active_camera->as<Camera>()->render( debugShaderProgram );
		glBindVertexArray( gl_origin_axes_vao );
		glDrawArrays( GL_LINES, 0, 6 );
		glBindVertexArray( 0 );
		glUseProgram( 0 );
	}

	GLint tex_units = 0;
	glGetIntegerv( GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &tex_units );
	glBindTextures( 0, tex_units, NULL );

	glUseProgram( 0 );
}

void randomiseHairColor()
{
	glm::vec3 hsv_colors[] = { {.71, .27, .486}, {.101, .27, .486}, {.09, .37, .51},  };
	// {.098, .274, .486}
	color_helpers::color_HSV_t hsv;
	hsv.h = math_helpers::uniform_randf(.071, .105); // 0.071 to 0.105
	hsv.s = math_helpers::uniform_randf(.15, .75); // 0.15 to 0.75
	hsv.v = math_helpers::uniform_randf(.06, .7); // 0.05 to 0.9

	color_helpers::color_RGB_t rgb;
	rgb.from_xyz( hsv.to_xyz() );

	hair_color = rgb;
}

void randomiseScene()
{
	int seed = 0;
	int frames_between_random_light = 0;
	int n_frames = 0;
	bool rand_dist = true;

	switch ( saving_stage )
	{
		case SavingStage::InputTrain:
		case SavingStage::TargetTrain:
			seed = train_seed;
			n_frames = n_train_frames;
			frames_between_random_light = n_train_frames_between_random_light;
			break;
		case SavingStage::InputVal:
		case SavingStage::TargetVal:
			seed = validation_seed;
			n_frames = n_validation_frames;
			frames_between_random_light = n_validation_frames_between_random_light;
			break;
		case SavingStage::InputEval:
		case SavingStage::TargetEval:
			setupEvalScene();
			return;
			break;
	}
	int start_frame = n_frames * (batch_number - 1) + 1;
	int frame_num = n_frames - n_frames_remaining + start_frame;

	frame_num -= 1;

	math_helpers::set_rand_seed( seed + 13 * frame_num );

	randomiseCameraPosition( rand_dist );

	if ( frames_between_random_light > 0 )
	{
		math_helpers::set_rand_seed( seed + 17 * (frame_num / frames_between_random_light) );

		randomiseLightPosition();
	}
}

bool update(void)
{
	//update currentTime
	std::chrono::duration<float> timeSinceStart = std::chrono::system_clock::now() - startTime;
	previousTime = currentTime;
	currentTime = timeSinceStart.count();
	deltaTime = currentTime - previousTime;

	ImGui::Checkbox( "Update frame number", &update_frame_number );
	if ( update_frame_number )
	{
		frame_number += 1;
	}
	else
	{
		frame_number = 1;
	}

	auto& io = ImGui::GetIO();
	static bool r_was_down = false;
	if ( !io.WantCaptureKeyboard && io.KeyCtrl && io.KeysDown[SDL_SCANCODE_R] && !r_was_down )
	{
		modules.reloadShaders();
	}
	r_was_down = !io.WantCaptureKeyboard && io.KeysDown[SDL_SCANCODE_R];

#define TEST_SAVE_TRAIN_DATA 0

#if TEST_SAVE_TRAIN_DATA
	static int frames_to_next_update = 0;
	if ( saving_stage != SavingStage::None )
	{
		if ( frames_to_next_update == 0 )
		{
			frames_to_next_update = 60;
			saving_stage_changed_last_frame = false;
		}
		else
		{
			frames_to_next_update -= 1;
			saving_stage_changed_last_frame = true;
		}
	}
#endif
	if ( saving_stage_changed_last_frame )
	{
		saving_stage_changed_last_frame = false;
	}
	else
	{
		switch ( saving_stage )
		{
			case SavingStage::Start:
				{
					scene.active_camera->as<Camera>()->control_enabled = false;
#if !TEST_SAVE_TRAIN_DATA
					chag::Settings::getGlobalInstance().set( "exporting_data", true, false );
					if ( !dont_force_update_frame_number_on_export )
					{
						update_frame_number = true;
					}
#endif

#define ENSURE_DIR(d) if ( !std::filesystem::is_directory( saving_to_dir / #d ) ) { std::filesystem::create_directories( saving_to_dir / #d ); }
					ENSURE_DIR( input )
						ENSURE_DIR( target )
						ENSURE_DIR( validation_input )
						ENSURE_DIR( validation_target )
						ENSURE_DIR( evaluation_input )
						ENSURE_DIR( evaluation_target )
#undef ENSURE_DIR
						n_frames_remaining = n_train_frames;
					n_samples_per_frame_remaining = n_samples_in_input;
					saving_stage = SavingStage::InputTrain;
					saving_stage_changed_last_frame = true;
					randomiseScene();
				}
				break;
			case SavingStage::InputTrain:
				{
					if ( n_frames_remaining <= 0 || !generate_train_data || !create_input_sets )
					{
						n_frames_remaining = n_validation_frames;
						n_samples_per_frame_remaining = n_samples_in_input;
						saving_stage = SavingStage::InputVal;
						saving_stage_changed_last_frame = true;
						randomiseScene();
						break;
					}
					int start_frame = n_train_frames * (batch_number - 1) + 1;
					int frame_num = n_train_frames - n_frames_remaining + start_frame;
					std::string fname = fmt::format( "{}/{}_{}.png", (saving_to_dir / "input").string(), frame_num, char( 'a' + n_samples_in_input - n_samples_per_frame_remaining ) );
					if ( modules.getActiveModule()->readyToSaveFrame() )
					{
						if ( n_frames_remaining <= n_train_frames )
						{
#if !TEST_SAVE_TRAIN_DATA
							modules.getActiveModule()->saveFrameToFile( fname, mainFB );
#endif
						}
						n_samples_per_frame_remaining -= 1;
					}
					if ( n_samples_per_frame_remaining <= 0 )
					{
						n_samples_per_frame_remaining = n_samples_in_input;
						n_frames_remaining -= 1;
						randomiseScene();
					}
				}
				break;
			case SavingStage::InputVal:
				{
					if ( n_frames_remaining <= 0 || !generate_validation_data || !create_input_sets )
					{
						n_samples_per_frame_remaining = n_samples_in_input;
						saving_stage = SavingStage::InputEval;
						saving_stage_changed_last_frame = true;
						randomiseScene();
						break;
					}
					int start_frame = n_validation_frames * (batch_number - 1) + 1;
					std::string fname = fmt::format( "{}/{}_{}.png", (saving_to_dir / "validation_input").string(), n_validation_frames - n_frames_remaining + start_frame, char( 'a' + n_samples_in_input - n_samples_per_frame_remaining ) );
					if ( modules.getActiveModule()->readyToSaveFrame() )
					{
						if ( n_frames_remaining <= n_validation_frames )
						{
#if !TEST_SAVE_TRAIN_DATA
							modules.getActiveModule()->saveFrameToFile( fname, mainFB );
#endif
						}
						n_samples_per_frame_remaining -= 1;
					}
					if ( n_samples_per_frame_remaining <= 0 )
					{
						n_samples_per_frame_remaining = n_samples_in_input;
						n_frames_remaining -= 1;
						randomiseScene();
					}
				}
				break;
			case SavingStage::InputEval:
				{
					if ( n_samples_per_frame_remaining <= 0 || !generate_eval_data || !create_input_sets )
					{
						n_frames_remaining = n_train_frames;
						saving_stage = SavingStage::TargetTrain;
						saving_stage_changed_last_frame = true;
						// Change to high-res-render
						modules.changeActiveModule( "High Resolution" );
						randomiseScene();
						break;
					}
					std::string fname = fmt::format( "{}/{}_{}.png", (saving_to_dir / "evaluation_input").string(), 0, char( 'a' + n_samples_in_input - n_samples_per_frame_remaining ) );
					if ( modules.getActiveModule()->readyToSaveFrame() )
					{
#if !TEST_SAVE_TRAIN_DATA
						modules.getActiveModule()->saveFrameToFile( fname, mainFB );
#endif
						n_samples_per_frame_remaining -= 1;
					}
				}
				break;
			case SavingStage::TargetTrain:
				{
					if ( n_frames_remaining <= 0 || !generate_train_data || !create_target_sets )
					{
						n_frames_remaining = n_validation_frames;
						saving_stage = SavingStage::TargetVal;
						saving_stage_changed_last_frame = true;
						randomiseScene();
						break;
					}
					int start_frame = n_train_frames * (batch_number - 1) + 1;
					std::string fname = fmt::format( "{}/{}.png", (saving_to_dir / "target").string(), n_train_frames - n_frames_remaining + start_frame );
					if ( modules.getActiveModule()->readyToSaveFrame() )
					{
						if ( n_frames_remaining <= n_train_frames )
						{
#if !TEST_SAVE_TRAIN_DATA
							modules.getActiveModule()->saveFrameToFile( fname, mainFB );
#endif
						}
						n_frames_remaining -= 1;
						randomiseScene();
					}
				}
				break;
			case SavingStage::TargetVal:
				{
					if ( n_frames_remaining <= 0 || !generate_validation_data || !create_target_sets )
					{
						saving_stage = SavingStage::TargetEval;
						saving_stage_changed_last_frame = true;
						randomiseScene();
						break;
					}
					int start_frame = n_validation_frames * (batch_number - 1) + 1;
					std::string fname = fmt::format( "{}/{}.png", (saving_to_dir / "validation_target").string(), n_validation_frames - n_frames_remaining + start_frame );
					if ( modules.getActiveModule()->readyToSaveFrame() )
					{
						if ( n_frames_remaining <= n_validation_frames )
						{
#if !TEST_SAVE_TRAIN_DATA
							modules.getActiveModule()->saveFrameToFile( fname, mainFB );
#endif
						}
						n_frames_remaining -= 1;
						randomiseScene();
					}
				}
				break;
			case SavingStage::TargetEval:
				{
					if ( generate_eval_data && create_target_sets )
					{
						std::string fname = fmt::format( "{}/{}.png", (saving_to_dir / "evaluation_target").string(), 0 );
						if ( modules.getActiveModule()->readyToSaveFrame() )
						{
#if !TEST_SAVE_TRAIN_DATA
							modules.getActiveModule()->saveFrameToFile( fname, mainFB );
#endif
						}
					}
					// TODO: Change back to original renderer?
					saving_stage = SavingStage::None;
					saving_stage_changed_last_frame = true;
					scene.active_camera->as<Camera>()->control_enabled = true;
					chag::Settings::getGlobalInstance().set( "exporting_data", false, false );
				}
				break;

			default:
				// Nothing?
				break;
		}
	}

	if ( ImGui::IsKeyReleased( SDL_SCANCODE_G ) )
	{
		gl_helpers::toggleGUI();
	}

	if ( animating )
	{
		if ( export_animation )
		{
			animation_time -= 0.01666;
		}
		else
		{
			animation_time -= deltaTime;
		}
		vec3 a = animation_start_pos - animation_look_at;
		vec3 b = animation_end_pos - animation_look_at;
		quat r = glm::rotation( glm::normalize(a), glm::normalize(b) );
		quat rt = glm::slerp( quat( 1, 0, 0, 0 ), r, 1 - animation_time / animation_duration );
		vec3 c = glm::rotate(rt, a);
		c *= glm::pow( glm::length( b ) / glm::length( a ), 1 - animation_time / animation_duration );

		scene.active_camera->as<Camera>()->setPosition( animation_look_at + c );
		scene.active_camera->as<Camera>()->setDirection( animation_look_at - c );
		if ( export_animation )
		{
			std::filesystem::path fpath = fmt::format( "animation/{}", modules.getActiveModuleName() );
			if ( !std::filesystem::exists( fpath ) )
			{
				std::filesystem::create_directories( fpath );
			}
			std::string fname = fmt::format( "{}/{}.png", fpath.string(), animation_frame );
			chag::Settings::getGlobalInstance().set( "exporting_teaser", true, false );
			modules.getActiveModule()->saveFrameToFile( fname, mainFB );
			chag::Settings::getGlobalInstance().set( "exporting_teaser", false, false );
		}
		if ( animation_time <= 0.f )
		{
			animating = false;
			gl_helpers::showGUI();
		}
		animation_frame += 1;
	}

	scene.active_camera->as<Camera>()->update( deltaTime );

	hair->shouldOverrideColor( override_hair_color );
	hair->setOverrideColor( hair_color );

	scene.lights[0]->setPosition( lightDistance * math_helpers::sphericalCoordsToCartesian( lightAttitude * vec2( 2 * M_PI, M_PI ) ) );
	scene.lights[0]->setOrientation( math_helpers::qLookAt( normalize( -scene.lights[0]->getPosition() ), Scene::worldFwd, Scene::worldUp ) );
	scene.lights[0]->as<Light>()->ortho_bounds = lightOrthoBounds;
	scene.lights[0]->update(deltaTime);

	for ( auto& obj : scene.objects )
	{
		obj->update(deltaTime);
	}

	modules.update( &scene, deltaTime );

	bool shouldQuit = false;
	if ( !ImGui::GetIO().WantCaptureKeyboard )
	{
		if (ImGui::IsKeyReleased( SDLK_ESCAPE ) )
		{
			shouldQuit = true;
		}
	}


	return shouldQuit;
}

void gui()
{
	if ( !ImGui::GetIO().WantCaptureKeyboard )
	{
		if (ImGui::IsKeyReleased(SDLK_g) )
		{
			showUI = !showUI;
		}
	}

	//ImGui::ShowDemoWindow();
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	ImGui::BeginMainMenuBar();
	{
		if ( ImGui::BeginMenu( "File" ) )
		{
			if ( ImGui::MenuItem( "Load Hair File" ) )
			{
				ImGui::FileDialog::Instance().Open( "Open Hair File Dialog", "Open a Hair file", "hair file (*.hair){.hair},.*" );
			}
			ImGui::MenuItem( "Debug geometry", nullptr, &showDebugGeometry );
			ImGui::EndMenu();
		}
		if ( ImGui::BeginMenu( "Switch Module" ) )
		{
			for ( auto& m : modules )
			{
				const char* name = m.second->getModuleName();
				bool selected = name == modules.getActiveModuleName();
				if ( ImGui::MenuItem( name, nullptr, &selected ) )
				{
					modules.changeActiveModule( name );
				}
			}
			ImGui::EndMenu();
		}
		if ( modules.getActiveModuleName() == "Realtime NN Eval"s )
		{
			if ( ImGui::BeginMenu( "Neural Network Model" ) )
			{
				std::string model_shape_str = chag::Settings::getGlobalInstance().get( "model-shape", "" );
				if ( ImGui::MenuItem( "Medium", nullptr, model_shape_str.empty() ) )
				{
					chag::Settings::getGlobalInstance().set( "model-shape", "", false );
					modules.reloadActiveModule();
				}
				else if ( ImGui::MenuItem( "Tiny", nullptr, model_shape_str == "tiny"s ) )
				{
					chag::Settings::getGlobalInstance().set( "model-shape", "tiny", false );
					modules.reloadActiveModule();
				}
				else if ( ImGui::MenuItem( "Large", nullptr, model_shape_str == "large"s ) )
				{
					chag::Settings::getGlobalInstance().set( "model-shape", "large", false );
					modules.reloadActiveModule();
				}
				else if ( ImGui::MenuItem( "1 spp", nullptr, model_shape_str == "1spp"s ) )
				{
					chag::Settings::getGlobalInstance().set( "model-shape", "1spp", false );
					modules.reloadActiveModule();
				}
				else if ( ImGui::MenuItem( "2 spp", nullptr, model_shape_str == "2spp"s ) )
				{
					chag::Settings::getGlobalInstance().set( "model-shape", "2spp", false );
					modules.reloadActiveModule();
				}
				else if ( ImGui::MenuItem( "2 steps", nullptr, model_shape_str == "2step"s ) )
				{
					chag::Settings::getGlobalInstance().set( "model-shape", "2step", false );
					modules.reloadActiveModule();
				}
				else if ( ImGui::MenuItem( "4 steps", nullptr, model_shape_str == "4step"s ) )
				{
					chag::Settings::getGlobalInstance().set( "model-shape", "4step", false );
					modules.reloadActiveModule();
				}
				ImGui::EndMenu();
			}
		}
		if ( ImGui::BeginMenu( "Windows" ) )
		{
			ImGui::MenuItem( "Performance", nullptr, &performance_window );
			ImGui::MenuItem( "Animation", nullptr, &animation_window );
			ImGui::EndMenu();
		}
	}
	ImGui::EndMainMenuBar();


	if ( ImGui::FileDialog::Instance().IsDone( "Open Hair File Dialog" ) )
	{
		if ( ImGui::FileDialog::Instance().HasResult() )
		{
			std::filesystem::path file = ImGui::FileDialog::Instance().GetResult();
			loadHairFile( file, true );
		}
		ImGui::FileDialog::Instance().Close();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	ImGui::Begin( "Data Generation" );
	{
		ImGui::PushItemWidth( 100 );
		ImGui::InputInt( "Training batch num.", &batch_number );
		ImGui::HelpMarker( "For multiple hairstyles, data generation will start at (#batch - 1) * #frames + 1" );
		if ( batch_number <= 0 )
		{
			batch_number = 1;
		}

		ImGui::InputInt( "Input samples per frame", &n_samples_in_input );
		if ( n_samples_in_input < 1 )
		{
			n_samples_in_input = 1;
		}

		ImGui::Separator();

		ImGui::Checkbox( "Create Input sets", &create_input_sets );
		ImGui::Checkbox( "Create Target sets", &create_target_sets );

		ImGui::Separator();

		ImGui::Checkbox( "Generate Train dataset", &generate_train_data );

		if ( generate_train_data )
		{
			ImGui::InputInt( "Training Seed", &train_seed );
			if ( train_seed < 0 )
			{
				train_seed = 0;
			}
			ImGui::InputInt( "Number of training frames", &n_train_frames );
			if ( n_train_frames < 0 )
			{
				n_train_frames = 0;
			}
			ImGui::InputInt( "Training frames between light change##training", &n_train_frames_between_random_light );
			ImGui::HelpMarker( "Every so many frames, the light will be moved to a new random position" );
			if ( n_train_frames_between_random_light < 0 )
			{
				n_train_frames_between_random_light = 0;
			}
		}

		ImGui::Separator();

		ImGui::Checkbox( "Generate Validation dataset", &generate_validation_data );

		if ( generate_validation_data )
		{
			ImGui::InputInt( "Validation Seed", &validation_seed );
			if ( validation_seed < 0 )
			{
				validation_seed = 0;
			}
			ImGui::InputInt( "Number of validation frames", &n_validation_frames );
			if ( n_validation_frames < 0 )
			{
				n_validation_frames = 0;
			}
			ImGui::InputInt( "Validation frames between light change##validation", &n_validation_frames_between_random_light );
			ImGui::HelpMarker( "Every so many frames, the light will be moved to a new random position" );
			if ( n_validation_frames_between_random_light < 0 )
			{
				n_validation_frames_between_random_light = 0;
			}
		}

		ImGui::Separator();

		ImGui::Checkbox( "Generate Evaluation dataset", &generate_eval_data );

		if ( generate_eval_data )
		{
			ImGui::InputFloat3( "Eval Camera Pos", &eval_frame_camera_position.x );

			ImGui::InputFloat( "Eval Camera Roll", &eval_frame_camera_roll );

			ImGui::InputFloat2( "Eval Light Attitude", &eval_frame_light_attitude.x );
			eval_frame_light_attitude = eval_frame_light_attitude - glm::floor( eval_frame_light_attitude );
		}

		ImGui::PopItemWidth();

		if ( ImGui::Button( "Generate Data" ) )
		{
			ImGui::FileDialog::Instance().Open( "Generate Data Dialog", "Generate Data", "" );
		}
		ImGui::Text( "Saving stage %d", (int)saving_stage );
		ImGui::Text("Frames to save %d", n_frames_remaining);
	}
	ImGui::End();

	/////////////////////////////////////////////////////////////////////////

	if ( ImGui::FileDialog::Instance().IsDone( "Generate Data Dialog" ) )
	{
		if ( ImGui::FileDialog::Instance().HasResult() )
		{
			saving_to_dir = ImGui::FileDialog::Instance().GetResult();
			saving_stage = SavingStage::Start;
		}
		ImGui::FileDialog::Instance().Close();
	}

	if ( ImGui::Button( "Export View" ) )
	{
		ImGui::FileDialog::Instance().Open( "Export View Dialog", "Export View", "" );
	}
	if ( ImGui::FileDialog::Instance().IsDone( "Export View Dialog" ) )
	{
		if ( ImGui::FileDialog::Instance().HasResult() )
		{
			std::filesystem::path path = ImGui::FileDialog::Instance().GetResult();
			std::string fname = str::tolower( str::strip_invalid_filename_chars(
			        str::replace_any( modules.getActiveModule()->getModuleName(), " ", '_' ) ) );
			modules.getActiveModule()->saveFrameToFile( fmt::format( "{}/{}.png", path.string(), fname ), mainFB );
		}
		ImGui::FileDialog::Instance().Close();
	}

	if ( ImGui::Button( "Export For Teaser" ) )
	{
		ImGui::FileDialog::Instance().Open( "Export Teaser Dialog", "Export View", "" );
	}
	if ( ImGui::FileDialog::Instance().IsDone( "Export Teaser Dialog" ) )
	{
		if ( ImGui::FileDialog::Instance().HasResult() )
		{
			std::filesystem::path path = ImGui::FileDialog::Instance().GetResult();
			std::string fname = str::tolower( str::strip_invalid_filename_chars(
			        str::replace_any( modules.getActiveModule()->getModuleName(), " ", '_' ) ) );
			chag::Settings::getGlobalInstance().set( "exporting_teaser", true, false );
			modules.getActiveModule()->saveFrameToFile( fmt::format( "{}/{}.png", path.string(), fname ), mainFB );
			chag::Settings::getGlobalInstance().set( "exporting_teaser", false, false );
		}
		ImGui::FileDialog::Instance().Close();
	}

	bool showing_head = chag::Settings::getGlobalInstance().get( "display_head_model", false );
	if ( ImGui::Checkbox( "Display Head Model", &showing_head ) )
	{
		chag::Settings::getGlobalInstance().set( "display_head_model", showing_head, false );
	}

	bool showing_env = chag::Settings::getGlobalInstance().get( "display_envmap", false );
	if ( ImGui::Checkbox( "Display Environment", &showing_env ) )
	{
		chag::Settings::getGlobalInstance().set( "display_envmap", showing_env, false );
	}

	ImGui::DragFloat( "Env Rotation", &scene.environment.rotate, 0.001f, 0, 1 );

	ImGui::Checkbox( "Override Hair Color", &override_hair_color );
	ImGui::ColorEdit3( "Hair Color", &hair_color.r, ImGuiColorEditFlags_DisplayHSV );
	if ( ImGui::Button( "Randomise Hair Color" ) )
	{
		randomiseHairColor();
	}

	ImGui::ColorEdit3( "Background Color", &background_color.r, ImGuiColorEditFlags_DisplayHSV );


	ImGui::DragFloat( "Light Distance", &lightDistance, 0.01, 0, std::numeric_limits<float>::infinity() );
	ImGui::DragFloat2( "Light Attitude", &lightAttitude.x, 0.01, -std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity() );
	ImGui::DragFloat4( "Light Bounds", &lightOrthoBounds.x, 0.01, -std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity() );
	lightAttitude = lightAttitude - glm::floor( lightAttitude );

	if ( ImGui::Button( "Random Light Position" ) )
	{
		randomiseLightPosition();
	}

	ImGui::ColorEdit3( "Light Color", &scene.lights[0]->as<Light>()->color.r, ImGuiColorEditFlags_DisplayHSV );
	ImGui::DragFloat( "Light Intensity",
	                  &scene.lights[0]->as<Light>()->intensity,
	                  1,
	                  0,
	                  10000,
	                  "%.3f",
	                  ImGuiSliderFlags_Logarithmic );

	glm::vec3 cam_pos = scene.active_camera->getPosition();
	ImGui::InputFloat3( "Camera Position", &cam_pos.x );
	//glm::vec3 cam_dir = glm::rotate( scene.active_camera->getOrientation(), Scene::worldFwd );
	glm::quat cam_dir = scene.active_camera->getOrientation();
	ImGui::InputFloat4( "Camera Orientation", &cam_dir.x );
	float cam_roll = scene.active_camera->as<Camera>()->roll;
	ImGui::InputFloat( "Camera Roll", &cam_roll );
	if ( ImGui::Button( "Random Camera Position" ) )
	{
		randomiseCameraPosition();
	}

	if ( ImGui::Button( "Setup Eval Scene" ) )
	{
		setupEvalScene();
	}

	if ( ImGui::Button( "Setup Teaser Scene" ) )
	{
		setupTeaserScene();
	}

	if ( ImGui::Button( "Setup Display Scene" ) )
	{
		scene.active_camera->as<Camera>()->setPosition( glm::normalize( glm::vec3( 0.0f, 50.0f, 80.0f ) ) );
		scene.active_camera->as<Camera>()->setOrientation( quat(0, 0, -0.485, 0.875) );
		scene.active_camera->as<Camera>()->roll = 0;
		lightAttitude = vec2(0, 0.250);
	}

	if ( ImGui::Button( "Setup Display Scene Curly" ) )
	{
		scene.active_camera->as<Camera>()->setPosition( vec3(0.715, 0.422, 0.243) );
		scene.active_camera->as<Camera>()->setOrientation( quat(0.536, -0.056, -0.097, 0.871) );
		scene.active_camera->as<Camera>()->roll = 0;
		lightAttitude = vec2(0, 0.250);
	}

	ImGui::Checkbox( "Don't Force Update Frame on Export", &dont_force_update_frame_number_on_export );

	ImGui::SliderFloat( "Factor", &scene.shadowmap.polygon_offset_factor, 0.0f, 10.0f, "%.3f", ImGuiSliderFlags_Logarithmic );
	ImGui::SliderFloat( "Units", &scene.shadowmap.polygon_offset_units, 0.0f, 100.0f, "%.3f", ImGuiSliderFlags_Logarithmic );


	if ( animation_window )
	{
		ImGui::Begin( "Animation", &animation_window );
		{
			ImGui::DragFloat3( "Start position", &animation_start_pos.x );
			ImGui::DragFloat3( "End position", &animation_end_pos.x );
			ImGui::DragFloat3( "Look At", &animation_look_at.x );
			ImGui::DragFloat( "Duration", &animation_duration );
			ImGui::Checkbox( "Export Animation", &export_animation );
			if ( ImGui::Button( "Animate" ) )
			{
				animating = true;
				gl_helpers::hideGUI();
				animation_time = animation_duration;
				animation_frame = 0;
			}
		}
		ImGui::End();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	performance_window = chag::perf::drawEventsWindow( performance_window, true );

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	// ----------------- Set variables --------------------------
	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate,
	            ImGui::GetIO().Framerate);
}

int main(int argc, const char* argv[])
{
	chag::Settings::getGlobalInstance().addCommandArguments( argc, argv );
	chag::Settings::getGlobalInstance().setFile( "settings.ini" );
	chag::Settings::getGlobalInstance().loadFromFile();

	g_window = gl_helpers::init_window_SDL("OpenGL Project", 1024, 1024);

	initialize();

	bool shouldQuit = false;
	startTime = std::chrono::system_clock::now();

	while(!shouldQuit)
	{
		// check events (keyboard among other)
		SDL_Event event;
		bool quitEvent = false;
		while(SDL_PollEvent(&event))
		{
			if(event.type == SDL_QUIT)
			{
				quitEvent = true;
			}

			gl_helpers::processEvent( &event );
		}

		// Inform imgui of new frame
		gl_helpers::newFrame(g_window);

		// check events (keyboard among other)
		shouldQuit = update();

		if ( quitEvent )
		{
			shouldQuit = true;
		}

		// Render overlay GUI.
		if(showUI)
		{
			gui();
		}

		// render to window
		display();

		gl_helpers::finishFrame();

		// Swap front and back buffer. This frame will now been displayed.
		SDL_GL_SwapWindow(g_window);

		modules.swapCurrentAndNextModules();
	}

	shutdown();

	// Shut down everything. This includes the window and all other subsystems.
	gl_helpers::shutDown(g_window);
	return 0;
}

