#pragma once

#include <filesystem>
#include <iostream>
#include <glm/glm.hpp>
#include <vector>

#include <scene.h>

class ObjModel : public SceneObject
{
public:
	uint32_t num_indices = 0;
	uint32_t num_vertices = 0;

	uint32_t gl_vao = 0;
	uint32_t gl_vertex_attribs = 0;
	uint32_t gl_idxs = 0;

	struct submesh_t
	{
		uint32_t index_start = 0;
		uint32_t index_count = 0;
		struct
		{
			glm::vec3 color;
			glm::vec3 emission;
			float roughness;
			float metallic;
			uint32_t color_tex = 0;
			uint32_t normal_tex = 0;
			uint32_t alpha_tex = 0;
		} material;
	};

	std::vector<submesh_t> submeshes;

public:
	ObjModel() : SceneObject("") { }
	~ObjModel();

	bool loadFromFile( const std::filesystem::path& filePath );

	bool isLoaded() const;

	void unload();

protected:
	virtual void _render( uint32_t shaderProgram ) const override;

	virtual void renderSubmesh( uint32_t shaderProgram, size_t idx ) const;

};


