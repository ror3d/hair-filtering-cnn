#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <scene.h>

class Camera : public SceneObject
{
public: 
	Camera( const std::string& name );
	~Camera() {}

	void setFoV( float _fov );
	void setClipPlanes( float near, float far );

	void setDirection( glm::vec3 dir );
	void setRoll( float angle );

	void updateWindowSize( float width, float height );

	glm::mat4 getViewMatrix() const;
	glm::mat4 getProjMatrix() const;

	float speed = .2f;
	float rotationSpeed = 0.001f;

	float fov = 45.f;
	float near_plane = 0.01f;
	float far_plane = 100.f;

	float roll = 0;

	float viewportWidth = 1, viewportHeight = 1;

	glm::mat4 view_mat;
	glm::mat4 proj_mat;

	bool control_enabled = true;

private:

	void _update(float dT) override;

	virtual void _render( uint32_t shaderProgram ) const override;

	void updateProjMat();
};

