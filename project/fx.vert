#version 420

layout(location = 0) in vec3 position;

out vec2 v_position;

void main() 
{
	gl_Position = vec4(position.xy, 0.0, 1.0);
	v_position = position.xy;
}

