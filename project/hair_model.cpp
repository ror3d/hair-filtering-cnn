#include "hair_model.h"
#include <fstream>
#include <GL/glew.h>
#include <utils/gl_helpers.h>
#include <utils/math_helpers.h>

static enum class HAIR_BIT_FLAGS : uint32_t
{
	HAS_SEGMENTS_ARRAY = 0b1,
	HAS_POINTS_ARRAY = 0b10, // Must be 1
	HAS_THICKNESS_ARRAY = 0b100,
	HAS_TRANSPARENCY_ARRAY = 0b1000,
	HAS_COLOR_ARRAY = 0b10000,
};


HairModel::~HairModel()
{
	if ( gl_vao )
	{
		glDeleteVertexArrays( 1, &gl_vao );
		gl_vao = 0;
	}
	if ( gl_points )
	{
		glDeleteBuffers( 1, &gl_points );
		gl_points = 0;
	}
	if ( gl_tangents )
	{
		glDeleteBuffers( 1, &gl_tangents );
		gl_tangents = 0;
	}
	if ( gl_idxs )
	{
		glDeleteBuffers( 1, &gl_idxs );
		gl_idxs = 0;
	}
}

HairModel::HairModel( HairModel&& o )
	: SceneObject(std::move((SceneObject&)o))
{
	std::swap( n_idxs, o.n_idxs );

	std::swap( gl_vao, o.gl_vao );
	std::swap( gl_points, o.gl_points );
	std::swap( gl_tangents, o.gl_tangents );
	std::swap( gl_idxs, o.gl_idxs );

	std::swap( n_strands, o.n_strands );
	std::swap( points, o.points );
	std::swap( tangents, o.tangents );
	std::swap( thickness, o.thickness );
	std::swap( transparency, o.transparency );
	std::swap( color, o.color );

	std::swap( default_thickness, o.default_thickness );
	std::swap( default_transparency, o.default_transparency );
	std::swap( default_color, o.default_color );

	std::swap( n_def_segments_per_strand, o.n_def_segments_per_strand );

	std::swap( override_color, o.override_color );
	std::swap( should_override_color, o.should_override_color );
}

HairModel HairModel::generateRandomSubsample( const HairModel& source, float prob_to_show )
{
	HairModel m;

	m.setPosition( source.position );
	m.setScale( source.scale );
	m.setOrientation( source.orientation );
	m.setOverrideColor( source.override_color );
	m.shouldOverrideColor( source.should_override_color );

	m.default_thickness = source.default_thickness;
	m.default_transparency = source.default_transparency;
	m.default_color = source.default_color;

	m.points.reserve( source.points.size() * prob_to_show );
	m.tangents.reserve( m.tangents.size() );
	m.thickness.reserve( m.points.size() );
	m.transparency.reserve( m.points.size() );
	m.color.reserve( m.points.size() );

	std::vector<uint32_t> idxs;
	idxs.reserve( m.points.size() );

	m.n_def_segments_per_strand = source.n_def_segments_per_strand;

	m.n_strands = 0;

	size_t vertex_start = 0;
	size_t cur_idx = 0;
	for ( size_t i = 0; i < source.n_strands; ++i )
	{
		uint16_t strand_length = source.n_def_segments_per_strand;
		if ( source.strand_length.size() > 0 )
		{
			strand_length = source.strand_length[i];
		}

		if ( math_helpers::randf() > prob_to_show )
		{
			vertex_start += strand_length + 1;
			continue;
		}

		m.n_strands += 1;

		if ( source.strand_length.size() > 0 )
		{
			m.strand_length.push_back( source.strand_length[i] );
		}

		for ( size_t j = 0; j < strand_length + 1; ++j )
		{
			idxs.push_back( cur_idx );
#if USE_QUADS_FOR_LINES
			////////////////////////////////////////////////////////////////////////////
			// For lines with adjacency, we need to duplicate the first and last vertex 
			// of each strand
			////////////////////////////////////////////////////////////////////////////
			if ( j == 0 || j == strand_length )
			{
				idxs.push_back( cur_idx );
			}
#endif
			cur_idx += 1;
		}

		strand_length += 1; // Add one because the length is counted in segments and we need vertices
		// We didn't do it sooner because we needed the original number to create duplicate indices for the geom shader

		idxs.push_back( prim_restart_idx );
		for ( size_t j = 0; j < strand_length; ++j )
		{
			m.points.push_back( source.points[vertex_start + j] );
			m.tangents.push_back( source.tangents[vertex_start + j] );
		}

		if ( !source.thickness.empty() )
		{
			for ( size_t j = 0; j < strand_length; ++j )
			{
				m.thickness.push_back( source.thickness[vertex_start + j] );
			}
		}
		if ( !source.transparency.empty() )
		{
			for ( size_t j = 0; j < strand_length; ++j )
			{
				m.transparency.push_back( source.transparency[vertex_start + j] );
			}
		}
		if ( !source.color.empty() )
		{
			for ( size_t j = 0; j < strand_length; ++j )
			{
				m.color.push_back( source.color[vertex_start + j] );
			}
		}

		vertex_start += strand_length;
	}

	m.moveToGPU( idxs );

	return std::move( m );
}


bool HairModel::load( std::istream& file )
{
	this->n_idxs = 0;
	char head[5] = {0};
	file.read( head, 4 );
	if ( strcmp( head, "HAIR" ) != 0 )
	{
		// TODO: Error
		return false;
	}
#define TEST_STREAM(message) do{ if(file.fail()) { /* TODO: Log message */ return false; } }while(0)

	TEST_STREAM( "read the intro bytes" );

	n_strands = 0;
	file.read( reinterpret_cast<char*>(&n_strands), 4 );

	TEST_STREAM( "read number of strands" );

	uint32_t n_points = 0;
	file.read( reinterpret_cast<char*>(&n_points), 4 );

	TEST_STREAM( "read number of points" );

	uint32_t bit_flags = 0;
	file.read( reinterpret_cast<char*>(&bit_flags), 4 );

	TEST_STREAM( "read bit flags" );

	file.read( reinterpret_cast<char*>(&n_def_segments_per_strand), 4 );

	TEST_STREAM( "read default strand length" );

	default_thickness = 0;
	file.read( reinterpret_cast<char*>(&default_thickness), 4 );

	TEST_STREAM( "read default thickness" );

	default_transparency = 0;
	file.read( reinterpret_cast<char*>(&default_transparency), 4 );

	TEST_STREAM( "read default transparency" );

	file.read( reinterpret_cast<char*>(&default_color), 3*4 );

	TEST_STREAM( "read default color" );

	char fileinfo[89] = {0};
	file.read( fileinfo, 88 );

	TEST_STREAM( "read extra info ????" );

	if ( file.tellg() != 128 )
	{
		// TODO: Error
		return false;
	}

	if ( n_def_segments_per_strand > 0xffff )
	{
		// TODO: Error, this overflows the original maximum number of segments per strand
		return false;
	}

	if ( bit_flags & uint32_t(HAIR_BIT_FLAGS::HAS_SEGMENTS_ARRAY) )
	{
		strand_length.resize( n_strands );

		file.read( reinterpret_cast<char*>(strand_length.data()), strand_length.size() * sizeof( uint16_t ) );

		TEST_STREAM( "read segments array" );
	}

	if ( !(bit_flags & uint32_t(HAIR_BIT_FLAGS::HAS_POINTS_ARRAY) ))
	{
		// TODO: Error, points are required
		return false;
	}
	points.resize( n_points );
	file.read( reinterpret_cast<char*>(points.data()), n_points * sizeof( glm::vec3 ) );

	bool good = file.good();
	bool eof = file.eof();
	bool fail = file.fail();
	bool bad = file.bad();
	size_t readb = file.gcount();
	TEST_STREAM( "read points array" );

	if ( bit_flags & uint32_t(HAIR_BIT_FLAGS::HAS_THICKNESS_ARRAY) )
	{
		thickness.resize( n_points );
		file.read( reinterpret_cast<char*>(thickness.data()), n_points * sizeof( float ) );

		TEST_STREAM( "read thickness array" );
	}

	if ( bit_flags & uint32_t(HAIR_BIT_FLAGS::HAS_TRANSPARENCY_ARRAY) )
	{
		transparency.resize( n_points );
		file.read( reinterpret_cast<char*>(transparency.data()), n_points * sizeof( float ) );

		TEST_STREAM( "read transparency array" );
	}

	if ( bit_flags & uint32_t(HAIR_BIT_FLAGS::HAS_COLOR_ARRAY) )
	{
		color.resize( n_points );
		file.read( reinterpret_cast<char*>(color.data()), n_points * sizeof( glm::vec3 ) );

		TEST_STREAM( "read color array" );
	}


	/*
	////////////////////////////////////////////
	// Create new jittered strands

	size_t new_nstrands = n_strands;
	size_t new_npoints = n_points;
	for ( size_t i = 0, idx = 0; i < n_strands; ++i )
	{
		size_t cur_strand_len = n_def_segments_per_strand;
		if ( !strand_length.empty() )
		{
			cur_strand_len = strand_length[i];
		}

		// Chance to duplicate
		if ( math_helpers::randf() > 0.3 )
		{
			idx += cur_strand_len + 1;
			continue;
		}
		new_nstrands += 1;

		using namespace glm;
		vec3 pos = points[idx];
		vec3 pos2 = points[idx + 1];
		vec3 t = pos2 - pos;
		float l = length( t );
		t /= l;
		vec3 u = dot( t, vec3{ 1, 0, 0 } ) < dot( t, vec3{ 0, 1, 0 } ) ? vec3{ 1, 0, 0 } : vec3{ 0, 1, 0 };
		vec3 v = normalize(cross( t, u ));
		u = cross( t, v );
		float r = math_helpers::randf();
		vec3 d = u * cosf( r * M_PI ) + v * sinf( r * M_PI );
		d *= 0.6 * l;
		for ( size_t j = 0; j < cur_strand_len + 1; ++j )
		{
			points.push_back(points[idx] + d);

			if ( !strand_length.empty() )
			{
				strand_length.push_back( strand_length[idx] );
			}
			if ( !thickness.empty() )
			{
				thickness.push_back( thickness[idx] );
			}
			if ( !transparency.empty() )
			{
				transparency.push_back( transparency[idx] );
			}
			if ( !color.empty() )
			{
				color.push_back( color[idx] );
			}
			idx += 1;
			new_npoints += 1;
		}
	}
	n_points = new_npoints;
	n_strands = new_nstrands;

	////////////////////////////////////////////
	*/


	// These are only used to create the tangents more easily
	std::vector<uint32_t> tangent_idxs;

	std::vector<uint32_t> idxs;
	size_t n_idxs = size_t( n_points ) + n_strands;
	idxs.reserve( n_idxs );
	for ( size_t i = 0, idx = 0; i < n_strands; ++i )
	{
		size_t cur_strand_len = n_def_segments_per_strand;
		if ( !strand_length.empty() )
		{
			cur_strand_len = strand_length[i];
		}
		for ( size_t j = 0; j < cur_strand_len + 1; ++j )
		{
			idxs.push_back( idx );
			tangent_idxs.push_back( idx );
#if USE_QUADS_FOR_LINES
			////////////////////////////////////////////////////////////////////////////
			// For lines with adjacency, we need to duplicate the first and last vertex 
			// of each strand
			////////////////////////////////////////////////////////////////////////////
			if (j == 0 || j == cur_strand_len) {
				idxs.push_back(idx);
			}
#endif
			idx += 1;
		}
		idxs.push_back( prim_restart_idx );
		tangent_idxs.push_back( prim_restart_idx );
	}

	//////////////////////////////////////////////////////////////////////////////
	// Calculate tangent per point
	// Unless it is the first or last segment in a strand, use average tangent
	// of segments.
	//////////////////////////////////////////////////////////////////////////////
	tangents.clear();
	for (size_t i = 0; i < tangent_idxs.size() - 1; i++)
	{
		if (tangent_idxs[i] == prim_restart_idx)
		{
			continue; 
		}

		bool first_point_in_strand = (i == 0) || (tangent_idxs[i - 1] == prim_restart_idx);
		bool last_point_in_strand = tangent_idxs[i + 1] == prim_restart_idx; 

		if (first_point_in_strand)
		{
			tangents.push_back(glm::normalize(points[tangent_idxs[i + 1]] - points[tangent_idxs[i]]));
		}
		else if (last_point_in_strand)
		{
			tangents.push_back(glm::normalize(points[tangent_idxs[i]] - points[tangent_idxs[i - 1]]));
		}
		else
		{
			tangents.push_back( glm::normalize( glm::normalize( points[tangent_idxs[i + 1]] - points[tangent_idxs[i]] )
			                                    + glm::normalize( points[tangent_idxs[i]] - points[tangent_idxs[i - 1]] ) ) );
		}
	}

	return moveToGPU( idxs );
}

bool HairModel::isLoaded() const
{
	return n_idxs != 0;
}

bool HairModel::moveToGPU( const std::vector<uint32_t>& idxs )
{
	if ( gl_points )
	{
		glDeleteBuffers( 1, &gl_points );
	}
	glCreateBuffers( 1, &gl_points );
	glNamedBufferStorage( gl_points, points.size() * sizeof( glm::vec3 ), points.data(), 0 );

	if ( gl_tangents )
	{
		glDeleteBuffers( 1, &gl_tangents );
	}
	glCreateBuffers( 1, &gl_tangents );
	glNamedBufferStorage( gl_tangents, tangents.size() * sizeof( glm::vec3 ), tangents.data(), 0 );

	if ( gl_idxs )
	{
		glDeleteBuffers( 1, &gl_idxs );
	}
	glCreateBuffers( 1, &gl_idxs );
	glNamedBufferStorage( gl_idxs, idxs.size() * sizeof( uint32_t ), idxs.data(), 0 );


	if ( gl_vao )
	{
		glDeleteVertexArrays( 1, &gl_vao );
	}
	glGenVertexArrays( 1, &gl_vao );
	glBindVertexArray( gl_vao );

	glBindBuffer( GL_ARRAY_BUFFER, gl_points );
	glVertexAttribPointer( 0, 3, GL_FLOAT, false /*normalized*/, 0 /*stride*/, 0 /*offset*/ );
	glEnableVertexAttribArray( 0 );

	glBindBuffer(GL_ARRAY_BUFFER, gl_tangents);
	glVertexAttribPointer( 4, 3, GL_FLOAT, false /*normalized*/, 0 /*stride*/, 0 /*offset*/);
	glEnableVertexAttribArray( 4 );

	glVertexArrayElementBuffer( gl_vao, gl_idxs );

	glBindVertexArray( 0 );

	n_idxs = idxs.size();
	return true;
}

void HairModel::_render( uint32_t shaderProgram ) const
{
	if ( !isLoaded() )
	{
		return;
	}

	glEnable( GL_PRIMITIVE_RESTART );

	gl_helpers::setUniformSlow( shaderProgram, "hasColorArray", !color.empty() );
	gl_helpers::setUniformSlow( shaderProgram, "defHairColor", default_color );

	gl_helpers::setUniformSlow( shaderProgram, "should_override_color", should_override_color ? 1u : 0u );
	gl_helpers::setUniformSlow( shaderProgram, "override_color", override_color );

	gl_helpers::setUniformSlow( shaderProgram, "hasThicknessArray", !thickness.empty() );
	gl_helpers::setUniformSlow( shaderProgram, "defHairThickness", default_thickness );

	gl_helpers::setUniformSlow( shaderProgram, "hasTransparencyArray", !transparency.empty() );
	gl_helpers::setUniformSlow( shaderProgram, "defHairTransparency", default_transparency );

	glBindVertexArray( gl_vao );
	glPrimitiveRestartIndex( prim_restart_idx );

#if USE_QUADS_FOR_LINES
	glDrawElements(GL_LINE_STRIP_ADJACENCY, n_idxs, GL_UNSIGNED_INT, 0);
#else
	glDrawElements( GL_LINE_STRIP, n_idxs, GL_UNSIGNED_INT, 0 );
#endif

	glBindVertexArray( 0 );

	glDisable( GL_PRIMITIVE_RESTART );
}

void HairModel::setOverrideColor( glm::vec3 color )
{
	override_color = color;
}

void HairModel::shouldOverrideColor( bool overide )
{
	should_override_color = overide;
}

glm::vec3 HairModel::getRenderingColor() const
{
	return should_override_color ? override_color : default_color;
}

bool HairModel::hasAlpha() const
{
	return true;
}

bool HairModel::loadFromFile( const std::filesystem::path& filePath )
{
	return load(std::ifstream(filePath, std::ios_base::binary));
}

