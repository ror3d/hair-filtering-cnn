#pragma once

#include <vector>
#include <memory>
#include <string>
#include <filesystem>
#include <map>

class Scene;
class Framebuffer;

class Module
{
public:
	Module() { }
	virtual ~Module() {}

	virtual const char* getModuleName() = 0;

	virtual void reloadShaders() = 0;

	virtual bool setup( uint32_t width, uint32_t height ) = 0;

	virtual void teardown() = 0;

	virtual void update( Scene* scene, float deltaTime ) = 0;

	virtual void windowSizeChanged( uint32_t width, uint32_t height ) = 0;

	virtual void sceneChanged( const Scene* scene ) {}

	virtual void render(const Scene* scene, Framebuffer& target_framebuffer) = 0;

	virtual bool readyToSaveFrame() const { return true; }

	virtual void saveFrameToFile( const std::filesystem::path& base_file_name, Framebuffer& target_framebuffer ) {};
};

class ModuleManager
{
public:
	using string = std::string;

	ModuleManager() { }
	~ModuleManager();

	void addModule( std::shared_ptr<Module> m );

	void changeActiveModule( const std::string& mod );
	inline string getActiveModuleName() const { return current_module_name; }
	std::shared_ptr<Module> getActiveModule();

	void reloadShaders();

	void windowSizeChanged( uint16_t windowWidth, uint16_t windowHeight );
	void render( const Scene* scene, Framebuffer& target_framebuffer );
	void update( Scene* scene, float deltaTime );

	void sceneChanged( const Scene* scene );

	void swapCurrentAndNextModules();

	void reloadActiveModule();

public:
	using map_t = std::map<string, std::shared_ptr<Module>>;

	map_t::iterator begin();
	map_t::const_iterator begin() const;
	map_t::iterator end();
	map_t::const_iterator end() const;
private:
	uint16_t windowWidth = 0, windowHeight = 0;

	map_t modules_map;
	string current_module_name = "";
	string next_module_name = "";

	bool should_reload = false;

	const Scene* scene_cache;
};

