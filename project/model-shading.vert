#version 420

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uvs;

uniform mat4 viewProjectionMatrix; 
uniform mat4 viewMatrix; 
uniform mat4 modelMatrix; 
//uniform mat3 normalMatrix; 

out vec4 v_color;
out vec3 v_view_space_position;
out vec3 v_view_space_normal;
out vec3 v_model_space_position;
out vec2 v_uvs;

void main() 
{
	gl_Position = viewProjectionMatrix * modelMatrix * vec4(position, 1.0);
	v_view_space_position = (viewMatrix * modelMatrix * vec4(position, 1.0)).xyz;
	v_model_space_position = position;

	// TODO: the normalMatrix should be calculated in C++ instead of glsl, but that means a bunch of refactoring
	mat3 normalMatrix = mat3(viewMatrix * modelMatrix);
	normalMatrix = transpose(inverse(normalMatrix));
	v_view_space_normal = normalMatrix * normal; 

	v_uvs = uvs;
}

