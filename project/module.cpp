#include "module.h"
#include <windows.h>

using namespace std;

ModuleManager::~ModuleManager()
{
	auto mod = getActiveModule();
	if ( mod )
	{
		mod->teardown();
	}
}

void ModuleManager::addModule( std::shared_ptr<Module> m )
{
	modules_map[m->getModuleName()] = m;
	changeActiveModule( m->getModuleName() );
}

void ModuleManager::changeActiveModule( const std::string& mod )
{
	next_module_name = mod;
}

std::shared_ptr<Module> ModuleManager::getActiveModule()
{
	auto mod = modules_map.find( current_module_name );
	if ( mod == modules_map.end() )
	{
		return {};
	}
	return mod->second;
}

void ModuleManager::reloadShaders()
{
	if ( should_reload )
	{
		return;
	}

	auto mod = getActiveModule();
	if ( mod )
	{
		mod->reloadShaders();
	}
}

void ModuleManager::windowSizeChanged( uint16_t Width, uint16_t Height )
{
	windowWidth = Width;
	windowHeight = Height;

	if ( should_reload )
	{
		return;
	}

	auto mod = getActiveModule();
	if ( mod )
	{
		mod->windowSizeChanged( Width, Height );
	}
}

void ModuleManager::render( const Scene* scene, Framebuffer& framebuffer )
{
	if ( should_reload )
	{
		return;
	}

	auto mod = getActiveModule();
	if ( mod )
	{
		mod->render( scene, framebuffer );
	}
}

void ModuleManager::update( Scene* scene, float deltaTime )
{
	if ( should_reload )
	{
		return;
	}

	auto mod = getActiveModule();
	if ( mod )
	{
		mod->update( scene, deltaTime );
	}
}

void ModuleManager::sceneChanged( const Scene* scene )
{
	scene_cache = scene;

	if ( should_reload )
	{
		return;
	}

	auto mod = getActiveModule();
	if ( mod )
	{
		mod->sceneChanged( scene );
	}
}

void ModuleManager::swapCurrentAndNextModules()
{
	std::string nmn = next_module_name;
	next_module_name = "";

	if ( should_reload )
	{
		next_module_name = current_module_name;
	}

	if ( !nmn.empty() && (nmn != current_module_name || should_reload) )
	{
		auto next_mod = modules_map.find( nmn );
		if ( next_mod == modules_map.end() )
		{
			return;
		}

		auto cur_mod = modules_map.find( current_module_name );

		if ( cur_mod != modules_map.end() )
		{
			cur_mod->second->teardown();
		}

		current_module_name = nmn;

		next_mod->second->setup( windowWidth, windowHeight );
		next_mod->second->sceneChanged( scene_cache );

		should_reload = false;
	}
}

void ModuleManager::reloadActiveModule()
{
	should_reload = true;
}

ModuleManager::map_t::iterator ModuleManager::begin()
{
	return modules_map.begin();
}

ModuleManager::map_t::const_iterator ModuleManager::begin() const
{
	return modules_map.begin();
}

ModuleManager::map_t::iterator ModuleManager::end()
{
	return modules_map.end();
}

ModuleManager::map_t::const_iterator ModuleManager::end() const
{
	return modules_map.end();
}

