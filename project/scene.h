#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <vector>
#include <memory>
#include <string>

class SceneObject
{
public:
	SceneObject(const std::string& name) : name(name) {}
	virtual ~SceneObject() {}

	template<typename _T>
	inline _T* as() { return dynamic_cast<_T*>(this); }

	std::string name;

	std::vector<std::shared_ptr<SceneObject>> children;
	std::weak_ptr<SceneObject> parent;


	inline void setPosition( glm::vec3 p ) { position = p; model_matrix_dirty = true; }
	inline void setScale( glm::vec3 s ) { scale = s; model_matrix_dirty = true; }
	inline void setOrientation( glm::quat q ) { orientation = q; model_matrix_dirty = true; }

	inline glm::vec3 getPosition() const { return position; }
	inline glm::vec3 getScale() const { return scale; }
	inline glm::quat getOrientation() const { return orientation; }

	void render( uint32_t shaderProgram ) const;

	void update(float dT);

	inline glm::mat4 getModelMatrix() const { return model_matrix; }

	virtual bool hasAlpha() const;

protected:

	virtual void _render( uint32_t shaderProgram ) const {};
	virtual void _update(float dT) {};

	void updateModelMatrix();

	glm::vec3 position = glm::vec3(0);
	glm::vec3 scale = glm::vec3(1);
	glm::quat orientation = glm::quat( 1, 0, 0, 0 );

	bool model_matrix_dirty = false;

	glm::mat4 model_matrix = glm::mat4(1);
};

class Scene
{
public: 
	static constexpr glm::vec3 worldUp = glm::vec3(0, 0, 1);
	static constexpr glm::vec3 worldRight = glm::vec3(1, 0, 0);
	static constexpr glm::vec3 worldFwd = glm::vec3(0, 1, 0);

	template<typename T> using ptr = std::shared_ptr<T>;
	template<typename T> using wptr = std::weak_ptr<T>;

public:

	Scene() { }
	~Scene() { }

	std::vector<ptr<SceneObject>> objects;

	ptr<SceneObject> active_camera;

	std::vector<ptr<SceneObject>> lights;

	struct
	{
		uint32_t texture = 0;
		float rotate = 0;
	} environment;

	struct
	{
		uint32_t size = 1024;
		float polygon_offset_factor = 3.f;
		float polygon_offset_units = 0.15f;
	} shadowmap;
};


