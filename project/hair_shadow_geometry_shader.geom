#version 450 compatibility
#extension GL_EXT_geometry_shader4 : enable

layout (lines_adjacency) in; 
layout (triangle_strip) out; 
layout (max_vertices=4) out; 

in float	v_transparency[];
in vec3		v_model_space_position[];
in float	v_hair_thickness[];

out float	g_transparency;
out vec3	g_model_space_position;
out float	g_hair_thickness;

uniform mat4 viewProjectionMatrix; 
uniform mat4 projectionMatrix; 
uniform mat4 invViewProjectionMatrix; 
uniform mat4 invProjectionMatrix; 

uniform vec2 screen_size;

uniform float line_width = 1.0;

vec3 GetVSCoord(vec4 clipspace_v)
{
	vec4 t = invProjectionMatrix * clipspace_v; 
	return vec3(t / t.w); 
}

vec3 GetMSCoord(vec4 clipspace_v)
{
	vec4 t = invViewProjectionMatrix * clipspace_v; 
	return vec3(t / t.w); 
}

void main()
{
	//////////////////////////////////////////////////////////////////////////////
	// Code mostly stolen from 
	// https://github.com/mhalber/Lines/blob/master/geometry_shader_lines.h
	//////////////////////////////////////////////////////////////////////////////
	const float aspect_ratio = screen_size.x / screen_size.y; 
    vec2 ndc_a = gl_PositionIn[0].xy / gl_PositionIn[0].w;
    vec2 ndc_b = gl_PositionIn[1].xy / gl_PositionIn[1].w;
    vec2 ndc_c = gl_PositionIn[2].xy / gl_PositionIn[2].w;
    vec2 ndc_d = gl_PositionIn[3].xy / gl_PositionIn[3].w;
	vec2 line_vector0 = ndc_b - ndc_a;
	vec2 line_vector1 = ndc_c - ndc_b;
	vec2 line_vector2 = ndc_d - ndc_c;
	//vec2 viewport_line_vector = line_vector * u_viewport_size;
    vec2 dir0 = normalize(vec2( line_vector0.x, line_vector0.y * aspect_ratio ));
    vec2 dir1 = normalize(vec2( line_vector1.x, line_vector1.y * aspect_ratio ));
    vec2 dir2 = normalize(vec2( line_vector2.x, line_vector2.y * aspect_ratio ));

	vec2 normal0 = vec2( -dir0.y, dir0.x );
	vec2 normal1 = vec2( -dir1.y, dir1.x );
	vec2 normal2 = vec2( -dir2.y, dir2.x );
   
    vec2 normala = vec2( line_width/screen_size.x, line_width/screen_size.y ) * normalize(normal0 + normal1); 
    vec2 normalb = vec2( line_width/screen_size.x, line_width/screen_size.y ) * normalize(normal1 + normal2);

	g_transparency	= v_transparency[1];
	g_hair_thickness = v_hair_thickness[1];


	gl_Position = vec4((ndc_b + normala)  * gl_PositionIn[1].w, gl_PositionIn[1].zw);
	g_model_space_position =  GetMSCoord(gl_Position);
	EmitVertex();
	gl_Position = vec4((ndc_b - normala)  * gl_PositionIn[1].w, gl_PositionIn[1].zw);
	g_model_space_position =  GetMSCoord(gl_Position);
	EmitVertex();

	g_transparency	= v_transparency[2];
	g_hair_thickness = v_hair_thickness[2];


	gl_Position = vec4((ndc_c + normalb)  * gl_PositionIn[2].w, gl_PositionIn[2].zw);
	g_model_space_position =  GetMSCoord(gl_Position);
	EmitVertex();
	gl_Position = vec4((ndc_c - normalb)  * gl_PositionIn[2].w, gl_PositionIn[2].zw);
	g_model_space_position =  GetMSCoord(gl_Position);
	EmitVertex();
}

