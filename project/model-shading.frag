﻿#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

in vec3 v_view_space_position;
in vec3 v_view_space_normal;
out vec3 v_model_space_position;
in vec2 v_uvs;

layout(binding = 0) uniform sampler2D colorTex;
layout(binding = 1) uniform sampler2D alphaTex;
layout(binding = 2) uniform sampler2D normalTex;

uniform int material_has_color_texture = 0;
uniform int material_has_alpha_texture = 0;
uniform int material_has_normal_texture = 0;

uniform vec3 material_color = vec3(1.0, 1.0, 1.0);
uniform vec3 material_emission = vec3(1.0, 1.0, 1.0);
uniform float material_roughness = 1.0;
uniform float material_metallic = 0.0;


uniform vec3 light_position_viewspace = vec3(100,100,100); 
uniform vec3 light_intensity = vec3(1000, 1000, 1000);

uniform uint frame_number = 1;


layout(binding = 10) uniform sampler2DShadow shadowMapTex;



layout(location = 0) out vec4 fragmentColor;


const float M_PI = 3.14159265358979;


#include "rnd_sample.glslh"

#ifndef USE_STOCHASTIC_ALPHA
#define USE_STOCHASTIC_ALPHA 0
#endif

void main() 
{
	vec4 color = vec4(material_color, 1);

	if(material_has_color_texture != 0)
	{
		//color.xyz = color.xyz * texture(colorTex, v_uvs).xyz;
	}

	if(material_has_alpha_texture != 0)
	{
		color.a = texture(alphaTex, v_uvs).a;
	}

#if USE_STOCHASTIC_ALPHA
	if (hashed_randf(v_model_space_position, float(frame_number)) > color.a)
	{
		discard;
	}
	color.a = 1.0;
#endif

	vec3 V = normalize(vec3(0) - v_view_space_position);
	vec3 L = normalize(light_position_viewspace - v_view_space_position);
	vec3 N = normalize(v_view_space_normal);
	vec3 H = normalize(L + V);
	float dotVH = clamp(dot(V, H), 0, 1);
	float dotLH = clamp(dot(L, H), 0, 1);
	float dotNH = clamp(dot(N, H), 0, 1);
	float dotNL = clamp(dot(N, L), 0, 1);
	float dotNV = clamp(dot(N, V), 0, 1);

	float cosTheta = clamp(dot(L, N), 0, 1);

	vec3 cdiff = mix(color.rgb * (1-0.04), vec3(0, 0, 0), material_metallic);

	vec3 F0 = mix(vec3(0.04), color.xyz, material_metallic);

	float roughness = material_roughness + 0.01;
	float alpha = roughness * roughness;

	vec3 F = F0 + (1-F0) * pow(1 - dotVH, 5);

	float r = roughness + 1;
	//float k = roughness * sqrt(2 / M_PI);
	float k = r * r / 8.0;
	//float G = (dotLH * dotNH) / ((dotLH * (1-k) + k) * (dotNH * (1-k) + k));
	float G = (dotNV * dotNL) / ((dotNV * (1-k) + k) * (dotNL * (1-k) + k));

	float den = dotNH * dotNH * (alpha * alpha - 1) + 1;
	float D = alpha * alpha / (M_PI * den * den);

	// (1-F)*Diffuse contribution + F*specular contribution
	//color.xyz = (1-F) * cdiff / M_PI + F * G * D / max(4 * dotNL * dotNV, 0.0001);
	vec3 specular = F * G * D / max(4 * dotNL * dotNV, 0.0001);
	vec3 diffuse = (vec3(1)-F) * (1-material_metallic) * color.xyz / M_PI;
	vec3 ambient = vec3(0.2);
	specular = vec3(0);

	vec3 LP = (light_position_viewspace - v_view_space_position);
	float dL2 = dot(LP, LP);
	vec3 radiance = light_intensity / dL2;

	fragmentColor.w = color.w;
	fragmentColor.xyz = (diffuse + specular) * dotNL * radiance + ambient * color.xyz;
	//fragmentColor.xyzw = vec4(0, 0, 0, 1);
}

