#include "scene.h"

#include <glm/gtx/transform.hpp>

#include <utils/gl_helpers.h>
#include <utils/log.h>

void SceneObject::render( uint32_t shaderProgram ) const
{
	if ( model_matrix_dirty )
	{
		LOG_WARN( "Model matrix has not been updated!" );
	}

	gl_helpers::setUniformSlow(shaderProgram, "modelMatrix", model_matrix);

	_render( shaderProgram );
}

void SceneObject::update(float dT)
{
	updateModelMatrix();
	_update(dT);
}

bool SceneObject::hasAlpha() const
{
	return false;
}

void SceneObject::updateModelMatrix()
{
	if ( !model_matrix_dirty )
	{
		return;
	}
	model_matrix = glm::translate( position ) * glm::mat4_cast( orientation ) * glm::scale( scale );
	model_matrix_dirty = false;
}
