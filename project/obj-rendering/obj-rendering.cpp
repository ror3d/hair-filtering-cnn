#include <GL/glew.h>
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <chrono>

#include <utils/gl_helpers.h>
#include <utils/math_helpers.h>
#include <imgui/imgui.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
using namespace glm;

#include <camera.h>
#include <light.h>
#include "utils/fbo.h"

#include <scene.h>
#include <hair_model.h>
#include <module.h>
#include <light.h>

#include <fstream>

#include <utils/log.h>
#include <utils/settings.h>

#include <obj_model.h>

#include <array>

using std::min;
using std::max;


class ObjRenderer : public Module
{
	int windowWidth, windowHeight;

	uint32_t frame_number = 0;
	bool update_frame_number = false;

	///////////////////////////////////////////////////////////////////////////////
	// Shader programs
	///////////////////////////////////////////////////////////////////////////////
	GLuint shaderProgram;       // Shader for rendering the final image
	GLuint shadowMapShaderProgram;
	GLuint fxShaderProgram;
	GLuint blendShaderProgram = 0;

	///////////////////////////////////////////////////////////////////////////////
	// Shadowmap
	///////////////////////////////////////////////////////////////////////////////

	Framebuffer shadowMapFB;
	int32_t shadow_resolution = 2048;

	float polygonOffset_factor = 2.f;
	float polygonOffset_units = 10.f;


	///////////////////////////////////////////////////////////////////////////////
	// FBO
	///////////////////////////////////////////////////////////////////////////////

	Framebuffer mainFB;
	Framebuffer blendedFB;
	Framebuffer blendedOldFB;

	///////////////////////////////////////////////////////////////////////////////
	// Models
	///////////////////////////////////////////////////////////////////////////////
	ObjModel model;

public:
	virtual const char* getModuleName() override
	{
		return "Obj Model Renderer";
	}

	void loadShaders(bool is_reload)
	{
		GLuint shader = gl_helpers::loadShaderProgram("model-shading.vert", "model-shading.frag", is_reload);
		if(shader != 0)
		{
			shaderProgram = shader;
		}

		shader = gl_helpers::loadShaderProgram("fx.vert", "fx.frag", is_reload);
		if(shader != 0)
		{
			fxShaderProgram = shader;
		}

		gl_helpers::tryLoadShaderProgram( &blendShaderProgram, "fx.vert", "obj-rendering/blend.frag", is_reload );
	}

	virtual void reloadShaders() override
	{
		loadShaders( true );
	}

	virtual bool setup( uint32_t width, uint32_t height ) override
	{
		loadShaders( false );

		windowWidth = width;
		windowHeight = height;

		mainFB.init( 1, true );
		mainFB.resize( width, height );

		blendedFB.init( 1 );
		blendedFB.resize( width, height );

		blendedOldFB.init( 1 );
		blendedOldFB.resize( width, height );

		///////////////////////////////////////////////////////////////////////
		// Setup Framebuffer for shadow map rendering
		///////////////////////////////////////////////////////////////////////
		shadowMapFB.init( 1, true );
		shadowMapFB.resize( shadow_resolution, shadow_resolution );

		glBindTexture( GL_TEXTURE_2D, shadowMapFB.depthBuffer );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE );

		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER );
		vec4 border( 1.f );
		glTexParameterfv( GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &border.x );

		glBindTexture( GL_TEXTURE_2D, 0 );

		glEnable(GL_DEPTH_TEST); // enable Z-buffering
		glDisable(GL_CULL_FACE);  // enables backface culling

		if ( !model.isLoaded() )
		{
			model.loadFromFile("models/head-black.obj");
		}
		model.setScale( glm::vec3( 200, -200, 200 ) );
		glm::quat ori = glm::angleAxis( glm::pi<float>()*0.5f, glm::vec3( 0, 0, 1 ) );
		model.setOrientation( ori );

		return true;
	}

	virtual void teardown() override
	{
		gl_helpers::unloadShaderProgram( shaderProgram );
		gl_helpers::unloadShaderProgram( shadowMapShaderProgram );
		gl_helpers::unloadShaderProgram( fxShaderProgram );

		mainFB.free();
		blendedFB.free();
		blendedOldFB.free();
		shadowMapFB.free();
	}

	virtual void update( Scene* scene, float deltaTime ) override
	{
		ImGui::Checkbox( "Update frame number", &update_frame_number );
		if ( update_frame_number )
		{
			frame_number += 1;
		}
		else
		{
			frame_number = 1;
		}

		if ( ImGui::Button( "Reset frame number" ) )
		{
			frame_number = 1;
		}

		model.update( deltaTime );
	}

	virtual void windowSizeChanged( uint32_t width, uint32_t height ) override
	{
		windowWidth = width;
		windowHeight = height;

		mainFB.resize( width, height );
		blendedFB.resize( width, height );
		blendedOldFB.resize( width, height );
	}

	virtual void render( const Scene* scene, Framebuffer& target_framebuffer ) override
	{
		if ( shadowMapFB.width != shadow_resolution || shadowMapFB.height != shadow_resolution )
		{
			shadowMapFB.resize( shadow_resolution, shadow_resolution );
		}

		///////////////////////////////////////////////////////////////////////////
		// setup matrices
		///////////////////////////////////////////////////////////////////////////
		/*
		mat4 projMatrix = scene->active_camera->as<Camera>()->getProjMatrix();
		mat4 viewMatrix = scene->active_camera->as<Camera>()->getViewMatrix();

		mat4 lightProjMatrix = scene->lights[0]->as<Light>()->proj_mat;
		mat4 lightViewMatrix = scene->lights[0]->as<Light>()->view_mat;

		///////////////////////////////////////////////////////////////////////////
		// Draw from light
		///////////////////////////////////////////////////////////////////////////
		glBindFramebuffer( GL_FRAMEBUFFER, shadowMapFB.framebufferId );
		glViewport( 0, 0, shadowMapFB.width, shadowMapFB.height );
		glClearColor( 1.f, 1.f, 1.f, 1.f );
		glClearDepth( 1.f );
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(shadowMapShaderProgram);

		gl_helpers::setUniformSlow(shadowMapShaderProgram, "frame_number", frame_number);

		gl_helpers::setUniformSlow(shadowMapShaderProgram, "viewProjectionMatrix", lightProjMatrix * lightViewMatrix);

		ImGui::SliderFloat( "Factor", &polygonOffset_factor, 0.0f, 10.0f, "%.3f", ImGuiSliderFlags_Logarithmic );
		ImGui::SliderFloat( "Units", &polygonOffset_units, 0.0f, 100.0f, "%.3f", ImGuiSliderFlags_Logarithmic );

		gl_helpers::setUniformSlow(shadowMapShaderProgram, "line_offset_factor", polygonOffset_factor);
		gl_helpers::setUniformSlow(shadowMapShaderProgram, "line_offset_units", polygonOffset_units);

		gl_helpers::setUniformSlow(shadowMapShaderProgram, "hair_transparency", hair_transparency);

		scene->root_object->as<HairModel>()->render( shadowMapShaderProgram );

		glUseProgram(0);
		*/
		mat4 viewMatrix = scene->active_camera->as<Camera>()->getViewMatrix();

	
		///////////////////////////////////////////////////////////////////////////
		// Draw from camera
		///////////////////////////////////////////////////////////////////////////

		{
			mainFB.bind();
			mainFB.clear();

			glUseProgram( shaderProgram );

			/*
			glActiveTexture( GL_TEXTURE10 );
			glBindTexture( GL_TEXTURE_2D, shadowMapFB.depthBuffer );

			mat4 lightMatrix = translate( vec3( 0.5f ) ) * scale( vec3( 0.5f ) ) * lightProjMatrix * lightViewMatrix * inverse( viewMatrix );

			gl_helpers::setUniformSlow(shaderProgram, "light_position_viewspace", vec3(viewMatrix * vec4(scene->lights[0]->getPosition(), 1.0f)));
			gl_helpers::setUniformSlow(shaderProgram, "frame_number", frame_number);
			gl_helpers::setUniformSlow(shaderProgram, "lightMatrix", lightMatrix);

			bool exporting = chag::Settings::getGlobalInstance().get( "exporting_data", false );
			gl_helpers::setUniformSlow(shaderProgram, "user_display_mode", (exporting ? 0u : 1u));
			gl_helpers::setUniformSlow(shaderProgram, "generate_train_data_mode", (exporting ? 1u : 0u));

			gl_helpers::setUniformSlow(shaderProgram, "hair_transparency", hair_transparency);
			*/

			scene->active_camera->render( shaderProgram );

			gl_helpers::setUniformSlow(shaderProgram, "light_position_viewspace", vec3(viewMatrix * vec4(scene->lights[0]->getPosition(), 1.0f)));
			gl_helpers::setUniformSlow( shaderProgram,
			                            "light_intensity",
			                            vec3( scene->lights[0]->as<Light>()->color ) * scene->lights[0]->as<Light>()->intensity );
			gl_helpers::setUniformSlow( shaderProgram, "frame_number", frame_number );
			gl_helpers::setUniformSlow( shaderProgram, "light_position_viewspace", vec3( viewMatrix * vec4( scene->lights[0]->getPosition(), 1.0f ) ) );

			//glEnable( GL_BLEND );
			//glBlendFuncSeparate( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_CONSTANT_ALPHA );
			//glBlendFunc( GL_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA );
			//glBlendColor( 1, 1, 1, 1.0 / float( frame_number ) );

			model.render( shaderProgram );

			//glDisable( GL_BLEND );

			glUseProgram( 0 );


			glActiveTexture( GL_TEXTURE10 );
			glBindTexture( GL_TEXTURE_2D, 0 );
			glUseProgram( 0 );
		}

		if (frame_number < 300 )
		{
			if ( frame_number == 1 )
			{
				blendedOldFB.clear();
			}

			blendedFB.bind();
			blendedFB.clear();

			glUseProgram( blendShaderProgram );

			gl_helpers::setUniformSlow( blendShaderProgram, "blend", 1.f / float( frame_number ) );

			glBindTextureUnit( 0, mainFB.colorTextureTargets[0] );

			glBindTextureUnit( 1, blendedOldFB.colorTextureTargets[0] );

			gl_helpers::drawFullScreenQuad();

			glUseProgram( 0 );

			blendedFB.blit( blendedOldFB );
		}


		{
			target_framebuffer.bind();
			glUseProgram( fxShaderProgram );

			glActiveTexture( GL_TEXTURE0 );

			glBindTexture( GL_TEXTURE_2D, blendedFB.colorTextureTargets[0] );

			glEnable( GL_BLEND );
			glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

			gl_helpers::drawFullScreenQuad();

			glDisable( GL_BLEND );


			glActiveTexture( GL_TEXTURE0 );
			glBindTexture( GL_TEXTURE_2D, 0 );

			glUseProgram( 0 );
		}
	}

	void saveFrameToFile( const std::filesystem::path& base_file_name, Framebuffer& target_framebuffer ) override
	{
	}
};

std::shared_ptr<Module> makeObjRender()
{
	return std::make_shared<ObjRenderer>();
}


