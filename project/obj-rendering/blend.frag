#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

in vec2 v_position;

uniform float blend = 0.0;

layout(binding = 0) uniform sampler2D tex0;
layout(binding = 1) uniform sampler2D tex1;

layout(location = 0) out vec4 output0;


void main() 
{
	vec2 uv = (v_position + 1)/2;

	vec4 col0 = texture(tex0, uv);
	vec4 col1 = texture(tex1, uv);

	output0 = col0 * blend + col1 * (1-blend);
}
