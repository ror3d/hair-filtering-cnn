#include <GL/glew.h>
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <chrono>

#include <utils/gl_helpers.h>
#include <utils/math_helpers.h>
#include <imgui/imgui.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
using namespace glm;

#include <camera.h>
#include <light.h>
#include "utils/fbo.h"
#include "utils/settings.h"

#include <scene.h>
#include <hair_model.h>
#include <module.h>

#include <fstream>

#include <utils/log.h>
#include <utils/perf.h>

#include <stb/stb_image_write.h>

#include <array>

using std::min;
using std::max;

#define ALL_MSAA_INPUT 1
#define USE_STOCHASTIC_TRANSPARENCY 1
#define MULTISAMPLES_PER_PIXEL 4

extern uint32_t frame_number;

class MSAAStochasticAlphaRenderer : public Module
{
	int windowWidth, windowHeight;

	///////////////////////////////////////////////////////////////////////////////
	// Shader programs
	///////////////////////////////////////////////////////////////////////////////
	GLuint msShaderProgram;       // Shader for rendering the final image
	GLuint ssShaderProgram;       // Shader for rendering the final image
	GLuint shadowMapShaderProgram;
	GLuint fxShaderProgram;
	GLuint msaaCopySampleProgram;
	GLuint objShaderProgram;
	GLuint objShadowShaderProgram;
	GLuint envShaderProgram;
	GLuint currentMainShader; // Used to keep track of wether we are rendering on multisample or single sample

	///////////////////////////////////////////////////////////////////////////////
	// Shadowmap
	///////////////////////////////////////////////////////////////////////////////

	Framebuffer shadowMapFB;


	///////////////////////////////////////////////////////////////////////////////
	// FBO
	///////////////////////////////////////////////////////////////////////////////

	Framebuffer mainFB;
	Framebuffer resolvedFB;

	///////////////////////////////////////////////////////////////////////////////
	// Models
	///////////////////////////////////////////////////////////////////////////////
	float hair_thickness_world_space = 0.00007; // 70um

	float line_width = 1.0;

	bool blending_enabled = true;

public:
	virtual const char* getModuleName() override
	{
		return "MSAA Stochastic Alpha";
	}

	void loadShaders(bool is_reload)
	{
#if ALL_MSAA_INPUT
		gl_helpers::setCompileDefinition( "RENDER_EXTRA_TRAINING_DATA", "1" );
#endif

#if !USE_STOCHASTIC_TRANSPARENCY
		gl_helpers::setCompileDefinition( "USE_STOCHASTIC_TRANSPARENCY", "0" );
		gl_helpers::setCompileDefinition( "WIDTH_MULT", "0.25" );
#endif

#if USE_QUADS_FOR_LINES
		gl_helpers::tryLoadShaderProgram( &msShaderProgram, "stoch-alpha-rendering/shading.vert", "hair_geometry_shader.geom", "msaa-sa-rendering/msaa-shading.frag", is_reload);
#else
		gl_helpers::tryLoadShaderProgram( &msShaderProgram, "stoch-alpha-rendering/shading.vert", "msaa-sa-rendering/msaa-shading.frag", is_reload );
#endif

		gl_helpers::tryLoadShaderProgram( &ssShaderProgram, "stoch-alpha-rendering/shading.vert", "stoch-alpha-rendering/shading.frag", is_reload);

#if USE_QUADS_FOR_LINES
		gl_helpers::tryLoadShaderProgram( &shadowMapShaderProgram, "stoch-alpha-rendering/shadowmap.vert", "hair_shadow_geometry_shader.geom", "stoch-alpha-rendering/shadowmap.frag", is_reload);
#else 
		gl_helpers::tryLoadShaderProgram( &shadowMapShaderProgram, "stoch-alpha-rendering/shadowmap.vert", "stoch-alpha-rendering/shadowmap.frag", is_reload);
#endif

		gl_helpers::tryLoadShaderProgram( &objShaderProgram, "model-shading.vert", "model-shading.frag", is_reload );
		gl_helpers::tryLoadShaderProgram( &objShadowShaderProgram, "model-shadow.vert", "model-shadow.frag", is_reload );

		gl_helpers::tryLoadShaderProgram( &msaaCopySampleProgram, "fx.vert", "msaa-sa-rendering/msaa-copy-single-sample.frag", is_reload);

		gl_helpers::tryLoadShaderProgram( &envShaderProgram, "fx.vert", "environment.frag", is_reload);

		gl_helpers::tryLoadShaderProgram( &fxShaderProgram, "fx.vert", "fx.frag", is_reload);
	}

	virtual void reloadShaders() override
	{
		loadShaders( true );
	}

	virtual bool setup( uint32_t width, uint32_t height ) override
	{
		glEnable( GL_MULTISAMPLE );
		loadShaders( false );

		windowWidth = width;
		windowHeight = height;

#if ALL_MSAA_INPUT
		mainFB.init( 2, true );
#else
		mainFB.init( 1, true );
#endif
		mainFB.setMultisample( MULTISAMPLES_PER_PIXEL );
		mainFB.resize( width, height );

		resolvedFB.init( 1, true );
		resolvedFB.resize( width, height );

		///////////////////////////////////////////////////////////////////////
		// Setup Framebuffer for shadow map rendering
		///////////////////////////////////////////////////////////////////////

		shadowMapFB.init( 0, true );
		shadowMapFB.resize( 1024, 1024 );

		glBindTexture( GL_TEXTURE_2D, shadowMapFB.depthBuffer );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE );

		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER );
		vec4 border( 1.f );
		glTexParameterfv( GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &border.x );

		glBindTexture( GL_TEXTURE_2D, 0 );

		glEnable(GL_DEPTH_TEST); // enable Z-buffering
		glDisable(GL_CULL_FACE);  // enables backface culling

		return true;
	}

	virtual void teardown() override
	{
		gl_helpers::unloadShaderProgram( msShaderProgram );
		gl_helpers::unloadShaderProgram( ssShaderProgram );
		gl_helpers::unloadShaderProgram( shadowMapShaderProgram );
		gl_helpers::unloadShaderProgram( fxShaderProgram );
		gl_helpers::unloadShaderProgram( msaaCopySampleProgram );
		gl_helpers::unloadShaderProgram( objShaderProgram );
		gl_helpers::unloadShaderProgram( objShadowShaderProgram );
		gl_helpers::unloadShaderProgram( envShaderProgram );

		mainFB.free();
		resolvedFB.free();
		shadowMapFB.free();
	}

	virtual void windowSizeChanged( uint32_t width, uint32_t height ) override
	{
		windowWidth = width;
		windowHeight = height;

		mainFB.resize( width, height );
		resolvedFB.resize( width, height );
	}

	virtual void update( Scene* scene, float deltaTime ) override
	{
		bool ms = glIsEnabled( GL_MULTISAMPLE );
		bool ms_old = ms;
		ImGui::Checkbox( "Multisample", &ms );
		if ( ms != ms_old )
		{
			if ( ms )
			{
				glEnable( GL_MULTISAMPLE );
			}
			else
			{
				glDisable( GL_MULTISAMPLE );
			}
		}
		if ( ms )
		{
			currentMainShader = msShaderProgram;
		}
		else
		{
			currentMainShader = ssShaderProgram;
		}

		ImGui::Checkbox( "Blend", &blending_enabled );

		ImGui::DragFloat( "Hair Thickness", &hair_thickness_world_space, 0.001, 0, 10, "%.5f", ImGuiSliderFlags_Logarithmic );

		ImGui::DragFloat( "Line Width", &line_width, 0.5, 0.01, 20, "%.3f", ImGuiSliderFlags_Logarithmic );
	}

	virtual void render(const Scene* scene, Framebuffer& target_framebuffer) override
	{
		if (shadowMapFB.width != scene->shadowmap.size || shadowMapFB.height != scene->shadowmap.size)
		{
			shadowMapFB.resize( scene->shadowmap.size, scene->shadowmap.size );
		}

		glLineWidth( 1.f );

		///////////////////////////////////////////////////////////////////////////
		// setup matrices
		///////////////////////////////////////////////////////////////////////////
		mat4 projMatrix = scene->active_camera->as<Camera>()->getProjMatrix();
		mat4 viewMatrix = scene->active_camera->as<Camera>()->getViewMatrix();

		mat4 lightProjMatrix = scene->lights[0]->as<Light>()->proj_mat;
		mat4 lightViewMatrix = scene->lights[0]->as<Light>()->view_mat;
		mat4 lightViewProjMatrix = lightProjMatrix * lightViewMatrix;

		///////////////////////////////////////////////////////////////////////////
		// Draw from light
		///////////////////////////////////////////////////////////////////////////
		{ 
			chag::perf::Scope s("Shadow Map");
			shadowMapFB.bind();
			shadowMapFB.clear_depth();

			if ( chag::Settings::getGlobalInstance().get( "display_head_model", false ) )
			{
				glUseProgram( objShadowShaderProgram );
				gl_helpers::setUniformSlow( objShadowShaderProgram, "viewProjectionMatrix", lightViewProjMatrix );
				for ( auto& obj : scene->objects )
				{
					if ( !obj->hasAlpha() )
					{
						obj->render( objShadowShaderProgram );
					}
				}
			}


			glUseProgram(shadowMapShaderProgram);

			gl_helpers::setUniformSlow(shadowMapShaderProgram, "frame_number", frame_number);

			gl_helpers::setUniformSlow( shadowMapShaderProgram, "line_width", line_width );

			gl_helpers::setUniformSlow(shadowMapShaderProgram, "viewProjectionMatrix", lightViewProjMatrix);
			gl_helpers::setUniformSlow(shadowMapShaderProgram, "projectionMatrix", lightProjMatrix);
			gl_helpers::setUniformSlow(shadowMapShaderProgram, "invViewProjectionMatrix", inverse(lightViewProjMatrix));
			gl_helpers::setUniformSlow(shadowMapShaderProgram, "invProjectionMatrix", inverse(lightProjMatrix));

			gl_helpers::setUniformSlow(shadowMapShaderProgram, "line_offset_factor", scene->shadowmap.polygon_offset_factor);
			gl_helpers::setUniformSlow(shadowMapShaderProgram, "line_offset_units", scene->shadowmap.polygon_offset_units);

			gl_helpers::setUniformSlow(shadowMapShaderProgram, "hair_thickness_world_space", hair_thickness_world_space);

			gl_helpers::setUniformSlow( shadowMapShaderProgram, "screen_size", glm::vec2( shadowMapFB.width, shadowMapFB.height ) );

			{ 
				chag::perf::Scope s("Render");
				for ( auto& obj : scene->objects )
				{
					if ( obj->as<HairModel>() )
					{
						obj->render( shadowMapShaderProgram );
					}
				}
			}

			// To DEBUG output shadowmap
			/*
			std::vector<float> fdata(shadowMapFB.width * shadowMapFB.height);
			std::vector<uint8_t> u8data(shadowMapFB.width * shadowMapFB.height);
			glReadPixels(0, 0, shadowMapFB.width, shadowMapFB.height, GL_DEPTH_COMPONENT, GL_FLOAT, fdata.data());
			for (int i = 0; i < fdata.size(); i++) u8data[i] = fdata[i] * 255.0f; 
			stbi_write_png("depth.png", shadowMapFB.width, shadowMapFB.height, 1, u8data.data(), 0);
			*/


			glUseProgram(0);
		}

		///////////////////////////////////////////////////////////////////////////
		// Draw from camera
		///////////////////////////////////////////////////////////////////////////

		{
			chag::perf::Scope s( "Camera View" );
			mainFB.bind();
			mainFB.clear( { 0, 0, 0, 0 } );


			if ( scene->environment.texture && chag::Settings::getGlobalInstance().get( "display_envmap", false ) )
			{
				mat4 projInvMatrix = glm::inverse( projMatrix );
				mat4 viewInvMatrix = glm::inverse( viewMatrix );

				glUseProgram( envShaderProgram );
				glActiveTexture( GL_TEXTURE0 );
				glBindTexture( GL_TEXTURE_2D, scene->environment.texture );
				glDepthMask( false );
				gl_helpers::setUniformSlow( envShaderProgram, "ViewInverse", viewInvMatrix );
				gl_helpers::setUniformSlow( envShaderProgram, "ProjInverse", projInvMatrix );
				gl_helpers::setUniformSlow( envShaderProgram, "EnvRot", scene->environment.rotate );
				gl_helpers::drawFullScreenQuad();
				glDepthMask( true );
			}


			vec3 light_position_viewspace = vec3( viewMatrix * vec4( scene->lights[0]->getPosition(), 1.0f ) );
			float light_intensity = scene->lights[0]->as<Light>()->intensity;

			if ( chag::Settings::getGlobalInstance().get( "display_head_model", false ) )
			{ 
				glUseProgram( objShaderProgram );
				scene->active_camera->render( objShaderProgram );

				gl_helpers::setUniformSlow( objShaderProgram, "light_position_viewspace", light_position_viewspace);
				gl_helpers::setUniformSlow( objShaderProgram, "light_intensity", vec3( light_intensity ) ); // scene->lights[0]->as<Light>()->color ) * 
				gl_helpers::setUniformSlow( objShaderProgram, "frame_number", frame_number );

				for ( auto& obj : scene->objects )
				{
					if ( !obj->hasAlpha() )
					{
						obj->render( objShaderProgram );
					}
				}
			}

			glUseProgram( currentMainShader );

			glActiveTexture( GL_TEXTURE10 );
			glBindTexture( GL_TEXTURE_2D, shadowMapFB.depthBuffer );

			mat4 lightMatrix = translate( vec3( 0.5f ) ) * scale( vec3( 0.5f ) ) * lightProjMatrix * lightViewMatrix * inverse( viewMatrix );

			gl_helpers::setUniformSlow( currentMainShader, "frame_number", frame_number );

			gl_helpers::setUniformSlow( currentMainShader, "line_width", line_width );

			gl_helpers::setUniformSlow( currentMainShader, "light_position_viewspace", light_position_viewspace );
			gl_helpers::setUniformSlow( currentMainShader, "lightMatrix", lightMatrix );
			gl_helpers::setUniformSlow( currentMainShader, "light_intensity", light_intensity );


			bool exporting = chag::Settings::getGlobalInstance().get( "exporting_data", false );
			gl_helpers::setUniformSlow( currentMainShader, "user_display_mode", (exporting ? 0u : 1u) );
			gl_helpers::setUniformSlow( currentMainShader, "generate_train_data_mode", (exporting ? 1u : 0u) );

			gl_helpers::setUniformSlow( currentMainShader, "hair_thickness_world_space", hair_thickness_world_space );

			gl_helpers::setUniformSlow( currentMainShader, "screen_size", glm::vec2( mainFB.width, mainFB.height ) );

			scene->active_camera->render( currentMainShader );

			{
				chag::perf::Scope s( "Render" );
				for ( auto& obj : scene->objects )
				{
					if ( obj->as<HairModel>() )
					{
						obj->render( currentMainShader );
					}
				}
			}

			glUseProgram( 0 );

			glActiveTexture( GL_TEXTURE10 );
			glBindTexture( GL_TEXTURE_2D, 0 );
			glUseProgram( 0 );

			{
				chag::perf::Scope s( "Blit" );
				mainFB.blit( resolvedFB );
			}
		}

		{
			chag::perf::Scope s( "Copy to target" );
			target_framebuffer.bind();

			glUseProgram( fxShaderProgram );

			glActiveTexture( GL_TEXTURE0 );
			glBindTexture( GL_TEXTURE_2D, resolvedFB.colorTextureTargets[0] );

			if ( blending_enabled )
			{
				glEnable( GL_BLEND );
				glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
			}
			else
			{
				glDisable( GL_BLEND );
			}

			gl_helpers::drawFullScreenQuad();

			if ( blending_enabled )
			{
				glDisable( GL_BLEND );
			}

			glBindTexture( GL_TEXTURE_2D, 0 );


			glUseProgram( 0 );
		}
	}

	void saveFrameToFile( const std::filesystem::path& base_file_name, Framebuffer& target_framebuffer ) override
	{
		bool exporting = chag::Settings::getGlobalInstance().get( "exporting_data", false );

		if (exporting )
		{
			std::filesystem::path base_name = base_file_name.stem();

#if ALL_MSAA_INPUT
		target_framebuffer.bind();
		glDisable( GL_BLEND );

		for ( int32_t i = 0; i < mainFB.multisampleSamples; ++i )
		{
			glUseProgram( msaaCopySampleProgram );
			glActiveTexture( GL_TEXTURE0 );
			gl_helpers::setUniformSlow( msaaCopySampleProgram, "sample_idx", i );
			glBindTexture( GL_TEXTURE_2D_MULTISAMPLE, mainFB.colorTextureTargets[0] );
			gl_helpers::drawFullScreenQuad();

			std::filesystem::path fname = base_file_name;
			fname.replace_filename( fmt::format( "{}_{}{}", base_name.string(), i, base_file_name.extension().string() ) );
			target_framebuffer.saveToFile( fname );

			glBindTexture( GL_TEXTURE_2D_MULTISAMPLE, mainFB.colorTextureTargets[1] );
			gl_helpers::drawFullScreenQuad();

			fname = base_file_name;
			fname.replace_filename( fmt::format( "{}_tan_{}{}", base_name.string(), i, base_file_name.extension().string() ) );
			target_framebuffer.saveToFile( fname );
		}
#else
			// Save shaded color
			std::filesystem::path ms_file_name = base_file_name;
			ms_file_name.replace_filename( base_name.string() + "_ms" + base_file_name.extension().string() );

			resolvedFB.saveToFile( ms_file_name );
#endif
		}
		else
		{
			resolvedFB.saveToFile( base_file_name );
		}

	}
};

std::shared_ptr<Module> makeMSAAStochasticAlphaRender()
{
	return std::make_shared<MSAAStochasticAlphaRenderer>();
}


