#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

layout(binding = 0) uniform sampler2DMS tex;

layout(location = 0) out vec4 out_color;

uniform int sample_idx = 0;

void main() 
{
	vec4 c = texelFetch(tex, ivec2(gl_FragCoord.xy), sample_idx);
	out_color = c;
}

