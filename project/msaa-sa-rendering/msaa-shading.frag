#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

uniform vec2 screen_size;

uniform mat4 lightMatrix;

uniform uint frame_number = 1;

uniform vec3 light_position_viewspace = vec3(1,1,1); 
uniform float light_intensity = 1000;

uniform vec3 ambient_w = vec3(0.2, 0.2, 0.2);

uniform uint should_override_color = 0;
uniform vec3 override_color = vec3(0, 0, 0);

uniform uint user_display_mode = 1;
uniform uint generate_train_data_mode = 0;
uniform uint eval_mode = 0;

uniform float line_width = 1.0;

layout(binding = 10) uniform sampler2DShadow shadowMapTex;

const float M_PI = 3.14159265358979;

#if !USE_QUADS_FOR_LINES // If not using geometry shader
in vec4 v_view_space_position;
in vec3 v_color;
in float v_transparency;
in vec3 v_tangent; 
in vec3 v_model_space_position; 
in float v_hair_thickness;
vec4	view_space_position		= v_view_space_position;
vec3	color					= v_color;
float	transparency			= v_transparency;
vec3	tangent					= v_tangent; 
vec3	model_space_position	= v_model_space_position; 
float	hair_thickness			= v_hair_thickness;
float   uv_u = 0;
#else 
in vec4 g_view_space_position;
in vec3 g_color;
in float g_transparency;
in vec3 g_tangent; 
in vec3 g_model_space_position; 
in float g_hair_thickness;
in float g_u;
vec4	view_space_position		= g_view_space_position;
vec3	color					= g_color;
float	transparency			= g_transparency;
vec3	tangent					= g_tangent; 
vec3	model_space_position	= g_model_space_position; 
float	hair_thickness			= g_hair_thickness;
float   uv_u = g_u;
#endif

layout(location = 0) out vec4 out_shadedColor;

#if RENDER_EXTRA_TRAINING_DATA == 1
layout(location = 1) out vec4 out_tangent;
#endif


#include "../rnd_sample.glslh"
#include "../hair_shading.glslh"

#ifndef USE_SINE
#define USE_SINE 0
#endif

#ifndef USE_STOCHASTIC_TRANSPARENCY
	#define USE_STOCHASTIC_TRANSPARENCY 1
#endif

#ifndef CHECK_AGAINST_DEPTH_TEXTURE
#define CHECK_AGAINST_DEPTH_TEXTURE 0
#endif

#ifndef USE_HASHED_ALPHA
#define USE_HASHED_ALPHA 1
#endif

#if CHECK_AGAINST_DEPTH_TEXTURE
layout(binding = 16) uniform sampler2DShadow sceneDepthTex;
#endif

void main() 
{
#if CHECK_AGAINST_DEPTH_TEXTURE
	float texel_visibility = texture(sceneDepthTex, vec3(gl_FragCoord.xy / screen_size, gl_FragCoord.z));

	if (texel_visibility == 0)
	{
		discard;
	}
#endif

#if USE_STOCHASTIC_TRANSPARENCY
	float ss_hair_thickness = length(screen_size) * hair_thickness * gl_FragCoord.w / 2;

	ss_hair_thickness = 0.25;
#if USE_SINE
	// TODO: use only the position at the center of the hair, add extra randomnes from the sent in uv
	ss_hair_thickness *= min(max(cos((uv_u * (line_width - 1) * 2 + 0.5) * M_PI) + 0.6, 0)*2, 1);
#endif

#if USE_HASHED_ALPHA
	gl_SampleMask[0] = -1; // Set all samples to 1
	vec3 coords3d = model_space_position; 
	const float HashScale = 1.0;
	float maxDeriv = max(length(dFdx(coords3d.xyz)), length(dFdy(coords3d.xyz)));
	float pixScale = 1.0/(maxDeriv * HashScale);
	vec2 pixScales = vec2( exp2(floor(log2(pixScale))), exp2(ceil(log2(pixScale))));
	for (uint i = 0; i < gl_NumSamples; ++i)
	{
		float time = float(frame_number * (i + 1));
		vec2 alpha = vec2(hash(vec4(floor(pixScales.x * coords3d.xyz), time)),
							hash(vec4(floor(pixScales.y * coords3d.xyz), time)));
		float lerpFactor = fract(log2(pixScale));
		float x = (1-lerpFactor) * alpha.x + lerpFactor * alpha.y;
		float a = min(lerpFactor, 1-lerpFactor);

		vec3 cases = vec3(x*x/(2*a*(1-a)), (x-0.5*a)/(1-a), 1.0-((1-x)*(1-x)/(2*a*(1-a))));
		float a_t = (x < (1-a)) ? ((x < a) ? cases.x : cases.y) : cases.z;
		a_t = clamp(a_t, 1.0e-6, 1.0);

		if (a_t > ss_hair_thickness * transparency)
		{
			gl_SampleMask[0] = gl_SampleMask[0] ^ (1 << i);
		}
		else if (gl_NumSamples == 1) {
			discard; 
		}
	}

#else

	gl_SampleMask[0] = -1; // Set all samples to 1
	for (uint i = 0; i < gl_NumSamples; ++i)
	{
		// Reset the samples randomly
		if (hash(vec4(model_space_position, float(frame_number * (i + 1)))) > ss_hair_thickness * transparency)
		{
			gl_SampleMask[0] = gl_SampleMask[0] ^ (1 << i);
		}
	}

#endif // USE_HASHED_ALPHA
#endif // USE_STOCHASTIC_TRANSPARENCY

	vec3 T = normalize(tangent); 

#if 0 // To test timing without shading
#if RENDER_EXTRA_TRAINING_DATA == 1
	out_tangent.xyz = T.xyz;
	out_tangent.w = 1.0;
#endif

	out_shadedColor.xy = vec2(0.5);
	out_shadedColor.z = 1.0;
	out_shadedColor.w = 0.1;
	return;
#endif


	vec4 shadowMapCoord = lightMatrix * vec4(view_space_position.xyz, 1.0);
	float light_visibility = textureProj(shadowMapTex, shadowMapCoord);

	vec3 light_d = view_space_position.xyz - light_position_viewspace.xyz;
	float light_radiance = light_visibility * light_intensity / dot(light_d, light_d);

	hair_shading hs = get_hair_shading(view_space_position.xyz, T, light_position_viewspace.xyz);

	vec3 hair_color;
	if (user_display_mode == 1)
	{
		hair_color = (should_override_color == 1 ? override_color : color);
	}
	else if(generate_train_data_mode == 1)
	{
		hair_color = vec3(0.5);
	}
	else // eval_mode
	{
		hair_color = vec3(1.0);
	}

	vec3 diffuse_contribution = light_radiance * (hs.diffuse * hair_color);
	float specular_contribution_R = light_radiance * (hs.specularR);
	vec3 specular_contribution_TRT = light_radiance * (hs.specularTRT * hair_color);

	if (user_display_mode == 1)
	{
		out_shadedColor.xyz = ambient_w * hair_color + diffuse_contribution + specular_contribution_TRT + specular_contribution_R;
		out_shadedColor.w = 1.0;
	}
	else // generate_train_data_mode || eval_mode
	{
#if RENDER_EXTRA_TRAINING_DATA == 1
		out_shadedColor.x = (ambient_w * hair_color + diffuse_contribution + specular_contribution_TRT).x;
		out_shadedColor.y = specular_contribution_R;
		out_shadedColor.z = gl_FragCoord.z;
		out_shadedColor.w = 1.0;

		if (eval_mode == 1)
		{
			// Just output tangent, we don't need to renormalise anything because the buffer is sent directly to cuda
			out_tangent.xyz = T.xyz;
			out_tangent.w = 1.0;
		}
		else
		{
			out_tangent.xyz = (T.xyz + 1.0) / 2.0;
			out_tangent.w = 1.0;
		}

		//out_shadedColor = vec4(0.0f); 
		//out_tangent = vec4(0.0f); 

#else
		out_shadedColor.xy = vec2(ambient_w * hair_color + diffuse_contribution + specular_contribution_TRT);
		out_shadedColor.z = 1.0;
		out_shadedColor.w = specular_contribution_R;
#endif
	}
}

