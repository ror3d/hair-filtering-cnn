#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

layout(binding = 0) uniform sampler2D envMap;

layout(location = 0) out vec4 fragmentColor;

uniform mat4 ViewInverse;
uniform mat4 ProjInverse;

uniform float EnvRot = 0;

#define M_PI 3.141592653

in vec2 v_position;

void main() 
{
	vec4 pos_vs = ProjInverse * vec4(v_position.xy, 1.0, 1.0);
	pos_vs /= pos_vs.w;

	vec3 dir_vs = normalize(pos_vs.xyz);
	vec4 dir = ViewInverse * vec4(dir_vs, 0);

	// Calculate the spherical coordinates of the direction
	float theta = acos(max(-1.0f, min(1.0f, dir.z)));
	float phi = atan(dir.y, dir.x);
	if(phi < 0.0f)
	{
		phi = phi + 2.0f * M_PI;
	}

	// Use these to lookup the color in the environment map
	vec2 lookup = vec2(phi / (2.0 * M_PI) + EnvRot, theta / M_PI);
	fragmentColor = texture(envMap, lookup);
}

