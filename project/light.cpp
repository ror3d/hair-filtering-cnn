#include "light.h"

#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>

using namespace glm;

Light::Light( const std::string& name )
	: SceneObject(name)
	, proj_mat(1)
	, view_mat(1)
{
}

void Light::_update(float dT)
{
	proj_mat = ortho( ortho_bounds.x, ortho_bounds.y, ortho_bounds.z, ortho_bounds.w, 0.1f, 100.f );
	view_mat = lookAt( position, position + rotate(orientation, Scene::worldFwd), Scene::worldUp );
}

