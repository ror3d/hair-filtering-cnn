#include "obj_model.h"
#include <fstream>
#include <GL/glew.h>
#include <utils/gl_helpers.h>

#include <unordered_map>
#include <map>

#include "utils/log.h"

#include <stb/stb_image.h>

#define TINYOBJLOADER_IMPLEMENTATION
#include <tinyobjloader/tiny_obj_loader.h>


static uint32_t load_texture( const std::filesystem::path& base, const std::string& file )
{
	if ( file.empty() )
	{
		return 0;
	}

	std::filesystem::path filepath = file;
	if ( filepath.is_relative() )
	{
		filepath = base;
		filepath.replace_filename( file );
	}

	int w = 0, h = 0, ch = 0;
	stbi_uc* data = stbi_load( filepath.string().c_str(), &w, &h, &ch, 0 );
	if ( data == nullptr )
	{
		LOG_ERROR( "Texture {} could not be loaded! Attempted to load from {}.", file, filepath.string() );
		return 0;
	}

	GLenum format = 0, internal_format = 0;
	if(ch == 1)
	{
		format = GL_RED;
		internal_format = GL_RED;
	}
	else if(ch == 2)
	{
		format = GL_RG;
		internal_format = GL_RG;
	}
	else if(ch == 3)
	{
		format = GL_RGB;
		internal_format = GL_RGB;
	}
	else if(ch == 4)
	{
		format = GL_RGBA;
		internal_format = GL_RGBA;
	}
	else
	{
		LOG_ERROR( "What the hell how did we get {} channels?", ch );
		stbi_image_free( data );
		return 0;
	}

	uint32_t glid = 0;

	glGenTextures(1, &glid);
	glBindTexture( GL_TEXTURE_2D, glid );

	glTexImage2D( GL_TEXTURE_2D, 0, internal_format, w, h, 0, format, GL_UNSIGNED_BYTE, data );

	stbi_image_free( data );

	glGenerateMipmap( GL_TEXTURE_2D );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16 );

	glBindTexture( GL_TEXTURE_2D, 0 );

	return glid;
}


ObjModel::~ObjModel()
{
	unload();
}


bool ObjModel::isLoaded() const
{
	return gl_vao != 0;
}

void ObjModel::unload()
{
	for ( auto& sm : submeshes )
	{
		glDeleteTextures( 1, &sm.material.color_tex );
		glDeleteTextures( 1, &sm.material.normal_tex );
		glDeleteTextures( 1, &sm.material.alpha_tex );
	}
	submeshes.clear();

	glDeleteBuffers( 1, &gl_vertex_attribs );
	gl_vertex_attribs = 0;

	glDeleteBuffers( 1, &gl_idxs );
	gl_idxs = 0;

	glDeleteVertexArrays( 1, &gl_vao );
	gl_vao = 0;
}

bool ObjModel::loadFromFile( const std::filesystem::path& filePath )
{
	if ( isLoaded() )
	{
		LOG_WARN( "There's already an object loaded!" );
		return false;
	}

	tinyobj::ObjReader reader;

	if ( !reader.ParseFromFile( filePath.string() ) )
	{
		LOG_ERROR( "Error loading model file {}: {}", filePath.string(), reader.Error() );
		return false;
	}
	if ( !reader.Warning().empty() )
	{
		LOG_WARN( "Error loading model file {}: {}", filePath.string(), reader.Warning() );
	}

	const tinyobj::attrib_t& obj_attrib = reader.GetAttrib();
	const std::vector<tinyobj::shape_t>& obj_shapes = reader.GetShapes();
	const std::vector<tinyobj::material_t>& obj_materials = reader.GetMaterials();

	std::unordered_map<std::string, uint32_t> textures;

	// Build the material information
	for ( auto& mat : obj_materials )
	{
		submesh_t smesh;

		smesh.material.color = { mat.diffuse[0], mat.diffuse[1], mat.diffuse[2] };
		smesh.material.emission = { mat.emission[0], mat.emission[1], mat.emission[2] };
		smesh.material.roughness = mat.roughness;
		smesh.material.metallic = mat.metallic;

		// Load textures
		if ( textures.find( mat.diffuse_texname ) == textures.end() )
		{
			textures[mat.diffuse_texname] = load_texture( filePath, mat.diffuse_texname );
		}
		smesh.material.color_tex = textures[mat.diffuse_texname];

		if ( textures.find( mat.normal_texname ) == textures.end() )
		{
			textures[mat.normal_texname] = load_texture( filePath, mat.normal_texname );
		}
		smesh.material.normal_tex = textures[mat.normal_texname];

		if ( textures.find( mat.alpha_texname ) == textures.end() )
		{
			textures[mat.alpha_texname] = load_texture( filePath, mat.alpha_texname );
		}
		smesh.material.alpha_tex = textures[mat.alpha_texname];

		submeshes.push_back( smesh );
	}

	std::map<std::tuple<uint32_t, uint32_t, uint32_t>, uint32_t> single_index_map;

	std::vector<std::vector<uint32_t>> indices_per_material;
	indices_per_material.resize( submeshes.size(), {} );

	std::vector<glm::vec3> v_positions;
	std::vector<glm::vec3> v_normals;
	std::vector<glm::vec2> v_uvs;

	for ( auto& shape : obj_shapes )
	{
		std::vector<uint32_t> corrected_indices;

		for ( auto& idx : shape.mesh.indices )
		{
			// We can't handle different indices for different attributes
			// so we are going to collect all the combinations of indices to recreate the vertex attributes arrays
			std::tuple<uint32_t, uint32_t, uint32_t> tup{ idx.vertex_index, idx.normal_index, idx.texcoord_index };

			uint32_t id = single_index_map.size();
			auto it = single_index_map.find( tup );
			if ( it == single_index_map.end() )
			{
				single_index_map[tup] = id;

				v_positions.push_back( { obj_attrib.vertices[3ull * idx.vertex_index + 0],
				                         obj_attrib.vertices[3ull * idx.vertex_index + 1],
				                         obj_attrib.vertices[3ull * idx.vertex_index + 2] } );

				if ( obj_attrib.normals.size() > 0 )
				{
					v_normals.push_back( { obj_attrib.normals[3ull * idx.normal_index + 0],
										   obj_attrib.normals[3ull * idx.normal_index + 1],
										   obj_attrib.normals[3ull * idx.normal_index + 2] } );
				}

				if ( obj_attrib.texcoords.size() > 0 )
				{
					v_uvs.push_back( { obj_attrib.texcoords[2ull * idx.texcoord_index + 0],
									   obj_attrib.texcoords[2ull * idx.texcoord_index + 1] } );
				}
				else
				{
					v_uvs.push_back( { 0.f, 0.f } );
				}
			}
			else
			{
				id = it->second;
			}
			corrected_indices.push_back( id );
		}

		// Check there's a material id for each 3-vertex face
		DebugAssert( shape.mesh.indices.size() == shape.mesh.material_ids.size() * 3 );

		// We just put together the triangles for each material separately.
		// I don't care about the original meshes right now
		for ( size_t i = 0; i < shape.mesh.material_ids.size(); ++i )
		{
			int mat = shape.mesh.material_ids[i];
			indices_per_material[mat].push_back( corrected_indices[3 * i + 0] );
			indices_per_material[mat].push_back( corrected_indices[3 * i + 1] );
			indices_per_material[mat].push_back( corrected_indices[3 * i + 2] );
		}
	}

	std::vector<uint32_t> indices;
	for ( size_t i = 0; i < submeshes.size(); ++i )
	{
		submeshes[i].index_start = indices.size();
		submeshes[i].index_count = indices_per_material[i].size();
		indices.insert( indices.end(), indices_per_material[i].begin(), indices_per_material[i].end() );
	}
	num_indices = indices.size();

	// Create normals if they are not present
	// We will create them by averaging the face normals around each vertex.
	// To have flat normals we would have to duplicate vertices for each face, or do it in a geom shader.
	// Since they are not provided, we don't care what they look like. If you care, provide them in the obj file.
	if ( v_normals.size() == 0 )
	{
		v_normals.resize( v_positions.size(), {} );
		for ( size_t i = 0; i < indices.size(); i += 3 )
		{
			glm::vec3 x = v_positions[indices[i]];
			glm::vec3 y = v_positions[indices[i + 1]];
			glm::vec3 z = v_positions[indices[i + 2]];
			glm::vec3 n = glm::normalize( glm::cross( y - x, z - x ) );
			v_normals[indices[i]] += n;
			v_normals[indices[i+1]] += n;
			v_normals[indices[i+2]] += n;
		}
		for ( size_t i = 0; i < v_positions.size(); ++i )
		{
			v_positions[i] = glm::normalize( v_positions[i] );
		}
	}

	num_vertices = v_positions.size();

	size_t vertex_attribs_size = 0;
	vertex_attribs_size += v_positions.size() * sizeof( glm::vec3 );
	vertex_attribs_size += v_normals.size() * sizeof( glm::vec3 );
	vertex_attribs_size += v_uvs.size() * sizeof( glm::vec2 );


	if ( !gl_vao )
	{
		glGenVertexArrays( 1, &gl_vao );
	}
	glBindVertexArray( gl_vao );


	if ( !gl_vertex_attribs )
	{
		glGenBuffers( 1, &gl_vertex_attribs );
	}
	glBindBuffer( GL_ARRAY_BUFFER, gl_vertex_attribs );
	glBufferData( GL_ARRAY_BUFFER, vertex_attribs_size * sizeof( tinyobj::real_t ), NULL, GL_STATIC_DRAW );

	// Position
	size_t vtx_att_offset = 0;
	glBufferSubData( GL_ARRAY_BUFFER, vtx_att_offset, v_positions.size() * sizeof( glm::vec3 ), v_positions.data() );
	glVertexAttribPointer( 0, 3, GL_FLOAT, false /*normalized*/, 0 /*stride*/, (void*)vtx_att_offset /*offset*/ );
	glEnableVertexAttribArray( 0 );
	vtx_att_offset += v_positions.size() * sizeof( glm::vec3 );

	// Normal
	glBufferSubData( GL_ARRAY_BUFFER, vtx_att_offset, v_normals.size() * sizeof( glm::vec3 ), v_normals.data() );
	glVertexAttribPointer( 1, 3, GL_FLOAT, false /*normalized*/, 0 /*stride*/, (void*)vtx_att_offset /*offset*/);
	glEnableVertexAttribArray( 1 );
	vtx_att_offset += v_normals.size() * sizeof( glm::vec3 );

	// UVs
	glBufferSubData( GL_ARRAY_BUFFER, vtx_att_offset, v_uvs.size() * sizeof( glm::vec2 ), v_uvs.data() );
	glVertexAttribPointer( 2, 2, GL_FLOAT, false /*normalized*/, 0 /*stride*/, (void*)vtx_att_offset /*offset*/);
	glEnableVertexAttribArray( 2 );
	vtx_att_offset += v_uvs.size() * sizeof( glm::vec2 );


	if ( !gl_idxs )
	{
		glGenBuffers( 1, &gl_idxs );
	}
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, gl_idxs );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof( uint32_t ), indices.data(), GL_STATIC_DRAW );

	glBindVertexArray( 0 );


	return true;
}

void ObjModel::_render( uint32_t shaderProgram ) const
{
	if ( !isLoaded() )
	{
		return;
	}

	glBindVertexArray( gl_vao );

	for ( size_t i = 0; i < submeshes.size(); ++i )
	{
		renderSubmesh( shaderProgram, i );
	}

	glBindVertexArray( 0 );
}

void ObjModel::renderSubmesh( uint32_t shaderProgram, size_t i ) const
{
	gl_helpers::setUniformSlow( shaderProgram, "material_color", submeshes[i].material.color );
	gl_helpers::setUniformSlow( shaderProgram, "material_emission", submeshes[i].material.emission );
	gl_helpers::setUniformSlow( shaderProgram, "material_roughness", submeshes[i].material.roughness );
	gl_helpers::setUniformSlow( shaderProgram, "material_metallic", submeshes[i].material.metallic );

	gl_helpers::setUniformSlow( shaderProgram, "material_has_color_texture", submeshes[i].material.color_tex != 0 );
	glBindTextureUnit( 0, submeshes[i].material.color_tex );

	gl_helpers::setUniformSlow( shaderProgram, "material_has_alpha_texture", submeshes[i].material.color_tex != 0 );
	glBindTextureUnit( 1, submeshes[i].material.color_tex );

	gl_helpers::setUniformSlow( shaderProgram, "material_has_normal_texture", submeshes[i].material.normal_tex != 0 );
	glBindTextureUnit( 2, submeshes[i].material.normal_tex );

	glDrawElements( GL_TRIANGLES,
					submeshes[i].index_count,
					GL_UNSIGNED_INT,
					reinterpret_cast<void*>( submeshes[i].index_start * sizeof( uint32_t ) ) );
}

