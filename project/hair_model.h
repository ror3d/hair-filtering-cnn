#pragma once

#include <filesystem>
#include <iostream>
#include <glm/glm.hpp>
#include <vector>

#define USE_QUADS_FOR_LINES 0

#include <scene.h>

class HairModel : public SceneObject
{
	static const uint32_t prim_restart_idx = ~uint32_t( 0 );

public:
	uint32_t n_def_segments_per_strand = 0;
	std::vector<uint16_t> strand_length;
	std::vector<glm::vec3> points;
	std::vector<glm::vec3> tangents;
	std::vector<float> thickness;
	float default_thickness = 1;
	std::vector<float> transparency;
	float default_transparency = 0;
	std::vector<glm::vec3> color;
	glm::vec3 default_color;
	glm::vec3 override_color;
	bool should_override_color = false;
	size_t n_strands = 0;

	size_t n_idxs = 0;
	uint32_t gl_vao = 0;
	uint32_t gl_points = 0;
	uint32_t gl_tangents = 0;
	uint32_t gl_idxs = 0;

public:
	HairModel() : SceneObject("") { }
	~HairModel();
	HairModel( HairModel&& );

	static HairModel generateRandomSubsample( const HairModel& source, float prob_to_show );

	bool loadFromFile( const std::filesystem::path& filePath );

	bool load( std::istream& fileStream );

	bool isLoaded() const;

	void setOverrideColor( glm::vec3 color );

	void shouldOverrideColor( bool overide );

	glm::vec3 getRenderingColor() const;

	bool hasAlpha() const override;

protected:

	bool moveToGPU( const std::vector<uint32_t>& idxs );

	virtual void _render( uint32_t shaderProgram ) const override;

};


