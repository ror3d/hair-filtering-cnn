#version 420

layout(location = 0) in vec3 position;
uniform mat4 viewProjectionMatrix; 
uniform mat4 modelMatrix; 

uniform int hasColorArray; 
layout(location = 1) in vec3 color;
uniform vec3 defHairColor;

uniform int hasThicknessArray; 
layout(location = 2) in float thickness;
uniform float defHairThickness;

uniform int hasTransparencyArray; 
layout(location = 3) in float transparency;
uniform float defHairTransparency;

out vec3 viewspace_coords; 
out vec3 v_model_space_position;

out float v_transparency;

void main() 
{
	gl_Position = viewProjectionMatrix * modelMatrix * vec4(position,1.0);
	v_model_space_position = position;
	v_transparency = (hasTransparencyArray != 0) ? transparency : defHairTransparency;
	viewspace_coords = vec3(viewProjectionMatrix * modelMatrix * vec4(position,1.0));
}

