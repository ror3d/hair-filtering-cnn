#version 420


// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

uniform mat4 lightMatrix;

uniform vec3 light_position_viewspace = vec3(1,1,1); 
uniform float light_intensity = 1000;
uniform vec3 ambient_w = vec3(0.2, 0.2, 0.2);

uniform uint should_override_color = 0;
uniform vec3 override_color = vec3(0, 0, 0);

uniform uint user_display_mode = 1;
uniform uint generate_train_data_mode = 0;

uniform float premul_factor = 1;

layout(binding = 10) uniform sampler2DShadow shadowMapTex;

in vec4 v_view_space_position;
in vec3 v_color;
in float v_transparency;
in vec3 v_tangent; 

layout(location = 0) out vec4 out_shadedColor;

#include "../hair_shading.glslh"

uniform int frame_number = 1; 
#include "../rnd_sample.glslh"

#define PI 3.14159265359f

float mod289(float x){return x - floor(x * (1.0 / 289.0)) * 289.0;}
vec4 mod289(vec4 x){return x - floor(x * (1.0 / 289.0)) * 289.0;}
vec4 perm(vec4 x){return mod289(((x * 34.0) + 1.0) * x);}

float noise(vec3 p){
    vec3 a = floor(p);
    vec3 d = p - a;
    d = d * d * (3.0 - 2.0 * d);

    vec4 b = a.xxyy + vec4(0.0, 1.0, 0.0, 1.0);
    vec4 k1 = perm(b.xyxy);
    vec4 k2 = perm(k1.xyxy + b.zzww);

    vec4 c = k2 + a.zzzz;
    vec4 k3 = perm(c);
    vec4 k4 = perm(c + 1.0);

    vec4 o1 = fract(k3 * (1.0 / 41.0));
    vec4 o2 = fract(k4 * (1.0 / 41.0));

    vec4 o3 = o2 * d.z + o1 * (1.0 - d.z);
    vec2 o4 = o3.yw * d.x + o3.xz * (1.0 - d.x);

    return o4.y * d.y + o4.x * (1.0 - d.y);
}

void main() 
{

	//////////////////////////////////////////////////////////////////////////////
	// Discard with stoachastic transparency (only for transparency, not coverage)
	//////////////////////////////////////////////////////////////////////////////
	if(hash(vec4(vec3(v_view_space_position), float(frame_number))) > v_transparency)
	{
		discard;
	}

	#if 0
	//////////////////////////////////////////////////////////////////////////////
	// PCF
	// Note: We don't really have a surface to do PCF around, so sample a small
	//       sphere surrounding the sample point. 
	//////////////////////////////////////////////////////////////////////////////
	float n0 =  noise(123.0f * v_view_space_position.xyz);
	float n1 =  noise(12345.0f * v_view_space_position.xyz);
	float shadow_sum = 0.0f; 
	float w_sum = 0.0f; 
	const int sqrt_samples = 1; 
	for(int iphi = 0; iphi < sqrt_samples; iphi++) {
		float phi = (iphi + n0) * (PI / float(sqrt_samples));
		for(int itheta = 0; itheta < 16; itheta++) {
			float theta = (itheta + n1) * (2.0f * PI / float(sqrt_samples));
			vec3 offset = vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta));
			float r = noise(100.0f * (v_view_space_position.xyz + offset));
			vec4 shadowMapCoord = lightMatrix * vec4(v_view_space_position.xyz + 0.05 * r * offset, 1.0);
			shadow_sum += textureProj(shadowMapTex, shadowMapCoord);
			w_sum += 1.0; 
		}
	}
	float light_visibility = shadow_sum / w_sum;
	#else
	vec4 shadowMapCoord = lightMatrix * vec4(v_view_space_position.xyz, 1.0);
	float light_visibility =  textureProj(shadowMapTex, shadowMapCoord);
	#endif

	vec3 light_d = v_view_space_position.xyz - light_position_viewspace.xyz;
	float light_radiance = light_visibility * light_intensity / dot(light_d, light_d);

	vec3 T = normalize(v_tangent);
	hair_shading hs = get_hair_shading(v_view_space_position.xyz, T, light_position_viewspace.xyz);

	vec3 hair_color;
	if (user_display_mode == 1)
	{
		hair_color = should_override_color == 1 ? override_color : v_color;
	}
	else // generate_train_data_mode
	{
		hair_color = vec3(0.5);
	}

	vec3 diffuse_contribution = light_radiance * (hs.diffuse * hair_color);
	float specular_contribution_R = light_radiance * (hs.specularR);
	vec3 specular_contribution_TRT = light_radiance * (hs.specularTRT * hair_color);

	if (user_display_mode == 1)
	{
		out_shadedColor.xyz = premul_factor * (ambient_w * hair_color + diffuse_contribution + specular_contribution_TRT + specular_contribution_R);
		out_shadedColor.w = premul_factor;
	}
	else // generate_train_data_mode
	{
		out_shadedColor.x = premul_factor * float(ambient_w * hair_color + diffuse_contribution + specular_contribution_TRT);
		out_shadedColor.y = premul_factor;
		out_shadedColor.z = premul_factor * specular_contribution_R;
		out_shadedColor.w = premul_factor;
	}
}

