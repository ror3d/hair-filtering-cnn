#include <GL/glew.h>
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <chrono>

#include <utils/gl_helpers.h>
#include <utils/math_helpers.h>
#include <imgui.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
using namespace glm;

#include "utils/fbo.h"
#include "utils/settings.h"

#include "hair_model.h"
#include <fstream>

#include <module.h>
#include <scene.h>
#include <camera.h>
#include <light.h>

#include <stb_image_write.h>

#include "high-res-rendering.h"

class HighResRendering : public Module
{
public:
	///////////////////////////////////////////////////////////////////////////////
	// Shader programs
	///////////////////////////////////////////////////////////////////////////////
	GLuint shaderProgram;       // Shader for rendering the final image
	GLuint shadowMapShaderProgram;
	GLuint fxShaderProgram;
	GLuint objShaderProgram;
	GLuint objShadowShaderProgram;
	GLuint envShaderProgram;

	///////////////////////////////////////////////////////////////////////////////
	// FBO
	///////////////////////////////////////////////////////////////////////////////
	Framebuffer shadowMapFB;

	Framebuffer mainFB;
	Framebuffer accumFB;
	Framebuffer sceneFB;

	int sqrt_spp = 4; 

	int iterations_per_frame = 32;

	///////////////////////////////////////////////////////////////////////////////
	// Models
	///////////////////////////////////////////////////////////////////////////////

	int windowWidth, windowHeight;

public:
	virtual const char* getModuleName() override
	{
		return "High Resolution";
	}

	void loadShaders(bool is_reload)
	{

		gl_helpers::tryLoadShaderProgram(&shaderProgram, "high-res-rendering/shading.vert", "high-res-rendering/shading.frag", is_reload);

		gl_helpers::tryLoadShaderProgram(&shadowMapShaderProgram, "high-res-rendering/shadowmap.vert", "high-res-rendering/shadowmap.frag", is_reload);

		gl_helpers::tryLoadShaderProgram(&fxShaderProgram, "fx.vert", "fx.frag", is_reload);

		gl_helpers::tryLoadShaderProgram( &objShaderProgram, "model-shading.vert", "model-shading.frag", is_reload );
		gl_helpers::tryLoadShaderProgram( &objShadowShaderProgram, "model-shadow.vert", "model-shadow.frag", is_reload );

		gl_helpers::tryLoadShaderProgram( &envShaderProgram, "fx.vert", "environment.frag", is_reload);
	}

	virtual void reloadShaders() override
	{
		loadShaders( true );
	}
	virtual bool setup( uint32_t width, uint32_t height ) override
	{
		loadShaders( false );

		windowWidth = width;
		windowHeight = height;

		///////////////////////////////////////////////////////////////////////
		// Setup Framebuffer for shadow map rendering
		///////////////////////////////////////////////////////////////////////
		//glGetIntegerv( GL_MAX_TEXTURE_SIZE, &shadow_resolution );
		//shadow_resolution /= 2; // Can't use the max value because it crashes

		shadowMapFB.init( 0, true );
		shadowMapFB.resize( 1024, 1024 );

		glBindTexture( GL_TEXTURE_2D, shadowMapFB.depthBuffer );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE );

		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER );
		vec4 border( 1.f );
		glTexParameterfv( GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &border.x );

		glBindTexture( GL_TEXTURE_2D, 0 );

		mainFB.init( 2, true );
		mainFB.resize( width * sqrt_spp, height * sqrt_spp);
		accumFB.init(1, false);
		accumFB.resize(width, height);
		sceneFB.init( 1, true );
		sceneFB.resize( width, height );

		glEnable(GL_DEPTH_TEST); // enable Z-buffering
		glDisable(GL_CULL_FACE);  // enables backface culling

		return true;
	}

	virtual void teardown() override
	{
		gl_helpers::unloadShaderProgram( shaderProgram );
		gl_helpers::unloadShaderProgram( shadowMapShaderProgram );
		gl_helpers::unloadShaderProgram( fxShaderProgram );
		gl_helpers::unloadShaderProgram( objShaderProgram );
		gl_helpers::unloadShaderProgram( objShadowShaderProgram );
		gl_helpers::unloadShaderProgram( envShaderProgram );
		mainFB.free();
		accumFB.free();
		shadowMapFB.free();
		sceneFB.free();
	}
	virtual void update( Scene* scene, float deltaTime ) override
	{
	}
	virtual void windowSizeChanged( uint32_t width, uint32_t height ) override
	{
		windowWidth = width;
		windowHeight = height;
		mainFB.resize(width * sqrt_spp, height * sqrt_spp);
		accumFB.resize(width, height);
		sceneFB.resize(width, height);
	}
	virtual void render( const Scene* scene, Framebuffer& target_framebuffer ) override
	{
		if ( shadowMapFB.width != scene->shadowmap.size || shadowMapFB.height != scene->shadowmap.size )
		{
			shadowMapFB.resize( scene->shadowmap.size, scene->shadowmap.size );
		}

		glLineWidth( 1.0 );

		///////////////////////////////////////////////////////////////////////////
		// setup matrices
		///////////////////////////////////////////////////////////////////////////

		mat4 projMatrix = scene->active_camera->as<Camera>()->getProjMatrix();
		mat4 viewMatrix = scene->active_camera->as<Camera>()->getViewMatrix();

		mat4 lightProjMatrix = scene->lights[0]->as<Light>()->proj_mat;
		mat4 lightViewMatrix = scene->lights[0]->as<Light>()->view_mat;
		mat4 lightViewProjMatrix = lightProjMatrix * lightViewMatrix;

		vec3 light_position_viewspace = vec3( viewMatrix * vec4( scene->lights[0]->getPosition(), 1.0f ) );
		float light_intensity = scene->lights[0]->as<Light>()->intensity;


		ImGui::SliderInt("Iterations", &iterations_per_frame, 0, 100);

		accumFB.bind();
		accumFB.clear_color( { 0, 0, 0, 0 } );
		float w = 1.f / iterations_per_frame;
		for (int i = 0; i < iterations_per_frame; i++)
		{
			///////////////////////////////////////////////////////////////////////////
			// Draw from light
			///////////////////////////////////////////////////////////////////////////
			shadowMapFB.bind();
			shadowMapFB.clear_depth();

			if ( chag::Settings::getGlobalInstance().get( "display_head_model", false ) )
			{
				glUseProgram( objShadowShaderProgram );
				gl_helpers::setUniformSlow( objShadowShaderProgram, "viewProjectionMatrix", lightViewProjMatrix );
				for ( auto& obj : scene->objects )
				{
					if ( !obj->hasAlpha() )
					{
						obj->render( objShadowShaderProgram );
					}
				}
			}


			glUseProgram( shadowMapShaderProgram );

			gl_helpers::setUniformSlow( shadowMapShaderProgram, "viewProjectionMatrix", lightViewProjMatrix );

			gl_helpers::setUniformSlow( shadowMapShaderProgram, "frame_number", i + 1 );
			gl_helpers::setUniformSlow( shadowMapShaderProgram, "line_offset_factor", scene->shadowmap.polygon_offset_factor );
			gl_helpers::setUniformSlow( shadowMapShaderProgram, "line_offset_units", scene->shadowmap.polygon_offset_units );

			for ( auto& obj : scene->objects )
			{
				if ( obj->as<HairModel>() )
				{
					obj->render( shadowMapShaderProgram );
				}
			}

			glUseProgram( 0 );


			///////////////////////////////////////////////////////////////////////////
			// Draw from camera
			///////////////////////////////////////////////////////////////////////////
			mainFB.bind();
			mainFB.clear();


			if ( chag::Settings::getGlobalInstance().get( "display_head_model", false ) )
			{ 
				glUseProgram(objShaderProgram);
				glColorMask( GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE );
				scene->active_camera->render( objShaderProgram );

				gl_helpers::setUniformSlow( objShaderProgram, "light_position_viewspace", light_position_viewspace);
				gl_helpers::setUniformSlow( objShaderProgram, "light_intensity", vec3( light_intensity ) );
				gl_helpers::setUniformSlow( objShaderProgram, "frame_number", i + 1 );
				gl_helpers::setUniformSlow( objShaderProgram, "premul_factor", w );

				for ( auto& obj : scene->objects )
				{
					if ( !obj->hasAlpha() )
					{
						obj->render( objShaderProgram );
					}
				}
				glColorMask( GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE );
			}


			glUseProgram( shaderProgram );

			glActiveTexture( GL_TEXTURE10 );
			glBindTexture( GL_TEXTURE_2D, shadowMapFB.depthBuffer );

			mat4 lightMatrix = translate( vec3( 0.5f ) ) * scale( vec3( 0.5f ) ) * lightProjMatrix * lightViewMatrix
			                   * inverse( viewMatrix );

			gl_helpers::setUniformSlow( shaderProgram, "frame_number", i + 1 );

			gl_helpers::setUniformSlow( shaderProgram, "light_position_viewspace", light_position_viewspace );
			gl_helpers::setUniformSlow( shaderProgram, "lightMatrix", lightMatrix );
			gl_helpers::setUniformSlow( shaderProgram, "light_intensity", light_intensity );
			gl_helpers::setUniformSlow( shaderProgram, "premul_factor", w );

			bool exporting = chag::Settings::getGlobalInstance().get( "exporting_data", false );
			gl_helpers::setUniformSlow( shaderProgram, "user_display_mode", ( exporting ? 0u : 1u ) );
			gl_helpers::setUniformSlow( shaderProgram, "generate_train_data_mode", ( exporting ? 1u : 0u ) );

			scene->active_camera->render( shaderProgram );

			for ( auto& obj : scene->objects )
			{
				if ( obj->as<HairModel>() )
				{
					obj->render( shaderProgram );
				}
			}

			glUseProgram( 0 );
			glActiveTexture( GL_TEXTURE0 );
			glBindTexture( GL_TEXTURE_2D, mainFB.colorTextureTargets[1] );
			glGenerateMipmap( GL_TEXTURE_2D );
			glBindTexture( GL_TEXTURE_2D, mainFB.colorTextureTargets[0] );
			glGenerateMipmap( GL_TEXTURE_2D );

			accumFB.bind();
			glUseProgram( fxShaderProgram );
			gl_helpers::setUniformSlow( fxShaderProgram, "copy_mipmap_level", int( std::log2( sqrt_spp ) ) );
			glEnable( GL_BLEND );
			glBlendEquation( GL_FUNC_ADD );
			glBlendFunc( GL_ONE, GL_ONE );
			//glBlendFunc( GL_CONSTANT_ALPHA, GL_ONE );
			//glBlendColor( 0, 0, 0, w );
			gl_helpers::drawFullScreenQuad();
			glDisable( GL_BLEND );
		}


		sceneFB.bind();
		sceneFB.clear();

		if ( scene->environment.texture
			 && chag::Settings::getGlobalInstance().get( "display_envmap", false ) )
		{
			mat4 projInvMatrix = glm::inverse( projMatrix );
			mat4 viewInvMatrix = glm::inverse( viewMatrix );

			glUseProgram( envShaderProgram );
			glActiveTexture( GL_TEXTURE0 );
			glBindTexture( GL_TEXTURE_2D, scene->environment.texture );
			glDepthMask( false );
			gl_helpers::setUniformSlow( envShaderProgram, "ViewInverse", viewInvMatrix );
			gl_helpers::setUniformSlow( envShaderProgram, "ProjInverse", projInvMatrix );
			gl_helpers::setUniformSlow( envShaderProgram, "EnvRot", scene->environment.rotate );
			gl_helpers::drawFullScreenQuad();
			glDepthMask( true );

		}

		if ( chag::Settings::getGlobalInstance().get( "display_head_model", false ) )
		{
			glUseProgram(objShaderProgram);
			scene->active_camera->render( objShaderProgram );

			gl_helpers::setUniformSlow( objShaderProgram, "light_position_viewspace", light_position_viewspace);
			gl_helpers::setUniformSlow( objShaderProgram, "light_intensity", vec3( light_intensity ) );
			gl_helpers::setUniformSlow( objShaderProgram, "frame_number", 1 );

			for ( auto& obj : scene->objects )
			{
				if ( !obj->hasAlpha() )
				{
					obj->render( objShaderProgram );
				}
			}
		}

		sceneFB.blit( target_framebuffer );

		target_framebuffer.bind();

		glUseProgram(fxShaderProgram);
		gl_helpers::setUniformSlow(fxShaderProgram, "copy_mipmap_level", -1);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, accumFB.colorTextureTargets[0]);

		glEnable( GL_BLEND );
		glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );


		gl_helpers::drawFullScreenQuad();

		glDisable( GL_BLEND );
		gl_helpers::setUniformSlow(fxShaderProgram, "copy_mipmap_level", -1);
		glActiveTexture( GL_TEXTURE0 );
		glBindTexture( GL_TEXTURE_2D, 0 );

		glUseProgram( 0 );
	}

	void saveFrameToFile( const std::filesystem::path& base_file_name, Framebuffer& target_framebuffer ) override
	{
		bool exporting_teaser = chag::Settings::getGlobalInstance().get( "exporting_teaser", false );

		std::filesystem::path base_name = base_file_name.stem();
		std::filesystem::path extension = base_file_name.extension();

		if ( exporting_teaser )
		{
			sceneFB.blit( target_framebuffer );

			target_framebuffer.bind();

			glUseProgram(fxShaderProgram);
			gl_helpers::setUniformSlow(fxShaderProgram, "copy_mipmap_level", -1);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, accumFB.colorTextureTargets[0]);
			glEnable( GL_BLEND );
			glBlendFuncSeparate( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ZERO, GL_ONE );
			gl_helpers::drawFullScreenQuad();
			glDisable( GL_BLEND );

			gl_helpers::setUniformSlow(fxShaderProgram, "copy_mipmap_level", -1);
			glActiveTexture( GL_TEXTURE0 );
			glBindTexture( GL_TEXTURE_2D, 0 );

			glUseProgram( 0 );

			target_framebuffer.saveToFile( base_file_name );
		}
		else
		{
			int level = int(std::log2(sqrt_spp));
			accumFB.saveToFile( base_file_name, 0 );
		}

	}
};



std::shared_ptr<Module> makeHighResRenderer()
{
	return std::make_shared<HighResRendering>();
}



