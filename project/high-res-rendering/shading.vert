#version 420

layout(location = 0) in vec3 position;
uniform mat4 viewProjectionMatrix; 
uniform mat4 viewMatrix; 
uniform mat4 modelMatrix; 

uniform int hasColorArray; 
layout(location = 1) in vec3 color;
uniform vec3 defHairColor;

uniform int hasThicknessArray; 
layout(location = 2) in float thickness;
uniform float defHairThickness;

uniform int hasTransparencyArray; 
layout(location = 3) in float transparency;
uniform float defHairTransparency;

layout(location = 4) in vec3 tangent;


out vec4 v_view_space_position;
out vec3 v_color;
out float v_transparency;
out vec3 v_tangent;

void main() 
{
	gl_Position = viewProjectionMatrix * modelMatrix * vec4(position,1.0);
	v_view_space_position = viewMatrix * modelMatrix * vec4(position, 1.f);
	// TODO: Should be normal matrix if any matrix has shearing or non uniform scaling
	v_tangent = (viewMatrix * modelMatrix * vec4(tangent, 0.0)).xyz; 
	v_color = bool(hasColorArray) ? color : defHairColor;
	v_transparency = bool(hasTransparencyArray) ? transparency : defHairTransparency;
}

