#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

uniform vec3 material_color;

uniform float line_offset_factor = 0;
uniform float line_offset_units = 0;

uniform int frame_number = 1; 

#include "../rnd_sample.glslh"

in vec3 viewspace_coords;

in vec3 v_model_space_position; 

in float v_transparency;

void main() 
{
	float thickness = 0.5;

	if(hashed_randf(v_model_space_position, float(frame_number + 13)) > thickness * v_transparency) discard; 

	float df = length(vec2(dFdx(gl_FragCoord.z), dFdy(gl_FragCoord.z)));

	// TODO: Modifying gl_FragDepth is usually a bad idea, because it invalidates 
	//       early z cull and hierarchical z. Probably doesn't matter here, though, 
	//       as we also discard. 
	gl_FragDepth = gl_FragCoord.z + line_offset_factor * df + line_offset_units * 0.0001f;
}

