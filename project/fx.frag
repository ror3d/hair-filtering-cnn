#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

layout(location = 0) out vec4 fragmentColor;

layout(binding = 0) uniform sampler2D colorTex;

in vec2 v_position;
uniform int copy_mipmap_level = -1; 
void main() 
{
	if(copy_mipmap_level >= 0) {
		fragmentColor = texelFetch(colorTex, ivec2(gl_FragCoord.xy), copy_mipmap_level);
	}
	else {
		fragmentColor = texelFetch(colorTex, ivec2(gl_FragCoord.xy), 0);
	}
}

