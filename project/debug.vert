#version 420

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 color;

uniform mat4 viewProjectionMatrix;

out vec3 v_color;

void main() 
{
	gl_Position = viewProjectionMatrix * vec4(position.xyz, 1.0);
	v_color = color;
}

