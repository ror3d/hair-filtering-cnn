import torch

class DeviceAwareModule(torch.nn.Module):
    def __init__(self, *args, **kwargs):
        self._device = kwargs.pop('device', torch.device('cpu'))
        super(DeviceAwareModule, self).__init__(*args, **kwargs)

    def cuda(self, device=None):
        super(DeviceAwareModule, self).cuda(device=device)
        if device:
            device = torch.device(device)
        else:
            device = torch.device('cuda')

        self._change_device(device)
        return self

    def to(self, *args, **kwargs):
        super(DeviceAwareModule, self).to(*args, **kwargs)
        dev = kwargs.get('device', None)
        if dev is None:
            dev = args[0]
            if not isinstance(dev, torch.device) and not isinstance(dev, str):
                dev = None
        if dev:
            self._change_device(dev)

        return self

    def _change_device(self, device):
        self._device = device
        for name, child in self.named_children():
            if isinstance(child, DeviceAwareModule):
                child._change_device(device)
        self._on_device_changed(device)

    def _on_device_changed(self, device):
        pass


