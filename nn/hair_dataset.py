from zlib import compressobj
import torch.utils.data
import torch
import numpy as np
import os
import re
import random
import math
import collections
import blosc

def _scan_dir(dir, recursive=True):
    files = []
    for f in os.scandir(dir):
        if f.is_dir() and recursive:
            files += [f.name + '/' + f2 for f2 in _scan_dir(dir + f.name + '/')]
        elif f.is_file():
            files.append(f.name)
    return files

#%%
"""Classes to load and preprocess the dataset"""

import imageio

def png_loader(path):
    return imageio.imread(path)

def load_png_as_tensor(path):
    """Returns a tensor of size [width, height, channels]"""
    i = np.asarray(png_loader(path), dtype=np.uint8)
    if i.ndim == 2:
        i = np.expand_dims(i, axis=2)
    if i.shape[2] < 4:
        i.resize(i.shape[0], i.shape[1], 4)
    t = torch.tensor(i, device='cpu', dtype=torch.uint8)
    return t

class TensorImageLoader:
    def __init__(self, device=None):
        self.files = {}
        self.device = device
        self.max_loaded = 0

    def load(self, path):
        if path in self.files:
            f = self.files[path].result()
            del self.files
            return f
        else:
            return load_png_as_tensor(path).to(self.device, dtype=torch.float16)
            

from torch.utils.data import Dataset, DataLoader

def transpose(listlist):
    r = []
    for i in range(len(listlist[0])):
        r.append([])
    for i, ls in enumerate(listlist):
        for j, v in enumerate(ls):
            r[j].append(v)
    return r


class HairDataset(Dataset):
    def __init__(self, root_dir, *, subdir, regex, channels=None, device=None, spp=None, randomise_samples=True):
        def fix_slash(d):
            return d + ('/' if (len(d) != 0 and d[-1] != '/') else '')
        self.root_dir = fix_slash(root_dir)
        self.subdirs = {k: fix_slash(d) for k,d in subdir.items()}
        file_names = [(k, f) for k,d in self.subdirs.items() for f in _scan_dir(self.root_dir + d)]
        regexes = []
        for k,r in regex.items():
            regexes.append( (k, r) )
        self.files = self._filter_files(file_names, regexes)

        self.n_sets = sum([len(l) for k, l in self.files[0].items()])

        self.file_loader = TensorImageLoader(device)

        if spp is None:
            self.spp = {k: None for k in regex.keys()}
        else:
            self.spp = spp

        self.channels = None
        if channels is not None:
            self.channels = {}
            for k, c in channels.items():
                self.channels[k] = []
                start_channel = 0
                for name, count in c:
                    self.channels[k].append( (start_channel, start_channel + count, name) )
                    start_channel += count
                self.channels[k].append( (start_channel, start_channel, "") )

        self.imgs_per_sample = {}
        for k, cb in self.channels.items():
            n_channels = cb[-1][0]
            self.imgs_per_sample[k] = (n_channels + 3) // 4

        self.randomise_samples = randomise_samples

    def count(self):
        return len(self)

    def _filter_files(self, files, filter):
        """ Returns [{}, ...]
                 a list of maps of lists file names.
            The index of each element in the internal list
            corresponds to the index of the regex in the filter list.
            The filtered elements are sorted per-regex, then added to the corresponding lists.
        """
        # TODO: I don't think the case where one regex might get fewer elements than others is taken into account here.
        # Could use shared back-references in the regexes to make sure all files are present for a specific combination
        if type(filter) == str:
            filter = [filter]
        rgx  = [(k, re.compile(f)) for k,fs in filter for f in fs]
        out_files = [list() for i in range(len(rgx))]
        for d, f in files:
            for i, r in enumerate(rgx):
                # check that the directory id is the same as the regex id
                if d == r[0] and r[1].fullmatch(f):
                    out_files[i].append( (r[0], self.subdirs[d] + f) )
        def atoi(t):
            try:
                r = int(t)
            except ValueError:
                r = t
            return r
        human_r = re.compile(r'([0-9]+)')
        out_files = list(map(lambda a: sorted(a, key=lambda s: [atoi(n) for n in human_r.split(s[1])]), out_files))
        #out_files = np.array(out_files).T.tolist()
        out_files = transpose(out_files)

        from collections import defaultdict
        out_maps = []
        for l in out_files:
            m = defaultdict(list)
            for k,v in l:
                m[k].append(v)
            out_maps.append(dict(m))

        return out_maps
    
    
    def __len__(self):
        # Return the length of the dataset
        return len(self.files)
    
    def __getitem__(self, idx: int):
        """Returns a dict formed by the names specified in `channels` in the constructor,
            each of them containing the slice of channels specified in that list of tuples.
            Each tensor is of size [nc, width, height]
            {'block_name': [{'channel_name': tensor, ...}, ...], ...}
        """
        files = self.files[idx]
        imgs = {}
        for k, fs in files.items():
            if self.spp[k] is None:
                files_to_load = fs
                print(files_to_load)
            else:
                ips = self.imgs_per_sample[k]
                n_samples = len(fs) // ips
                sample_idxs = list(range(n_samples))
                if self.randomise_samples:
                    sample_idxs = random.sample(sample_idxs, self.spp[k])
                else:
                    sample_idxs = sample_idxs[:self.spp[k]]
                files_to_load = [f for i in sample_idxs for f in fs[i*ips:(i+1)*ips]]
            imgs[k] = torch.cat(list(self.file_loader.load(self.root_dir + f) for f in files_to_load), dim=-1).transpose(0, 2).transpose(1, 2).float() / float(255)


        channel_blocks = {}
        if self.channels is None:
            # TODO: This probably is not right
            for k, v in imgs:
                channel_blocks[k] = {'image': [v]}
        else:
            for k, img in imgs.items():
                channel_blocks[k] = []
                if k in self.channels:
                    # Split the image in chunks of the size of this block of channels
                    xs = torch.split(img, self.channels[k][-1][0], dim=0)
                    block = []
                    for x in xs:
                        b = {}
                        # skip the last one as it's only used for length
                        for start, end, name in self.channels[k][:-1]:
                            b[name] = x[start:end, :, :]
                        block.append(b)
                    channel_blocks[k] = block
                else:
                    # TODO: This probably is not right
                    channel_blocks[k] = [img]
        
        return channel_blocks


class TiledDataset(torch.utils.data.IterableDataset):
    def __init__(self, internal_dataset, tile_size=128, tile_stride=64, validity_channel_path=None):
        """
            validity_channel_path is a tuple(str, str) that indexes the input as ['x'][0]['y']
        """
        super(TiledDataset).__init__()
        self.tile_size = tile_size
        self.tile_stride = tile_stride
        self.internal = internal_dataset
        self.last_count = None
        self.validity_channel_path = validity_channel_path
        self.zero_tiles_counter = 0

    def tile(self, image_channels_dict):
        d = image_channels_dict
        i = list(list(d.values())[0][0].values())[0]
        w = i.size(-1)
        h = i.size(-2)

        unf = torch.nn.Unfold(kernel_size=self.tile_size, stride=self.tile_stride)

        tiled = []
        # For each named block in the data sample ('input' and 'target', usually)
        for bname, block in d.items():
            # For each different image
            for bi, b in enumerate(block):
                # For each bunch of named channels
                for k, v in b.items():
                    # This is in case some other kind of data was loaded
                    # I don't think it is right now though
                    if type(v) is not torch.Tensor:
                        raise TypeError("Expected tensor")
                    if v.dim() == 3:
                        v = v.unsqueeze(0)
                    t = unf(v).reshape(v.size(1), self.tile_size, self.tile_size, -1).permute(3, 0, 1, 2)
                    #print(np.prod(t.size()))
                    num_tiles = t.size(0)

                    while len(tiled) < num_tiles:
                        tiled.append(collections.defaultdict(list))
                    for i in range(num_tiles):
                        while len(tiled[i][bname]) < len(block):
                            tiled[i][bname].append({})
                        tiled[i][bname][bi][k] = t[i, :, :, :]

        for t in tiled:
            if self.validity_channel_path is None:
                yield t
            else:
                valid_channels = t[self.validity_channel_path[0]][0][self.validity_channel_path[1]]
                z = torch.count_nonzero(valid_channels, (-2,-1))
                non_zero_channels = torch.count_nonzero(z)
                if non_zero_channels == len(z):
                    yield t
                else:
                    continue
                    self.zero_tiles_counter += 1
                    if self.zero_tiles_counter >= 1000:
                        self.zero_tiles_counter = 0
                        yield t


    def __iter__(self):
        this_count = 0
        # For datum (this corresponds to several pngs, including input and target tensors)
        for d in self.internal:
            for t in self.tile(d):
                this_count += 1
                yield t

        self.last_count = this_count

    def count(self):
        if self.last_count is None:
            return self.approx_len()
        else:
            return self.last_count

    def approx_len(self):
        n_imgs = len(self.internal)
        d = next(iter(self.internal))
        i = list(list(d.values())[0][0].values())[0]
        if type(self.internal) is DataLoader:
            i = i.squeeze(0)
        w = i.size(2)
        h = i.size(1)
        x = math.floor((w - (self.tile_size + self.tile_stride))/self.tile_stride)
        y = math.floor((h - (self.tile_size + self.tile_stride))/self.tile_stride)
        return n_imgs * x * y


class CachedTiledHairDataset(Dataset):
    def __init__(self, root_dir, *, device, spp=None, shuffle=True, randomise_samples=True) -> None:
        super().__init__()
        def fix_slash(d):
            return d + ('/' if (len(d) != 0 and d[-1] != '/') else '')
        self.root_dir = fix_slash(root_dir)
        self.shuffle = shuffle
        self.randomise_samples = randomise_samples
        self.device = device
        self.spp = spp
        self.tile_size = 0

        self.block_names = None
        self.channel_names = None
        # Does not account for multisampling
        self.n_channels_per_img = None

        # Does account for multisampling
        self.n_channels_per_block = None
        self.n_channels_per_tile = 0

        self.dtype = None

        # Read files in root_dir and store their info
        self.files = [f for f in _scan_dir(self.root_dir, recursive=False) if f.endswith(".tds")]
        self.tiles = []
        for f in self.files:
            self.tiles += [(f, *t) for t in self._read_cache_header(f)]

        self.num_tiles = len(self.tiles)
        self.shuffled_idxs = list(range(self.num_tiles))

        self.one_tensor_size = self.n_channels_per_tile * self.tile_size * self.tile_size

    def count(self):
        return self.num_tiles

    def _read_cache_header(self, file):
        import struct
        l = []
        with open(os.path.join(self.root_dir, file), "rb") as f:
            head = f.read(3)
            if head != b'tds':
                print("File {} is not of the right format".format(f))
                return []
            w = struct.unpack("<H", f.read(2))[0]
            h = struct.unpack("<H", f.read(2))[0]
            if w != h:
                raise NotImplementedError("Width and height expected to be same")
            if self.tile_size == 0:
                self.tile_size = w

            dtype_s = f.read(1)
            if dtype_s == b'e':
                dtype = np.float16
            elif dtype_s == b'f':
                dtype = np.float32
            else:
                raise NotImplementedError("Only doing float16 or 32!")
            if self.dtype is None:
                self.dtype = dtype
            elif self.dtype != dtype:
                raise RuntimeError("Type of float must match in all cache files, found a different one in {}".format(file))
            
            # Read block names
            nblocks = struct.unpack("<H", f.read(2))[0]
            names = []
            for i in range(nblocks):
                names.append(bytearray())
                c = f.read(1)
                while c != b'\0':
                    names[-1] += c
                    c = f.read(1)
            names = [c.decode(encoding='ascii') for c in names]
            if self.block_names is None:
                self.block_names = names
            elif self.block_names != names:
                raise RuntimeError("Blocks don't match in file {}:\n\t{}\n\t{}".format(file, str(self.block_names), str(names)))
            
            # Read num images per block
            nnames = []
            for i in range(nblocks):
                nnames.append(struct.unpack("<H", f.read(2))[0])

            # Read image names per block
            img_names = collections.defaultdict(list)
            for bn, n in zip(self.block_names, nnames):
                names = []
                for i in range(n):
                    names.append(bytearray())
                    c = f.read(1)
                    while c != b'\0':
                        names[-1] += c
                        c = f.read(1)
                names = [c.decode(encoding='ascii') for c in names]
                img_names[bn] = names

            if self.channel_names is None:
                self.channel_names = img_names
            elif self.channel_names != img_names:
                raise RuntimeError("Channel names don't match in file {}:\n\t{}\n\t{}".format(file, str(self.channel_names), img_names))

            # Read multisamples per block
            mspp = {}
            for i in range(nblocks):
                mspp[self.block_names[i]] = struct.unpack("<H", f.read(2))[0]
            if self.spp is None:
                self.spp = mspp
            #elif any(self.spp[n] < mspp[n] for n in self.spp.keys()):
            elif self.spp != mspp:
                raise RuntimeError("Samples per pixel don't match in file {}:\n\t{}\n\t{}".format(file, self.spp, mspp))
            
            # Read channels per image
            nchannels = {}
            for bn in self.block_names:
                ims = self.channel_names[bn]
                nchannels[bn] = {}
                for cn in ims:
                    nchannels[bn][cn] = struct.unpack("<H", f.read(2))[0]

            if self.n_channels_per_img is None:
                self.n_channels_per_img = nchannels
            elif self.n_channels_per_img != nchannels:
                raise RuntimeError("Number of channels per image doesn't match in file {}:\n\t{}\n\t{}".format(file, self.n_channels_per_img, nchannels))

            # Calculate number of channels based on spp
            if self.n_channels_per_tile == 0:
                self.n_channels_per_block = {}
                for k, spp in self.spp.items():
                    self.n_channels_per_block[k] = 0
                    for imn, n in self.n_channels_per_img[k].items():
                        self.n_channels_per_block[k] += spp * n
                        self.n_channels_per_tile += spp * n

            # Read number of tiles
            ntiles = struct.unpack("<I", f.read(4))[0]

            # Read compressed flags
            compressed = []
            for i in range(ntiles):
                compressed.append(f.read(1) != b'\x00')

            # Read the sizes of each block
            sizes = []
            for i in range(ntiles):
                b = f.read(4)
                sizes.append(struct.unpack("<I", b)[0])

            starts = []
            start = f.tell()
            for s in sizes:
                starts.append(start)
                start += s

            l = zip(starts, sizes, compressed)

        return l

    def __len__(self):
        return self.num_tiles


    def __getitem__(self, index):
        def uncompress(data):
            #c = zlib.decompressobj()
            #d = c.decompress(data)
            #d += c.flush()
            #d = zlib.decompress(data, bufsize=self.one_tensor_size*2)
            d = blosc.decompress(data)
            return d
        import zlib
        if index == 0 and self.shuffle:
            # Shuffle entries
            random.shuffle(self.shuffled_idxs)

        # Read file and return tile data
        t = self.tiles[self.shuffled_idxs[index]]
        with open(os.path.join(self.root_dir, t[0]), "rb") as f:
            f.seek(t[1])
            comp = f.read(t[2])
            if t[3]:
                unc = uncompress(comp)
            else:
                unc = comp
            arr = np.frombuffer(unc, dtype=self.dtype).reshape(self.n_channels_per_tile, self.tile_size, self.tile_size)
            tn = torch.tensor(arr, device=self.device, dtype=torch.float32)
            r = {}
            tstart = 0
            for bn in self.block_names:
                bt = tn[tstart:(tstart+self.n_channels_per_block[bn]), :, :]
                tstart += self.n_channels_per_block[bn]
                r[bn] = []
                for i in range(self.spp[bn]):
                    r[bn].append({})
                    cstart = 0
                    for cn in self.channel_names[bn]:
                        start = cstart + i * self.n_channels_per_img[bn][cn]
                        r[bn][-1][cn] = bt[start:(start + self.n_channels_per_img[bn][cn])]
                        cstart += self.spp[bn] * self.n_channels_per_img[bn][cn]
            return r


from typing import TypeVar, Iterator, List
class BufferedShuffleDataset(torch.utils.data.IterableDataset):
    dataset: torch.utils.data.IterableDataset
    buffer_size: int

    def count(self):
        if self.last_count is not None:
            return self.last_count
        elif 'count' in set(dir(self.dataset)):
            return self.dataset.count()
        else:
            return 1

    def __init__(self, dataset: torch.utils.data.IterableDataset, buffer_size: int) -> None:
        super(BufferedShuffleDataset, self).__init__()
        assert buffer_size > 0, "buffer_size should be larger than 0"
        self.dataset = dataset
        self.buffer_size = buffer_size
        self.last_count = None

    def __iter__(self) -> Iterator:
        buf: List = []
        this_count = 0
        for x in self.dataset:
            if len(buf) == self.buffer_size:
                idx = random.randint(0, self.buffer_size - 1)
                this_count += 1
                yield buf[idx]
                buf[idx] = x
            else:
                buf.append(x)
        random.shuffle(buf)
        while buf:
            this_count += 1
            yield buf.pop()
        self.last_count = this_count


def cartesian_format(string, **kwargs):
    from string import Template
    class No_Template(Template):
        idpattern=r"(?ai:[a-z][a-z0-9]*)"
    single_str = {}
    list_str = {}
    for arg in kwargs:
        if type(kwargs[arg]) is str:
            single_str[arg] = kwargs[arg]
        elif type(kwargs[arg]) is list:
            list_str[arg] = kwargs[arg]
        else:
            print("Unexpected input arg '{}' of type '{}'".format(arg, type(kwargs[arg])))
    string = No_Template(string).safe_substitute(single_str)
    output = [string]
    for k in list_str:
        output = [No_Template(s).safe_substitute({k:v}) for s in output for v in list_str[k]]
    return output

def template_substitute(string, **kwargs):
    from string import Template
    class No_Template(Template):
        idpattern=r"(?ai:[a-z][a-z0-9]*)"
    subs = {}
    for arg in kwargs:
        if type(kwargs[arg]) is str:
            subs[arg] = kwargs[arg]
    if type(string) is str:
        return No_Template(string).safe_substitute(subs)
    elif type(string) is list:
        return [No_Template(s).safe_substitute(subs) for s in string]
    else:
        raise NotImplementedError("`string` type must be string or list")


def get_dataset_item(dset):
    return next(iter(dset))

#%%
if __name__ == "__main__":
    train_tiler = CachedTiledHairDataset("test_hair_dataset_py/cache", device='cuda', shuffle=False)
    item = get_dataset_item(train_tiler)


#%%
if __name__ == "__main__":
    input_channels = [("color", 1), ("specular", 1), ("depth", 1), ("alpha-redundant", 1),
                  ("tangents", 2), ("tangent-depth-skipped", 1), ("alpha", 1)]

    target_channels = [("color-redundant", 1), ("color", 1), ("alpha", 1), ("specular", 1)]

    test_frame = '(12|13)'

    images_pattern = r'$name$variant$component$sample\.png'

    input_variants = ['_a', '_b', '_c', '_d', '_e']
    target_variants = ''

    input_components = ['', '_tan']
    target_components = ['']

    multisample_samples = 4

    input_samples = ['_'+str(i) for i in range(0,multisample_samples)]
    target_samples = ''

    input_regexes = cartesian_format( images_pattern,
                                        name=test_frame,
                                        variant=input_variants,
                                        sample=input_samples,
                                        component=input_components)

    target_regexes = cartesian_format( images_pattern,
                                        name=test_frame,
                                        variant=target_variants,
                                        sample=target_samples,
                                        component=target_components)

    hair_dataset = HairDataset('train_data',
                            subdir={'input': 'validation_input_thin_strands', 'target': 'validation_target'},
                            regex={'input': input_regexes, 'target': target_regexes},
                            channels={'input': input_channels, 'target': target_channels},
                            device='cuda')
    train_loader = DataLoader(hair_dataset, batch_size=1, shuffle=True, drop_last=False)
    train_tiler = TiledDataset(train_loader, validity_channel_path=("input", "alpha"))

    item = get_dataset_item(train_tiler)

    """"
    loader = DataLoader(dataset, batch_size=1, shuffle=False, drop_last=False)
    everything_tiler = TiledDataset(loader)
    only_valid_tiler = TiledDataset(loader, validity_channel_path=("input", "alpha"))

    batcher = DataLoader(only_valid_tiler, batch_size=32, drop_last=False)

    print("All the tiles:")
    print("Expected: {}".format(everything_tiler.approx_len()))
    print()
    count = 0
    for t in everything_tiler:
        count += 1
    print()
    print("Obtained: {}".format(count))

    print("-----------------------")
    print("Non-zero:")
    print("Expected: {}".format(only_valid_tiler.approx_len()))
    print()
    count = 0
    for t in only_valid_tiler:
        count += 1
    print("Obtained: {}".format(count))

    print("-----------------------")
    print("Batched:")
    print("Expected: ?")
    count = 0
    for b in batcher:
        count += 1
    print("Obtained: {}".format(count))

    b = get_dataset_item(batcher)
    """


