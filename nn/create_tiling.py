#%%
import torch
from torch import nn, renorm
import torch.functional as F
import torch.nn.functional as nnF
import torch.utils.data
from dev_aware_module import DeviceAwareModule
import os
import random
import sys, getopt
import blosc



#%%
import struct
import zlib
import io

class TilesFileSaver:
    def __init__(self, channels, size, multisamples, should_compress, float_type) -> None:
        self.tiles = []
        self.channels = channels
        self.size = size
        self.multisamples = multisamples
        self.should_compress = should_compress
        self.float_type = float_type

    def add_tile(self, t):
        chan_tensors = []
        # For each data block
        for k, b in self.channels.items():
            # For each image in the datablock
            for c, n in b:
                # For each sample in the image
                for s in range(self.multisamples[k]):
                    chan_tensors.append(t[k][s][c])
        self.tiles.append(chan_tensors)


    def save(self, path, name):
        def tensor_to_bytes(t):
            t = t.to(dtype=self.float_type, device='cpu')
            return bytearray(t.numpy())

        def compress(unc):
            try:
                #c = zlib.compressobj()
                #d = c.compress(unc)
                #d += c.flush()
                #d = zlib.compress(unc, level=1)
                d = blosc.compress(unc, typesize=8)
                return d
            except:
                return None

        block_names = [k for k,b in self.channels.items()]
        channels = [(k, c) for k in block_names for c, n in self.channels[k]]

        data = bytearray()
        data.extend(b"tds")

        # Width/Height of a single image
        data.extend(struct.pack("<H", self.size[0]))
        data.extend(struct.pack("<H", self.size[1]))

        # Data type. e: float16, f: float32, etc
        if self.float_type == torch.float16:
            data.extend(b'e')
        else:
            data.extend(b'f')

        # Amount of image block names
        data.extend(struct.pack("<H", len(block_names)))

        for k in block_names:
            data.extend(k.encode(encoding="ascii"))
            data.extend(b"\0")

        # Number of images per block
        for k in block_names:
            data.extend(struct.pack("<H", len(self.channels[k])))

        # Image names
        for k, c in channels:
            data.extend(c.encode(encoding="ascii"))
            data.extend(b"\0")

        # Number of multisamples per block
        for k in block_names:
            data.extend(struct.pack("<H", self.multisamples[k]))

        # Number of channels per image (usually 1, 2 for tangents)
        for k, b in self.channels.items():
            for c, n in b:
                data.extend(struct.pack("<H", n))

        # Number of tiles
        data.extend(struct.pack("<I", len(self.tiles)))

        image_data = bytearray()
        compressed = []
        sizes = []
        for tile in self.tiles:
            tensor_data = bytearray()
            for t in tile:
                tensor_data.extend(tensor_to_bytes(t))
            unc = tensor_data
            comp = None
            if self.should_compress:
                comp = compress(unc)
            if comp:
                image_data.extend(comp)
                compressed.append(True)
                sizes.append(len(comp))
            else:
                image_data.extend(unc)
                compressed.append(False)
                sizes.append(len(unc))

        # Compressed flag
        for c in compressed:
            data.extend(b"\x01" if c else b"\x00")

        # Size of each compressed data block
        for s in sizes:
            data.extend(struct.pack("<I", s))

        # Channel data
        data.extend(image_data)

        with open(os.path.join(path, name), "wb") as f:
            f.write(data)


#%%

if __name__ == "__main__":
    from hair_dataset import *
    from torch.utils.data import DataLoader

    images_pattern = r'$name$variant$component$sample\.png'

    input_variants = ['_a', '_b']
    target_variants = ''

    input_components = ['', '_tan']
    target_components = ['']

    input_samples = ['_'+str(i) for i in range(0,multisample_samples)]
    target_samples = ''

    spp = {'input': multisample_samples, 'target': 1}

    #validation_variants = ['_a', '_b', '_c', '_d', '_e']

    # Channels are:
    # 1 for stochastic color and specular
    # 2 for stochastic tangent x and y, 1 for stochastic alpha
    # 1 for multisampled color, 1 for multisampled alpha, 1 for multisampled specular
    input_channels = [("color", 1), ("specular", 1), ("depth", 1), ("alpha-redundant", 1),
                    ("tangents", 2), ("tangent-depth-skipped", 1), ("alpha", 1)]

    target_channels = [("color", 1), ("alpha", 1), ("specular", 1), ("alpha-redundant", 1)]

    input_regexes = cartesian_format( images_pattern,
                                        name=r'[0-9]+',
                                        variant=input_variants,
                                        sample=input_samples,
                                        component=input_components)
    target_regexes = cartesian_format( images_pattern,
                                        name=r'[0-9]+',
                                        variant=target_variants,
                                        sample=target_samples,
                                        component=target_components)

    dataset = HairDataset(training_source,
                subdir={'input': 'input', 'target': 'target'},
                regex={'input': input_regexes, 'target': target_regexes},
                channels={'input': input_channels, 'target': target_channels},
                spp=spp,
                device=nn_device)

    train_loader = DataLoader(dataset, batch_size=1, shuffle=False, drop_last=False)

    train_tiler = TiledDataset(train_loader, validity_channel_path=("input", "alpha"), tile_size=tile_size, tile_stride=tile_stride)




    validation_loader = DataLoader(
                            HairDataset(training_source,
                                        subdir={'input': 'validation_input', 'target': 'validation_target'},
                                        regex={'input': input_regexes, 'target': target_regexes},
                                        channels={'input': input_channels, 'target': target_channels},
                                        spp=spp,
                                        randomise_samples=False,
                                        device=nn_device
                                        ),
                            batch_size=1,
                            shuffle=False,
                            drop_last=False)

    validation_tiler = TiledDataset(
                            validation_loader,
                            validity_channel_path=("input", "alpha"),
                            tile_size=tile_size,
                            tile_stride=tile_stride
                            )


    def create_cache(cache_dir, data_loader, tiler):

        if not os.path.exists(cache_dir):
            os.makedirs(cache_dir)

        channels = {
            "input": [("color", 1), ("specular", 1), ("depth", 1), ("tangents", 2), ("alpha", 1)],
            "target": [("color", 1), ("alpha", 1), ("specular", 1)]
        }

        for i, b in enumerate(data_loader):
            print("File {}".format(i))
            tfs = TilesFileSaver(channels, (tile_size, tile_size), {"input": 4, "target": 1})

            for t in tiler.tile(b):
                tfs.add_tile(t)

            tfs.save(cache_dir, "{}.tds".format(i))

    create_cache(os.path.join(training_source, "train_cache"), train_loader, train_tiler)
    create_cache(os.path.join(training_source, "validation_cache"), validation_loader, validation_tiler)

#%%
