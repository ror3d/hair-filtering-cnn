#%%
from random import shuffle
import numpy as np
import time
import torch
import matplotlib.pyplot as plt
import imageio

from torch.cuda.amp import autocast

import interrupt_iterator

import os
import csv

class Log:
    def __init__(self, *, out_dir="", fname="log.txt"):
        self.current_line = ""
        self.file = open(out_dir + "/" + fname, mode='w')

    def __del__(self):
        if self.file is not None:
            self.file.write(self.current_line)
            self.file.close()
            self.current_line = ""
            self.file = None

    def log(self, string, *args, end:str ='\n', sep=' '):
        string = str(string)
        print(string, *args, end=end, sep=sep)
        for s in args:
            string += sep + str(s)
        string += end
        for c in string:
            if c == '\r':
                self.current_line = ""
            elif c == '\n':
                self.file.write(self.current_line + '\n')
                self.current_line = ""
            else:
                self.current_line += c
        self.file.flush()

    _instance = None
    def init(out_dir="", fname="log.txt"):
        if Log._instance is None:
            Log._instance = Log(out_dir=out_dir, fname=fname)

    def print(*args, **kwargs):
        Log._instance.log(*args, **kwargs)


#%%
"""Training functions"""

def run_epoch(model, data, backprop=True):
    t0 = time.time()
    total_loss = 0

    batch_t0 = time.time()
    batch_t1 = batch_t0
    # Keeping track of how many batches there were in the first epoch, and use that to calculate how many there are now, since the streamed dataset can't count them by itself
    n_batches = int((data.dataset.count() + data.batch_size - 1) / data.batch_size)
    Log.print("Batch 0/{}.".format(n_batches), end='')
    total_inputs_count = 0
    total_tiles_count = 0
    for i, batch in enumerate(data):
        if backprop:
            model.optimizer.zero_grad()

        # We only shuffle if we are training, to reduce the variance in the validation MSE
        loss = model.loss(batch, shuffle=backprop)

        if backprop:
            loss.backward()
            model.optimizer.step()

        total_loss += loss.item()
        color_t = batch['input'][0]['color']
        total_tiles_count += color_t.size(0)
        total_inputs_count += 1 #color_t.size(0) * color_t.size(2) * color_t.size(3)
        batch_t1 = time.time()
        Log.print("\rBatch {}/{} ({:.2%}). LR={:.2e}. T={:.1f} ETA={:.1f} BPS={:.4f} TPS={:.4f}".format(
            i+1,
            n_batches,
            float(i+1)/n_batches,
            model.optimizer.state_dict()['param_groups'][0]['lr'],
            (batch_t1 - t0),
            (n_batches - i) * (batch_t1 - t0) / (i+1),
            (i+1) / (batch_t1 - t0),
            total_tiles_count / (batch_t1 - t0) ),
            end='')
    total_loss /= total_inputs_count
    if backprop and model.lr_scheduler != None:
        model.lr_scheduler.step()
    Log.print("")
    if model._device == 'cuda':
        torch.cuda.empty_cache()
    return total_loss


def next_to_n(l, n):
    i = iter(l)
    for k in range(n):
        x = next(i)
    return next(i)

def expand_to_3(t : torch.Tensor):
    d = t.size()
    return t[0:1, :, :, :].expand(1, 3, d[2], d[3])

def ishow(t, o=plt, fig=None):
    t = t[0:1,:,:,:]
    if t.size(1) == 2:
        t = torch.cat([t, torch.zeros_like(t)[:, :1, :, :]], dim=1)
    o.imshow(t.squeeze(0).transpose(1,2).transpose(0,2).clamp(0.0, 1.0).cpu(), resample=False)
    if fig:
        fig.set_size_inches(t.size(3)/float(fig.dpi), t.size(2)/float(fig.dpi))

def iout(t, path):
    t = t[0,:,:,:]
    if t.size(0) == 2:
        t = torch.cat([t, torch.zeros_like(t[:1, :, :])], dim=0)
    im = t.clamp(0.0, 1.0).mul(torch.tensor(255)).to(torch.uint8).permute(1,2,0).cpu().detach().numpy()
    imageio.imwrite(path, im)

def write_model_graph(model, eval_data, output_directory="results/"):
    import torch
    with torch.no_grad():
        import importlib.util
        onnx_spec = importlib.util.find_spec("onnx")
        if onnx_spec is not None:
            import torch.onnx
            import io
            import onnx
            model.eval()
            params = model.get_kwparams(eval_data)
            input_names = [n.capitalize() for n in params.keys()]
            output_names = [n.capitalize() for n in model.output_names()]
            dynamic_axes = {k: {0: "Batch", 2: "Width", 3: "Height"} for k in input_names + output_names}
            file_like = io.BytesIO()
            torch.onnx.export(model,
                            (params,),
                            file_like,
                            export_params=True,
                            opset_version=11,
                            strip_doc_string=True,
                            input_names=input_names,
                            output_names=output_names,
                            dynamic_axes=dynamic_axes)
            m = onnx.load_model_from_string(file_like.getvalue())
            with open(output_directory + "model.txt", "w") as f:
                f.write(str(onnx.helper.printable_graph(m.graph)))
        else:
            print("ONNX not found! you might wanna install it to get better output of the model: https://github.com/onnx/onnx/#installation")
            with open(output_directory + "model.txt", "w") as f:
                f.write(str(model))
    return m

def write_reference_images(eval_value, output_directory, input_sample_index=0, name_suffix=""):
    iout(expand_to_3(eval_value['input'][input_sample_index]['depth']), output_directory + "depth_input{}.png".format(name_suffix))

    iout(expand_to_3(eval_value['input'][input_sample_index]['color']), output_directory + "color_input{}.png".format(name_suffix))

    iout(expand_to_3(eval_value['input'][input_sample_index]['alpha']), output_directory + "alpha_input{}.png".format(name_suffix))

    iout(eval_value['input'][input_sample_index]['tangents'], output_directory + "tg_input{}.png".format(name_suffix))

    #spec = eval_value['input'][0]['specular']
    #spec = torch.cat([expand_to_3(spec), torch.ones_like(spec)], dim=1)
    iout(expand_to_3(eval_value['input'][input_sample_index]['specular']), output_directory + "spec_input{}.png".format(name_suffix))

    iout(expand_to_3(eval_value['target'][0]['color']), output_directory + "color_expected.png")

    iout(expand_to_3(eval_value['target'][0]['alpha']), output_directory + "alpha_expected.png")

    #spec = eval_value['target'][0]['specular']
    #spec = torch.cat([expand_to_3(spec), torch.ones_like(spec)], dim=1)
    iout(expand_to_3(eval_value['target'][0]['specular']), output_directory + "spec_expected.png")

    # Just to sanity-check
    """
    params = model.get_kwparams(eval_value)
    for k, t in params.items():
        for i in range(t.size(1)):
            ishow(expand_to_3(t[:, i:(i+1), :, :]), plt, f)
            plt.savefig(output_directory + "debug_ref_" + k + "_" + str(i) + ".png")
            f.clear()
    """

def write_reference_info(model, data, output_directory="results/"):
    import torch
    eval_value = next(iter(data))
    write_model_graph(model, eval_value, output_directory=output_directory)

    write_reference_images(eval_value, output_directory)


def write_eval_model(model, eval_value, output_directory, output_file):
    if type(eval_value) is not dict:
        raise TypeError("Expected dict containing key-values to pass to the model")

    alpha_output_file = output_directory + "alpha_" + output_file
    color_output_file = output_directory + "color_" + output_file
    spec_output_file = output_directory + "spec_" + output_file

    model.eval()
    with torch.no_grad():
        with autocast(): # evaluate with fp16
            evaled = model.forward_batch_dict(eval_value, shuffle=False)
        evaled = {k: v.float() for k,v in evaled.items()}
        # save color output
        iout(expand_to_3(evaled["color"] * 0.5), color_output_file)

        # save alpha output
        iout(expand_to_3(evaled["alpha"]), alpha_output_file)

        # Save specular output
        #d = evaled.size()
        #spec = evaled[:,3:4,:,:].expand(d[0], 3, d[2], d[3])
        #spec = torch.cat([expand_to_3(evaled["specular"]), torch.ones([d[0], 1, d[2], d[3]], device=spec.device)], dim=1)
        iout(expand_to_3(evaled["specular"]), spec_output_file)

        return evaled


def train(model, train_data, eval_data, validation_data, start_epoch=0, num_epochs=None, output_directory="results/"):
    with open(output_directory + "model.txt", "w") as f:
        f.write(str(model))
    loss_list = []
    validation_loss_list = []
    if start_epoch > 0:
        # Initialise with either old values or something
        if os.path.exists(output_directory + "mse.csv"):
            with open(output_directory + "mse.csv", "r") as f:
                reader = csv.DictReader(f)
                for row in reader: 
                    loss_list.append(float(row['train_mse']))
                    validation_loss_list.append(float(row['valiation_mse']))
        else:
            for i in range(start_epoch):
                loss_list.append(1)
                validation_loss_list.append(1)

    eval_value = next(iter(eval_data))

    write_reference_info(model, eval_data, output_directory)

    with open(output_directory + "mse.csv", "w") as f:
        f.write("epoch,train_mse,validation_mse\n")

    for epoch in (interrupt_iterator.iterate_range(range(start_epoch, num_epochs)) if num_epochs is not None else interrupt_iterator.iterate_forever(start_epoch)):
        Log.print("Epoch ", epoch + 1)
        model.train()

        loss = run_epoch(model, train_data)
        Log.print("Epoch loss was {}, {:.4%} improvement".format(loss, 1 - (loss / loss_list[-1]) if len(loss_list) > 0 else 1))
        loss_list.append(loss)

        model.eval()
        model.save_to_file(output_directory + 'last_epoch_model.pth')
        if epoch > 5 and (epoch + 1) % 5 == 0:
            model.save_to_file(output_directory + 'epoch_{}_model.pth'.format(epoch+1))
            model.export_for_cpp(output_directory + 'epoch_{}_model.json'.format(epoch+1))
        with torch.no_grad():
            write_eval_model(model, eval_value, output_directory, "evaled_{}.png".format(epoch+1))

            loss = run_epoch(model, validation_data, backprop=False)
            validation_loss_list.append(loss)

            # Save sample tiles
            """evaled = model(eval_value_2['input'])
            f = plt.figure(2)
            f.clear()
            ax1, ax2 = f.subplots(1, 2)
            ishow(eval_value_2['target'][0][:3,:,:], ax1)
            ishow(evaled[0][:3,:,:], ax2)
            f.set_size_inches(2*evaled.size(3)/float(f.dpi), evaled.size(2)/float(f.dpi))
            plt.savefig(output_directory + "tile_evaled_{}.png".format(epoch+1)) """
        f = plt.figure(3)
        f.clear()

        x = np.linspace(1, len(loss_list), len(loss_list))
        fig, ax1 = plt.subplots()
        ax1.set_ylabel("Training MSE", color="tab:blue")
        ax1.loglog(x, loss_list, color="tab:blue")
        ax1.tick_params(axis='y', labelcolor="tab:blue")
        top = 1.2 * loss_list[10] if len(loss_list) > 10 else ax1.get_ylim()[1]
        ax1.set_ylim(ax1.get_ylim()[0], top)
        ax2 = ax1.twinx()
        ax2.set_ylabel("Validation MSE", color="tab:orange")
        ax2.loglog(x, validation_loss_list, color="tab:orange")
        ax2.tick_params(axis='y', labelcolor="tab:orange")
        top = 1.2 * validation_loss_list[10] if len(validation_loss_list) > 10 else ax2.get_ylim()[1]
        ax2.set_ylim(ax2.get_ylim()[0], 1.2 * validation_loss_list[min(10, len(validation_loss_list)-1)])
        plt.tight_layout(pad=1)
        plt.savefig(output_directory + "mse.png")
        plt.close(fig)
        with open(output_directory + "mse.csv", "a+") as f:
            f.write("{},{},{}\n".format(len(loss_list), loss_list[-1], validation_loss_list[-1]))

    Log.print("Finished running training")
    if not interrupt_iterator.is_iteration_interrupted():
        print("\a") # sound the bell
    return loss_list, validation_loss_list

