#%%
import torch
from torch import nn, renorm
from torch._C import ModuleDict
import torch.functional as F
import torch.nn.functional as nnF
import torch.utils.data
from dev_aware_module import DeviceAwareModule
import os
import random
import sys, getopt

multisample_samples = 4
training_source = 'train_data'
network_input_channels = None
output_directory = "results/"
num_epochs = None
model_name = 'default'

use_cached = False

create_cache = False
cache_float_type = torch.float16
cache_should_compress = True
tile_size = 128
tile_half_stride = False
use_lr_decay = True
starting_parameters = None
start_epoch = 0

test_splotches = False
validate_nan = False

if __name__=='__main__' and not 'ipykernel' in sys.argv[0]:
    if len(sys.argv) > 1:
        try:
            opts, args = getopt.getopt(sys.argv[1:], "h", ["channels=","out-dir=", "epochs=", "in-dir=", "cached", "make-cache", "compress-cache", "cache-float32", "tile-size=", "tile-half-stride", "spp=", "model=", "start-params=", "start-epoch=", "test-splotches", "validate-nan"])
        except getopt.GetoptError as e:
            print("Error processing arguments: " + e.msg)
            exit(-1)
            opts = []
            args = []
        for opt, arg in opts:
            print(opt, arg)
            if opt == "-h":
                print("""Usage
    python hair-filter.py [-h] [--channels=<str>] [--out-dir=<dir>] [--epochs=<n>]

        --channels=<str>    :   comma-separated list of channels to use as the input to the network.
                            default: color,specular,depth,tangents,alpha
        --out-dir=<dir>     :   path to the output directory where the results will be written.
        --spp=<num>   :   Number of samples per pixel to use. default 4
                            default: results/
        --in-dir=<dir>     :   path to the output directory where the results will be written.
                            default: train_data
        --epochs=<n>        :   number of epochs to train
                            default: train indefinitely
        --tile-size=        :   Size of the tiles to use
                            default: 128
        --tile-half-stride  :   Stride between tiles is half the size of the tile
                            default: False
        --cached            :   Use cached data

        --make-cache        :   Create cache instead of training

        --model=<name>      :   model to use. Can be 'default', 'tiny', 'small', 'medium', 'large', '2step', '3step', '4step'

        --start-params=<file> : .pth file with parameters for the network

        --start-epoch=<num> :   epoch of the start-params

        --compress-cache
        --cache-float32

        --test-splotches
        --validate-nan
""")
                exit(0)
            if opt == "--channels":
                network_input_channels = set(arg.split(","))
            elif opt == "--out-dir":
                output_directory = arg
            elif opt == "--in-dir":
                training_source = arg
            elif opt == "--epochs":
                num_epochs = int(arg)
            elif opt == "--spp":
                multisample_samples = int(arg)
            elif opt == "--cached":
                use_cached = True
            elif opt == "--make-cache":
                create_cache = True
            elif opt == "--compress-cache":
                cache_should_compress = True
            elif opt == "--cache-float32":
                cache_float_type = torch.float32
            elif opt == "--tile-half-stride":
                tile_half_stride = True
            elif opt == "--model":
                model_name = arg
            elif opt == "--start-params":
                starting_parameters = arg
            elif opt == "--start-epoch":
                start_epoch = int(arg)
            elif opt == "--test-splotches":
                test_splotches = True
            elif opt == "--validate-nan":
                validate_nan = True

output_directory = os.path.abspath(output_directory).replace("\\", "/")
if output_directory[-1] != "/":
    output_directory += "/"

if not os.path.exists(output_directory):
    os.makedirs(output_directory)

if tile_half_stride:
    tile_stride = tile_size // 2
else:
    tile_stride = tile_size

#%%

nn_device = 'cuda' # Can't do cuda with so little memory
#nn_device = 'cpu'


#%%
"""Model Base"""

def softshrink_negative(input, lambd=1):
    mx = torch.maximum(input, torch.tensor(0))
    mn = torch.minimum(input, torch.tensor(-lambd)) + lambd
    return mx + mn

def softstairs(input, lambd=1):
    lambd = torch.tensor(lambd)
    mn = torch.minimum(input, -lambd)
    mx = torch.minimum(torch.maximum(input, torch.tensor(0)), lambd)
    mm = torch.maximum(input, 2*lambd)-lambd
    return mn + mx + mm

def normalize_channels(x):
    m = torch.amax(torch.abs(x), (2,3), keepdim=True)
    m = torch.maximum(m, torch.tensor(1e-8, device=m.device))
    return torch.div(x, m)


class HairNetBase(DeviceAwareModule):
    def __init__(self, *, loss_function=None):
        super(HairNetBase, self).__init__()

        if loss_function:
            self.loss_function = loss_function
        else:
            self.loss_function = torch.nn.MSELoss()

        self.optimizer = None
        self.lr_scheduler = None

    def forward_start(self):
        if self.optimizer is None:
            self._init_optimizer()

    def forward(self, x):
        self.forward_start()
        return x

    def output_names(self):
        return ["x"]

    def named_outputs(self, outputs):
        return {k: v for k,v in zip(self.output_names(), outputs)}

    def get_kwparams(self, data):
        return self.get_forward_input_kwparams_from_batch_dict(data)

    def get_forward_input_kwparams_from_batch_dict(self, data, *, shuffle=True):
        xs = data['input']

        if shuffle:
            shuf = random.randrange(len(xs))
        else:
            shuf = 0

        x = xs[shuf]

        return self.get_forward_input_kwparams(x)


    def forward_batch_dict(self, *params, shuffle=True):
        dic_params = self.get_forward_input_kwparams_from_batch_dict(*params, shuffle=shuffle)

        return self.named_outputs(self.forward(**dic_params))

    def get_forward_input_kwparams(self, data, *p):
        return data

    def forward_dict(self, data, *params):
        dic_params = self.get_forward_input_kwparams(data, *params)

        return self.forward(**dic_params)



    def _init_optimizer(self):
        self.optimizer = torch.optim.Adam(self.parameters(), lr=1e-4)
        if(use_lr_decay):
            #self.lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(self.optimizer, factor=0.25, patience=10, threshold=1e-2, eps=1e-5, verbose=True)
            self.lr_scheduler = torch.optim.lr_scheduler.StepLR(self.optimizer, step_size=100, gamma=0.5)

    def loss(self, b_input, b_target):
        generated = self.forward(b_input)
        return self.loss_function(generated, b_target)
    
    def _loss_named(self, evaled, target):
        total_channels = 0
        for t in evaled.values():
            total_channels += t.size(1)
        l = 0
        for k in evaled.keys():
            l += self.loss_function(target[k], evaled[k]) * target[k].size(1) / total_channels
        return l

    def save_to_file(self, fname, extra_values_dict={}):
        with torch.no_grad():
            dic = {'model': self.state_dict(), 'optim': self.optimizer.state_dict(), 'lr_scheduler': self.lr_scheduler.state_dict() if self.lr_scheduler else None}
            dic.update(extra_values_dict)
            torch.save(dic, fname)

    def load_from_file(self, fname):
        dic = torch.load(fname)
        self.load_state_dict(dic.pop('model'))
        if self.optimizer is None:
            self._init_optimizer()
        self.optimizer.load_state_dict(dic.pop('optim'))
        if 'lr_scheduler' in dic and dic['lr_scheduler']:
            self.lr_scheduler.load_state_dict(dic.pop('lr_scheduler'))
        self.eval()
        return dic

    def export_for_cpp(self, fname):
        import json
        import base64
        import struct
        def tensor_to_b64(t):
            b = bytearray()
            def subtensor_append(t, arr):
                if len(t.size()) == 1:
                    for i in range(t.size(0)):
                        arr.extend(struct.pack('<f', t[i].item()))
                else:
                    for i in range(t.size(0)):
                        subtensor_append(t[i], arr)
            subtensor_append(t, b)
            return base64.encodebytes(b).decode('utf-8')

        def tensor_to_b64_dict(t):
            d = dict()
            d['shape'] = t.size()
            d['values'] = tensor_to_b64(t)
            return d

        with torch.no_grad():
            out = dict()
            sd = self.state_dict()
            for k in sd:
                if type(sd[k]) == torch.Tensor:
                    if sd[k].dim() > 0:
                        out[k] = tensor_to_b64_dict(sd[k])
                    else:
                        out[k] = sd[k].item()
                else:
                    out[k] = sd[k]
            with open(fname, 'w') as f:
                json.dump(out, f)

    def _on_device_changed(self, device):
        self._init_optimizer()


#%%
"""Model"""

class HairNetMultisampleBase(HairNetBase):
    def __init__(self, *, n_frames=3, **kwargs):
        super(HairNetMultisampleBase, self).__init__(**kwargs)

        # {color(4) + specular(4) + depth(4) + tangents_xy(2 * 4) + alpha(4)} = 24
        self.input_channels = 0
        if 'color' in network_input_channels:
            self.input_channels += multisample_samples
        if 'specular' in network_input_channels:
            self.input_channels += multisample_samples
        if 'alpha' in network_input_channels:
            self.input_channels += multisample_samples
        if 'depth' in network_input_channels:
            self.input_channels += multisample_samples
        if 'tangents' in network_input_channels:
            self.input_channels += 2 * multisample_samples
        # {color(1) + specular(1)} + alpha(1)}
        self.output_channels = 3

        self.n_frames = n_frames

        self.color_gradient_loss_multiplier = 0.25

        self.init_network()

    def init_network(self):
        raise NotImplementedError()

    def loss(self, batch, shuffle=True):
        target = {k: v.clone().detach() for k,v in batch['target'][0].items()}
        # Maybe training with premultiplied alpha might give better results?
        # Should try with the premultiplied as extra channels or as a replacement for the output
        def premultiply(data):
            data['color'] = torch.mul(data['color'], data['alpha'])
            data['specular'] = torch.mul(data['specular'], data['alpha'])

        def stencil(t):
            sz = t.size()
            left = self.padding_stencil_sizes[0]+2
            right = sz[2] - self.padding_stencil_sizes[2]-3
            top = self.padding_stencil_sizes[1]+2
            bottom = sz[3] - self.padding_stencil_sizes[3]-3
            return t[:, :, left:right, top:bottom]

        def stencil_all(td):
            return {k: stencil(t) for k, t in td.items()}

        target['color'] = target['color'] * 2
        target['color'].detach_()

        premultiply(target)

        xs = batch['input']
        shuffled = list(range(len(xs)))
        if shuffle:
            shuf = random.sample(shuffled, len(shuffled))
        else:
            shuf = shuffled

        # Color gradient
        def color_gradient(data):
            cat = torch.cat([data['color'], data['specular'], data['alpha']], dim=1)
            dx = cat[:, :, 1:, :-1] - cat[:, :, :-1, :-1]
            dy = cat[:, :, :-1, 1:] - cat[:, :, :-1, :-1]
            return torch.cat([dx, dy], dim=1)

        target_gradient = color_gradient(target)

        target_cut = stencil_all(target)

        xl = [xs[shuf[i]] for i in range(multisample_samples)]
        # cat the inputs
        x = {}
        for k in xl[0].keys():
            x[k] = torch.cat([l[k] for l in xl], dim=1)

        generated = self.named_outputs(self.forward_dict(x))

        premultiply(generated)
        generated_cut = stencil_all(generated)
        loss = self._loss_named(generated_cut, target_cut)

        ev_gradient = color_gradient(generated)
        ev_gradient_cut = stencil(ev_gradient)
        target_gradient_cut = stencil(target_gradient)
        loss += self.color_gradient_loss_multiplier * self.loss_function(ev_gradient_cut, target_gradient_cut)

        return loss

    def get_forward_input_kwparams_from_batch_dict(self, data, shuffle=True):
        xs = data['input']

        shuffled = list(range(len(xs)))
        if shuffle:
            shuf = random.sample(shuffled, len(shuffled))
        else:
            shuf = shuffled

        xl = [xs[shuf[i]] for i in range(multisample_samples)]
        # cat the inputs
        x = {}
        for k in xl[0].keys():
            x[k] = torch.cat([l[k] for l in xl], dim=1)

        return self.get_forward_input_kwparams(x)

    def get_forward_input_kwparams(self, data):
        color = data['color'] * 2
        depth = data['depth']
        specular = data['specular']
        tangents = data['tangents']
        alpha = data['alpha']

        return {"color": color,
                "specular": specular,
                "depth": depth,
                "tangents": tangents,
                "alpha": alpha}

    def forward(self, color, specular, depth, tangents, alpha):
        return self.forward_multisample(color, specular, depth, tangents, alpha)

    def forward_multisample(self, color, specular, depth, tangents, alpha):
        self.forward_start()
        global network_input_channels

        # Bring the tangents from [0,1] to [-1, 1]
        tangents = torch.add(torch.mul(tangents, 2), -1)

        x = []
        if 'color' in network_input_channels:
            x.append(color)
        if 'specular' in network_input_channels:
            x.append(specular)
        if 'alpha' in network_input_channels:
            x.append(alpha)
        if 'depth' in network_input_channels:
            x.append(depth)
        if 'tangents' in network_input_channels:
            x.append(tangents)

        x = torch.cat(x, dim=1)

        x = self.forward_tensor(x)

        color = x[:, 0:1, :, :]
        specular = x[:, 1:2, :, :]
        alpha = x[:, 2:3, :, :]

        # Let's apply the clampings that will be used in the real-time evaluation
        # We use the soft functions to let the model have negative outputs to help it converge
        # but during evaluation we can just clip it to 0
        if self.training:
            alpha = softstairs(alpha)
            specular = softshrink_negative(specular)
        else:
            alpha = 1-nnF.relu(1-nnF.relu(alpha))
            specular = nnF.relu(specular)

        color = torch.pow(color, 3)/32.0 + color/5.0 + 1

        return color, specular, alpha

    def forward_tensor(self, x):
        raise NotImplementedError()

    def output_names(self):
        return ["color", "specular", "alpha"]


class HairNetMultisampleWithSkip0(HairNetMultisampleBase):
    def __init__(self, *args, **kwargs):
        self.skip_0_size = 0
        if 'color' in network_input_channels:
            self.skip_0_size += 1
        if 'specular' in network_input_channels:
            self.skip_0_size += 1
        if 'alpha' in network_input_channels:
            self.skip_0_size += 1
        
        super(HairNetMultisampleWithSkip0, self).__init__(*args, **kwargs)

    def init_network(self):
        self.conv1 = nn.Conv2d(self.input_channels, 32, 3, padding=1, stride=2)
        self.conv2 = nn.Conv2d(32, 32, 3, padding=1, stride=2)
        self.conv3 = nn.Conv2d(32, 64, 3, padding=1, stride=2)
        self.deconv3 = nn.ConvTranspose2d(64, 32, 3, padding=1, stride=2, output_padding=1)
        #self.deconv2 = nn.ConvTranspose2d(32+32, 32, 3, padding=1, stride=2, output_padding=1)
        #self.deconv1 = nn.ConvTranspose2d(32+32, 24 - self.skip_0_size, 3, padding=1, stride=2, output_padding=1)
        self.deconv2 = nn.ConvTranspose2d(32+32, 32, 3, padding=1, stride=2, output_padding=1)
        self.deconv1 = nn.ConvTranspose2d(32+32, 16, 3, padding=1, stride=2, output_padding=1)
        self.autoconv = nn.Conv2d(16, self.output_channels, 3, padding=1)

        self.padding_stencil_sizes = [9, 9, 8, 8]

    def forward_tensor(self, x):

        # Take one sample of the parts that are similar to the output
        #skip0 = x[:, 0:(self.skip_0_size * multisample_samples):multisample_samples, :, :]
        x = nnF.relu(self.conv1(x))
        skip1 = x[:, :, :, :]
        x = nnF.relu(self.conv2(x))
        skip2 = x[:, :, :, :]
        x = nnF.relu(self.conv3(x))

        x = torch.cat([nnF.relu(self.deconv3(x)), skip2], dim=1)
        x = torch.cat([nnF.relu(self.deconv2(x)), skip1], dim=1)
        
        #x = torch.cat([nnF.relu(self.deconv1(x)), skip0], dim=1)
        x = nnF.relu(self.deconv1(x))
        x = self.autoconv(x)

        return x

class HairNetMultisampleTiny(HairNetMultisampleBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def init_network(self):
        self.conv1 = nn.Conv2d(self.input_channels, 16, 3, padding=1, stride=2)
        self.conv2 = nn.Conv2d(16, 32, 3, padding=1, stride=2)
        self.conv3 = nn.Conv2d(32, 64, 3, padding=1, stride=2)
        self.deconv3 = nn.ConvTranspose2d(64, 32, 3, padding=1, stride=2, output_padding=1)
        self.deconv2 = nn.ConvTranspose2d(32+32, 16, 3, padding=1, stride=2, output_padding=1)
        self.deconv1 = nn.ConvTranspose2d(16+16, self.output_channels, 3, padding=1, stride=2, output_padding=1)

        self.padding_stencil_sizes = [8, 8, 7, 7]

    def forward_tensor(self, x):

        # Take one sample of the parts that are similar to the output
        x = nnF.relu(self.conv1(x))
        skip1 = x[:, :, :, :]
        x = nnF.relu(self.conv2(x))
        skip2 = x[:, :, :, :]
        x = nnF.relu(self.conv3(x))

        x = torch.cat([nnF.relu(self.deconv3(x)), skip2], dim=1)
        x = torch.cat([nnF.relu(self.deconv2(x)), skip1], dim=1)
        x = self.deconv1(x)

        return x

class HairNetMultisampleMedium(HairNetMultisampleBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def init_network(self):
        self.conv1 = nn.Conv2d(self.input_channels, 32, 3, padding=1, stride=2)
        self.conv2 = nn.Conv2d(32, 64, 3, padding=1, stride=2)
        self.conv3 = nn.Conv2d(64, 128, 3, padding=1, stride=2)
        self.deconv3 = nn.ConvTranspose2d(128, 64, 3, padding=1, stride=2, output_padding=1)
        self.deconv2 = nn.ConvTranspose2d(64+64, 32, 3, padding=1, stride=2, output_padding=1)
        self.deconv1 = nn.ConvTranspose2d(32+32, self.output_channels, 3, padding=1, stride=2, output_padding=1)

        self.padding_stencil_sizes = [8, 8, 7, 7]

    def forward_tensor(self, x):

        # Take one sample of the parts that are similar to the output
        x = nnF.relu(self.conv1(x))
        skip1 = x[:, :, :, :]
        x = nnF.relu(self.conv2(x))
        skip2 = x[:, :, :, :]
        x = nnF.relu(self.conv3(x))

        x = torch.cat([nnF.relu(self.deconv3(x)), skip2], dim=1)
        x = torch.cat([nnF.relu(self.deconv2(x)), skip1], dim=1)
        x = self.deconv1(x)

        return x

class HairNetMultisampleLarge(HairNetMultisampleBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def init_network(self):
        self.conv1 = nn.Conv2d(self.input_channels, 64, 3, padding=1, stride=2)
        self.conv2 = nn.Conv2d(64, 128, 3, padding=1, stride=2)
        self.conv3 = nn.Conv2d(128, 256, 3, padding=1, stride=2)
        self.deconv3 = nn.ConvTranspose2d(256, 128, 3, padding=1, stride=2, output_padding=1)
        self.deconv2 = nn.ConvTranspose2d(128+128, 64, 3, padding=1, stride=2, output_padding=1)
        self.deconv1 = nn.ConvTranspose2d(64+64, self.output_channels, 3, padding=1, stride=2, output_padding=1)

        self.padding_stencil_sizes = [8, 8, 7, 7]

    def forward_tensor(self, x):

        # Take one sample of the parts that are similar to the output
        x = nnF.relu(self.conv1(x))
        skip1 = x[:, :, :, :]
        x = nnF.relu(self.conv2(x))
        skip2 = x[:, :, :, :]
        x = nnF.relu(self.conv3(x))

        x = torch.cat([nnF.relu(self.deconv3(x)), skip2], dim=1)
        x = torch.cat([nnF.relu(self.deconv2(x)), skip1], dim=1)
        x = self.deconv1(x)

        return x

class HairNetMultisample2step(HairNetMultisampleBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def init_network(self):
        self.conv1 = nn.Conv2d(self.input_channels, 32, 3, padding=1, stride=2)
        self.conv2 = nn.Conv2d(32, 64, 3, padding=1, stride=2)
        self.deconv2 = nn.ConvTranspose2d(64, 32, 3, padding=1, stride=2, output_padding=1)
        self.deconv1 = nn.ConvTranspose2d(32+32, self.output_channels, 3, padding=1, stride=2, output_padding=1)

        self.padding_stencil_sizes = [4, 4, 3, 3]

    def forward_tensor(self, x):

        # Take one sample of the parts that are similar to the output
        x = nnF.relu(self.conv1(x))
        skip1 = x[:, :, :, :]
        x = nnF.relu(self.conv2(x))

        x = torch.cat([nnF.relu(self.deconv2(x)), skip1], dim=1)
        x = self.deconv1(x)

        return x

class HairNetMultisample4step(HairNetMultisampleBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def init_network(self):
        self.conv1 = nn.Conv2d(self.input_channels, 32, 3, padding=1, stride=2)
        self.conv2 = nn.Conv2d(32, 64, 3, padding=1, stride=2)
        self.conv3 = nn.Conv2d(64, 128, 3, padding=1, stride=2)
        self.conv4 = nn.Conv2d(128, 256, 3, padding=1, stride=2)
        self.deconv4 = nn.ConvTranspose2d(256, 128, 3, padding=1, stride=2, output_padding=1)
        self.deconv3 = nn.ConvTranspose2d(128+128, 64, 3, padding=1, stride=2, output_padding=1)
        self.deconv2 = nn.ConvTranspose2d(64+64, 32, 3, padding=1, stride=2, output_padding=1)
        self.deconv1 = nn.ConvTranspose2d(32+32, self.output_channels, 3, padding=1, stride=2, output_padding=1)

        self.padding_stencil_sizes = [16, 16, 15, 15]

    def forward_tensor(self, x):

        # Take one sample of the parts that are similar to the output
        x = nnF.relu(self.conv1(x))
        skip1 = x[:, :, :, :]
        x = nnF.relu(self.conv2(x))
        skip2 = x[:, :, :, :]
        x = nnF.relu(self.conv3(x))
        skip3 = x[:, :, :, :]
        x = nnF.relu(self.conv4(x))

        x = torch.cat([nnF.relu(self.deconv4(x)), skip3], dim=1)
        x = torch.cat([nnF.relu(self.deconv3(x)), skip2], dim=1)
        x = torch.cat([nnF.relu(self.deconv2(x)), skip1], dim=1)
        x = self.deconv1(x)

        return x

class HairNetMultisampleHugeAutoconv(HairNetMultisampleBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def init_network(self):
        self.conv1 = nn.Conv2d(self.input_channels, 32, 3, padding=1, stride=2)
        self.conv2 = nn.Conv2d(32, 64, 3, padding=1, stride=2)
        self.conv3 = nn.Conv2d(64, 128, 3, padding=1, stride=2)
        self.conv4 = nn.Conv2d(128, 128, 3, padding=1, stride=2)
        self.deconv4 = nn.ConvTranspose2d(128, 128, 3, padding=1, stride=2, output_padding=1)
        self.deconv3 = nn.ConvTranspose2d(128+128, 64, 3, padding=1, stride=2, output_padding=1)
        self.deconv2 = nn.ConvTranspose2d(64+64, 32, 3, padding=1, stride=2, output_padding=1)
        self.deconv1 = nn.ConvTranspose2d(32+32, 32, 3, padding=1, stride=2, output_padding=1)
        self.autoconv = nn.Conv2d(32, 3, 3, padding=1, stride=1)

        self.padding_stencil_sizes = [17, 17, 16, 16]

    def forward_tensor(self, x):

        # Take one sample of the parts that are similar to the output
        x = nnF.relu(self.conv1(x))
        skip1 = x[:, :, :, :]
        x = nnF.relu(self.conv2(x))
        skip2 = x[:, :, :, :]
        x = nnF.relu(self.conv3(x))
        skip3 = x[:, :, :, :]
        x = nnF.relu(self.conv4(x))

        x = torch.cat([nnF.relu(self.deconv4(x)), skip3], dim=1)
        x = torch.cat([nnF.relu(self.deconv3(x)), skip2], dim=1)
        x = torch.cat([nnF.relu(self.deconv2(x)), skip1], dim=1)
        x = nnF.relu(self.deconv1(x))
        x = self.autoconv(x)

        return x



#%%
"""Create the dataset objects"""

from hair_dataset import *
from torch.utils.data import DataLoader

images_pattern = r'$name$variant$component$sample\.png'

input_variants = ['_a', '_b']
target_variants = ''

input_components = ['', '_tan']
target_components = ['']

input_samples = ['_'+str(i) for i in range(0,multisample_samples)]
target_samples = ''

spp = {'input': multisample_samples, 'target': 1}

#validation_variants = ['_a', '_b', '_c', '_d', '_e']

# Channels are:
# 1 for stochastic color and specular
# 2 for stochastic tangent x and y, 1 for stochastic alpha
# 1 for multisampled color, 1 for multisampled alpha, 1 for multisampled specular
input_channels = [("color", 1), ("specular", 1), ("depth", 1), ("alpha-redundant", 1),
                  ("tangents", 2), ("tangent-depth-skipped", 1), ("alpha", 1)]

target_channels = [("color", 1), ("alpha", 1), ("specular", 1), ("alpha-redundant", 1)]

input_regexes = cartesian_format( images_pattern,
                                    name=r'[0-9]+',
                                    variant=input_variants,
                                    sample=input_samples,
                                    component=input_components)
target_regexes = cartesian_format( images_pattern,
                                    name=r'[0-9]+',
                                    variant=target_variants,
                                    sample=target_samples,
                                    component=target_components)

dataset = HairDataset(training_source,
            subdir={'input': 'input', 'target': 'target'},
            regex={'input': input_regexes, 'target': target_regexes},
            channels={'input': input_channels, 'target': target_channels},
            spp=spp,
            device=nn_device)

train_loader = DataLoader(dataset, batch_size=1, shuffle=True, drop_last=False)
train_tiler = TiledDataset(train_loader, validity_channel_path=("input", "alpha"), tile_size=tile_size, tile_stride=tile_stride)

if use_cached:
    train_tiler = CachedTiledHairDataset(os.path.join(training_source, "train_cache"), device=nn_device)

train_shuffled = BufferedShuffleDataset(train_tiler, 128)

train_data = DataLoader(train_shuffled, batch_size=24, drop_last=False)

validation_loader = DataLoader(
                            HairDataset(training_source,
                                        subdir={'input': 'validation_input', 'target': 'validation_target'},
                                        regex={'input': input_regexes, 'target': target_regexes},
                                        channels={'input': input_channels, 'target': target_channels},
                                        spp=spp,
                                        randomise_samples=False,
                                        device=nn_device
                                        ),
                            batch_size=1,
                            shuffle=False,
                            drop_last=False
                            )

validation_tiler = TiledDataset( validation_loader, validity_channel_path=("input", "alpha"), tile_size=tile_size, tile_stride=tile_stride)

if use_cached:
    validation_tiler = CachedTiledHairDataset(os.path.join(training_source, "validation_cache"), device=nn_device)

validation_data = DataLoader(validation_tiler, batch_size=24)


test_frame = '[0-9]+' # There should be only one sample in this directory

eval_input_regexes = cartesian_format( images_pattern,
                                    name=test_frame,
                                    variant=input_variants,
                                    sample=input_samples,
                                    component=input_components)

eval_target_regexes = cartesian_format( images_pattern,
                                    name=test_frame,
                                    variant=target_variants,
                                    sample=target_samples,
                                    component=target_components)

eval_data = DataLoader(
                HairDataset(training_source,
                        subdir={'input': 'evaluation_input', 'target': 'evaluation_target'},
                        regex={'input': eval_input_regexes, 'target': eval_target_regexes},
                        channels={'input': input_channels, 'target': target_channels},
                        spp=spp,
                        randomise_samples=False,
                        device=nn_device
                        ),
                batch_size=1, shuffle=False, drop_last=False
                )

if create_cache:
    import create_tiling
    def build_cache(cache_dir, data_loader, tiler):

        if not os.path.exists(cache_dir):
            os.makedirs(cache_dir)

        channels = {
            "input": [("color", 1), ("specular", 1), ("depth", 1), ("tangents", 2), ("alpha", 1)],
            "target": [("color", 1), ("alpha", 1), ("specular", 1)]
        }

        for i, b in enumerate(data_loader):
            print("File {}".format(i))
            tfs = create_tiling.TilesFileSaver(channels, (tile_size, tile_size), multisamples={"input": 4, "target": 1}, should_compress=cache_should_compress, float_type=cache_float_type)

            for t in tiler.tile(b):
                tfs.add_tile(t)

            tfs.save(cache_dir, "{}.tds".format(i))

    build_cache(os.path.join(training_source, "train_cache"), train_loader, train_tiler)
    build_cache(os.path.join(training_source, "validation_cache"), validation_loader, validation_tiler)
    exit(0)

#%%
"""Create the model"""
if network_input_channels is None:
    from inspect import getfullargspec
    args = getfullargspec(HairNetMultisampleBase.forward)
    network_input_channels = set(args.args[1:])

#torch.use_deterministic_algorithms(True)
# 'small', 'medium', 'large', '2step', '3step', '4step'

if model_name == 'tiny':
    model = HairNetMultisampleTiny()
elif model_name == 'medium':
    model = HairNetMultisampleMedium()
elif model_name == 'large':
    model = HairNetMultisampleLarge()
elif model_name == '2step':
    model = HairNetMultisample2step()
elif model_name == '3step':
    model = HairNetMultisampleMedium()
elif model_name == '4step':
    model = HairNetMultisample4step()
else:
    model = HairNetMultisampleMedium()

model.to(nn_device)

if starting_parameters:
    model.load_from_file(starting_parameters)
    model.lr_scheduler = torch.optim.lr_scheduler.StepLR(model.optimizer, step_size=100, gamma=0.5, last_epoch=start_epoch if start_epoch > 0 else -1)

#%%
def export_for_cpp(fname):
    model.load_from_file(fname + ".pth")
    model.export_for_cpp(fname + ".json")

#export_for_cpp("results_210803/epoch_40_model")

# %%
"""Debug"""
import train_fn
import matplotlib.pyplot as plt

eval_item = get_dataset_item(eval_data)
#dt = get_dataset_item(eval_data)['target']

#%%
"""
tiled_item = get_dataset_item(train_data)
if not os.path.exists("results_0_test/"):
    os.makedirs("results_0_test/")
train_fn.write_reference_images(tiled_item, "results_0_test/", 0, "_0")
train_fn.write_reference_images(tiled_item, "results_0_test/", 1, "_1")
train_fn.write_reference_images(tiled_item, "results_0_test/", 2, "_2")
train_fn.write_reference_images(tiled_item, "results_0_test/", 3, "_3")
"""

#%%
""" Just to re-create images that were evaled with the old reference image"""
"""
import train_fn
import re

#directory = 'results_batched_210908/results_csta/'
directory = output_directory

updated_dir = os.path.join(directory, "updated/")
if not os.path.exists(updated_dir):
    os.makedirs(updated_dir)

rgx  = re.compile(r"epoch_([0-9]+)_model\.pth")

highest_n = 0
highest = None

#files = []
for f in os.scandir(directory):
    match = rgx.fullmatch(f.name)
    if f.is_file() and match:
        n = int(match.group(1))
        if highest_n < n:
            highest = f.name
            highest_n = n
        #files.append((f.name, match.group(1)))

train_fn.write_reference_images(eval_item, updated_dir)

def output_eval(f, n):
    print(n)
    model = HairNetMultisampleHuge()
    model.to(nn_device)
    model.load_from_file(os.path.join(directory, f))
    train_fn.write_eval_model(model, eval_item, updated_dir, 'evaled_{}.png'.format(n))

#for f, n in files:
#    output_eval(f, n)
if highest is not None:
    output_eval(highest, highest_n)


exit()
#"""

if validate_nan:
    def test_data(d):
        n_batches = int((d.dataset.count() + d.batch_size - 1) / d.batch_size)
        print("0/{}".format(n_batches), end="")
        for i, b in enumerate(d):
            print("\r{}/{}".format(i, n_batches), end="")
            for k, block in b.items():
                for s in block:
                    for c, t in s.items():
                        if torch.any(torch.isnan(t)):
                            print ("Found a NaN!")
                            exit()
                        elif torch.any(torch.isinf(t)):
                            print ("Found a Inf!")
                            exit()
    test_data(train_data)
    test_data(validation_data)
    exit()


if test_splotches:
    import train_fn
    eval_data = DataLoader(
                    HairDataset(training_source,
                            subdir={'input': 'input', 'target': 'target'},
                            regex={'input': input_regexes, 'target': target_regexes},
                            channels={'input': input_channels, 'target': target_channels},
                            spp=spp,
                            randomise_samples=False,
                            device=nn_device
                            ),
                    batch_size=1, shuffle=False, drop_last=False
                    )
    output_directory += "splotch_test/"
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    i = 1
    for b in eval_data:
        train_fn.write_eval_model(model, b, output_directory, "{}.png".format(i))
        i += 1

    exit()

#%%
"""This is to export the images for each step of training. It reimplements the `forward` function of the network, so needs changing to work if that changes."""
"""
testing_output_differences = True
use_eval_data_for_testing = True

if testing_output_differences:
    import imageio
    model.load_from_file("results_rand_light_dist_16_32_64_64_32s32_16s16/epoch_90_model.pth")
    test_path = "test_diff/"

    if not os.path.exists(test_path):
        os.makedirs(test_path)

    def export_tensor_as_images(step_name, path, tensor):
        ms = []
        for i in range(tensor.size(1)):
            im = tensor[0, i:i+1, :, :]
            m = im.min()
            M = im.max()
            ms.append((m, M))
            d = (M - m)
            if abs(d) < 1e-8:
                d = 1e-8
            im = (im - m) / d
            im = im.mul(torch.tensor(255)).to(torch.uint8).permute(1,2,0).squeeze(0).cpu().detach().numpy()
            imageio.imwrite(os.path.join(path, "{}_{}.png".format(step_name, i)), im)
        return step_name, ms

    norm = []

    if use_eval_data_for_testing:
        xs = model.get_forward_input_kwparams_from_batch_dict(eval_item, shuffle=False)

        x = []
        if 'color' in network_input_channels:
            x.append(xs['color'])
        if 'specular' in network_input_channels:
            x.append(xs['specular'])
        if 'alpha' in network_input_channels:
            x.append(xs['alpha'])
        if 'depth' in network_input_channels:
            x.append(xs['depth'])
        if 'tangents' in network_input_channels:
            tangents = torch.add(torch.mul(xs['tangents'], 2), -1)
            x.append(tangents)

        x = torch.cat(x, dim=1)

        r = export_tensor_as_images("input", test_path, x)
        with open(os.path.join(test_path, "norm_input.txt"), "w") as f:
            f.write("{}:\n{}\n".format(r[0], "\n".join(["\t{}: {} .. {}".format(i, m[0], m[1]) for i, m in enumerate(r[1])])))
        exit()
    else:
        x = torch.zeros(1, model.input_channels, 1024, 1024, device=nn_device)

    # Copy of the forward function
    x = nnF.relu(model.conv1(x))
    skip1 = x[:, :, :, :]

    r = export_tensor_as_images("0_conv1", test_path, x)
    norm.append(r)

    x = nnF.relu(model.conv2(x))
    skip2 = x[:, :, :, :]

    r = export_tensor_as_images("1_conv2", test_path, x)
    norm.append(r)

    x = nnF.relu(model.conv3(x))

    r = export_tensor_as_images("2_conv3", test_path, x)
    norm.append(r)
    #x = nnF.relu(self.conv4(x))

    #x = nnF.relu(self.deconv4(x))
    x = nnF.relu(model.deconv3(x))

    r = export_tensor_as_images("3_deconv3", test_path, x)
    norm.append(r)

    x = torch.cat([x, skip2], dim=1)

    x = nnF.relu(model.deconv2(x))

    r = export_tensor_as_images("4_deconv2", test_path, x)
    norm.append(r)

    x = torch.cat([x, skip1], dim=1)

    #x = torch.cat([nnF.relu(self.deconv1(x)), skip0], dim=1)
    x = model.deconv1(x)

    r = export_tensor_as_images("5_deconv1", test_path, x)
    norm.append(r)

    with open(os.path.join(test_path, "norm.txt"), "w") as f:
        for r in norm:
            f.write("{}:\n{}\n".format(r[0], "\n".join(["\t{}: {} .. {}".format(i, m[0], m[1]) for i, m in enumerate(r[1])])))

    exit()
#"""

#%%
"""Train"""

import train_fn

train_fn.Log.init(out_dir=output_directory)

import datetime
train_fn.Log.print("{}".format(datetime.datetime.now().isoformat()))
train_fn.Log.print("Training {} for channels {} with {} spp for {} epochs".format(str(type(model)), network_input_channels, multisample_samples, num_epochs))
train_fn.Log.print("Writing output to {}".format(output_directory))

train_fn.train(model, train_data, eval_data, validation_data, start_epoch=start_epoch, num_epochs=num_epochs, output_directory=output_directory)



# %%
