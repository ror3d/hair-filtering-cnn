
#%%
"""Interrupt handler"""
import signal
import sys

_user_interrupt_signaled = False
_user_interrupt_handler_setup = False

def setup_interrupt_signal_handler():
    def interrupt_signal_handler(sig, frame):
        global _user_interrupt_signaled
        print("\n\033[1;31m[  User interrupt received, finishing processing...  ]\033[0m")
        _user_interrupt_signaled = True
        signal.signal(signal.SIGINT, signal.SIG_DFL)
    global _user_interrupt_signaled
    _user_interrupt_signaled = False
    signal.signal(signal.SIGINT, interrupt_signal_handler)
    global _user_interrupt_handler_setup
    _user_interrupt_handler_setup = True

def iterate_forever(start=0):
    global _user_interrupt_handler_setup
    if not _user_interrupt_handler_setup:
        setup_interrupt_signal_handler()
    global _user_interrupt_signaled
    _user_interrupt_signaled = False
    i = start
    while True:
        yield i
        if _user_interrupt_signaled:
            break
        i += 1

def iterate_range(r):
    global _user_interrupt_handler_setup
    if not _user_interrupt_handler_setup:
        setup_interrupt_signal_handler()
    global _user_interrupt_signaled
    _user_interrupt_signaled = False
    for i in r:
        yield i
        if _user_interrupt_signaled:
            break


def is_iteration_interrupted():
    global _user_interrupt_signaled
    return _user_interrupt_signaled

