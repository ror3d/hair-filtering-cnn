#include "error_handling.cuh"
#include "layers.cuh"
#include <cublas_v2.h>
#include <cuda_fp16.h>
#include <mutex>
#include <GL/glew.h>
#include <cuda_gl_interop.h>
#include <utils/perf.h>
#include <glm/glm.hpp>


#define USE_ADAM 1

#define MAX_TEXTURES 6

const int BATCH_SIZE = 32;

using namespace std;

#define ADAM_T_VAR s_epoch

#if defined( __CUDA_ARCH__ ) && defined( __CUDACC__ ) && __CUDA_ARCH__ < 530
#error "Only works with CUDA architecture 5.3 and above"
#endif

template<typename _F>
static __device__ __inline__ bool is_nan( _F f );

template<>
static __device__ __inline__ bool is_nan<float>( float f )
{
	return isnan( f );
}

template<>
static __device__ __inline__ bool is_nan<half>( half f )
{
	return __hisnan( f );
}

namespace dnn
{
cublasHandle_t cublas_handle = nullptr;
cudnnHandle_t cudnn_handle = nullptr;

void initializeCUDA()
{
	if ( !dnn::cudnn_handle )
	{
		checkCudaErr( cudaSetDevice( 0 ) );
		int device;
		struct cudaDeviceProp device_properties;
		checkCudaErr( cudaGetDevice( &device ) );
		checkCudaErr( cudaGetDeviceProperties( &device_properties, device ) );
		// cout << device << ": " << device_properties.name << ":" << endl;
		// cout << "=========================================\n";
		// cout << "Compute capability: " << device_properties.major << "." << device_properties.minor << endl;
		// cout << "Concurrent copy and kernel execution: " << (device_properties.deviceOverlap ? "Yes" : "No") << " with "
		//	<< device_properties.asyncEngineCount << " copy engine(s)\n";
		// cout << "=========================================\n";
		///////////////////////////////////////////////////////////////////////
		// Init cudnn and cublas
		///////////////////////////////////////////////////////////////////////
		checkCudnnErr( cudnnCreate( &dnn::cudnn_handle ) );
		checkCublasErr( cublasCreate( &dnn::cublas_handle ) );
	}
}

typedef glm::vec<3, nnfloat, glm::highp> nnvec3;
typedef glm::mat<3, 3, nnfloat, glm::highp> nnmat3;

#if defined( __CUDACC__ ) && __CUDA_ARCH__ >= 530
static __device__ __inline__ __half hpow( const __half b, const __half e )
{
	return hexp( hlog( b ) * e );
}
#else
static __device__ __inline__ __half hpow( const __half b, const __half e )
{
	return expf( logf( float( b ) ) * float( e ) );
}
#endif

uint64_t s_epoch;
uint64_t s_batch;

tensor_t ones_tensor;

struct NNWorkspace
{
	void* memory = nullptr;
	size_t size = 0;

private:
	std::mutex mutex;
	size_t refs = 0;

public:
	void addRef()
	{
		std::unique_lock<std::mutex> lock( mutex );
		refs += 1;
	}

	void setSize( size_t required_size )
	{
		std::unique_lock<std::mutex> lock( mutex );
		if ( required_size > size )
		{
			if ( memory )
			{
				cudaFree( memory );
			}
			checkCudaErr( cudaMalloc( &memory, required_size ) );
			size = required_size;
		}
	}

	void remRef()
	{
		std::unique_lock<std::mutex> lock( mutex );
		refs -= 1;
		if ( refs == 0 )
		{
			if ( memory )
			{
				cudaFree( memory );
				memory = nullptr;
			}
			size = 0;
		}
	}
};

/////////////////////////////////////////////////////////////////////////////
// Shared Workspace
/////////////////////////////////////////////////////////////////////////////
static NNWorkspace s_shared_workspace;


/////////////////////////////////////////////////////////////////////////////
// Smart Layer functions
/////////////////////////////////////////////////////////////////////////////
void smart_layer_t::connectInput( smart_layer_t* t )
{
	input_connection = t;
	t->_connectOutput( this );
}

void smart_layer_t::updateInputAndOutputDims()
{
	tensor_dimension prev_input_dim = input_dim;
	tensor_dimension prev_output_dim = output_dim;
	if ( !input_connection )
	{
		CUDA_FATAL_ERROR( "A layer always needs an input! Use the network_input_layer struct as the beginning of the "
		                  "network." );
	}

	input_dim = input_connection->getOutputDim();

	updateOutputDim();
	if ( input_dim != prev_input_dim || output_dim != prev_output_dim )
	{
		dimensions_dirty = true;
	}
}

bool smart_layer_t::updateInputAndOutputTensors()
{
	bool mode_changed = mode_dirty;
	mode_dirty = false;
	if ( !dimensions_dirty )
	{
		return mode_changed;
	}
	dimensions_dirty = false;

	std::optional<tensor_view_t> out_tensor;

	for ( auto c : output_connections )
	{
		std::optional<tensor_view_t> tmp = c->getOutputTensorForInputConnection( this );
		if ( tmp.has_value() )
		{
			if ( out_tensor.has_value() )
			{
				CUDA_FATAL_ERROR( "More than one output is trying to set an output tensor for us!" );
			}
			std::swap( tmp, out_tensor );
		}
	}

	this->output_tensor = {};
	if ( out_tensor.has_value() )
	{
		output_tensor_view = out_tensor.value();
	}
	else
	{
		output_tensor.emplace( output_dim );
		output_tensor_view = tensor_view_t( output_tensor.value(), output_dim );
	}

	for ( auto c : output_connections )
	{
		c->_setInputTensorFromInputConnection( this, output_tensor_view );
	}

	return true;
}


/////////////////////////////////////////////////////////////////////////////
// Input Layer functions
/////////////////////////////////////////////////////////////////////////////
void network_input_layer::updateInputAndOutputDims()
{
	tensor_dimension prev_input_dim = input_dim;
	tensor_dimension prev_output_dim = output_dim;
	input_dim = input_tensor_view.dim();
	updateOutputDim();
	if ( input_dim != prev_input_dim || output_dim != prev_output_dim )
	{
		dimensions_dirty = true;
	}
}

void network_input_layer::updateOutputDim()
{
	output_dim = input_dim;
}

void network_input_layer::forward()
{
	size_t count = input_dim.size();
	cudaMemcpy( output_tensor_view.data(), input_tensor_view.data(), count * sizeof( nnfloat ), cudaMemcpyDeviceToDevice );
}

void network_input_layer::backward()
{
	if ( input_tensor_view.has_gradient() )
	{
		// Copy the gradient back
		size_t count = input_dim.size();
		cudaMemcpy( input_tensor_view.grad(), output_tensor_view.grad(), count * sizeof( nnfloat ), cudaMemcpyDeviceToDevice );
	}
}

void network_input_layer::connectInput( smart_layer_t* t )
{
	CUDA_FATAL_ERROR( "Cannot connect input to a network input!" );
}

/////////////////////////////////////////////////////////////////////////////
// Recurrent Input Layer functions
/////////////////////////////////////////////////////////////////////////////

void network_input_recurrent_layer::updateOutputDim()
{
	output_dim = input_dim;
	output_dim.c = n_channels;
}

bool network_input_recurrent_layer::updateInputAndOutputTensors()
{
	updateInputAndOutputDims();

	bool mode_changed = mode_dirty;
	mode_dirty = false;
	if ( !dimensions_dirty )
	{
		return mode_changed;
	}
	dimensions_dirty = false;

	output_tensor.emplace( output_dim );
	output_tensor_view = tensor_view_t( output_tensor.value(), output_dim );

	for ( auto c : output_connections )
	{
		c->_setInputTensorFromInputConnection( this, output_tensor_view );
	}
	return true;
}

void network_input_recurrent_layer::forward()
{
	if ( input_dim.c != n_channels )
	{
		CUDA_FATAL_ERROR( "Number of channels doesn't match!" );
	}

	CUDA_FATAL_ERROR( "Not implemented for NHWC" );

	//size_t count = input_dim.size();
	//cudaMemcpy( output_tensor_view.data(), input_tensor_view.data(), count * sizeof( nnfloat ), cudaMemcpyHostToDevice );
}

void network_input_recurrent_layer::backward()
{
	// Need to copy the stuff back
	CUDA_FATAL_ERROR( "Not implemented for NHWC" );

	// Copy the gradient back
	//size_t count = input_dim.size();
	//cudaMemcpy( input_tensor_view.grad(), output_tensor_view.grad(), count * sizeof( nnfloat ), cudaMemcpyDeviceToDevice );
}

/////////////////////////////////////////////////////////////////////////////
// Image Input Layer functions
/////////////////////////////////////////////////////////////////////////////
void network_input_image_layer::connectInput( smart_layer_t* t )
{
	CUDA_FATAL_ERROR( "Cannot connect input to a network input!" );
}

void network_input_image_layer::setInputImagesSize( int width, int height )
{
	tensor_dimension prev_input_dim = input_dim;
	input_dim.w = width;
	input_dim.h = height;
	input_dim.c = gl_textures.size() * 4;
	input_dim.n = 1;
	if ( input_dim != prev_input_dim )
	{
		dimensions_dirty = true;
	}
}

void network_input_image_layer::setInputImage( const std::vector<glm::vec4>& pixels )
{
	CUDA_FATAL_ERROR( "Not implemented for multiple images" );
	if ( dimensions_dirty )
	{
		CUDA_FATAL_ERROR( "You need to first update the networks tensors!" );
	}
	/*
	gl_texture = 0;

	std::vector<nnfloat> tdata( input_dim.size() );
	for ( size_t c = 0; c < input_dim.c; ++c )
	{
		for ( size_t y = 0; y < input_dim.h; ++y )
		{
			for ( size_t x = 0; x < input_dim.w; ++x )
			{
				size_t src_idx = y * input_dim.w + x;
				size_t tgt_idx = c * input_dim.w * input_dim.w + y * input_dim.w + x;

				tdata.data()[tgt_idx] = pixels[src_idx][c];
			}
		}
	}
	size_t count = input_dim.size();
	cudaMemcpy( output_tensor_view.data(), tdata.data(), count * sizeof( nnfloat ), cudaMemcpyHostToDevice );
	*/
}

void network_input_image_layer::setInputImages( std::vector<uint32_t> textures )
{
	//////////////////////////////////////////////////////////////////////////////
	// Clear old
	//////////////////////////////////////////////////////////////////////////////
	for ( size_t i = 0; i < gl_textures.size(); ++i )
	{
		if ( resources[i] != nullptr )
		{
			cudaDestroyTextureObject( texture_objects[i] );
			cudaGraphicsUnregisterResource( resources[i] );
		}
	}
	texture_objects.clear();
	resources.clear();


	gl_textures = textures;

	//////////////////////////////////////////////////////////////////////////////
	// Verify 4 channel, 16 bits
	//////////////////////////////////////////////////////////////////////////////
	for ( size_t i = 0; i < gl_textures.size(); ++i )
	{
		GLint format;
		glGetTextureLevelParameteriv(GLuint(gl_textures[i]), 0, GL_TEXTURE_INTERNAL_FORMAT, &format);

		// TODO: Verify
	}

	//////////////////////////////////////////////////////////////////////////////
	// Register
	//////////////////////////////////////////////////////////////////////////////
	resources.resize( gl_textures.size(), nullptr );
	texture_objects.resize( gl_textures.size() );

	for ( size_t i = 0; i < gl_textures.size(); ++i )
	{
		cudaArray_t cuda_array;

		checkCudaErr( cudaGraphicsGLRegisterImage( &(resources[i]), gl_textures[i], GL_TEXTURE_2D, cudaGraphicsRegisterFlagsReadOnly ) );

		checkCudaErr( cudaGraphicsMapResources( 1, &(resources[i]) ) );

		checkCudaErr( cudaGraphicsSubResourceGetMappedArray( &cuda_array, resources[i], 0, 0 ) );

		cudaResourceDesc cuda_array_resource_desc;
		memset( &cuda_array_resource_desc, 0, sizeof( cudaResourceDesc ) );
		cuda_array_resource_desc.resType = cudaResourceTypeArray;
		cuda_array_resource_desc.res.array.array = cuda_array;
		// Specify texture object parameters
		struct cudaTextureDesc texDesc;
		memset( &texDesc, 0, sizeof( texDesc ) );
		texDesc.addressMode[0] = cudaAddressModeClamp;
		texDesc.addressMode[1] = cudaAddressModeClamp;
		texDesc.filterMode = cudaFilterModePoint;
		texDesc.readMode = cudaReadModeElementType;
		texDesc.normalizedCoords = 0;
		// texDesc.normalizedCoords = 0;

		checkCudaErr( cudaCreateTextureObject( &(texture_objects[i]), &cuda_array_resource_desc, &texDesc, NULL ) );

		checkCudaErr( cudaGraphicsUnmapResources( 1, &(resources[i]) ) );
	}

	tensor_dimension prev_input_dim = input_dim;
	input_dim.c = 4 * (gl_textures.size() + padding_images);
	if ( input_dim != prev_input_dim )
	{
		dimensions_dirty = true;
	}
}

void network_input_image_layer::addPaddingImages( int n )
{
	padding_images = n;

	tensor_dimension prev_input_dim = input_dim;
	input_dim.c = 4 * (gl_textures.size() + padding_images);
	if ( input_dim != prev_input_dim )
	{
		dimensions_dirty = true;
	}
}

void network_input_image_layer::updateInputAndOutputDims()
{
	tensor_dimension prev_output_dim = output_dim;
	updateOutputDim();
	if ( output_dim != prev_output_dim )
	{
		dimensions_dirty = true;
	}
}

void network_input_image_layer::updateOutputDim()
{
	output_dim = input_dim;
}

//template<typename T>
//__global__ void copyTextureToTensor_kernel(
//        T* tensor, cudaTextureObject_t image, uint32_t width, uint32_t height, uint32_t channel_start, uint32_t channel_stride )
//{
//	int tx = blockIdx.x * blockDim.x + threadIdx.x;
//	int ty = blockIdx.y * blockDim.y + threadIdx.y;
//	if (tx >= width || ty >= height) return;
//	//float4 color = tex2D<float4>(image, tx, height - ty - 1);
//	//if (color.x == 1.123) tensor[0] = T(1); 
//	tensor[channel_start + 0 + channel_stride * (ty * width + tx)] = T(1.0f);
//	//tensor[channel_start + 0 + channel_stride * (ty * width + tx)] = T(color.x);
//	//tensor[channel_start + 1 + channel_stride * (ty * width + tx)] = T(color.y);
//	//tensor[channel_start + 2 + channel_stride * (ty * width + tx)] = T(color.z);
//	//tensor[channel_start + 3 + channel_stride * (ty * width + tx)] = T(color.w);
//}

__constant__ cudaTextureObject_t c_tbo[MAX_TEXTURES];

template<typename T>
__global__ void copyTextureToTensor_kernel(
	T* tensor, uint32_t width, uint32_t height, uint32_t num_channels, uint32_t num_pad_channels)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x; 
	if (tid >= width * height * num_channels) return;

	int channel = tid % num_channels; 
	int texture = channel / 4; 
	int pixel = tid / num_channels; 
	int x = pixel % width; 
	int y = pixel / width; 
	float4 color = tex2D<float4>(c_tbo[texture], x, height - 1 - y);
	float c = 
		(channel % 4 == 0) ? color.x :
		(channel % 4 == 1) ? color.y :
		(channel % 4 == 2) ? color.z : color.w;
	int out_p = (y * width + x) * (num_channels + num_pad_channels) + channel;
	tensor[out_p] = T(c);
}

void network_input_image_layer::forward()
{
	chag::perf::Scope s("network_input_image_layer::forward()");
	//////////////////////////////////////////////////////////////////////////////
	// Start one thread per output value in tensor, to coalesce the writes, 
	// and hope that texture caches are enough to make reads fast enough. 
	//////////////////////////////////////////////////////////////////////////////
	{
		chag::perf::Scope s("map");
		checkCudaErr(cudaGraphicsMapResources(resources.size(), resources.data()));
	}

	{
		chag::perf::Scope s("kernel");

		cudaMemcpyToSymbol(c_tbo, (void*)texture_objects.data(), texture_objects.size() * sizeof(cudaTextureObject_t), 0);

		dim3 block = { 256 };
		dim3 grid = { (output_tensor_view.dim().w * output_tensor_view.dim().h * output_tensor_view.dim().c - 1u) / 256 + 1 };
		copyTextureToTensor_kernel<<<grid, block>>>( (nnfloat*)output_tensor_view.data(),
		                                             output_tensor_view.dim().w,
		                                             output_tensor_view.dim().h,
		                                             output_tensor_view.dim().c - padding_images * 4,
		                                             padding_images * 4 );
	}

	{
		chag::perf::Scope s("unmap");
		checkCudaErr(cudaGraphicsUnmapResources(resources.size(), resources.data()));
	}
}

bool network_input_image_layer::updateInputAndOutputTensors()
{
	if ( smart_layer_t::updateInputAndOutputTensors() )
	{
		if ( gl_textures.size() == 0 && !output_tensor.has_value() )
		{
			output_tensor.emplace( output_dim );
			output_tensor_view = tensor_view_t( output_tensor.value(), output_dim );
		}
		for ( auto c : output_connections )
		{
			c->_setInputTensorFromInputConnection( this, output_tensor_view );
		}
		return true;
	}
	return false;
}



/////////////////////////////////////////////////////////////////////////////
// Network output image layer
/////////////////////////////////////////////////////////////////////////////

void network_output_image_layer::setOutputImagesSize( int width, int height )
{
	tensor_dimension prev_output_dim = output_dim;
	output_dim.w = width;
	output_dim.h = height;
	if ( output_dim != prev_output_dim )
	{
		dimensions_dirty = true;
	}
}

void network_output_image_layer::setOutputImages( std::vector<uint32_t> textures )
{
	//////////////////////////////////////////////////////////////////////////////
	// Clear old
	//////////////////////////////////////////////////////////////////////////////
	for ( size_t i = 0; i < gl_textures.size(); ++i )
	{
		if ( resources[i] != nullptr )
		{
			cudaDestroySurfaceObject( surface_objects[i] );
			cudaGraphicsUnregisterResource( resources[i] );
		}
	}
	surface_objects.clear();
	resources.clear();


	gl_textures = textures;

	//////////////////////////////////////////////////////////////////////////////
	// Verify 4 channel, 16 bits
	//////////////////////////////////////////////////////////////////////////////
	for ( size_t i = 0; i < gl_textures.size(); ++i )
	{
		GLint format;
		glGetTextureLevelParameteriv(GLuint(gl_textures[i]), 0, GL_TEXTURE_INTERNAL_FORMAT, &format);

		// TODO: Verify
	}

	//////////////////////////////////////////////////////////////////////////////
	// Register
	//////////////////////////////////////////////////////////////////////////////
	resources.resize( gl_textures.size(), nullptr );
	surface_objects.resize( gl_textures.size() );

	for ( size_t i = 0; i < gl_textures.size(); ++i )
	{
		cudaArray_t cuda_array;
		checkCudaErr( cudaGraphicsGLRegisterImage(
		        &( resources[i] ), gl_textures[i], GL_TEXTURE_2D, cudaGraphicsRegisterFlagsSurfaceLoadStore ) );
		checkCudaErr( cudaGraphicsMapResources( 1, &( resources[i] ) ) );
		checkCudaErr( cudaGraphicsSubResourceGetMappedArray( &cuda_array, resources[i], 0, 0 ) );
		cudaResourceDesc cuda_array_resource_desc;
		memset( &cuda_array_resource_desc, 0, sizeof( cudaResourceDesc ) );
		cuda_array_resource_desc.resType = cudaResourceTypeArray;
		cuda_array_resource_desc.res.array.array = cuda_array;
		checkCudaErr( cudaCreateSurfaceObject( &( surface_objects[i] ), &cuda_array_resource_desc ) );
		checkCudaErr( cudaGraphicsUnmapResources( 1, &( resources[i] ) ) );
	}
}

void network_output_image_layer::updateOutputDim()
{
	if ( output_dim.w != input_dim.w || output_dim.h != input_dim.h )
	{
		CUDA_FATAL_ERROR( "Tensor dimension doesn't match output image size" );
	}
	output_dim = input_dim;
}



__global__ void copyTensorToSurfaceWithOffset_kernel( const __half* tensor,
                                            cudaSurfaceObject_t image,
                                            tensor_dimension_cu dim,
                                            tensor_dimension_cu stride,
                                            uint32_t channel_offset )
{
	int tx = blockIdx.x * blockDim.x + threadIdx.x;
	int ty = blockIdx.y * blockDim.y + threadIdx.y;
	if (tx >= dim.w || ty >= dim.h) return;
	const unsigned short one = __half_as_ushort( __float2half( 1.f ) );
	unsigned short color_v[4] = { one };

	int max_channel = min( dim.c, int(channel_offset + 4) );
	for ( int i = 0; channel_offset + i < max_channel; ++i )
	{
		color_v[i] = __half_as_ushort(tensor[(dim.h - ty - 1) * stride.h + tx * stride.w + (channel_offset + i) * stride.c]);
	}

	ushort4 color;
	color.x = color_v[0];
	color.y = color_v[1];
	color.z = color_v[2];
	color.w = color_v[3];

	surf2Dwrite(color, image, tx * sizeof(color), ty);
}


void network_output_image_layer::forward()
{
	tensor_dimension stride;
	stride.c = 1;
	stride.w = stride.c * output_dim.c;
	stride.h = stride.w * output_dim.w;
	stride.n = stride.h * output_dim.h;

	for ( size_t i = 0, c = 0; i < gl_textures.size() && c < output_dim.c; ++i, c += 4 )
	{
		checkCudaErr( cudaGraphicsMapResources( 1, &(resources[i]) ) );
		dim3 block = { 16, 16, 1 };
		dim3 grid = { (output_dim.w + block.x - 1) / block.x, (output_dim.h + block.y - 1) / block.y };

		copyTensorToSurfaceWithOffset_kernel<<<grid, block>>>(
		        (__half*)output_tensor_view.data(), surface_objects[i], output_tensor_view.dim()._cuda, stride._cuda, c );

		checkCudaErr( cudaGraphicsUnmapResources( 1, &(resources[i]) ) );
	}

}

std::optional<tensor_view_t> network_output_image_layer::getOutputTensorForInputConnection( const smart_layer_t* connection )
{
	return output_tensor_view;
}


/////////////////////////////////////////////////////////////////////////////
// Combined upsample and skip layer
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// Insert consecutive channels from one tensor into another
/////////////////////////////////////////////////////////////////////////////////

__global__ void insert_channels_kernel (
	nnfloat *from_layer,
	nnfloat *to_layer,
	tensor_dimension from_layer_dim,
	tensor_dimension to_layer_dim,
	int source_channel,
	int target_channel,
	int num_channels )
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if (tid >= to_layer_dim.h * to_layer_dim.w * num_channels) return;
	int pixel = tid / num_channels;
	int channel_idx = tid % num_channels;
	int from_channel = source_channel + channel_idx; 
	int to_channel = target_channel + channel_idx;
	//////////////////////////////////////////////////////////////////////////////
	// TODO: HACK: HARDCODED: Since the channels we have trained for are _not_ 
	//                        consecutive, need to hack here. 
	from_channel = source_channel + channel_idx * 4;
	//////////////////////////////////////////////////////////////////////////////
	//to_layer[pixel * to_layer_dim.c + to_channel] = from_layer[pixel * from_layer_dim.c + from_channel];
	}

void insert_channels_layer::forward()
{
	dim3 block = { 256 };
	dim3 grid = { (output_tensor_view.dim().w * output_tensor_view.dim().h * num_channels - 1u) / 256 + 1 };
	insert_channels_kernel << <grid, block >> > (
		(nnfloat *)from_layer->output_tensor_view.data(),
		(nnfloat *)output_tensor_view.data(),
		from_layer->output_tensor_view.dim(),
		output_tensor_view.dim(), 
		source_channel,
		target_channel,
		num_channels
		);
}

void insert_channels_layer::updateOutputDim() 
{
	/////////////////////////////////////////////////////////////////////////
	// The output tensor of this layer is _the same_ as the output
	// tensor of the next layer. 
	/////////////////////////////////////////////////////////////////////////
	output_dim = input_dim;
};


/////////////////////////////////////////////////////////////////////////////
// Renormalize tangents layer
/////////////////////////////////////////////////////////////////////////////

void renormalize_rgb_vectors::updateOutputDim()
{
	output_dim = input_dim;
}

__global__ void renormalize_vectors_from_rgb(
        const nnfloat* in_img, nnfloat* out_img, tensor_dimension_cu dim, uint32_t channel_start, uint32_t num_channels )
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	using namespace glm;
	size_t chan_stride = dim.w * dim.h;
	if ( tid >= chan_stride )
	{
		return;
	}

	for ( int i = channel_start; i < num_channels && i < dim.c; ++i )
	{
		out_img[tid * dim.c + i] = in_img[tid * dim.c + i] * nnfloat( 2.f ) - nnfloat( 1.f );
	}

	for ( int i = 0; i < channel_start && i < dim.c; ++i )
	{
		out_img[tid * dim.c + i] = in_img[tid * dim.c + i];
	}

	for ( int i = channel_start + num_channels; i < dim.c; ++i )
	{
		out_img[tid * dim.c + i] = in_img[tid * dim.c + i];
	}
}

void renormalize_rgb_vectors::forward()
{
	if ( input_dim.n > 1 )
	{
		CUDA_FATAL_ERROR( "Not implemented!" );
	}
	dim3 block = { 128, 1, 1 };
	dim3 grid = { ( input_dim.w * input_dim.h + block.x - 1 ) / block.x };
	renormalize_vectors_from_rgb<<<grid, block>>>(
	        (nnfloat*)input_tensor_view.data(), (nnfloat*)output_tensor_view.data(), input_dim._cuda, channel_start, num_channels );
	checkCudaErr( cudaGetLastError() );
}

/////////////////////////////////////////////////////////////////////////////
// Adam optimizer kernel
/////////////////////////////////////////////////////////////////////////////
__global__ void adam_update_parameters_kernel( int num_values,
                                               nnfloat* parameters,
                                               float* s,
                                               float* r,
                                               const nnfloat* g,
                                               const float beta_1,
                                               const float beta_2,
                                               const int t,
                                               const float step_size )
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if ( tid >= num_values )
		return;
#if USE_ADAM
	// Correct bias in first moment
	s[tid] = nnfloat( beta_1 * s[tid] + ( 1.0f - beta_1 ) ) * g[tid];
	r[tid] = nnfloat( beta_2 * r[tid] + ( 1.0f - beta_2 ) ) * g[tid] * g[tid];
	float shat = s[tid] / ( 1.0f - pow( beta_1, t + 1 ) );
	float rhat = r[tid] / ( 1.0f - pow( beta_2, t + 1 ) );
	const float epsilon = 1e-8f;
	float delta = -step_size * ( shat / ( sqrt( rhat ) + epsilon ) );
	// if (isnan(delta)) delta = 0.0f;
#else
	float delta = -step_size * g[tid];
#endif
	parameters[tid] = parameters[tid] + nnfloat( delta );
}

/////////////////////////////////////////////////////////////////////////////
// A convolution layer with square, odd-sized filter, no stride or dilation.
/////////////////////////////////////////////////////////////////////////////

convolution_layer::convolution_layer( int num_input_channels, int num_output_channels, int filter_size )
{
	init( num_input_channels,
	      num_output_channels,
	      filter_size,
	      { 1, 1 },
	      { ( filter_size - 1 ) / 2, ( filter_size - 1 ) / 2 },
	      { 1, 1 } );
}

convolution_layer::convolution_layer(
        int num_input_channels, int num_output_channels, int filter_size, int stride, int padding, int dilation )
{
	init( num_input_channels, num_output_channels, filter_size, { stride, stride }, { padding, padding }, { dilation, dilation } );
}

convolution_layer::convolution_layer(
        int num_input_channels, int num_output_channels, int filter_size, size2d stride, size2d padding, size2d dilation )
{
	init( num_input_channels, num_output_channels, filter_size, stride, padding, dilation );
}

convolution_layer::~convolution_layer()
{
	s_shared_workspace.remRef();
	if ( d_adam_s )
	{
		cudaFree( d_adam_s );
		d_adam_s = nullptr;
	}
	if ( d_adam_r )
	{
		cudaFree( d_adam_r );
		d_adam_r = nullptr;
	}
}

void convolution_layer::init(
        int num_input_channels, int num_output_channels, int filter_size, size2d stride, size2d padding, size2d dilation )
{
	// Create filter and bias with xavier random weights
	f.Resize( num_output_channels, num_input_channels, filter_size, filter_size );
	// float dist_size = sqrt(3.0f / float(f.dim.w * f.dim.h* f.dim.c));
	// cout << dist_size << "\n\n\n\n";
	float variance = 1.0f / float( f.dim.w * f.dim.h * f.dim.c );
	f.ResetRandom( variance );
	bias.Resize( 1, num_output_channels, 1, 1 );
	// Bias can safely be initialized to zero
	bias.ResetZero();

	// Adam optimizer init
	checkCudaErr( cudaMalloc( &d_adam_s, ( f.dim.size() + bias.dim.size() ) * sizeof( nnfloat ) ) );
	checkCudaErr( cudaMalloc( &d_adam_r, ( f.dim.size() + bias.dim.size() ) * sizeof( nnfloat ) ) );
	checkCudaErr( cudaMemset( d_adam_s, nnfloat( 0.f ), ( f.dim.size() + bias.dim.size() ) * sizeof( nnfloat ) ) );
	checkCudaErr( cudaMemset( d_adam_r, nnfloat( 0.f ), ( f.dim.size() + bias.dim.size() ) * sizeof( nnfloat ) ) );

#if DNN_HALF_FLOAT
	cudnnDataType_t cudt = CUDNN_DATA_HALF;
#else
	cudnnDataType_t cudt = CUDNN_DATA_FLOAT;
#endif
	// Create convolution descriptor
	checkCudnnErr( cudnnCreateConvolutionDescriptor( &convolution_descriptor ) );
	checkCudnnErr( cudnnSetConvolutionNdDescriptor(
	        convolution_descriptor, 2, padding.arr, stride.arr, dilation.arr, CUDNN_CROSS_CORRELATION, cudt ) );
	cudnnSetConvolutionMathType( convolution_descriptor, CUDNN_TENSOR_OP_MATH );
	s_shared_workspace.addRef();

	// Create Activation descriptor
	checkCudnnErr( cudnnCreateActivationDescriptor( &activation_descriptor ) );
	checkCudnnErr( cudnnSetActivationDescriptor( activation_descriptor, CUDNN_ACTIVATION_IDENTITY, CUDNN_NOT_PROPAGATE_NAN, 1.0 ) );

	output_dim.c = num_output_channels;
}

void convolution_layer::findAlgorithmsAndWorkspace()
{
	uint64_t required_workspace_size;
	{
		cudnnConvolutionFwdAlgoPerf_t perfResults;
		int nPerfResults = 0;
		/*
		checkCudnnErr( cudnnGetConvolutionForwardAlgorithm_v7( cudnn_handle,
		                                                       input_tensor_view.descriptor(),
		                                                       f.descriptor,
		                                                       convolution_descriptor,
		                                                       output_tensor_view.descriptor(),
		                                                       1,
		                                                       &nPerfResults,
		                                                       &perfResults ) );
		*/
		checkCudnnErr( cudnnFindConvolutionForwardAlgorithm( cudnn_handle,
															 input_tensor_view.descriptor(),
															 f.descriptor,
															 convolution_descriptor,
															 output_tensor_view.descriptor(),
															 1,
															 &nPerfResults,
															 &perfResults ) );
		if ( nPerfResults != 1 )
		{
			CUDA_FATAL_ERROR( "Error! unexpected value for nPerfResults" );
		}
		forward_algorithm = perfResults.algo;
	}
	checkCudnnErr( cudnnGetConvolutionForwardWorkspaceSize( cudnn_handle,
	                                                        input_tensor_view.descriptor(),
	                                                        f.descriptor,
	                                                        convolution_descriptor,
	                                                        output_tensor_view.descriptor(),
	                                                        forward_algorithm,
	                                                        &required_workspace_size ) );
	uint64_t new_workspace_size = required_workspace_size;

	if ( mode == Mode::Train )
	{
		{
			cudnnConvolutionBwdFilterAlgoPerf_t perfResults;
			int nPerfResults = 0;
			checkCudnnErr( cudnnGetConvolutionBackwardFilterAlgorithm_v7( cudnn_handle,
			                                                              input_tensor_view.descriptor(),
			                                                              output_tensor_view.descriptor(),
			                                                              convolution_descriptor,
			                                                              f.descriptor,
			                                                              1,
			                                                              &nPerfResults,
			                                                              &perfResults ) );
			if ( nPerfResults != 1 )
			{
				CUDA_FATAL_ERROR( "Error! unexpected value for nPerfResults" );
			}
			backward_filter_algorithm = perfResults.algo;
		}
		checkCudnnErr( cudnnGetConvolutionBackwardFilterWorkspaceSize( cudnn_handle,
		                                                               input_tensor_view.descriptor(),
		                                                               output_tensor_view.descriptor(),
		                                                               convolution_descriptor,
		                                                               f.descriptor,
		                                                               backward_filter_algorithm,
		                                                               &required_workspace_size ) );
		new_workspace_size = max( new_workspace_size, required_workspace_size );

		{
			cudnnConvolutionBwdDataAlgoPerf_t perfResults;
			int nPerfResults = 0;
			checkCudnnErr( cudnnGetConvolutionBackwardDataAlgorithm_v7( cudnn_handle,
			                                                            f.descriptor,
			                                                            output_tensor_view.descriptor(),
			                                                            convolution_descriptor,
			                                                            input_tensor_view.descriptor(),
			                                                            1,
			                                                            &nPerfResults,
			                                                            &perfResults ) );
			if ( nPerfResults != 1 )
			{
				CUDA_FATAL_ERROR( "Error! unexpected value for nPerfResults" );
			}
			backward_data_algorithm = perfResults.algo;
		}
		checkCudnnErr( cudnnGetConvolutionBackwardDataWorkspaceSize( cudnn_handle,
		                                                             f.descriptor,
		                                                             output_tensor_view.descriptor(),
		                                                             convolution_descriptor,
		                                                             input_tensor_view.descriptor(),
		                                                             backward_data_algorithm,
		                                                             &required_workspace_size ) );
		new_workspace_size = max( new_workspace_size, required_workspace_size );
	}

	s_shared_workspace.setSize( new_workspace_size );

	algorithm_dirty = false;
}

void convolution_layer::forward()
{
	if ( algorithm_dirty )
	{
		findAlgorithmsAndWorkspace();
	}

	checkCudnnErr( cudnnConvolutionBiasActivationForward( cudnn_handle,
	                                                      &alpha,
	                                                      input_tensor_view.descriptor(),
	                                                      input_tensor_view.data(),
	                                                      f.descriptor,
	                                                      f.d_data,
	                                                      convolution_descriptor,
	                                                      forward_algorithm,
	                                                      s_shared_workspace.memory,
	                                                      s_shared_workspace.size,
	                                                      &beta,
	                                                      output_tensor_view.descriptor(),
	                                                      output_tensor_view.data(),
	                                                      bias.descriptor,
	                                                      bias.d_data,
	                                                      activation_descriptor,
	                                                      output_tensor_view.descriptor(),
	                                                      output_tensor_view.data() ) );
}

void convolution_layer::backward()
{
	if ( mode != Mode::Train )
	{
		CUDA_FATAL_ERROR( "State not set to training, so backward pass will fail!" );
	}
	checkCudnnErr( cudnnConvolutionBackwardBias(
	        cudnn_handle, &alpha, output_tensor_view.descriptor(), output_tensor_view.grad(), &beta, bias.descriptor, bias.d_grad ) );
	checkCudnnErr( cudnnConvolutionBackwardFilter( cudnn_handle,
	                                               &alpha,
	                                               input_tensor_view.descriptor(),
	                                               input_tensor_view.data(),
	                                               output_tensor_view.descriptor(),
	                                               output_tensor_view.grad(),
	                                               convolution_descriptor,
	                                               backward_filter_algorithm,
	                                               s_shared_workspace.memory,
	                                               s_shared_workspace.size,
	                                               &beta,
	                                               f.descriptor,
	                                               f.d_grad ) );
	if ( input_tensor_view.has_gradient() )
	{
		checkCudnnErr( cudnnConvolutionBackwardData( cudnn_handle,
		                                             &alpha,
		                                             f.descriptor,
		                                             f.d_data,
		                                             output_tensor_view.descriptor(),
		                                             output_tensor_view.grad(),
		                                             convolution_descriptor,
		                                             backward_data_algorithm,
		                                             s_shared_workspace.memory,
		                                             s_shared_workspace.size,
		                                             &beta,
		                                             input_tensor_view.descriptor(),
		                                             input_tensor_view.grad() ) );
	}
}

void convolution_layer::updateWeights( float learning_rate )
{
	int num_weights = f.dim.size();
	int num_biases = bias.dim.size();
	{   // Update weights
		int threads = 256;
		int blocks = ( num_weights - 1 ) / threads + 1;
		adam_update_parameters_kernel<<<blocks, threads>>>(
		        num_weights, (nnfloat*)f.d_data, d_adam_s, d_adam_r, (nnfloat*)f.d_grad, adam_beta_1, adam_beta_2, ADAM_T_VAR, learning_rate );
	}
	{   // Update biases
		int threads = 256;
		int blocks = ( num_biases - 1 ) / threads + 1;
		adam_update_parameters_kernel<<<blocks, threads>>>( num_biases,
		                                                    (nnfloat*)bias.d_data,
		                                                    d_adam_s + num_weights,
		                                                    d_adam_r + num_weights,
		                                                    (nnfloat*)bias.d_grad,
		                                                    adam_beta_1,
		                                                    adam_beta_2,
		                                                    ADAM_T_VAR,
		                                                    learning_rate );
	}
}

bool convolution_layer::updateInputAndOutputTensors()
{
	if ( smart_layer_t::updateInputAndOutputTensors() )
	{
		algorithm_dirty = true;
		return true;
	}
	return false;
}

void convolution_layer::updateOutputDim()
{
	cudnnTensorDescriptor_t inputTensorDesc;
	checkCudnnErr( cudnnCreateTensorDescriptor( &inputTensorDesc ) );
#if DNN_HALF_FLOAT
	cudnnDataType_t cudt = CUDNN_DATA_HALF;
#else
	cudnnDataType_t cudt = CUDNN_DATA_FLOAT;
#endif

	checkCudnnErr( cudnnSetTensor4dDescriptor(
	        inputTensorDesc, CUDNN_TENSOR_NHWC, cudt, input_dim.n, filter_input_channels(), input_dim.h, input_dim.w ) );
	checkCudnnErr( cudnnGetConvolution2dForwardOutputDim(
	        convolution_descriptor, inputTensorDesc, f.descriptor, &output_dim.n, &output_dim.c, &output_dim.h, &output_dim.w ) );
	checkCudnnErr( cudnnDestroyTensorDescriptor( inputTensorDesc ) );
}

bool convolution_layer::checkBadFloat()
{
	bool a = f.CheckForBadFloat();
	bool b = bias.CheckForBadFloat();
	return a || b;
}

int convolution_layer::filter_input_channels() const
{
	return f.dim.c;
}

int convolution_layer::filter_output_channels() const
{
	return f.dim.n;
}


/////////////////////////////////////////////////////////////////////////////
// A transposed convolution (with bias) implemented as a convolution with 
// flipped forward/backward. 
/////////////////////////////////////////////////////////////////////////////
transpose_convolution_layer::transpose_convolution_layer(
	int num_input_channels, int num_output_channels, int filter_size, int stride, int padding, int dilation, bool ReLU)
{
	init(num_input_channels, num_output_channels, filter_size, { stride, stride }, { padding, padding }, { dilation, dilation }, ReLU);
	s_shared_workspace.addRef();
}
transpose_convolution_layer::~transpose_convolution_layer()
{
	s_shared_workspace.remRef();
}

void transpose_convolution_layer::init(
	int num_input_channels, int num_output_channels, int filter_size, size2d stride, size2d padding, size2d dilation, bool ReLU)
{
	// Since we are going to implement this using a convolution, we need to flip what is input and what is output
	std::swap(num_input_channels, num_output_channels);

	// Create filter and bias with xavier random weights
	f.Resize(num_output_channels, num_input_channels, filter_size, filter_size);
	float variance = 1.0f / float(f.dim.w * f.dim.h * f.dim.c);
	f.ResetRandom(variance);

	// Bias is added to the OUTPUT of the transposed convolution, which is the input of the convolution we are using
	bias.Resize(1, num_input_channels, 1, 1);
	// Bias can safely be initialized to zero
	bias.ResetZero();

	do_relu = ReLU; 

#if DNN_HALF_FLOAT
	cudnnDataType_t cudt = CUDNN_DATA_HALF;
#else
	cudnnDataType_t cudt = CUDNN_DATA_FLOAT;
#endif
	// Create convolution descriptor
	checkCudnnErr(cudnnCreateConvolutionDescriptor(&convolution_descriptor));
	checkCudnnErr(cudnnSetConvolutionNdDescriptor(
		convolution_descriptor, 2, padding.arr, stride.arr, dilation.arr, CUDNN_CROSS_CORRELATION, cudt));
	cudnnSetConvolutionMathType(convolution_descriptor, CUDNN_TENSOR_OP_MATH);

	output_dim.c = num_input_channels; 
}

void transpose_convolution_layer::findAlgorithmsAndWorkspace()
{
	uint64_t required_workspace_size;
	{
		cudnnConvolutionBwdDataAlgoPerf_t perfResults;
		int nPerfResults = 0;
		checkCudnnErr(cudnnGetConvolutionBackwardDataAlgorithm_v7(cudnn_handle,
			f.descriptor,
			input_tensor_view.descriptor(),
			convolution_descriptor,
			output_tensor_view.descriptor(),
			1,
			&nPerfResults,
			&perfResults));
		if (nPerfResults != 1) CUDA_FATAL_ERROR("Error! unexpected value for nPerfResults");
		backward_data_algorithm = perfResults.algo;
		backward_data_algorithm = CUDNN_CONVOLUTION_BWD_DATA_ALGO_1;
	}
	checkCudnnErr(cudnnGetConvolutionBackwardDataWorkspaceSize(cudnn_handle,
		f.descriptor,
		input_tensor_view.descriptor(),
		convolution_descriptor,
		output_tensor_view.descriptor(),
		backward_data_algorithm,
		&required_workspace_size));
	s_shared_workspace.setSize(required_workspace_size);
	algorithm_dirty = false;
}

template<typename T>
__global__ void bias_and_relu_kernel(T* output, T* bias, tensor_dimension dim, bool do_relu)
{
	const int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if (tid > dim.n * dim.c * dim.h * dim.w) return; 
	int channel = tid % dim.c; 
	if (do_relu) {
		output[tid] = T(max(float(output[tid]) + float(bias[channel]), 0.0f));
	}
	else {
		output[tid] = T(float(output[tid]) + float(bias[channel]));
	}
}

void transpose_convolution_layer::forward()
{
	if (algorithm_dirty) findAlgorithmsAndWorkspace();
	checkCudnnErr(
		cudnnConvolutionBackwardData(dnn::cudnn_handle,
			&alpha,
			f.descriptor,
			f.d_data,
			input_tensor_view.descriptor(),
			input_tensor_view.data(),
			convolution_descriptor,
			backward_data_algorithm,
			s_shared_workspace.memory,
			s_shared_workspace.size,
			&beta,
			output_tensor_view.descriptor(),
			output_tensor_view.data())
	);

	// TODO: Bias and relu
	dim3 block = { 256 };
	dim3 grid = { (output_dim.w * output_dim.h * output_dim.c * output_dim.n - 1) / block.x + 1 };
	bias_and_relu_kernel << <grid, block >> > ((dnn::nnfloat*)output_tensor_view.data(), (dnn::nnfloat*)bias.d_data, output_dim, do_relu);
}

void transpose_convolution_layer::updateInputAndOutputDims()
{
	smart_layer_t::updateInputAndOutputDims(); 
	
	//tensor_dimension prev_input_dim = input_dim;
	//tensor_dimension prev_output_dim = output_dim;
	//input_dim = input_connection->getOutputDim();
	//output_dim = { 1, output_dim.c, input_dim.h * 2, input_dim.w * 2 };
	//if (input_dim != prev_input_dim || output_dim != prev_output_dim)
	//{
	//	dimensions_dirty = true;
	//}
}

void transpose_convolution_layer::updateOutputDim()
{
	output_dim = { input_dim.n, output_dim.c, input_dim.h * 2, input_dim.w * 2 };
}

bool transpose_convolution_layer::updateInputAndOutputTensors()
{
	if (smart_layer_t::updateInputAndOutputTensors())
	{
		algorithm_dirty = true;
		return true;
	}
	return false;
}


/////////////////////////////////////////////////////////////////////////////
// A convolution layer with square, odd-sized filter, no stride or dilation.
/////////////////////////////////////////////////////////////////////////////

convbiasrelu_layer::convbiasrelu_layer( int num_input_channels, int num_output_channels, int filter_size )
{
	init( num_input_channels,
	      num_output_channels,
	      filter_size,
	      { 1, 1 },
	      { ( filter_size - 1 ) / 2, ( filter_size - 1 ) / 2 },
	      { 1, 1 } );
}

convbiasrelu_layer::convbiasrelu_layer(
        int num_input_channels, int num_output_channels, int filter_size, int stride, int padding, int dilation )
{
	init( num_input_channels, num_output_channels, filter_size, { stride, stride }, { padding, padding }, { dilation, dilation } );
}

convbiasrelu_layer::convbiasrelu_layer(
        int num_input_channels, int num_output_channels, int filter_size, size2d stride, size2d padding, size2d dilation )
{
	init( num_input_channels, num_output_channels, filter_size, stride, padding, dilation );
}

convbiasrelu_layer::~convbiasrelu_layer()
{
	s_shared_workspace.remRef();
}

void convbiasrelu_layer::backward()
{
	CUDA_FATAL_ERROR( "NOT IMPLEMENTED" );
}

void convbiasrelu_layer::updateWeights( float learning_rate )
{
	CUDA_FATAL_ERROR( "NOT IMPLEMENTED" );
}

int convbiasrelu_layer::filter_input_channels() const
{
	return f.dim.c;
}

int convbiasrelu_layer::filter_output_channels() const
{
	return f.dim.n;
}

void convbiasrelu_layer::init(
        int num_input_channels, int num_output_channels, int filter_size, size2d stride, size2d padding, size2d dilation )
{
	// Create filter and bias with xavier random weights
	f.Resize( num_output_channels, num_input_channels, filter_size, filter_size );
	float variance = 1.0f / float( f.dim.w * f.dim.h * f.dim.c );
	f.ResetRandom( variance );
	bias.Resize( 1, num_output_channels, 1, 1 );
	// Bias can safely be initialized to zero
	bias.ResetZero();

#if DNN_HALF_FLOAT
	cudnnDataType_t cudt = CUDNN_DATA_HALF;
#else
	cudnnDataType_t cudt = CUDNN_DATA_FLOAT;
#endif
	// Create convolution descriptor
	checkCudnnErr( cudnnCreateConvolutionDescriptor( &convolution_descriptor ) );
	checkCudnnErr( cudnnSetConvolutionNdDescriptor(
	        convolution_descriptor, 2, padding.arr, stride.arr, dilation.arr, CUDNN_CROSS_CORRELATION, cudt ) );
	cudnnSetConvolutionMathType( convolution_descriptor, CUDNN_TENSOR_OP_MATH );
	// Create Activation descriptor
	checkCudnnErr( cudnnCreateActivationDescriptor( &activation_descriptor ) );
	checkCudnnErr( cudnnSetActivationDescriptor( activation_descriptor, CUDNN_ACTIVATION_RELU, CUDNN_NOT_PROPAGATE_NAN, 1.0 ) );

	s_shared_workspace.addRef();

	output_dim.c = num_output_channels;
}

void convbiasrelu_layer::findAlgorithmsAndWorkspace()
{
	uint64_t required_workspace_size;
	{
		cudnnConvolutionFwdAlgoPerf_t perfResults;
		int nPerfResults = 0;
		/*
		checkCudnnErr( cudnnGetConvolutionForwardAlgorithm_v7( cudnn_handle,
		                                                       input_tensor_view.descriptor(),
		                                                       f.descriptor,
		                                                       convolution_descriptor,
		                                                       output_tensor_view.descriptor(),
		                                                       1,
		                                                       &nPerfResults,
		                                                       &perfResults ) );
															   */
		checkCudnnErr( cudnnFindConvolutionForwardAlgorithm( cudnn_handle,
		                                                     input_tensor_view.descriptor(),
		                                                     f.descriptor,
		                                                     convolution_descriptor,
		                                                     output_tensor_view.descriptor(),
		                                                     1,
		                                                     &nPerfResults,
		                                                     &perfResults ) );
		if ( nPerfResults != 1 )
			CUDA_FATAL_ERROR( "Error! unexpected value for nPerfResults" );
		forward_algorithm = perfResults.algo;
		forward_algorithm = CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_PRECOMP_GEMM;
	}
	checkCudnnErr( cudnnGetConvolutionForwardWorkspaceSize( cudnn_handle,
	                                                        input_tensor_view.descriptor(),
	                                                        f.descriptor,
	                                                        convolution_descriptor,
	                                                        output_tensor_view.descriptor(),
	                                                        forward_algorithm,
	                                                        &required_workspace_size ) );
	uint64_t new_workspace_size = required_workspace_size;
	s_shared_workspace.setSize( new_workspace_size );
	algorithm_dirty = false;
}

void convbiasrelu_layer::forward()
{
	if ( algorithm_dirty )
	{
		findAlgorithmsAndWorkspace();
	}
	checkCudnnErr( cudnnConvolutionBiasActivationForward( cudnn_handle,
	                                                      &alpha,
	                                                      input_tensor_view.descriptor(),
	                                                      input_tensor_view.data(),
	                                                      f.descriptor,
	                                                      f.d_data,
	                                                      convolution_descriptor,
	                                                      forward_algorithm,
	                                                      s_shared_workspace.memory,
	                                                      s_shared_workspace.size,
	                                                      &beta,
	                                                      output_tensor_view.descriptor(),
	                                                      output_tensor_view.data(),
	                                                      bias.descriptor,
	                                                      bias.d_data,
	                                                      activation_descriptor,
	                                                      output_tensor_view.descriptor(),
	                                                      output_tensor_view.data() ) );
}

bool convbiasrelu_layer::updateInputAndOutputTensors()
{
	if ( smart_layer_t::updateInputAndOutputTensors() )
	{
		algorithm_dirty = true;
		return true;
	}
	return false;
}

void convbiasrelu_layer::updateOutputDim()
{
#if DNN_HALF_FLOAT
	cudnnDataType_t cudt = CUDNN_DATA_HALF;
#else
	cudnnDataType_t cudt = CUDNN_DATA_FLOAT;
#endif
	if ( input_dim.c != f.dim.c )
	{
		LOG_FATAL( "Number of input channels doesn't match filter's input channels. {} was expected, but received {}.", f.dim.c, input_dim.c );
	}

	cudnnTensorDescriptor_t inputTensorDesc;
	checkCudnnErr( cudnnCreateTensorDescriptor( &inputTensorDesc ) );
	checkCudnnErr( cudnnSetTensor4dDescriptor(
	        inputTensorDesc, CUDNN_TENSOR_NHWC, cudt, input_dim.n, filter_input_channels(), input_dim.h, input_dim.w ) );
	checkCudnnErr( cudnnGetConvolution2dForwardOutputDim(
	        convolution_descriptor, inputTensorDesc, f.descriptor, &output_dim.n, &output_dim.c, &output_dim.h, &output_dim.w ) );
	checkCudnnErr( cudnnDestroyTensorDescriptor( inputTensorDesc ) );
}

bool convbiasrelu_layer::checkBadFloat()
{
	bool a = f.CheckForBadFloat();
	bool b = bias.CheckForBadFloat();
	return a || b;
}


/////////////////////////////////////////////////////////////////////////////
// A convolution layer with square, odd-sized filter, no stride or dilation.
/////////////////////////////////////////////////////////////////////////////

convbiasrelu_recurrent_layer::convbiasrelu_recurrent_layer( int num_input_channels,
                                                            int num_output_channels,
                                                            int num_recurrent_channels,
                                                            int filter_size )
{
	init( num_input_channels,
	      num_output_channels,
	      num_recurrent_channels,
	      filter_size,
	      { 1, 1 },
	      { ( filter_size - 1 ) / 2, ( filter_size - 1 ) / 2 },
	      { 1, 1 } );
}

convbiasrelu_recurrent_layer::convbiasrelu_recurrent_layer( int num_input_channels,
                                                            int num_output_channels,
                                                            int num_recurrent_channels,
                                                            int filter_size,
                                                            int stride,
                                                            int padding,
                                                            int dilation )
{
	init( num_input_channels,
	      num_output_channels,
	      num_recurrent_channels,
	      filter_size,
	      { stride, stride },
	      { padding, padding },
	      { dilation, dilation } );
}

void convbiasrelu_recurrent_layer::init( int num_input_channels,
                                         int num_output_channels,
                                         int num_recurrent_channels,
                                         int filter_size,
                                         size2d stride,
                                         size2d padding,
                                         size2d dilation )
{
	// Create filter and bias with xavier random weights
	f.Resize( num_output_channels + num_recurrent_channels, num_input_channels + num_recurrent_channels, filter_size, filter_size );
	float variance = 1.0f / float( f.dim.w * f.dim.h * f.dim.c );
	f.ResetRandom( variance );
	bias.Resize( 1, num_output_channels + num_recurrent_channels, 1, 1 );
	// Bias can safely be initialized to zero
	bias.ResetZero();

	// Apparently these are float regardless of whether we use fp16 or fp32
	bnScale.dtype = DType::Float32;
	bnBias.dtype = DType::Float32;
	bnMean.dtype = DType::Float32;
	bnVar.dtype = DType::Float32;
	bnScale.Resize( 1, num_output_channels + num_recurrent_channels, 1, 1 );
	bnBias.Resize( 1, num_output_channels + num_recurrent_channels, 1, 1 );
	bnMean.Resize( 1, num_output_channels + num_recurrent_channels, 1, 1 );
	bnVar.Resize( 1, num_output_channels + num_recurrent_channels, 1, 1 );

#if DNN_HALF_FLOAT
	cudnnDataType_t cudt = CUDNN_DATA_HALF;
#else
	cudnnDataType_t cudt = CUDNN_DATA_FLOAT;
#endif
	// Create convolution descriptor
	checkCudnnErr( cudnnCreateConvolutionDescriptor( &convolution_descriptor ) );
	checkCudnnErr( cudnnSetConvolutionNdDescriptor(
	        convolution_descriptor, 2, padding.arr, stride.arr, dilation.arr, CUDNN_CROSS_CORRELATION, cudt ) );
	cudnnSetConvolutionMathType( convolution_descriptor, CUDNN_TENSOR_OP_MATH );
	// Create Activation descriptor
	checkCudnnErr( cudnnCreateActivationDescriptor( &activation_descriptor ) );
	checkCudnnErr( cudnnSetActivationDescriptor( activation_descriptor, CUDNN_ACTIVATION_TANH, CUDNN_NOT_PROPAGATE_NAN, 1.0 ) );

	s_shared_workspace.addRef();

	n_recurrent_channels = num_recurrent_channels;
	output_dim.c = num_output_channels;
	true_input_dim.c = num_input_channels + num_recurrent_channels;
	true_output_dim.c = num_output_channels + num_recurrent_channels;
}

convbiasrelu_recurrent_layer::~convbiasrelu_recurrent_layer()
{
	s_shared_workspace.remRef();
}

bool convbiasrelu_recurrent_layer::updateInputAndOutputTensors()
{
	if ( smart_layer_t::updateInputAndOutputTensors() )
	{
		network_input_tensor.emplace( true_input_dim );
		input_tensor_view = tensor_view_t( network_input_tensor.value(), input_dim );
		network_output_tensor.emplace( true_output_dim );
		algorithm_dirty = true;
		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////
// Upsample Layer (simple nearest neighbour 2x2 upsampling)
/////////////////////////////////////////////////////////////////////////////
__global__ void upsample_nearest_kernel( nnfloat* d_output,
                                         tensor_dimension_cu output_dim,
                                         tensor_dimension_cu output_stride,
                                         tensor_dimension_cu output_storage_stride,
                                         const nnfloat* d_input,
                                         tensor_dimension_cu input_dim,
                                         tensor_dimension_cu input_storage_stride )
{
	const int num_output_values = output_dim.n * output_dim.c * output_dim.h * output_dim.w;
	const int thread_index = blockIdx.x * blockDim.x + threadIdx.x;
	if ( thread_index >= num_output_values )
	{
		return;
	}

	int batch = (thread_index / output_stride.n) % output_dim.n;
	int o_y = (thread_index / output_stride.h) % output_dim.h;
	int o_x = (thread_index / output_stride.w) % output_dim.w;
	int channel = (thread_index / output_stride.c) % output_dim.c;

	int i_x = ( o_x * input_dim.w ) / output_dim.w;
	int i_y = ( o_y * input_dim.h ) / output_dim.h;

	// Simple nearest neighbour

	// cmake-format off
	nnfloat value = d_input[batch * input_storage_stride.n
	                        + channel * input_storage_stride.c
	                        + i_y * input_storage_stride.h
	                        + i_x * input_storage_stride.w];
	if ( is_nan( value ) )
	{
		value = nnfloat( 0.f );
	}
	d_output[batch * output_storage_stride.n
	         + channel * output_storage_stride.c
	         + o_y * output_storage_stride.h
	         + o_x * output_storage_stride.w]
		= value;
	// cmake-format on
}

__global__ void copy_strided_tensor_kernel( nnfloat* out_tensor,
											tensor_dimension_cu out_storage_stride,
											const nnfloat* in_tensor,
											tensor_dimension_cu in_dim,
											tensor_dimension_cu in_stride,
											tensor_dimension_cu in_storage_stride );


void convbiasrelu_recurrent_layer::forward()
{
	if ( algorithm_dirty )
	{
		findAlgorithmsAndWorkspace();
	}

	tensor_view_t net_input_tensor_view( network_input_tensor.value(), true_input_dim );
	tensor_view_t net_output_tensor_view( network_output_tensor.value(), true_output_dim );

	tensor_dimension net_input_stride;
	net_input_stride.n = true_input_dim.h * true_input_dim.w * true_input_dim.c;
	net_input_stride.c = 1;
	net_input_stride.h = true_input_dim.w * true_input_dim.c;
	net_input_stride.w = true_input_dim.c;

	tensor_dimension net_output_stride;
	net_output_stride.n = true_output_dim.h * true_output_dim.w * true_output_dim.c;
	net_output_stride.c = 1;
	net_output_stride.h = true_output_dim.w * true_output_dim.c;
	net_output_stride.w = true_output_dim.c;

	// Copy the input tensor

	const int threads = 256;
	int blocks = (input_dim.size() + threads - 1) / threads;

	tensor_dimension in_stride;
	in_stride.n = input_dim.c * input_dim.h * input_dim.w;
	in_stride.c = 1;
	in_stride.h = input_dim.w * input_dim.c;
	in_stride.w = input_dim.c;

	copy_strided_tensor_kernel<<<blocks, threads>>> ((nnfloat*)net_input_tensor_view.data(),
													 net_input_stride._cuda,
													 (nnfloat*)input_tensor_view.data(),
													 input_dim._cuda,
													 in_stride._cuda,
													 in_stride._cuda );
	checkCudaErr( cudaGetLastError() );


	// Run the network
	checkCudnnErr( cudnnConvolutionBiasActivationForward( cudnn_handle,
	                                                      &alpha,
	                                                      net_input_tensor_view.descriptor(),
	                                                      net_input_tensor_view.data(),
	                                                      f.descriptor,
	                                                      f.d_data,
	                                                      convolution_descriptor,
	                                                      forward_algorithm,
	                                                      s_shared_workspace.memory,
	                                                      s_shared_workspace.size,
	                                                      &beta,
	                                                      net_output_tensor_view.descriptor(),
	                                                      net_output_tensor_view.data(),
	                                                      bias.descriptor,
	                                                      bias.d_data,
	                                                      activation_descriptor,
	                                                      net_output_tensor_view.descriptor(),
	                                                      net_output_tensor_view.data() ) );
	// Apply the BatchNorm
	/*
	if ( mode == Mode::Train )
	{
		CUDA_FATAL_ERROR( "NOT IMPLEMENTED, need to use the ForwardTraining functions here" );
	}
	checkCudnnErr( cudnnBatchNormalizationForwardInference( cudnn_handle,
															cudnnBatchNormMode_t::CUDNN_BATCHNORM_SPATIAL,
															&alpha,
															&beta,
															net_output_tensor_view.descriptor(),
															net_output_tensor_view.data(),
															net_output_tensor_view.descriptor(),
															net_output_tensor_view.data(),
															bnScale.descriptor,
															bnScale.d_data,
															bnBias.d_data,
															bnMean.d_data,
															bnVar.d_data,
															1e-5f ) );
	*/
	tensor_dimension hidden_input_dim( input_dim.n, n_recurrent_channels, input_dim.h, input_dim.w );
	tensor_dimension hidden_output_dim( output_dim.n, n_recurrent_channels, output_dim.h, output_dim.w );

	tensor_dimension hidden_input_stride;
	hidden_input_stride.n = hidden_input_dim.c * hidden_input_dim.h * hidden_input_dim.w;
	hidden_input_stride.c = 1;
	hidden_input_stride.h = hidden_input_dim.w * hidden_input_dim.c;
	hidden_input_stride.w = hidden_input_dim.c;

	size_t hidden_input_offset = true_input_dim.c - n_recurrent_channels;
	size_t hidden_output_offset = true_output_dim.c - n_recurrent_channels;

	// Upsample the recurrent channels back to the input
	blocks = ( hidden_input_dim.size() - 1 ) / threads + 1;
	upsample_nearest_kernel<<<blocks, threads>>>( (nnfloat*)net_input_tensor_view.data() + hidden_input_offset * net_input_stride.c,
	                                              hidden_input_dim._cuda,
												  hidden_input_stride._cuda,
												  net_input_stride._cuda,
	                                              (nnfloat*)net_output_tensor_view.data() + hidden_output_offset * net_output_stride.c,
	                                              hidden_output_dim._cuda,
												  net_output_stride._cuda);
	checkCudaErr( cudaGetLastError() );

	// Copy the output to the final output tensor
	blocks = (output_dim.size() + threads - 1) / threads;

	tensor_dimension output_stride;
	output_stride.n = output_dim.c * output_dim.h * output_dim.w;
	output_stride.c = 1;
	output_stride.h = output_dim.w * output_dim.c;
	output_stride.w = output_dim.c;

	copy_strided_tensor_kernel<<<blocks, threads>>> ((nnfloat*)output_tensor_view.data(),
													 output_stride._cuda,
													 (nnfloat*)net_output_tensor_view.data(),
													 output_dim._cuda,
													 output_stride._cuda,
													 net_output_stride._cuda);
	checkCudaErr( cudaGetLastError() );

}

bool convbiasrelu_recurrent_layer::checkBadFloat()
{
	bool a = f.CheckForBadFloat();
	bool b = bias.CheckForBadFloat();
	return a || b;
}

void convbiasrelu_recurrent_layer::resetRecurrentValues()
{
	network_input_tensor->ResetZero();
}

void convbiasrelu_recurrent_layer::backward()
{
	CUDA_FATAL_ERROR( "NOT IMPLEMENTED" );
}

void convbiasrelu_recurrent_layer::updateWeights( float learning_rate )
{
	CUDA_FATAL_ERROR( "NOT IMPLEMENTED" );
}

int convbiasrelu_recurrent_layer::filter_input_channels() const
{
	return f.dim.c;
}

int convbiasrelu_recurrent_layer::filter_output_channels() const
{
	return f.dim.n;
}

std::optional<tensor_view_t> convbiasrelu_recurrent_layer::getOutputTensorForInputConnection( const smart_layer_t* connection )
{
	return {};
}

void convbiasrelu_recurrent_layer::_setInputTensorFromInputConnection( const smart_layer_t* connection, tensor_view_t input )
{
	// For NHWC
	input_tensor_view = std::move( input );

	/*
	// For NCHW
	if ( input.get_underlying_tensor() != input_tensor_view.get_underlying_tensor() )
	{
		CUDA_FATAL_ERROR( "These two should be the same!" );
	}
	*/
}

void convbiasrelu_recurrent_layer::findAlgorithmsAndWorkspace()
{
	uint64_t required_workspace_size;
	{
		cudnnConvolutionFwdAlgoPerf_t perfResults;
		int nPerfResults = 0;
		checkCudnnErr( cudnnGetConvolutionForwardAlgorithm_v7( cudnn_handle,
		                                                       network_input_tensor.value().descriptor,
		                                                       f.descriptor,
		                                                       convolution_descriptor,
		                                                       network_output_tensor.value().descriptor,
		                                                       1,
		                                                       &nPerfResults,
		                                                       &perfResults ) );
		if ( nPerfResults != 1 )
			CUDA_FATAL_ERROR( "Error! unexpected value for nPerfResults" );
		forward_algorithm = perfResults.algo;
	}
	checkCudnnErr( cudnnGetConvolutionForwardWorkspaceSize( cudnn_handle,
	                                                        network_input_tensor.value().descriptor,
	                                                        f.descriptor,
	                                                        convolution_descriptor,
	                                                        network_output_tensor.value().descriptor,
	                                                        forward_algorithm,
	                                                        &required_workspace_size ) );
	uint64_t new_workspace_size = required_workspace_size;
	s_shared_workspace.setSize( new_workspace_size );
	algorithm_dirty = false;
}

void convbiasrelu_recurrent_layer::updateOutputDim()
{
	true_input_dim = input_dim;
	true_input_dim.c += n_recurrent_channels;

#if DNN_HALF_FLOAT
	cudnnDataType_t cudt = CUDNN_DATA_HALF;
#else
	cudnnDataType_t cudt = CUDNN_DATA_FLOAT;
#endif

	cudnnTensorDescriptor_t inputTensorDesc;
	checkCudnnErr( cudnnCreateTensorDescriptor( &inputTensorDesc ) );
	checkCudnnErr( cudnnSetTensor4dDescriptor( inputTensorDesc,
	                                           CUDNN_TENSOR_NHWC,
	                                           cudt,
	                                           true_input_dim.n,
	                                           filter_input_channels(),
	                                           true_input_dim.h,
	                                           true_input_dim.w ) );
	checkCudnnErr( cudnnGetConvolution2dForwardOutputDim( convolution_descriptor,
	                                                      inputTensorDesc,
	                                                      f.descriptor,
	                                                      &true_output_dim.n,
	                                                      &true_output_dim.c,
	                                                      &true_output_dim.h,
	                                                      &true_output_dim.w ) );
	checkCudnnErr( cudnnDestroyTensorDescriptor( inputTensorDesc ) );

	output_dim = true_output_dim;
	output_dim.c -= n_recurrent_channels;
}

/////////////////////////////////////////////////////////////////////////////
// Pooling layer
/////////////////////////////////////////////////////////////////////////////
void pool_layer::init()
{
	checkCudnnErr( cudnnCreatePoolingDescriptor( &pooling_descriptor ) );
	checkCudnnErr( cudnnSetPooling2dDescriptor( pooling_descriptor, CUDNN_POOLING_MAX, CUDNN_PROPAGATE_NAN, 2, 2, 0, 0, 2, 2 ) );
}

void pool_layer::forward()
{
	checkCudnnErr( cudnnPoolingForward( cudnn_handle,
	                                    pooling_descriptor,
	                                    &alpha,
	                                    input_tensor_view.descriptor(),
	                                    input_tensor_view.data(),
	                                    &beta,
	                                    output_tensor_view.descriptor(),
	                                    output_tensor_view.data() ) );
}

void pool_layer::backward()
{
	if ( mode != Mode::Train )
	{
		CUDA_FATAL_ERROR( "State not set to training!" );
	}
	if ( input_tensor_view.has_gradient() )
	{
		checkCudnnErr( cudnnPoolingBackward( cudnn_handle,
		                                     pooling_descriptor,
		                                     &alpha,
		                                     output_tensor_view.descriptor(),
		                                     output_tensor_view.data(),
		                                     output_tensor_view.descriptor(),
		                                     output_tensor_view.grad(),
		                                     input_tensor_view.descriptor(),
		                                     input_tensor_view.data(),
		                                     &beta,
		                                     input_tensor_view.descriptor(),
		                                     input_tensor_view.grad() ) );
	}
}

void pool_layer::updateOutputDim()
{
	cudnnTensorDescriptor_t inputTensorDesc;
	checkCudnnErr( cudnnCreateTensorDescriptor( &inputTensorDesc ) );
#if DNN_HALF_FLOAT
	cudnnDataType_t cudt = CUDNN_DATA_HALF;
#else
	cudnnDataType_t cudt = CUDNN_DATA_FLOAT;
#endif
	checkCudnnErr( cudnnSetTensor4dDescriptor(
	        inputTensorDesc, CUDNN_TENSOR_NHWC, cudt, input_dim.n, input_dim.c, input_dim.h, input_dim.w ) );
	checkCudnnErr( cudnnGetPooling2dForwardOutputDim(
	        pooling_descriptor, inputTensorDesc, &output_dim.n, &output_dim.c, &output_dim.h, &output_dim.w ) );
	checkCudnnErr( cudnnDestroyTensorDescriptor( inputTensorDesc ) );
}

/////////////////////////////////////////////////////////////////////////////
// Activation layer
/////////////////////////////////////////////////////////////////////////////

void activation_layer::init( cudnnActivationMode_t mode )
{
	checkCudnnErr( cudnnCreateActivationDescriptor( &activation_descriptor ) );
	checkCudnnErr( cudnnSetActivationDescriptor( activation_descriptor, mode, CUDNN_NOT_PROPAGATE_NAN, 1.0 ) );
}

void activation_layer::forward()
{
	checkCudnnErr( cudnnActivationForward( cudnn_handle,
	                                       activation_descriptor,
	                                       &alpha,
	                                       input_tensor_view.descriptor(),
	                                       input_tensor_view.data(),
	                                       &beta,
	                                       output_tensor_view.descriptor(),
	                                       output_tensor_view.data() ) );
}

void activation_layer::backward()
{
	if ( input_tensor_view.has_gradient() )
	{
		checkCudnnErr( cudnnActivationBackward( cudnn_handle,
		                                        activation_descriptor,
		                                        &alpha,
		                                        output_tensor_view.descriptor(),
		                                        output_tensor_view.data(),
		                                        output_tensor_view.descriptor(),
		                                        output_tensor_view.grad(),
		                                        input_tensor_view.descriptor(),
		                                        input_tensor_view.data(),
		                                        &beta,
		                                        input_tensor_view.descriptor(),
		                                        input_tensor_view.grad() ) );
	}
}

void activation_layer::updateOutputDim()
{
	output_dim = input_dim;
}

/////////////////////////////////////////////////////////////////////////////
// Fully Connected Layer
/////////////////////////////////////////////////////////////////////////////
void fc_layer::init( int _num_input_nodes, int _num_output_nodes )
{
	num_input_nodes = _num_input_nodes;
	num_output_nodes = _num_output_nodes;
	weights.Resize( 1, 1, num_input_nodes, num_output_nodes );
	// float dist_size = sqrt(3.0f / float(num_input_nodes * num_output_nodes));
	float variance = 1.0f / float( num_input_nodes );
	weights.ResetRandom( variance );
	bias.Resize( 1, 1, 1, num_output_nodes );
	// Bias can safely be initialized to zero
	bias.ResetZero();
	// Adam optimizer init
	int num_weights = num_input_nodes * num_output_nodes;
	int num_biases = num_output_nodes;
	checkCudaErr( cudaMalloc( &d_adam_s, ( num_weights + num_biases ) * sizeof( nnfloat ) ) );
	checkCudaErr( cudaMalloc( &d_adam_r, ( num_weights + num_biases ) * sizeof( nnfloat ) ) );
	checkCudaErr( cudaMemset( d_adam_s, 0, ( num_weights + num_biases ) * sizeof( nnfloat ) ) );
	checkCudaErr( cudaMemset( d_adam_r, 0, ( num_weights + num_biases ) * sizeof( nnfloat ) ) );
}

// TODO: Reimplement this
#undef cublasSgemm
#define cublasSgemm( ... ) 0
#undef cublasSgemv
#define cublasSgemv( ... ) 0

void fc_layer::Forward( tensor_view_t input_tensor, tensor_view_t output_tensor )
{
	checkCublasErr( cublasSgemm( cublas_handle,
	                             CUBLAS_OP_T,
	                             CUBLAS_OP_N,
	                             /*M*/ num_output_nodes,
	                             /*N*/ BATCH_SIZE,
	                             /*K*/ num_input_nodes,
	                             &alpha,
	                             weights.d_data,
	                             num_input_nodes,
	                             input_tensor.data(),
	                             num_input_nodes,
	                             &beta,
	                             output_tensor.data(),
	                             num_output_nodes ) );

	vector<float> w = weights.DownloadFlatData();
	vector<float> b = bias.DownloadFlatData();

	CUDA_FATAL_ERROR( "ones_tensor is not initialized, since it depends on BATCH_SIZE???" );
	checkCublasErr( cublasSgemm( cublas_handle,
	                             CUBLAS_OP_N,
	                             CUBLAS_OP_N,
	                             num_output_nodes,
	                             BATCH_SIZE,
	                             1,
	                             &alpha,
	                             bias.d_data,
	                             num_output_nodes,
	                             ones_tensor.d_data,
	                             1,
	                             &alpha,
	                             output_tensor.data(),
	                             num_output_nodes ) );
}
void fc_layer::Backward( tensor_view_t input_tensor, tensor_view_t output_tensor )
{
	// Compute derivative of error with respect to weights
	// We have dMSE/d0 and we want dMSE/dw = (dO/dw)(dMSE/dO)
	// dO/dw = i, so:
	checkCublasErr( cublasSgemm( cublas_handle,
	                             CUBLAS_OP_N,
	                             CUBLAS_OP_T,
	                             /*M*/ num_input_nodes,
	                             /*N*/ num_output_nodes,
	                             /*K*/ BATCH_SIZE,
	                             &alpha,
	                             input_tensor.data(),
	                             num_input_nodes,
	                             output_tensor.grad(),
	                             num_output_nodes,
	                             &beta,
	                             weights.d_grad,
	                             num_input_nodes ) );
	// Compute derivative of error with respect to bias
	// dMSE/dB = (dO/dB)(dMSE/dO)
	// dO/dB = 1, so:
	CUDA_FATAL_ERROR( "ones_tensor is not initialized, since it depends on BATCH_SIZE???" );
	checkCublasErr( cublasSgemv( cublas_handle,
	                             CUBLAS_OP_N,
	                             /*M*/ num_output_nodes,
	                             /*N*/ BATCH_SIZE,
	                             &alpha,
	                             output_tensor.grad(),
	                             num_output_nodes,
	                             ones_tensor.d_data,
	                             1,
	                             &beta,
	                             bias.d_grad,
	                             1 ) );

	if ( input_tensor.has_gradient() )
	{
		// Compute derivative of error with respect to input
		// dMSE/di = (dO/di)(dMSE/dO)
		// dO/di = w, so:
		checkCublasErr( cublasSgemm( cublas_handle,
		                             CUBLAS_OP_N,
		                             CUBLAS_OP_N,
		                             /*M*/ num_input_nodes,
		                             /*N*/ BATCH_SIZE,
		                             /*K*/ num_output_nodes,
		                             &alpha,
		                             weights.d_data,
		                             num_input_nodes,
		                             output_tensor.grad(),
		                             num_output_nodes,
		                             &beta,
		                             input_tensor.grad(),
		                             num_input_nodes ) );
	}
}
void fc_layer::UpdateWeights( float learning_rate )
{
	int num_weights = num_input_nodes * num_output_nodes;
	int num_biases = num_output_nodes;
	{   // Update weights
		int threads = 256;
		int blocks = ( num_weights - 1 ) / threads + 1;
		adam_update_parameters_kernel<<<blocks, threads>>>(
		        num_weights, (nnfloat*)weights.d_data, d_adam_s, d_adam_r, (nnfloat*)weights.d_grad, adam_beta_1, adam_beta_2, ADAM_T_VAR, learning_rate );
	}
	{   // Update biases
		int threads = 256;
		int blocks = ( num_biases - 1 ) / threads + 1;
		adam_update_parameters_kernel<<<blocks, threads>>>( num_biases,
		                                                    (nnfloat*)bias.d_data,
		                                                    d_adam_s + num_weights,
		                                                    d_adam_r + num_weights,
		                                                    (nnfloat*)bias.d_grad,
		                                                    adam_beta_1,
		                                                    adam_beta_2,
		                                                    ADAM_T_VAR,
		                                                    learning_rate );
	}
}

/////////////////////////////////////////////////////////////////////////////
// Softmax Layer
/////////////////////////////////////////////////////////////////////////////
void softmax_layer::Forward( tensor_view_t input_tensor, tensor_view_t output_tensor )
{
	checkCudnnErr( cudnnSoftmaxForward( cudnn_handle,
	                                    CUDNN_SOFTMAX_ACCURATE,
	                                    CUDNN_SOFTMAX_MODE_INSTANCE,
	                                    &alpha,
	                                    input_tensor.descriptor(),
	                                    input_tensor.data(),
	                                    &beta,
	                                    output_tensor.descriptor(),
	                                    output_tensor.data() ) );
}
void softmax_layer::Backward( tensor_view_t input_tensor, tensor_view_t output_tensor )
{
	if ( input_tensor.has_gradient() )
	{
		checkCudnnErr( cudnnSoftmaxBackward( cudnn_handle,
		                                     CUDNN_SOFTMAX_ACCURATE,
		                                     CUDNN_SOFTMAX_MODE_INSTANCE,
		                                     &alpha,
		                                     output_tensor.descriptor(),
		                                     output_tensor.data(),
		                                     output_tensor.descriptor(),
		                                     output_tensor.grad(),
		                                     &beta,
		                                     input_tensor.descriptor(),
		                                     input_tensor.grad() ) );
	}
}

/////////////////////////////////////////////////////////////////////////////
// Dropout Layer
/////////////////////////////////////////////////////////////////////////////
dropout_layer::dropout_layer( float amount )
{
	checkCudnnErr( cudnnCreateDropoutDescriptor( &descriptor ) );
	checkCudnnErr( cudnnDropoutGetStatesSize( cudnn_handle, &states_size ) );
	checkCudaErr( cudaMalloc( &states, states_size ) );
	checkCudnnErr( cudnnSetDropoutDescriptor( descriptor, cudnn_handle, amount, states, states_size, 0 ) );
}

dropout_layer::~dropout_layer()
{
	if ( workspace )
	{
		cudaFree( workspace );
	}
}

void dropout_layer::FindWorkspace( tensor_view_t& input_tensor )
{
	if ( input_tensor.dim() != last_input_dim )
	{
		last_input_dim = input_tensor.dim();
		size_t new_workspace_size;
		checkCudnnErr( cudnnDropoutGetReserveSpaceSize( input_tensor.descriptor(), &new_workspace_size ) );
		if ( new_workspace_size > workspace_size )
		{
			if ( workspace )
			{
				cudaFree( workspace );
			}
			workspace_size = new_workspace_size;
			checkCudaErr( cudaMalloc( &workspace, workspace_size ) );
		}
	}
}
void dropout_layer::Forward( tensor_view_t input_tensor, tensor_view_t output_tensor )
{
	if ( training )
	{
		FindWorkspace( input_tensor );
		checkCudnnErr( cudnnDropoutForward( cudnn_handle,
		                                    descriptor,
		                                    input_tensor.descriptor(),
		                                    input_tensor.data(),
		                                    output_tensor.descriptor(),
		                                    output_tensor.data(),
		                                    workspace,
		                                    workspace_size ) );
	}
}
void dropout_layer::Backward( tensor_view_t input_tensor, tensor_view_t output_tensor )
{
	if ( !training )
	{
		CUDA_FATAL_ERROR( "Currently not in the training state!" );
	}
	if ( input_tensor.has_gradient() )
	{
		checkCudnnErr( cudnnDropoutBackward( cudnn_handle,
		                                     descriptor,
		                                     output_tensor.descriptor(),
		                                     output_tensor.grad(),
		                                     input_tensor.descriptor(),
		                                     input_tensor.grad(),
		                                     workspace,
		                                     workspace_size ) );
	}
}

/////////////////////////////////////////////////////////////////////////////
// Upsample Layer (simple nearest neighbour 2x2 upsampling)
/////////////////////////////////////////////////////////////////////////////
__global__ void upsample_2x2_nearest_kernel(
        const int num_output_values, const nnfloat* d_input, nnfloat* d_output, tensor_dimension_cu out_dim )
{
	int thread_index = blockIdx.x * blockDim.x + threadIdx.x;
	if ( thread_index >= num_output_values )
		return;
	tensor_dimension_cu out_stride;
	out_stride.n = out_dim.h * out_dim.w * out_dim.c;
	out_stride.c = 1;
	out_stride.h = out_dim.w * out_dim.c;
	out_stride.w = out_dim.c;

	int batch = (thread_index / out_stride.n) % out_dim.n;
	int channel = (thread_index / out_stride.c) % out_dim.c;
	int o_y = (thread_index / out_stride.h) % out_dim.h;
	int o_x = (thread_index / out_stride.w) % out_dim.w;
	int i_y = o_y / 2;
	int i_x = o_x / 2;
	int i_w = out_dim.w / 2;
	int i_h = out_dim.h / 2;

	tensor_dimension_cu in_stride;
	in_stride.n = i_h * i_w * out_dim.c;
	in_stride.c = 1;
	in_stride.h = i_w * out_dim.c;
	in_stride.w = out_dim.c;

	nnfloat v = d_input[batch * in_stride.n + channel * in_stride.c + i_y * in_stride.h + i_x * in_stride.w];
	if ( is_nan( v ) )
	{
		v = nnfloat(0.f);
	}

	// Simple nearest neighbour
	d_output[batch * out_stride.n + channel * out_stride.c + o_y * out_stride.h + o_x * out_stride.w] = v;
}
// Simple average 2x2 downsampling
__global__ void
        downsample_nearest_kernel( const int num_output_values, const nnfloat* d_input, nnfloat* d_output, tensor_dimension_cu out_dim )
{
	int thread_index = blockIdx.x * blockDim.x + threadIdx.x;
	if ( thread_index >= num_output_values )
		return;
	tensor_dimension_cu out_stride;
	out_stride.n = out_dim.h * out_dim.w * out_dim.c;
	out_stride.c = 1;
	out_stride.h = out_dim.w * out_dim.c;
	out_stride.w = out_dim.c;

	int batch = (thread_index / out_stride.n) % out_dim.n;
	int channel = (thread_index / out_stride.c) % out_dim.c;
	int o_y = (thread_index / out_stride.h) % out_dim.h;
	int o_x = (thread_index / out_stride.w) % out_dim.w;

	nnfloat avg = 0.0f;
	int i_w = out_dim.w * 2;
	int i_h = out_dim.h * 2;

	tensor_dimension_cu in_stride;
	in_stride.n = i_h * i_w * out_dim.c;
	in_stride.c = 1;
	in_stride.h = i_w * out_dim.c;
	in_stride.w = out_dim.c;

	for ( int Y = 0; Y < 2; Y++ )
	{
		for ( int X = 0; X < 2; X++ )
		{
			int i_y = o_y * 2 + Y;
			int i_x = o_x * 2 + X;
			avg += nnfloat( 1.0f / 4.0f ) * d_input[batch * in_stride.n + channel * in_stride.c + i_y * in_stride.h + i_x * in_stride.w];
		}
	}
	d_output[batch * out_stride.n + channel * out_stride.c + o_y * out_stride.h + o_x * out_stride.w] = avg;
}

void upsample_nearest_layer::forward()
{
	int threads = 256;
	int blocks = ( output_dim.size() - 1 ) / threads + 1;
	upsample_2x2_nearest_kernel<<<blocks, threads>>>( output_dim.size(),
	                                                  (nnfloat*)input_tensor_view.data(),
	                                                  (nnfloat*)output_tensor_view.data(),
	                                                  output_dim._cuda );
	checkCudaErr( cudaGetLastError() );
}

void upsample_nearest_layer::backward()
{
	if ( input_tensor_view.has_gradient() )
	{
		int threads = 256;
		int blocks = ( input_dim.size() - 1 ) / threads + 1;
		downsample_nearest_kernel<<<blocks, threads>>>( input_dim.size(),
		                                                (nnfloat*)output_tensor_view.grad(),
		                                                (nnfloat*)input_tensor_view.grad(),
		                                                input_dim._cuda );
		checkCudaErr( cudaGetLastError() );
	}
}

void upsample_nearest_layer::updateOutputDim()
{
	output_dim = input_dim;
	output_dim.w *= 2;
	output_dim.h *= 2;
}

/////////////////////////////////////////////////////////////////////////////
// Upsample Bilinear Layer
/////////////////////////////////////////////////////////////////////////////
__global__ void
        upsample_bilinear_kernel( const nnfloat* d_input, nnfloat* d_output, tensor_dimension_cu input_dim, tensor_dimension_cu out_dim )
{
	int thread_index = blockIdx.x * blockDim.x + threadIdx.x;
	int num_output_values = out_dim.w * out_dim.h * out_dim.c * out_dim.n;
	if ( thread_index >= num_output_values )
		return;

	tensor_dimension_cu in_stride;
	in_stride.n = input_dim.h * input_dim.w * input_dim.c;
	in_stride.c = 1;
	in_stride.h = input_dim.w * input_dim.c;
	in_stride.w = input_dim.c;

	tensor_dimension_cu out_stride;
	out_stride.n = out_dim.h * out_dim.w * out_dim.c;
	out_stride.c = 1;
	out_stride.h = out_dim.w * out_dim.c;
	out_stride.w = out_dim.c;

	int batch = (thread_index / out_stride.n) % out_dim.n;
	int channel = (thread_index / out_stride.c) % out_dim.c;
	int o_y = ( thread_index / out_stride.h ) % out_dim.h;
	int o_x = ( thread_index / out_stride.w ) % out_dim.w;

	nnfloat i_x = float( o_x ) * float( input_dim.w - 1 ) / float( out_dim.w - 1 );
	nnfloat i_y = float( o_y ) * float( input_dim.h - 1 ) / float( out_dim.h - 1 );

	int i_x0 = int( i_x );
	int i_x1 = i_x0 + 1;
	int i_y0 = int( i_y );
	int i_y1 = i_y0 + 1;

	int i_start = batch * in_stride.n + channel * in_stride.c;
	nnfloat a00 = d_input[i_start + i_y0 * in_stride.h + i_x0 * in_stride.w];
	nnfloat a01 = d_input[i_start + i_y1 * in_stride.h + i_x0 * in_stride.w];
	nnfloat a10 = d_input[i_start + i_y0 * in_stride.h + i_x1 * in_stride.w];
	nnfloat a11 = d_input[i_start + i_y1 * in_stride.h + i_x1 * in_stride.w];

	if ( is_nan( a00 ) )
	{
		a00 = 0.f;
	}
	if ( is_nan( a01 ) )
	{
		a01 = 0.f;
	}
	if ( is_nan( a10 ) )
	{
		a10 = 0.f;
	}
	if ( is_nan( a11 ) )
	{
		a11 = 0.f;
	}

	nnfloat f_x0 = float( i_x0 );
	nnfloat f_x1 = float( i_x1 );
	nnfloat f_y0 = float( i_y0 );
	nnfloat f_y1 = float( i_y1 );
	nnfloat dv = nnfloat( float( ( i_x1 - i_x0 ) * ( i_y1 - i_y0 ) ) );

	d_output[thread_index] =
	        ( a00 * ( f_x1 - i_x ) * ( f_y1 - i_y ) + a10 * ( i_x - f_x0 ) * ( f_y1 - i_y )
	          + a01 * ( f_x1 - i_x ) * ( i_y - f_y0 ) + a11 * ( i_x - f_x0 ) * ( i_y - f_y0 ) )
	        / dv;
}

// Simple average 2x2 downsampling
// This might be implemented better? I don't care we don't really use it anyway
/* TODO: Reimplement for NHWC!!!
__global__ void
        downsample_bilinear_kernel( const int num_output_values, const nnfloat* d_input, nnfloat* d_output, int n, int c, int h, int w )
{
	int thread_index = blockIdx.x * blockDim.x + threadIdx.x;
	if ( thread_index >= num_output_values )
		return;
	int batch = thread_index / ( c * h * w );
	int channel = ( thread_index % ( c * h * w ) ) / ( h * w );
	int o_y = ( ( thread_index % ( c * h * w ) ) % ( h * w ) ) / w;
	int o_x = ( ( thread_index % ( c * h * w ) ) % ( h * w ) ) % w;
	nnfloat avg = 0.0f;
	int i_w = w * 2;
	int i_h = h * 2;
	for ( int Y = 0; Y < 2; Y++ )
	{
		for ( int X = 0; X < 2; X++ )
		{
			int i_y = o_y * 2 + Y;
			int i_x = o_x * 2 + X;
			avg += nnfloat( 1.0f / 4.0f ) * d_input[batch * ( c * i_h * i_w ) + channel * ( i_h * i_w ) + i_y * i_w + i_x];
		}
	}
	d_output[batch * ( c * h * w ) + channel * ( h * w ) + o_y * w + o_x] = avg;
}*/


upsample_bilinear_layer::upsample_bilinear_layer( uint32_t factor_ )
	: smart_layer_t()
	, factor( factor_ )
{
}

void upsample_bilinear_layer::forward()
{
	int threads = 256;
	int blocks = ( output_dim.size() + threads - 1 ) / threads;
	upsample_bilinear_kernel<<<blocks, threads>>>( (nnfloat*)input_tensor_view.data(), (nnfloat*)output_tensor_view.data(), input_dim._cuda, output_dim._cuda );
	checkCudaErr( cudaGetLastError() );
}

void upsample_bilinear_layer::backward()
{
	if ( input_tensor_view.has_gradient() )
	{
		CUDA_FATAL_ERROR( "NOT IMPLEMENTED" );
		int threads = 256;
		int blocks = ( input_dim.size() - 1 ) / threads + 1;
		// TODO: This is wrong, needs to be reimplemented??
		// downsample_bilinear_kernel<<<blocks, threads>>>(output_tensor_view.dim().size(), output_tensor_view.grad(),
		// input_tensor_view.grad(), input_dim._cuda);
		checkCudaErr( cudaGetLastError() );
	}
}

void upsample_bilinear_layer::updateOutputDim()
{
	output_dim = input_dim;
	output_dim.w *= factor;
	output_dim.h *= factor;
}


/////////////////////////////////////////////////////////////////////////////
// BatchNorm Layer
/////////////////////////////////////////////////////////////////////////////
void batchnorm2d_layer::init()
{
	// Allocate memory for each layer's stddev and mean values
	checkCudaErr( cudaMalloc( &std_dev, num_channels * sizeof( nnfloat ) ) );
	checkCudaErr( cudaMalloc( &mean, num_channels * sizeof( nnfloat ) ) );
}

batchnorm2d_layer::~batchnorm2d_layer()
{
	if ( std_dev )
	{
		cudaFree( std_dev );
		std_dev = nullptr;
	}
	if ( mean )
	{
		cudaFree( mean );
		mean = nullptr;
	}
}

void batchnorm2d_layer::Forward( tensor_view_t input_tensor, tensor_view_t output_tensor )
{
	// TODO: This should differentiate between training and eval phases, because in eval we just apply the learned values,
	// whereas in training we have to calculate the mean and std dev of each channel.
	CUDA_FATAL_ERROR( "Not implemented" );
}

void batchnorm2d_layer::Backward( tensor_view_t input_tensor, tensor_view_t output_tensor )
{
	// TODO: Should this do anything? not with the mean at least, but maybe it needs to add the std dev to the derivative
	// tensor, since it would be multiplied?
	CUDA_FATAL_ERROR( "Not implemented" );
}

void batchnorm2d_layer::UpdateWeights( float learning_rate )
{
	CUDA_FATAL_ERROR( "Not implemented" );
}


/////////////////////////////////////////////////////////////////////////////
// Copy Layer
/////////////////////////////////////////////////////////////////////////////
void copy_layer::forward()
{
	size_t count = input_dim.size();
	cudaMemcpy( output_tensor_view.data(), input_tensor_view.data(), count * sizeof( nnfloat ), cudaMemcpyDeviceToDevice );
}

void copy_layer::backward()
{
	// Copy the gradient back
	size_t count = input_dim.size();
	cudaMemcpy( input_tensor_view.grad(), output_tensor_view.grad(), count * sizeof( nnfloat ), cudaMemcpyDeviceToDevice );
}

void copy_layer::updateOutputDim()
{
	output_dim = input_dim;
}


/////////////////////////////////////////////////////////////////////////////
// Slice Layer
/////////////////////////////////////////////////////////////////////////////
__global__ void copy_slice_kernel( const nnfloat* source_tensor,
                                   tensor_dimension_cu source_tensor_dim,
                                   tensor_dimension_cu source_tensor_stride,
                                   nnfloat* target_tensor,
                                   tensor_dimension_cu target_tensor_dim,
                                   tensor_dimension_cu target_tensor_stride,
                                   uint32_t dim,
                                   uint32_t idx_start )
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	int num_output_values = target_tensor_dim.n * target_tensor_dim.c * target_tensor_dim.h * target_tensor_dim.w;
	if ( tid >= num_output_values )
	{
		return;
	}
	int batch = (tid / target_tensor_stride.n ) % target_tensor_dim.n;
	int channel = (tid / target_tensor_stride.c) % target_tensor_dim.c;
	int o_y = (tid / target_tensor_stride.h) % target_tensor_dim.h;
	int o_x = (tid / target_tensor_stride.w) % target_tensor_dim.w;

	int src_idx = idx_start + source_tensor_stride.n * batch + source_tensor_stride.c * channel + source_tensor_stride.h * o_y
	              + source_tensor_stride.w * o_x;
	target_tensor[tid] = source_tensor[src_idx];
}

void slice_layer::forward()
{
	int32_t actual_first = begin;
	if ( begin < 0 )
	{
		actual_first = input_dim.values[dim] + begin;
	}
	int threads = 256;
	int blocks = ( output_dim.size() + threads - 1 ) / threads;
	tensor_dimension input_stride;
	input_stride.n = input_dim.c * input_dim.h * input_dim.w;
	input_stride.c = 1;
	input_stride.h = input_dim.w * input_dim.c;
	input_stride.w = input_dim.c;
	tensor_dimension output_stride;
	output_stride.n = output_dim.c * output_dim.h * output_dim.w;
	output_stride.c = 1;
	output_stride.h = output_dim.w * output_dim.c;
	output_stride.w = output_dim.c;
	uint32_t start = input_stride.values[dim] * actual_first;
	copy_slice_kernel<<<blocks, threads>>>( (nnfloat*)input_tensor_view.data(),
	                                        input_dim._cuda,
	                                        input_stride._cuda,
	                                        (nnfloat*)output_tensor_view.data(),
	                                        output_dim._cuda,
	                                        output_stride._cuda,
	                                        dim,
	                                        start );
	checkCudaErr( cudaGetLastError() );
}

void slice_layer::backward()
{
	CUDA_FATAL_ERROR( "Not implemented" );
}

void slice_layer::updateOutputDim()
{
	output_dim = input_dim;
	int32_t actual_first = begin;
	int32_t actual_last = end - 1;
	if ( begin < 0 )
	{
		actual_first = input_dim.values[dim] + begin;
	}
	if ( end < 0 )
	{
		actual_last = input_dim.values[dim] + end;
	}

	if ( actual_first >= input_dim.values[dim] )
	{
		LOG_FATAL( "Start of slice is greater than input tensor's dimension {} >= {}", actual_first, input_dim.values[dim] );
	}

	if ( actual_last < actual_first || actual_last >= input_dim.values[dim] )
	{
		actual_last = input_dim.values[dim] - 1;
	}
	output_dim.values[dim] = actual_last - actual_first + 1;
}


/////////////////////////////////////////////////////////////////////////////
// Concat Layer
/////////////////////////////////////////////////////////////////////////////
concat_layer::~concat_layer()
{
}

void concat_layer::addConcatInput( smart_layer_t* l )
{
	to_concat.push_back( l );
	l->_connectOutput( this );
}

void concat_layer::_setInputTensorFromInputConnection( const smart_layer_t* connection, tensor_view_t input )
{
	to_concat_tensor.resize( to_concat.size() );
	for ( int i = 0; i < to_concat.size(); ++i )
	{
		if ( connection == to_concat[i] )
		{
			to_concat_tensor[i] = input;
			return;
		}
	}
	CUDA_FATAL_ERROR( "Input layer hasn't been added as an input." );
}

void concat_layer::connectInput( smart_layer_t* connection )
{
	// We will always have this connection as the first one
	if ( to_concat.size() > 0 )
	{
		if ( to_concat[0] == input_connection )
		{
			to_concat[0] = connection;
		}
		else
		{
			to_concat.insert( to_concat.begin(), connection );
		}
	}
	else
	{
		to_concat.push_back( connection );
	}

	smart_layer_t::connectInput( connection );
}

void concat_layer::updateOutputDim()
{
	if ( dimension != 1 )
	{
		CUDA_FATAL_ERROR( "Concatenation only implemented for the first dimension." );
	}

	// Calculate the stride of each subtensor along Dim
	uint32_t dim_stride = 1;

	// Add up the size of all the tensors along the concat dimension to use as our output size
	uint32_t dim_size = 0;
	to_concat_dim.clear();
	to_concat_offset.clear();
	for ( auto& l : to_concat )
	{
		// Calculate actual byte offset
		tensor_dimension dim = l->getOutputDim();
		size_t dim_offset = dim_size;
		size_t num_floats_offset = dim_offset * dim_stride;
		to_concat_dim.push_back( dim );
		to_concat_offset.push_back( num_floats_offset );
		dim_size += dim.values[dimension];
	}
	output_dim = input_connection->getOutputDim();
	output_dim.values[dimension] = dim_size;
}

std::optional<tensor_view_t> concat_layer::getOutputTensorForInputConnection( const smart_layer_t* connection )
{
	return {};
}

/* TODO: Reimplement for NHWC
__global__ void concat_tensor_kernel( nnfloat* target_tensor,
                                      tensor_dimension_cu target_tensor_dim,
                                      tensor_dimension_cu target_tensor_stride,
                                      nnfloat* source_tensor,
                                      tensor_dimension_cu source_tensor_dim,
                                      tensor_dimension_cu source_tensor_stride,
                                      tensor_dimension_cu copy_tensor_dim,
                                      tensor_dimension_cu copy_tensor_stride,
                                      uint32_t dim,
                                      uint32_t concat_offset )
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;

	tensor_dimension_cu src_p = { tid % copy_tensor_dim.n,
		                          ( tid / copy_tensor_stride.n ) % copy_tensor_dim.c,
		                          ( tid / copy_tensor_stride.c ) % copy_tensor_dim.h,
		                          ( tid / copy_tensor_stride.h ) % copy_tensor_dim.w };

	tensor_dimension_cu tgt_p = src_p;
	tgt_p.values[dim] += concat_offset;


	uint32_t source_idx =
	        src_p.w + src_p.h * source_tensor_stride.h + src_p.c * source_tensor_stride.c + src_p.n * source_tensor_stride.n;
	uint32_t target_idx =
	        tgt_p.w + tgt_p.h * target_tensor_stride.h + tgt_p.c * target_tensor_stride.c + tgt_p.n * target_tensor_stride.n;

	if ( target_idx >= target_tensor_dim.n * target_tensor_stride.n )
		return;

	if ( source_idx >= source_tensor_dim.n * source_tensor_stride.n )
		return;

	target_tensor[target_idx] = source_tensor[source_idx];
}
*/

__global__ void copy_strided_tensor_kernel( nnfloat* out_tensor,
											tensor_dimension_cu out_storage_stride,
											const nnfloat* in_tensor,
											tensor_dimension_cu in_dim,
											tensor_dimension_cu in_stride,
											tensor_dimension_cu in_storage_stride )
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;

	int num_input_values = in_dim.n * in_dim.c * in_dim.h * in_dim.w;
	if ( tid >= num_input_values )
	{
		return;
	}
	
	tensor_dimension_cu src_p = { ( tid / in_stride.n ) % in_dim.n,
		                          ( tid / in_stride.c ) % in_dim.c,
		                          ( tid / in_stride.h ) % in_dim.h,
		                          ( tid / in_stride.w ) % in_dim.w };

	out_tensor[src_p.n * out_storage_stride.n + src_p.c * out_storage_stride.c
	           + src_p.h * out_storage_stride.h + src_p.w * out_storage_stride.w]
		= in_tensor[src_p.n * in_storage_stride.n + src_p.c * in_storage_stride.c
	           + src_p.h * in_storage_stride.h + src_p.w * in_storage_stride.w];
}

__global__ void concatenate_kernel(nnfloat* in0, int in0_channels, nnfloat *in1, int in1_channels, nnfloat *out, int num_threads)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if (tid >= num_threads) return; 
	int out_channels = (in0_channels + in1_channels);
	int channel = tid % out_channels; 
	int value = tid / out_channels; 
	if (channel < in0_channels)
		out[tid] = in0[value * in0_channels + channel];
	else
		out[tid] = in1[value * in1_channels + (channel - in0_channels)];
}

void concat_layer::forward()
{
	// Optimized version for the case of two tensors being concatenated into one
	// and concatenating channels
	if (to_concat.size() == 2) 
	{
		// Start one thread per output value
		int num_threads = output_tensor_view.dim().size();
		const int num_threads_in_block = 256;
		int blocks = (num_threads - 1) / num_threads_in_block + 1; 
		concatenate_kernel << <blocks, num_threads_in_block >> > (
			(nnfloat*)to_concat_tensor[0].data(),
			to_concat_dim[0].c,
			(nnfloat*)to_concat_tensor[1].data(),
			to_concat_dim[1].c,
			(nnfloat*)output_tensor_view.data(), 
			num_threads
			); 
	}
	else 
	{
		tensor_dimension output_stride;
		output_stride.n = output_dim.c * output_dim.h * output_dim.w;
		output_stride.c = 1;
		output_stride.h = output_dim.w * output_dim.c;
		output_stride.w = output_dim.c;

		int threads = 256;
		for (int i = 0; i < to_concat.size(); ++i)
		{
			int blocks = (to_concat_dim[i].size() + threads - 1) / threads;
			uint32_t start = to_concat_offset[i];

			tensor_dimension in_stride;
			in_stride.n = to_concat_dim[i].c * to_concat_dim[i].h * to_concat_dim[i].w;
			in_stride.c = 1;
			in_stride.h = to_concat_dim[i].w * to_concat_dim[i].c;
			in_stride.w = to_concat_dim[i].c;

			tensor_dimension in_storage_stride = in_stride;

			copy_strided_tensor_kernel << <blocks, threads >> > ((nnfloat*)output_tensor_view.data() + start,
				output_stride._cuda,
				(nnfloat*)to_concat_tensor[i].data(),
				to_concat_dim[i]._cuda,
				in_stride._cuda,
				in_storage_stride._cuda);
			checkCudaErr(cudaGetLastError());
		}
	}
}

void concat_layer::backward()
{
	// Need to copy the stuff back
	CUDA_FATAL_ERROR( "Not implemented" );
}


/////////////////////////////////////////////////////////////////////////////
// Average Layer
/////////////////////////////////////////////////////////////////////////////

average_layer::~average_layer()
{
}

void average_layer::connectInput( smart_layer_t* connection )
{
	// We will always have this connection as the first one
	if ( to_add.size() > 0 )
	{
		if ( to_add[0] == input_connection )
		{
			to_add[0] = connection;
		}
		else
		{
			to_add.insert( to_add.begin(), connection );
		}
	}
	else
	{
		to_add.push_back( connection );
	}

	smart_layer_t::connectInput( connection );
}

void average_layer::addExtraInput( smart_layer_t* l )
{
	to_add.push_back( l );
	l->_connectOutput( this );
}

__global__ void multiply_tensor_kernel( const nnfloat* d_input, nnfloat* d_output, tensor_dimension_cu dim, nnfloat factor_in )
{
	int thread_index = blockIdx.x * blockDim.x + threadIdx.x;
	int num_output_values = dim.w * dim.h * dim.c * dim.n;
	if ( thread_index >= num_output_values )
		return;

	d_output[thread_index] = d_input[thread_index] * factor_in;
}

__global__ void
        multiply_and_add_tensor_kernel( const nnfloat* d_input, nnfloat* d_output, tensor_dimension_cu dim, nnfloat factor_in )
{
	int thread_index = blockIdx.x * blockDim.x + threadIdx.x;
	int num_output_values = dim.w * dim.h * dim.c * dim.n;
	if ( thread_index >= num_output_values )
		return;

	d_output[thread_index] = d_input[thread_index] * factor_in + d_output[thread_index];
}

void average_layer::forward()
{
	float factor = 1.f / to_add.size();
	dim3 block = { 128, 1, 1 };
	dim3 grid = { ( input_dim.w * input_dim.h * input_dim.c * input_dim.n + block.x - 1 ) / block.x };

	multiply_tensor_kernel<<<grid, block>>>( (nnfloat*)input_tensor_views[0].data(), (nnfloat*)output_tensor_view.data(), input_dim._cuda, factor );

	for ( size_t i = 1; i < to_add.size(); ++i )
	{
		multiply_and_add_tensor_kernel<<<grid, block>>>(
		        (nnfloat*)input_tensor_views[i].data(), (nnfloat*)output_tensor_view.data(), input_dim._cuda, factor );
	}
}

void average_layer::backward()
{
	CUDA_FATAL_ERROR( "Not implemented" );
}

void average_layer::updateWeights( float learning_rate )
{
	CUDA_FATAL_ERROR( "Not implemented" );
}

void average_layer::_setInputTensorFromInputConnection( const smart_layer_t* connection, tensor_view_t input )
{
	for ( size_t i = 0; i < to_add.size(); ++i )
	{
		if ( to_add[i] == connection )
		{
			input_tensor_views[i] = std::move( input );
		}
	}
}

void average_layer::updateOutputDim()
{
	output_dim = input_connection->getOutputDim();
	for ( auto& l : to_add )
	{
		// Calculate actual byte offset
		tensor_dimension dim = l->getOutputDim();
		if ( output_dim != dim )
		{
			CUDA_FATAL_ERROR( "All inputs must have the same dimension!" );
		}
	}
	input_tensor_views.resize( to_add.size() );
}


/////////////////////////////////////////////////////////////////////////////
// OkLab Layers
/////////////////////////////////////////////////////////////////////////////

__global__ void rgb_to_oklab( const nnfloat* in_img, nnfloat* out_img, tensor_dimension_cu dim )
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	using namespace glm;
	size_t chan_stride = dim.w * dim.h;
	if ( tid >= chan_stride )
	{
		return;
	}
	typedef nnvec3 _nnvec3;
	typedef nnmat3 _nnmat3;
	_nnvec3 c = _nnvec3( in_img[tid], in_img[tid + chan_stride], in_img[tid + 2 * chan_stride] );

	_nnmat3 m1 = transpose( _nnmat3(
	        0.4122214708, 0.5363325363, 0.0514459929, 0.2119034982, 0.6806995451, 0.1073969566, 0.0883024619, 0.2817188376, 0.6299787005 ) );
	_nnmat3 m2 = transpose( _nnmat3(
	        0.2104542553, 0.7936177850, -0.0040720468, 1.9779984951, -2.4285922050, 0.4505937099, 0.0259040371, 0.7827717662, -0.8086757660 ) );

	c = m1 * c;
	c = _nnvec3( hpow( c.x, 1.f / 3.f ), hpow( c.y, 1.f / 3.f ), hpow( c.z, 1.f / 3.f ) );
	c = m2 * c;
	out_img[tid] = c.x;
	out_img[tid + chan_stride] = c.y;
	out_img[tid + 2 * chan_stride] = c.z;

	for ( int i = 3; i < dim.c; ++i )
	{
		out_img[tid + i * chan_stride] = in_img[tid + i * chan_stride];
	}
}

__global__ void oklab_to_rgb( const nnfloat* in_img, nnfloat* out_img, tensor_dimension_cu dim )
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	using namespace glm;
	size_t chan_stride = dim.w * dim.h;
	if ( tid >= chan_stride )
	{
		return;
	}
	typedef nnvec3 _nnvec3;
	typedef nnmat3 _nnmat3;
	_nnvec3 c = _nnvec3( in_img[tid], in_img[tid + chan_stride], in_img[tid + 2 * chan_stride] );

	_nnmat3 m1 = transpose(
	        _nnmat3( 1, 0.3963377774, 0.2158037573, 1, -0.1055613458, -0.0638541728, 1, -0.0894841775, -1.2914855480 ) );
	_nnmat3 m2 = transpose( _nnmat3(
	        4.0767416621, -3.3077115913, 0.2309699292, -1.2684380046, 2.6097574011, -0.3413193965, -0.0041960863, -0.7034186147, 1.7076147010 ) );

	c = m1 * c;
	c = _nnvec3( hpow( c.x, 3.f ), hpow( c.y, 3.f ), hpow( c.z, 3.f ) );
	c = m2 * c;
	out_img[tid] = c.x;
	out_img[tid + chan_stride] = c.y;
	out_img[tid + 2 * chan_stride] = c.z;

	for ( int i = 3; i < dim.c; ++i )
	{
		out_img[tid + i * chan_stride] = in_img[tid + i * chan_stride];
	}
}


void rgb_to_oklab_layer::forward()
{
	LOG_WARN( "Not implemented for NHWC!!!" );
	if ( input_dim.n > 1 )
	{
		CUDA_FATAL_ERROR( "Not implemented!" );
	}
	dim3 block = { 128, 1, 1 };
	dim3 grid = { ( input_dim.w * input_dim.h + block.x - 1 ) / block.x };
	rgb_to_oklab<<<grid, block>>>( (nnfloat*)input_tensor_view.data(), (nnfloat*)output_tensor_view.data(), input_dim._cuda );
	checkCudaErr( cudaGetLastError() );
}

void rgb_to_oklab_layer::backward()
{
	LOG_WARN( "Not implemented for NHWC!!!" );
	if ( mode != Mode::Train )
	{
		CUDA_FATAL_ERROR( "Mode not set to training to tensors won't work well!" );
	}
	if ( input_dim.n > 1 )
	{
		CUDA_FATAL_ERROR( "Not implemented!" );
	}
	dim3 block = { 128, 1, 1 };
	dim3 grid = { input_dim.w * input_dim.h / block.x + 1 };
	oklab_to_rgb<<<grid, block>>>( (nnfloat*)output_tensor_view.grad(), (nnfloat*)input_tensor_view.grad(), input_dim._cuda );
	checkCudaErr( cudaGetLastError() );
}

void rgb_to_oklab_layer::updateOutputDim()
{
	output_dim = input_dim;
}

std::optional<tensor_view_t> rgb_to_oklab_layer::getOutputTensorForInputConnection( const smart_layer_t* connection )
{
	if ( mode == Mode::Train )
	{
		return {};
	}
	else
	{
		return output_tensor_view;
	}
}

void oklab_to_rgb_layer::forward()
{
	LOG_WARN( "Not implemented for NHWC!!!" );
	if ( input_dim.n > 1 )
	{
		CUDA_FATAL_ERROR( "Not implemented!" );
	}
	dim3 block = { 128, 1, 1 };
	dim3 grid = { input_dim.w * input_dim.h / block.x + 1 };
	oklab_to_rgb<<<grid, block>>>( (nnfloat*)input_tensor_view.data(), (nnfloat*)output_tensor_view.data(), input_dim._cuda );
	checkCudaErr( cudaGetLastError() );
}

void oklab_to_rgb_layer::backward()
{
	LOG_WARN( "Not implemented for NHWC!!!" );
	if ( mode != Mode::Train )
	{
		CUDA_FATAL_ERROR( "Mode not set to training to tensors won't work well!" );
	}
	if ( input_dim.n > 1 )
	{
		CUDA_FATAL_ERROR( "Not implemented!" );
	}
	dim3 block = { 128, 1, 1 };
	dim3 grid = { input_dim.w * input_dim.h / block.x + 1 };
	rgb_to_oklab<<<grid, block>>>( (nnfloat*)output_tensor_view.grad(), (nnfloat*)input_tensor_view.grad(), input_dim._cuda );
	checkCudaErr( cudaGetLastError() );
}

void oklab_to_rgb_layer::updateOutputDim()
{
	output_dim = input_dim;
}

std::optional<tensor_view_t> oklab_to_rgb_layer::getOutputTensorForInputConnection( const smart_layer_t* connection )
{
	if ( mode == Mode::Train )
	{
		return {};
	}
	else
	{
		return output_tensor_view;
	}
}

}   // namespace dnn
