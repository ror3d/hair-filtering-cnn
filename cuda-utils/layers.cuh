#pragma once

#include "tensor.cuh"
#include <cuda_runtime.h>
#include <cudnn.h>
#include <fmt/format.h>
#include <optional>
#include <variant>

namespace dnn
{
void initializeCUDA();

	extern cudnnHandle_t cudnn_handle;

const float adam_beta_1 = 0.65f;
const float adam_beta_2 = 0.9f;
__global__ void adam_update_parameters_kernel( int num_values,
                                               float* parameters,
                                               float* s,
                                               float* r,
                                               const float* g,
                                               const float beta_1,
                                               const float beta_2,
                                               const int t,
                                               const float step_size );

struct size2d
{
	union
	{
		struct
		{
			int32_t w, h;
		};
		int32_t arr[2];
	};
	size2d() : w( 0 ), h( 0 ) {}
	size2d( int32_t s_ ) : w( s_ ), h( s_ ) {}
	size2d( int32_t w_, int32_t h_ ) : w( w_ ), h( h_ ) {}
};
inline bool operator==( size2d a, size2d b )
{
	return a.w == b.w && a.h == b.h;
}
inline bool operator!=( size2d a, size2d b )
{
	return a.w != b.w || a.h != b.h;
}

/////////////////////////////////////////////////////////////////////////////
// Layer Interface
/////////////////////////////////////////////////////////////////////////////
struct layer_t
{
	bool training = true;
	inline void eval() { training = false; }
	inline void train() { training = true; }

	virtual void Forward( tensor_view_t input_tensor, tensor_view_t output_tensor ) = 0;
	virtual void Backward( tensor_view_t input_tensor, tensor_view_t output_tensor ) = 0;
	virtual void UpdateWeights( float learning_rate ) = 0;
	virtual void CheckForBadFloat(){};
};

struct smart_layer_t
{
	enum class Mode { Eval, Train };
	Mode mode = Mode::Eval;
	bool mode_dirty = false;

	tensor_view_t input_tensor_view;

	std::optional<tensor_t> output_tensor = {};

	smart_layer_t* input_connection = nullptr;
	std::vector<smart_layer_t*> output_connections;

	tensor_dimension input_dim;
	tensor_dimension output_dim;

	bool dimensions_dirty = false;

public:
	tensor_view_t output_tensor_view;

	smart_layer_t() {}

	virtual ~smart_layer_t() {}

	// Only to be used for the first layer of the network
	virtual void connectInput( smart_layer_t* connection );

	virtual void updateInputAndOutputDims();

	// Returns true if it updated anything, including the mode, false otherwise
	virtual bool updateInputAndOutputTensors();


	virtual tensor_dimension getOutputDim() const { return output_dim; }

	virtual void setMode( Mode m )
	{
		mode = m;
		mode_dirty = true;
	}

	virtual void forward() = 0;
	virtual void backward() = 0;
	virtual void updateWeights( float learning_rate ) = 0;

	virtual bool checkBadFloat() { return false; }

	virtual void _connectOutput( smart_layer_t* layer ) { output_connections.push_back( layer ); }

	// Used by the preceding layer to give this one the input
	virtual void _setInputTensorFromInputConnection( const smart_layer_t* connection, tensor_view_t input )
	{
		input_tensor_view = std::move( input );
	}

protected:
	virtual void updateOutputDim() = 0;

	// Used by the preceding layer to get an output tensor that's handled by this layer (basically to share memory among them
	// and avoid copy operations)
	virtual std::optional<tensor_view_t> getOutputTensorForInputConnection( const smart_layer_t* connection ) { return {}; }

public:
	virtual std::string info() = 0;
};

struct network_input_image_layer : public smart_layer_t
{
	std::vector<uint32_t> gl_textures;
	std::vector<cudaGraphicsResource_t> resources;
	std::vector<cudaTextureObject_t> texture_objects;
	int padding_images = 0;


	network_input_image_layer() { dimensions_dirty = true; }

	void setInputImagesSize( int width, int height );
	void setInputImage( const std::vector<glm::vec4>& pixels );
	void setInputImages( std::vector<uint32_t> gl_textures );
	void addPaddingImages( int n );

	void connectInput( smart_layer_t* connection ) override;

	void updateInputAndOutputDims() override;

	bool updateInputAndOutputTensors() override;

	void updateOutputDim() override;

	void forward() override;
	void backward() override {}
	void updateWeights( float learning_rate ) override {}

	std::string info() override { return "network_input_image_layer"; }
};

struct network_output_image_layer : public smart_layer_t
{
	std::vector<uint32_t> gl_textures;
	std::vector<cudaGraphicsResource_t> resources;
	std::vector<cudaSurfaceObject_t> surface_objects;

	network_output_image_layer() { dimensions_dirty = true; }

	void setOutputImagesSize( int width, int height );
	void setOutputImages( std::vector<uint32_t> gl_textures );

	void updateOutputDim() override;

	void forward() override;
	void backward() override {}
	void updateWeights( float learning_rate ) override {}

	std::string info() override { return "network_output_image_layer"; }

protected:
	std::optional<tensor_view_t> getOutputTensorForInputConnection( const smart_layer_t* connection ) override;
};


///////////////////////////////////////////////////////////////////////////////
// Insert certain channels of an input tensor into some other channels of 
// an out tensor. 
///////////////////////////////////////////////////////////////////////////////
struct insert_channels_layer : public smart_layer_t
{
	smart_layer_t *from_layer;
	smart_layer_t *to_layer;
	//tensor_dimension from_layer_dim = { 0, 0, 0, 0 };
	//tensor_dimension to_layer_dim = { 0, 0, 0, 0 };
	int source_channel;
	int target_channel; 
	int num_channels; 

	insert_channels_layer(
		smart_layer_t* from_layer,
		smart_layer_t* to_layer,
		int source_channel,
		int target_channel,
		int num_channels
	) : from_layer(from_layer), to_layer(to_layer), source_channel(source_channel), target_channel(target_channel), num_channels(num_channels) {}

	void updateOutputDim() override;
	void forward() override;
	void backward() override {}
	void updateWeights(float learning_rate) override {}
	std::optional<tensor_view_t> getOutputTensorForInputConnection(const smart_layer_t* connection) override { return output_tensor_view; }

	std::string info() override { return "insert_channels_layer"; }
};

struct renormalize_rgb_vectors : public smart_layer_t
{
	const uint32_t channel_start;
	const uint32_t num_channels;

	renormalize_rgb_vectors( uint32_t channel_start = 0, uint32_t num_channels = 3 )
	    : channel_start( channel_start )
		, num_channels( num_channels )
	{
	}

	virtual std::optional<tensor_view_t> getOutputTensorForInputConnection( const smart_layer_t* connection )
	{
		return output_tensor_view;
	}

	void updateOutputDim() override;

	void forward() override;
	void backward() override {}
	void updateWeights( float learning_rate ) override {}
	std::string info() override { return "renormalize_rgb_vectors"; }
};


struct network_input_layer : public smart_layer_t
{
	virtual void setInputTensor( tensor_view_t t ) { std::swap( input_tensor_view, t ); }

	void connectInput( smart_layer_t* connection ) override;

	void updateInputAndOutputDims() override;

	void updateOutputDim() override;

	void forward() override;
	void backward() override;
	void updateWeights( float learning_rate ) override {}
	std::string info() override { return "network_input_layer"; }
};


// Doesn't really work
struct network_input_recurrent_layer : public smart_layer_t
{
	int n_channels = 0;

	network_input_recurrent_layer() = delete;
	network_input_recurrent_layer( int channels ) : n_channels( channels ) {}

	void updateOutputDim() override;

	bool updateInputAndOutputTensors() override;

	void forward() override;
	void backward() override;
	void updateWeights( float learning_rate ) override {}
	std::string info() override { return "network_input_recurrent_layer"; }
};

/////////////////////////////////////////////////////////////////////////////
// A convolution layer with square, odd-sized filter, no stride or dilation.
/////////////////////////////////////////////////////////////////////////////
struct convolution_layer : public smart_layer_t
{
	float alpha = 1.0f, beta = 0.0f;
	filter_t f;
	tensor_t bias;
	cudnnConvolutionDescriptor_t convolution_descriptor;
	cudnnConvolutionFwdAlgo_t forward_algorithm;
	cudnnActivationDescriptor_t activation_descriptor;
	cudnnConvolutionBwdFilterAlgo_t backward_filter_algorithm;
	cudnnConvolutionBwdDataAlgo_t backward_data_algorithm;
	// Adam optimizer moments
	float *d_adam_s = nullptr, *d_adam_r = nullptr;
	bool algorithm_dirty = true;

public:
	convolution_layer() = delete;

	convolution_layer( int num_input_channels, int num_output_channels, int filter_size );
	convolution_layer( int num_input_channels, int num_output_channels, int filter_size, int stride, int padding, int dilation = 1 );
	convolution_layer( int num_input_channels,
	                   int num_output_channels,
	                   int filter_size,
	                   size2d stride,
	                   size2d padding,
	                   size2d dilation = size2d( 1 ) );
	~convolution_layer();

	bool updateInputAndOutputTensors() override;

	void forward() override;
	void backward() override;
	void updateWeights( float learning_rate ) override;
	bool checkBadFloat() override;

	int filter_input_channels() const;
	int filter_output_channels() const;

protected:
	void init( int num_input_channels, int num_output_channels, int filter_size, size2d stride, size2d padding, size2d dilation );
	void findAlgorithmsAndWorkspace();

	void updateOutputDim() override;

public:
	std::string info() override
	{
		return fmt::format( "conv_bias_relu_layer [{} -> {}] ({},{},{},{}) -> ({},{},{},{})",
		                    input_dim.c,
		                    output_dim.c,
		                    input_dim.n,
		                    input_dim.c,
		                    input_dim.h,
		                    input_dim.w,
		                    output_dim.n,
		                    output_dim.c,
		                    output_dim.h,
		                    output_dim.w );
	}
};

/////////////////////////////////////////////////////////////////////////////
// A transposed convolution (with bias) implemented as a convolution with 
// flipped forward/backward. 
/////////////////////////////////////////////////////////////////////////////
struct transpose_convolution_layer : public smart_layer_t
{
	float alpha = 1.0f, beta = 0.0f;
	filter_t f;
	tensor_t bias;
	bool do_relu = true; 
	cudnnConvolutionDescriptor_t convolution_descriptor;
	cudnnConvolutionBwdDataAlgo_t backward_data_algorithm;
	bool algorithm_dirty = true;

public:
	transpose_convolution_layer() = delete;
	transpose_convolution_layer(int num_input_channels, int num_output_channels, int filter_size, int stride, int padding, int dilation = 1, bool ReLU = true);
	~transpose_convolution_layer();

	void forward() override;
	void backward() override {};
	void updateWeights(float learning_rate) override {};
	//bool checkBadFloat() override;
	//int filter_input_channels() const;
	//int filter_output_channels() const;
	void init(int num_input_channels, int num_output_channels, int filter_size, size2d stride, size2d padding, size2d dilation, bool ReLU);
	void findAlgorithmsAndWorkspace();

	bool updateInputAndOutputTensors() override; 
	void updateInputAndOutputDims() override;
	void updateOutputDim() override;

public:
	std::string info() override
	{
		return fmt::format("transpose_convolution_layer [{} -> {}] ({},{},{},{}) -> ({},{},{},{})",
			input_dim.c,
			output_dim.c,
			input_dim.n,
			input_dim.c,
			input_dim.h,
			input_dim.w,
			output_dim.n,
			output_dim.c,
			output_dim.h,
			output_dim.w);
	}
};


/////////////////////////////////////////////////////////////////////////////
// A convolution, bias and ReLU layer
/////////////////////////////////////////////////////////////////////////////
struct convbiasrelu_layer : public smart_layer_t
{
	float alpha = 1.0f, beta = 0.0f;
	filter_t f;
	tensor_t bias;
	cudnnConvolutionDescriptor_t convolution_descriptor;
	cudnnConvolutionFwdAlgo_t forward_algorithm;
	cudnnActivationDescriptor_t activation_descriptor;
	bool algorithm_dirty = true;

public:
	convbiasrelu_layer() = delete;

	convbiasrelu_layer( int num_input_channels, int num_output_channels, int filter_size );
	convbiasrelu_layer( int num_input_channels, int num_output_channels, int filter_size, int stride, int padding, int dilation = 1 );
	convbiasrelu_layer( int num_input_channels,
	                    int num_output_channels,
	                    int filter_size,
	                    size2d stride,
	                    size2d padding,
	                    size2d dilation = size2d( 1 ) );
	~convbiasrelu_layer();

	bool updateInputAndOutputTensors() override;
	void forward() override;
	bool checkBadFloat() override;

	// Not implemented
	void backward() override;
	void updateWeights( float learning_rate ) override;

	int filter_input_channels() const;
	int filter_output_channels() const;

protected:
	void init( int num_input_channels, int num_output_channels, int filter_size, size2d stride, size2d padding, size2d dilation );
	void findAlgorithmsAndWorkspace();

	void updateOutputDim() override;

public:
	std::string info() override
	{
		return fmt::format( "conv_bias_relu_layer [{} -> {}] ({},{},{},{}) -> ({},{},{},{})",
		                    input_dim.c,
		                    output_dim.c,
		                    input_dim.n,
		                    input_dim.c,
		                    input_dim.h,
		                    input_dim.w,
		                    output_dim.n,
		                    output_dim.c,
		                    output_dim.h,
		                    output_dim.w );
	}
};

/////////////////////////////////////////////////////////////////////////////
// A convolution, bias and ReLU layer, with recurrent channels
/////////////////////////////////////////////////////////////////////////////
struct convbiasrelu_recurrent_layer : public smart_layer_t
{
	float alpha = 1.0f, beta = 0.0f;
	filter_t f;
	tensor_t bias;

	int n_recurrent_channels = 0;
	tensor_dimension true_input_dim;
	tensor_dimension true_output_dim;

	std::optional<tensor_t> network_output_tensor = {};
	std::optional<tensor_t> network_input_tensor = {};
	cudnnConvolutionDescriptor_t convolution_descriptor;
	cudnnConvolutionFwdAlgo_t forward_algorithm;
	cudnnActivationDescriptor_t activation_descriptor;
	bool algorithm_dirty = true;

	tensor_t bnBias;
	tensor_t bnScale;
	tensor_t bnMean;
	tensor_t bnVar;

public:
	convbiasrelu_recurrent_layer() = delete;

	convbiasrelu_recurrent_layer( int num_input_channels, int num_output_channels, int num_recurrent_channels, int filter_size );
	convbiasrelu_recurrent_layer( int num_input_channels,
	                              int num_output_channels,
	                              int num_recurrent_channels,
	                              int filter_size,
	                              int stride,
	                              int padding,
	                              int dilation = 1 );
	~convbiasrelu_recurrent_layer();

	bool updateInputAndOutputTensors() override;
	void forward() override;
	bool checkBadFloat() override;

	void resetRecurrentValues();

	// Not implemented
	void backward() override;
	void updateWeights( float learning_rate ) override;

	int filter_input_channels() const;
	int filter_output_channels() const;

	std::optional<tensor_view_t> getOutputTensorForInputConnection( const smart_layer_t* connection ) override;

	void _setInputTensorFromInputConnection( const smart_layer_t* connection, tensor_view_t input ) override;

protected:
	void init( int num_input_channels,
	           int num_output_channels,
	           int num_recurrent_channels,
	           int filter_size,
	           size2d stride,
	           size2d padding,
	           size2d dilation );
	void findAlgorithmsAndWorkspace();

	void updateOutputDim() override;

public:
	std::string info() override
	{
		return fmt::format( "conv_bias_relu_recurrent_layer [{} (+{}) -> {} (+{})] ({},{},{},{}) -> ({},{},{},{})",
		                    input_dim.c,
		                    n_recurrent_channels,
		                    output_dim.c,
		                    n_recurrent_channels,
		                    true_input_dim.n,
		                    true_input_dim.c,
		                    true_input_dim.h,
		                    true_input_dim.w,
		                    true_output_dim.n,
		                    true_output_dim.c,
		                    true_output_dim.h,
		                    true_output_dim.w );
	}
};


/////////////////////////////////////////////////////////////////////////////
// Pooling layer
/////////////////////////////////////////////////////////////////////////////
struct pool_layer : public smart_layer_t
{
	cudnnPoolingDescriptor_t pooling_descriptor;
	float alpha = 1.0f, beta = 0.0f;
	void init();
	pool_layer() { init(); }
	void forward() override;
	void backward() override;
	void updateWeights( float learning_rate ) override
	{ /* Nothing */
	}

protected:
	void updateOutputDim() override;
	std::string info() override { return "pool_layer"; }
};

/////////////////////////////////////////////////////////////////////////////
// Activation layer
/////////////////////////////////////////////////////////////////////////////
struct activation_layer : public smart_layer_t
{
	float alpha = 1.0f, beta = 0.0f;
	cudnnActivationDescriptor_t activation_descriptor;
	void init( cudnnActivationMode_t mode );
	activation_layer( cudnnActivationMode_t mode ) { init( mode ); }
	void forward() override;
	void backward() override;
	void updateWeights( float learning_rate ) override
	{ /* Nothing */
	}
	std::string info() override { return "activation_layer"; }

protected:
	void updateOutputDim() override;
};

/////////////////////////////////////////////////////////////////////////////
// Fully Connected Layer
/////////////////////////////////////////////////////////////////////////////
struct fc_layer : public layer_t
{
	float alpha = 1.0f, beta = 0.0f;
	int num_input_nodes, num_output_nodes;
	tensor_t weights;
	tensor_t bias;
	// Adam Optimizer Moments
	float *d_adam_s, *d_adam_r;
	void init( int _num_input_nodes, int _num_output_nodes );
	fc_layer( int _num_input_nodes, int _num_output_nodes ) { init( _num_input_nodes, _num_output_nodes ); }
	void Forward( tensor_view_t input_tensor, tensor_view_t output_tensor ) override;
	void Backward( tensor_view_t input_tensor, tensor_view_t output_tensor ) override;
	void UpdateWeights( float learning_rate ) override;
	void CheckForBadFloat() override
	{
		weights.CheckForBadFloat();
		bias.CheckForBadFloat();
	}
};

/////////////////////////////////////////////////////////////////////////////
// Softmax Layer
/////////////////////////////////////////////////////////////////////////////
struct softmax_layer : public layer_t
{
	const float alpha = 1.0f, beta = 0.0f;
	void Forward( tensor_view_t input_tensor, tensor_view_t output_tensor ) override;
	void Backward( tensor_view_t input_tensor, tensor_view_t output_tensor ) override;
	void UpdateWeights( float learning_rate ) override
	{ /* Nothing */
	}
};

/////////////////////////////////////////////////////////////////////////////
// Dropout Layer
/////////////////////////////////////////////////////////////////////////////
struct dropout_layer : public layer_t
{
	cudnnDropoutDescriptor_t descriptor;
	void* workspace = nullptr;
	uint64_t workspace_size = 0;
	void* states;
	uint64_t states_size;
	tensor_dimension last_input_dim;
	dropout_layer( float amount );
	~dropout_layer();
	void FindWorkspace( tensor_view_t& input_tensor );
	void Forward( tensor_view_t input_tensor, tensor_view_t output_tensor ) override;
	void Backward( tensor_view_t input_tensor, tensor_view_t output_tensor ) override;
	void UpdateWeights( float learning_rate ) override
	{ /* Nothing */
	}
};

/////////////////////////////////////////////////////////////////////////////
// Upsample Layer (simple nearest neighbour 2x2 upsampling)
/////////////////////////////////////////////////////////////////////////////
struct upsample_nearest_layer : public smart_layer_t
{
	void forward() override;
	void backward() override;
	void updateWeights( float learning_rate ) override
	{ /* Nothing */
	}

protected:
	void updateOutputDim() override;
	std::string info() override { return "upsample_nearest_layer (x2)"; }
};

/////////////////////////////////////////////////////////////////////////////
// Upsample Layer (simple nearest neighbour 2x2 upsampling)
/////////////////////////////////////////////////////////////////////////////
struct upsample_bilinear_layer : public smart_layer_t
{
	uint32_t factor;

	upsample_bilinear_layer( uint32_t factor );

	void forward() override;
	void backward() override;
	void updateWeights( float learning_rate ) override
	{ /* Nothing */
	}

protected:
	void updateOutputDim() override;
	std::string info() override { return fmt::format( "upsample_bilinear_layer (x{})", factor ); }
};

struct batchnorm2d_layer : public layer_t
{
	size_t num_channels;
	void* std_dev;
	void* mean;

	void init();
	batchnorm2d_layer( size_t num_channels_ ) : num_channels( num_channels_ ) { init(); }
	~batchnorm2d_layer();
	void Forward( tensor_view_t input_tensor, tensor_view_t output_tensor ) override;
	void Backward( tensor_view_t input_tensor, tensor_view_t output_tensor ) override;
	void UpdateWeights( float learning_rate ) override;
};

struct copy_layer : public smart_layer_t
{
	copy_layer() {}
	~copy_layer() {}

	void forward() override;
	void backward() override;
	void updateWeights( float learning_rate ) override {}

protected:
	void updateOutputDim() override;
	std::string info() override { return "copy_layer"; }
};

struct slice_layer : public smart_layer_t
{
	const int32_t dim, begin, end;
	// Takes [begin, end) channels. So slice(_, 1, 2) will take 1 channel.
	slice_layer( uint32_t _dim, uint32_t _begin = 0, int32_t _end = -1 ) : dim( _dim ), begin( _begin ), end( _end ) {}
	~slice_layer() {}

	void forward() override;
	void backward() override;
	void updateWeights( float learning_rate ) override {}

protected:
	void updateOutputDim() override;
	std::string info() override { return "copy_layer"; }
};

struct concat_layer : public smart_layer_t
{
	const uint32_t dimension;
	std::vector<smart_layer_t*> to_concat;
	std::vector<tensor_view_t> to_concat_tensor;
	std::vector<tensor_dimension> to_concat_dim;
	std::vector<size_t> to_concat_offset;

	concat_layer( uint32_t dim ) : dimension( dim ) {}
	~concat_layer();

	void connectInput( smart_layer_t* connection ) override;
	void addConcatInput( smart_layer_t* l );

	void _setInputTensorFromInputConnection( const smart_layer_t* connection, tensor_view_t input ) override;

	void forward() override;
	void backward() override;
	void updateWeights( float learning_rate ) override {}
	std::string info() override
	{
		return fmt::format( "concat_layer (dim {} -> {})", dimension, output_dim.values[dimension] );
	}

protected:
	void updateOutputDim() override;

	std::optional<tensor_view_t> getOutputTensorForInputConnection( const smart_layer_t* connection ) override;
};

struct average_layer : public smart_layer_t
{
	std::vector<tensor_view_t> input_tensor_views;

	std::vector<smart_layer_t*> to_add;

	~average_layer();

	void connectInput( smart_layer_t* connection ) override;
	void addExtraInput( smart_layer_t* l );

	void forward() override;
	void backward() override;
	void updateWeights( float learning_rate ) override;
	std::string info() override { return "average_layer"; }

	void _setInputTensorFromInputConnection( const smart_layer_t* connection, tensor_view_t input ) override;

protected:
	void updateOutputDim() override;
};

struct rgb_to_oklab_layer : public smart_layer_t
{
	void forward() override;
	void backward() override;
	void updateWeights( float learning_rate ) override {}

protected:
	void updateOutputDim() override;

	std::optional<tensor_view_t> getOutputTensorForInputConnection( const smart_layer_t* connection ) override;
	std::string info() override { return "rgb_to_oklab_layer"; }
};

struct oklab_to_rgb_layer : public smart_layer_t
{
	void forward() override;
	void backward() override;
	void updateWeights( float learning_rate ) override {}

protected:
	void updateOutputDim() override;

	std::optional<tensor_view_t> getOutputTensorForInputConnection( const smart_layer_t* connection ) override;
	std::string info() override { return "oklab_to_rgb_layer"; }
};

}   // namespace dnn
