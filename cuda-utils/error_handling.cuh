#include <cudnn.h>
#include <cstdio>
#include <sstream>
#include <iostream>
#include <vector>
#include <utils/log.h>
#include <cuda_fp16.h>

inline void FatalError(const std::string & s, const char * file, int line)
{
    std::stringstream _where, _message;
    _where << file << ':' << line;
    _message << std::string(s) + "\n" << file << ':' << line;
    LOG_ERROR( "{}\nAborting...", _message.str() );
	//OutputDebugStringA( (_message.str() + "\n").c_str() );
    //__debugbreak();
    cudaDeviceReset();
    exit(1);
}

#define CUDA_FATAL_ERROR(err_str) do {                                               \
    LOG_ERROR( "Error in {}({})\n{}\nAborting...", __FILE__, __LINE__, err_str );    \
    cudaDeviceReset();                                                               \
	DEBUG_BREAK();                                                                   \
    exit(1);                                                                         \
} while (0)

#define checkCudnnErr(status) do {                                     \
    cudnnStatus_t _status = status;                                    \
    std::string _error;                                                \
    if (_status != CUDNN_STATUS_SUCCESS) {                             \
        _error = "CUDNN failure: ";                                    \
        _error += cudnnGetErrorString(_status);                        \
        CUDA_FATAL_ERROR(_error);                                      \
    }                                                                  \
} while(0)

#define checkCublasErr(status) do {                                    \
    auto _status = status;                                             \
    std::stringstream _error;                                          \
    if (_status != 0) {                                                \
        _error << "CUBLAS failure: " << _status;                       \
        CUDA_FATAL_ERROR(_error.str());                                \
    }                                                                  \
} while(0)


#define checkCudaErr(status) do {                                      \
    auto _status = status;                                             \
    std::stringstream _error;                                          \
    if (_status != 0) {                                                \
        _error << "Cuda failure: " << cudaGetErrorString(_status);     \
        CUDA_FATAL_ERROR(_error.str());                                \
    }                                                                  \
} while(0)

inline bool CheckForBadFloat(const std::vector<float> & values)
{
    bool bad = false;
	for (uint64_t i = 0; i < values.size(); i++) {
		if (isnan(values[i])) {
            LOG_WARN( "nan at index: {}", i );
            bad = true;
		}
		if (isinf(values[i])) {
            LOG_WARN( "inf at index: {}", i );
            bad = true;
		}
	}
    return bad;
}

inline bool CheckForBadFloat(const std::vector<half> & values)
{
    bool bad = false;
#if defined(__CUDACC__) && __CUDA_ARCH__ >= 530
#define cbf_isnan(x) __hisnan(x)
#define cbf_isinf(x) __hisinf(x)
#else
#define cbf_isnan(x) isnan(__half2float(x))
#define cbf_isinf(x) isinf(__half2float(x))
#endif
	for (uint64_t i = 0; i < values.size(); i++) {
		if ( cbf_isnan(values[i])) {
            LOG_WARN( "nan at index: {}", i );
            bad = true;
		}
		if ( cbf_isinf(values[i])) {
            LOG_WARN( "inf at index: {}", i );
            bad = true;
		}
	}
    return bad;
}

