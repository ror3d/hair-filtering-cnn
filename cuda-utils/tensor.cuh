#pragma once

#include <vector>
#include <string>
#include <cuda_runtime.h>
#include <cudnn.h>
#include <glm/glm.hpp>
#include <initializer_list>
#include <array>

#define DNN_HALF_FLOAT 1

namespace dnn {

#if DNN_HALF_FLOAT
	typedef half nnfloat;
#else
	typedef float nnfloat;
#endif

/////////////////////////////////////////////////////////////////////////////
// A 4d tensor
/////////////////////////////////////////////////////////////////////////////
struct tensor_dimension_cu
{
	union
	{
		struct {
			int n, c, h, w;
		};
		int values[4];
	};
};
struct tensor_dimension {
	union
	{
		struct {
			int n, c, h, w;
		};
		int values[4] = { 0, 0, 0, 0 };
		tensor_dimension_cu _cuda;
	};
	tensor_dimension() = default;
	tensor_dimension(tensor_dimension&&) = default;
	tensor_dimension(const tensor_dimension&) = default;
	constexpr tensor_dimension( int n_, int c_, int h_, int w_ ) : n(n_), c(c_), h(h_), w(w_) { }
	constexpr tensor_dimension( std::initializer_list<int> ilist )
	{
		if ( ilist.size() == 1 )
		{
			for ( int i = 0; i < 4; ++i )
			{
				values[i] = *ilist.begin();
			}
		}
		else
		{
			int i = 0;
			for ( auto n = ilist.begin(); i < 4 && n != ilist.end(); ++i, ++n )
			{
				values[i] = *n;
			}
		}
	}
	tensor_dimension& operator=( const tensor_dimension& ) = default;
	tensor_dimension& operator=( tensor_dimension&& ) = default;

	bool operator==(const tensor_dimension& b) const { return (n == b.n) && (c == b.c) && (h == b.h) && (w == b.w); }
	bool operator!=(const tensor_dimension& b) const { return !(*this == b); }

	inline int size() const { return n * c * h * w; }
};

enum class DType
{
	Float32,
	Float16
};

// Use one of the child specializations: either tensor4D or filter4D
struct tensor4D_base {
	tensor_dimension dim;
	void* d_data = nullptr;
	void* d_grad = nullptr;
	bool has_gradient = true;

#if DNN_HALF_FLOAT
	DType dtype = DType::Float16;
#else
	DType dtype = DType::Float32;
#endif

public:
	virtual void Resize(int n, int c, int h, int w, bool _has_gradient = true) = 0;

	uint64_t SizeInBytes() const;
	void ResetRandom(float variance);
	void ResetZero();
	std::vector<float> DownloadFlatData() const;
	std::vector<float> DownloadFlatGrad() const;
	bool CheckForBadFloat();
	void SetData(float* host_data);
	void SetGrad(float* host_data);
	void SetData(__half* host_data);
	void SetGrad(__half* host_data);

	virtual ~tensor4D_base();

protected:
	tensor4D_base() = default;
	tensor4D_base(const tensor4D_base&) = default;
	tensor4D_base& operator=( const tensor4D_base& ) = default;
	tensor4D_base( tensor4D_base&& t );
	tensor4D_base& operator=( tensor4D_base&& t );
};

struct tensor4D;

class tensor4D_view_t
{
	tensor4D* _tensor = nullptr;

	tensor_dimension _dim;
	tensor_dimension *_dimp = nullptr;

	cudnnTensorDescriptor_t _descriptor = nullptr;

	size_t _offset = 0;
public:
	tensor4D_view_t();
	tensor4D_view_t( const tensor4D_view_t& t );
	tensor4D_view_t( tensor4D_view_t&& t );
	tensor4D_view_t& operator=( const tensor4D_view_t& t );
	tensor4D_view_t& operator=( tensor4D_view_t&& t );

	tensor4D_view_t( tensor4D& t );
	tensor4D_view_t( tensor4D& t, tensor_dimension dim );
	tensor4D_view_t( tensor4D& t, tensor_dimension dim, size_t byte_offset );
	tensor4D_view_t( tensor4D_view_t& t, tensor_dimension dim, size_t byte_offset );

	~tensor4D_view_t();

	tensor_dimension dim();

	void* data();

	void* grad();

	bool has_gradient();

	cudnnTensorDescriptor_t descriptor() const;

	DType dtype();

	bool valid() const { return _tensor != nullptr; }
	operator bool() const { return valid(); }

	tensor4D* get_underlying_tensor() { return _tensor; }
};

struct tensor4D : public tensor4D_base
{
	cudnnTensorDescriptor_t descriptor = nullptr;
public:

	void Resize( int n, int c, int h, int w, bool _has_gradient = true ) override;

	tensor4D( int n, int c, int h, int w, bool has_gradient = true ) { Resize( n, c, h, w, has_gradient ); }
	tensor4D( tensor_dimension dim, bool has_gradient = true ) { Resize( dim.n, dim.c, dim.h, dim.w, has_gradient ); }
	tensor4D() = default;
	tensor4D( const tensor4D& ) = default;

	~tensor4D();

	tensor4D& operator=( const tensor4D& ) = default;
	tensor4D( tensor4D&& t );
	tensor4D& operator=( tensor4D&& t );
};

struct filter4D : public tensor4D_base
{
	cudnnFilterDescriptor_t descriptor = nullptr;
public:

	void Resize(int n, int c, int h, int w, bool _has_gradient = true) override;

	filter4D(int n, int c, int h, int w, bool has_gradient = true) { Resize( n, c, h, w, has_gradient ); }
	filter4D() = default;
	filter4D(const filter4D&) = default;

	~filter4D();

	filter4D& operator=( const filter4D& ) = default;
	filter4D( filter4D&& t );
	filter4D& operator=( filter4D&& t );
};

//typedef tensor4D<cudnnTensorDescriptor_t> tensor_t;
//typedef tensor4D<cudnnFilterDescriptor_t> filter;
typedef tensor4D tensor_t;
typedef tensor4D_view_t tensor_view_t;
typedef filter4D filter_t;


void DumpTensorAsImage( const tensor_t& t, const std::string& filename, float scale = 1.0f, bool gradient = false );
void DumpTensorAsText( const tensor_t& t, const std::string& filename, bool gradient = false );

template<typename T>
std::vector<T> nchw2nhwc(const std::vector<T>& in, dnn::tensor_dimension dim)
{
	std::vector<T> out;
	out.resize(in.size());
	for (size_t n = 0; n < dim.n; ++n)
		for (size_t c = 0; c < dim.c; ++c)
			for (size_t y = 0; y < dim.h; ++y)
				for (size_t x = 0; x < dim.w; ++x)
					out[((n * dim.h + y) * dim.w + x) * dim.c + c] = in[((n * dim.c + c) * dim.h + y) * dim.w + x];
	return out;
};


// WARNING! Avoid using this every frame, it takes much longer than it should
void copyImageToTensor( uint32_t gl_texture_id, dnn::tensor_view_t& tensor );

// WARNING! Avoid using this every frame, it takes much longer than it should
void copyTensorToImage( dnn::tensor_view_t& tensor, uint32_t gl_texture_id, glm::uvec4 channels = {0, 1, 2, 3} );


}  // namespace dnn

