cmake_minimum_required ( VERSION 3.17.0 )

project ( cuda-utils LANGUAGES CXX CUDA )

find_package(CUDAToolkit)

find_package(CUDNN)

set(SOURCE_FILES
    layers.cu
    layers.cuh
    tensor.cu
    tensor.cuh
    error_handling.cuh
    cuda-utils.natvis
    )

set(CUH_REGEX "[^\.]*\.cuh")
set(HEADER_FILES ${SOURCE_FILES})
list(FILTER HEADER_FILES INCLUDE REGEX "${CUH_REGEX}")
list(FILTER SOURCE_FILES EXCLUDE REGEX "${CUH_REGEX}")
source_group("Header Files" FILES ${HEADER_FILES})

# Build and link library.
add_library ( ${PROJECT_NAME} 
    ${SOURCE_FILES}
    ${HEADER_FILES}
    )

set_property(TARGET ${PROJECT_NAME} PROPERTY CUDA_ARCHITECTURES 75)

target_include_directories(${PROJECT_NAME}
    PRIVATE
    ${CMAKE_SOURCE_DIR}/cuda-utils
    )

target_include_directories(${PROJECT_NAME}
    SYSTEM INTERFACE
    ${CMAKE_SOURCE_DIR}/
    )

target_link_libraries ( ${PROJECT_NAME}
    PUBLIC
    CUDA::toolkit
    CUDA::cudart
    CUDA::cublas
    utils
    cudnn
    )

