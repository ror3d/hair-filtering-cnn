#include "error_handling.cuh"
#include "tensor.cuh"
#include <cuda_runtime.h>
#include <cudnn.h>
#include <random>
#include <fstream>

#include <GL/glew.h>

#include <cuda_gl_interop.h>
#include <surface_functions.h>
#include <cuda_fp16.h>

#include <utils/perf.h>
#include "glm/glm.hpp"
#include "stb/stb_image_write.h"

using namespace std;

namespace dnn {
std::default_random_engine generator;

inline static void cloneTensorDescriptor(const cudnnTensorDescriptor_t desc,  cudnnTensorDescriptor_t *cloned)
{
	cudnnDataType_t type;
	int n, c, h, w;
	int nS, cS, hS, wS;
	checkCudnnErr( cudnnGetTensor4dDescriptor(desc, &type, &n, &c, &h, &w, &nS, &cS, &hS, &wS ) );
	if ( *cloned == nullptr )
	{
		checkCudnnErr( cudnnCreateTensorDescriptor( cloned ) );
	}
	checkCudnnErr( cudnnSetTensor4dDescriptorEx( *cloned, type, n, c, h, w, nS, cS, hS, wS ) );
}

void tensor4D::Resize(int n, int c, int h, int w, bool _has_gradient) {
	tensor_dimension new_dim = { n, c, h, w };
	if ( new_dim == dim && has_gradient == _has_gradient)
	{
		return;
	}
	if ( d_data )
	{
		cudaFree( d_data );
	}
	if ( d_grad )
	{
		cudaFree( d_grad );
	}
	has_gradient = _has_gradient;
	dim          = new_dim;
	cudnnDataType_t cudt = CUDNN_DATA_FLOAT;
	if ( dtype == DType::Float16 )
	{
		cudt = CUDNN_DATA_HALF;
	}
	checkCudnnErr(cudnnCreateTensorDescriptor(&descriptor));
	checkCudnnErr(cudnnSetTensor4dDescriptor(descriptor, CUDNN_TENSOR_NHWC, cudt, n, c, h, w));
	//checkCudnnErr(cudnnSetTensor4dDescriptor(descriptor, CUDNN_TENSOR_NCHW, cudt, n, c, h, w));
	checkCudaErr(cudaMalloc(&d_data, SizeInBytes()));
	if(has_gradient) {
		checkCudaErr(cudaMalloc(&d_grad, SizeInBytes()));
		checkCudaErr(cudaMemset(d_grad, 0, SizeInBytes()));
	}
}
void filter4D::Resize(int n, int c, int h, int w, bool _has_gradient) {
	tensor_dimension new_dim = { n, c, h, w };
	if ( new_dim == dim && has_gradient == _has_gradient)
	{
		return;
	}
	if ( d_data )
	{
		cudaFree( d_data );
	}
	if ( d_grad )
	{
		cudaFree( d_grad );
	}
	has_gradient = _has_gradient;
	dim          = new_dim;
	cudnnDataType_t cudt = CUDNN_DATA_FLOAT;
	if ( dtype == DType::Float16 )
	{
		cudt = CUDNN_DATA_HALF;
	}
	checkCudnnErr(cudnnCreateFilterDescriptor(&descriptor));
	//checkCudnnErr(cudnnSetFilter4dDescriptor(descriptor, cudt, CUDNN_TENSOR_NCHW, n, c, h, w));
	checkCudnnErr(cudnnSetFilter4dDescriptor(descriptor, cudt, CUDNN_TENSOR_NHWC, n, c, h, w));
	checkCudaErr(cudaMalloc(&d_data, SizeInBytes()));
	if(has_gradient) {
		checkCudaErr(cudaMalloc(&d_grad, SizeInBytes()));
		checkCudaErr(cudaMemset(d_grad, 0, SizeInBytes()));
	}
}

uint64_t tensor4D_base::SizeInBytes() const
{
	switch ( dtype )
	{
		case DType::Float16:
			return dim.size() * sizeof( half );
		case DType::Float32:
			return dim.size() * sizeof( float );
		default:
			CUDA_FATAL_ERROR( "Not implemented" );
	}
}

void tensor4D_base::ResetRandom(float variance) {
	// Assuming no initialization for gradient.
	float stddev = sqrt(variance);
	float mean   = 0.0f;
	std::normal_distribution<float> distribution(mean, stddev);
	vector<float> values(dim.size());
	for(int i = 0; i < dim.size(); i++) values[i] = distribution(generator);
	checkCudaErr(cudaMemcpy(d_data, values.data(), SizeInBytes(), cudaMemcpyHostToDevice));
}

void tensor4D_base::ResetZero() {
	checkCudaErr(cudaMemset(d_data, 0, SizeInBytes()));
	if(has_gradient) checkCudaErr(cudaMemset(d_grad, 0, SizeInBytes()));
}

vector<float> tensor4D_base::DownloadFlatData() const
{
	vector<float> values(dim.size());
	if ( dtype == DType::Float16 )
	{
		vector<__half> data(dim.size());
		checkCudaErr(cudaMemcpy(data.data(), d_data, SizeInBytes(), cudaMemcpyDeviceToHost));
		for ( size_t i = 0; i < dim.size(); ++i )
		{
			values[i] = __half2float( data[i] );
		}
	}
	else
	{
		checkCudaErr(cudaMemcpy(values.data(), d_data, SizeInBytes(), cudaMemcpyDeviceToHost));
	}
	return values;
}

vector<float> tensor4D_base::DownloadFlatGrad() const
{
	vector<float> values(dim.size());
	if ( dtype == DType::Float16 )
	{
		vector<__half> data(dim.size());
		checkCudaErr(cudaMemcpy(data.data(), d_grad, SizeInBytes(), cudaMemcpyDeviceToHost));
		for ( size_t i = 0; i < dim.size(); ++i )
		{
			values[i] = __half2float( data[i] );
		}
	}
	else
	{
		checkCudaErr(cudaMemcpy(values.data(), d_grad, SizeInBytes(), cudaMemcpyDeviceToHost));
	}
	return values;
}

bool tensor4D_base::CheckForBadFloat()
{
	bool bad = true;
	if ( dtype == DType::Float16 )
	{
		vector<half> values(dim.size());
		checkCudaErr(cudaMemcpy(values.data(), d_data, SizeInBytes(), cudaMemcpyDeviceToHost));
		bad = ::CheckForBadFloat(values);
		if (has_gradient)
		{
			checkCudaErr(cudaMemcpy(values.data(), d_grad, SizeInBytes(), cudaMemcpyDeviceToHost));
			bad = bad || ::CheckForBadFloat(values);
		}
	}
	else
	{
		vector<float> values(dim.size());
		checkCudaErr(cudaMemcpy(values.data(), d_data, SizeInBytes(), cudaMemcpyDeviceToHost));
		bad = ::CheckForBadFloat(values);
		if (has_gradient)
		{
			checkCudaErr(cudaMemcpy(values.data(), d_grad, SizeInBytes(), cudaMemcpyDeviceToHost));
			bad = bad || ::CheckForBadFloat(values);
		}
	}
	return bad;
}

void tensor4D_base::SetData( float* host_data )
{
	if ( dtype == DType::Float32 )
	{
		checkCudaErr(cudaMemcpy(d_data, host_data, SizeInBytes(), cudaMemcpyHostToDevice));
	}
	else
	{
		std::vector<__half> data( dim.size(), __half( 0.f ) );
		for ( size_t i = 0; i < dim.size(); ++i )
		{
			data[i] = __float2half( host_data[i] );
		}
		SetData( data.data() );
	}
}

void tensor4D_base::SetGrad(float* host_data)
{
	if ( dtype == DType::Float32 )
	{
		checkCudaErr(cudaMemcpy(d_grad, host_data, SizeInBytes(), cudaMemcpyHostToDevice));
	}
	else
	{
		std::vector<__half> data(dim.size(), __half(0.f));
		for ( size_t i = 0; i < dim.size(); ++i )
		{
			data[i] = __float2half( host_data[i] );
		}
		SetGrad( data.data() );
	}
}

void tensor4D_base::SetData(__half* host_data)
{
	if ( dtype == DType::Float16 )
	{
		checkCudaErr(cudaMemcpy(d_data, host_data, SizeInBytes(), cudaMemcpyHostToDevice));
	}
	else
	{
		std::vector<float> data(dim.size(), 0.f);
		for ( size_t i = 0; i < dim.size(); ++i )
		{
			data[i] = __half2float( host_data[i] );
		}
		SetData( data.data() );
	}
}

void tensor4D_base::SetGrad(__half* host_data)
{
	if ( dtype == DType::Float16 )
	{
		checkCudaErr( cudaMemcpy( d_grad, host_data, SizeInBytes(), cudaMemcpyHostToDevice ) );
	}
	else
	{
		std::vector<float> data(dim.size(), 0.f);
		for ( size_t i = 0; i < dim.size(); ++i )
		{
			data[i] = __half2float( host_data[i] );
		}
		SetGrad( data.data() );
	}
}

tensor4D_base::tensor4D_base( tensor4D_base && t )
{
	*this = std::move( t );
}

tensor4D_base& tensor4D_base::operator=( tensor4D_base && t )
{
	std::swap( dim, t.dim );
	std::swap( d_data, t.d_data );
	std::swap( d_grad, t.d_grad );
	std::swap( has_gradient, t.has_gradient );
	std::swap( dtype, t.dtype );
	return *this;
}


tensor4D::tensor4D( tensor4D && t )
{
	*this = std::move( t );
}

tensor4D::~tensor4D()
{
	if ( descriptor )
	{
		checkCudnnErr( cudnnDestroyTensorDescriptor( descriptor ) );
		descriptor = nullptr;
	}
}

tensor4D& tensor4D::operator=( tensor4D && t )
{
	tensor4D_base::operator=( std::move( static_cast<tensor4D_base&>(t) ) );

	cudnnTensorDescriptor_t tmpd;
	std::memcpy( &tmpd, &descriptor, sizeof( descriptor ) );
	std::memcpy( &descriptor, &t.descriptor, sizeof( descriptor ) );
	std::memcpy( &t.descriptor, &tmpd, sizeof( descriptor ) );
	return *this;
}

tensor4D_view_t::tensor4D_view_t( tensor4D& t )
{
	_tensor = &t;
	cloneTensorDescriptor( t.descriptor, &_descriptor );
	_dimp = &_tensor->dim;
}

tensor4D_view_t::tensor4D_view_t()
{
	_dimp = &_dim;
}

tensor4D_view_t::tensor4D_view_t( const tensor4D_view_t& t )
{
	*this = t;
}

tensor4D_view_t::tensor4D_view_t( tensor4D_view_t&& t )
{
	*this = std::move(t);
}

tensor4D_view_t& tensor4D_view_t::operator=( const tensor4D_view_t& t )
{
	_tensor = t._tensor;
	cloneTensorDescriptor( t._descriptor, &_descriptor );
	_dim = t._dim;
	if ( t._dimp == &t._dim )
	{
		_dimp = &_dim;
	}
	else
	{
		_dimp = &_tensor->dim;
	}
	_offset = t._offset;
	return *this;
}

tensor4D_view_t& tensor4D_view_t::operator=( tensor4D_view_t&& t )
{
	std::swap( _tensor, t._tensor );
	std::swap( _dim, t._dim );

	auto old_dimp = _dimp;
	if ( t._dimp == &t._dim )
	{
		_dimp = &_dim;
	}
	else
	{
		_dimp = &_tensor->dim;
	}

	if ( old_dimp == &_dim )
	{
		t._dimp = &t._dim;
	}
	else
	{
		t._dimp = &t._tensor->dim;
	}

	std::swap( _descriptor, t._descriptor );
	std::swap( _offset, t._offset );
	return *this;
}

tensor4D_view_t::tensor4D_view_t( tensor4D& t, tensor_dimension dim )
{
	_tensor = &t;
	_dim = dim;
	_dimp = &_dim;

	// Create descriptor
	cudnnDataType_t cudt = CUDNN_DATA_FLOAT;
	if ( t.dtype == DType::Float16 )
	{
		cudt = CUDNN_DATA_HALF;
	}
	checkCudnnErr(cudnnCreateTensorDescriptor(&_descriptor));
	checkCudnnErr(cudnnSetTensor4dDescriptor(_descriptor, CUDNN_TENSOR_NHWC, cudt, dim.n, dim.c, dim.h, dim.w));
}

tensor4D_view_t::tensor4D_view_t( tensor4D& t, tensor_dimension dim, size_t byte_offset )
	: tensor4D_view_t(t, dim)
{
	_offset = byte_offset;
}

tensor4D_view_t::tensor4D_view_t( tensor4D_view_t& t, tensor_dimension dim, size_t byte_offset )
	: tensor4D_view_t(*t._tensor, dim, byte_offset)
{
}


tensor_dimension tensor4D_view_t::dim()
{
	return *_dimp;
}

void* tensor4D_view_t::data()
{
	return reinterpret_cast<void*>(reinterpret_cast<uint8_t*>(_tensor->d_data) + _offset);
}

bool tensor4D_view_t::has_gradient()
{
	return _tensor->has_gradient;
}

tensor4D_view_t::~tensor4D_view_t()
{
	if ( _descriptor )
	{
		checkCudnnErr( cudnnDestroyTensorDescriptor( _descriptor ) );
		_descriptor = nullptr;
	}
}

void* tensor4D_view_t::grad()
{
	return reinterpret_cast<void*>(reinterpret_cast<char*>(_tensor->d_grad) + _offset);
}

cudnnTensorDescriptor_t tensor4D_view_t::descriptor() const
{
	return _descriptor;
}

DType tensor4D_view_t::dtype()
{
	return _tensor->dtype;
}


filter4D::filter4D( filter4D && t )
{
	*this = std::move( t );
}

filter4D::~filter4D()
{
	if ( descriptor )
	{
		checkCudnnErr( cudnnDestroyFilterDescriptor( descriptor ) );
		descriptor = nullptr;
	}
}

filter4D& filter4D::operator=( filter4D && t )
{
	tensor4D_base::operator=( std::move( static_cast<tensor4D_base&>(t) ) );

	cudnnTensorDescriptor_t tmpd;
	std::memcpy( &tmpd, &descriptor, sizeof( descriptor ) );
	std::memcpy( &descriptor, &t.descriptor, sizeof( descriptor ) );
	std::memcpy( &t.descriptor, &tmpd, sizeof( descriptor ) );
	return *this;
}



tensor4D_base::~tensor4D_base()
{
	if ( d_data )
	{
		checkCudaErr(cudaFree(d_data));
	}
	if ( has_gradient && d_grad )
	{
		checkCudaErr( cudaFree( d_grad ) );
	}
}

// Explicit instantiation of both types of tensors
//template struct tensor4D<cudnnTensorDescriptor_t>;
//template struct tensor4D<cudnnFilterDescriptor_t>;


void DumpTensorAsImage( const tensor_t& t, const std::string& filename, float scale, bool gradient )
{
	LOG_WARN( "Currently only implemented for NCHW tensors, not NHWC!" );
	if ( t.dim.c != 3 )
	{
		/*cout << "ERROR: DumpTensorAsImage only implemented for 3-channel tensors so far.\n";
		exit(0);*/
		// Nah, just dump three first channels, might wanna add an offset argument
	}
	int image_channels = 3;
	vector<uint8_t> batch_data( t.dim.w * 4 * t.dim.h * 4 * image_channels );
	vector<float> tdata;
	if ( !gradient )
		tdata = t.DownloadFlatData();
	else
		tdata = t.DownloadFlatGrad();
	for ( int Y = 0; Y < 4; Y++ )
	{
		for ( int X = 0; X < 4; X++ )
		{
			vector<uint8_t> data( t.dim.w * t.dim.h * image_channels );
			int batch_id = Y * 4 + X;
			for ( int y = 0; y < t.dim.h; y++ )
			{
				for ( int x = 0; x < t.dim.w; x++ )
				{
					glm::vec3 frgb =
						glm::vec3( tdata[batch_id * t.dim.c * t.dim.h * t.dim.w + 0 * t.dim.h * t.dim.w + y * t.dim.w + x],
								   tdata[batch_id * t.dim.c * t.dim.h * t.dim.w + 1 * t.dim.h * t.dim.w + y * t.dim.w + x],
								   tdata[batch_id * t.dim.c * t.dim.h * t.dim.w + 2 * t.dim.h * t.dim.w + y * t.dim.w + x] )
						* scale;
					glm::ivec3 irgb;
					irgb = glm::clamp( glm::ivec3( abs( frgb * 255.0f ) ), glm::ivec3( 0 ), glm::ivec3( 255 ) );
					if ( isnan( frgb.x ) || isnan( frgb.y ) || isnan( frgb.z ) )
						irgb = glm::vec3( 255, 0, 255 );
					else if ( isinf( frgb.x ) || isinf( frgb.y ) || isinf( frgb.z ) )
						irgb = glm::vec3( 0, 255, 0 );
					// else if (abs(frgb.x) > 100.0f || abs(frgb.y) > 100.0f || abs(frgb.z) > 100.0f) irgb = glm::vec3(255, 255,
					// 0); else if (frgb.x < 0.0f || frgb.y < 0.0f || frgb.z < 0.0f) { irgb.r = 255; } else if (frgb.x > 1.0f ||
					// frgb.y > 1.0f || frgb.z > 1.0f) { irgb.b = 255; }
					data[y * t.dim.w * image_channels + x * image_channels + 0] = irgb.x;
					data[y * t.dim.w * image_channels + x * image_channels + 1] = irgb.y;
					data[y * t.dim.w * image_channels + x * image_channels + 2] = irgb.z;
				}
			}
			for ( int y = 0; y < t.dim.h; y++ )
			{
				for ( int x = 0; x < t.dim.w; x++ )
				{
					batch_data[((Y * t.dim.h + y) * (t.dim.w * 4) + X * t.dim.w + x) * 3 + 0] =
						data[y * t.dim.w * image_channels + x * image_channels + 0];
					batch_data[((Y * t.dim.h + y) * (t.dim.w * 4) + X * t.dim.w + x) * 3 + 1] =
						data[y * t.dim.w * image_channels + x * image_channels + 1];
					batch_data[((Y * t.dim.h + y) * (t.dim.w * 4) + X * t.dim.w + x) * 3 + 2] =
						data[y * t.dim.w * image_channels + x * image_channels + 2];
				}
			}
		}
	}
	stbi_flip_vertically_on_write( true );
	stbi_write_png( filename.c_str(), t.dim.w * 4, t.dim.h * 4, image_channels, batch_data.data(), 0 );
}

void DumpTensorAsText( const tensor_t& t, const std::string& filename, bool gradient )
{
	vector<float> tdata;
	if ( !gradient )
		tdata = t.DownloadFlatData();
	else
		tdata = t.DownloadFlatGrad();

	std::ofstream f( filename );

	f << "[\n";
	for ( size_t b = 0; b < t.dim.n; b++ )
	{
		f << "\t[\n";
		for ( size_t c = 0; c < t.dim.c; c++ )
		{
			f << "\t\t[\n";
			for ( size_t y = 0; y < t.dim.h; y++ )
			{
				f << "\t\t\t[";
				for ( size_t x = 0; x < t.dim.w; x++ )
				{
					f << fmt::format("{: .6f}", tdata[b * t.dim.c * t.dim.h * t.dim.w + c * t.dim.h * t.dim.w + y * t.dim.w + x]) << ", ";
				}
				f << "],\n";
			}
			f << "\t\t],\n";
		}
		f << "\t],\n";
	}
	f << "]\n";
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copy Image to tensor
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define COPY_TENSOR_TO_IMAGE_USE_SURFACE 1

__global__ void copyImageToTensor_rgba16_fp16( half* tensor, cudaSurfaceObject_t image, uint32_t width, uint32_t height )
{
	int tx = blockIdx.x * blockDim.x + threadIdx.x;
	int ty = blockIdx.y * blockDim.y + threadIdx.y;

	if ( tx >= width || ty >= height )
	{
		return;
	}

	//ushort4 color = tex2D<ushort4>(image, tx * sizeof(color), height - ty - 1);
	ushort4 color = surf2Dread<ushort4>( image, tx * sizeof( color ), height - ty - 1, cudaBoundaryModeZero );


	tensor[0 + 4 * (ty * width + tx)] = __ushort_as_half(color.x);
	tensor[1 + 4 * (ty * width + tx)] = __ushort_as_half(color.y);
	tensor[2 + 4 * (ty * width + tx)] = __ushort_as_half(color.z);
	tensor[3 + 4 * (ty * width + tx)] = __ushort_as_half(color.w);
}

__global__ void copyImageToTensor_rgba16_fp32( float* tensor, cudaSurfaceObject_t image, uint32_t width, uint32_t height )
{
	int tx = blockIdx.x * blockDim.x + threadIdx.x;
	int ty = blockIdx.y * blockDim.y + threadIdx.y;

	if ( tx >= width || ty >= height )
	{
		return;
	}

	uint32_t stride = width * height;

	ushort4 color;
	color = surf2Dread<ushort4>( image, tx * sizeof( color ), height - ty - 1, cudaBoundaryModeZero );

	tensor[0 * stride + ty * width + tx] = __half2float(__ushort_as_half(color.x));
	tensor[1 * stride + ty * width + tx] = __half2float(__ushort_as_half(color.y));
	tensor[2 * stride + ty * width + tx] = __half2float(__ushort_as_half(color.z));
	tensor[3 * stride + ty * width + tx] = __half2float(__ushort_as_half(color.w));
}


void copyImageToTensor( uint32_t gl_texture, dnn::tensor_view_t& tensor )
{
	// Let's first check the texture has the right properties:
	GLint format;
	glGetTextureLevelParameteriv( GLuint(gl_texture), 0, GL_TEXTURE_INTERNAL_FORMAT, &format );

	if ( format != GL_RGBA16F )
	{
		CUDA_FATAL_ERROR( "Input image does not have 4 channels with 16bit floats per channel!" );
	}
	if ( tensor.dim().c != 4 )
	{
		CUDA_FATAL_ERROR( "Tensor does not have 4 channels!" );
	}

	cudaGraphicsResource* input_img_res = nullptr;

	checkCudaErr( cudaGraphicsGLRegisterImage( &input_img_res, gl_texture, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsReadOnly ) );

	checkCudaErr( cudaGraphicsMapResources( 1, &input_img_res, 0 ) );

	cudaArray_t d_array;
	checkCudaErr( cudaGraphicsSubResourceGetMappedArray( &d_array, input_img_res, 0, 0 ) );

	cudaSurfaceObject_t surfInput;
	cudaResourceDesc    surfRes;
	memset( &surfRes, 0, sizeof( cudaResourceDesc ) );
	surfRes.resType = cudaResourceTypeArray;
	surfRes.res.array.array = d_array;

	checkCudaErr( cudaCreateSurfaceObject( &surfInput, &surfRes ) );

	dim3 block = { 16, 16, 1 };
	dim3 grid = { (tensor.dim().w + block.x - 1) / block.x, (tensor.dim().h + block.y - 1) / block.y };

	if ( tensor.dtype() == DType::Float16 )
	{
		chag::perf::Scope s("copyImageToTensor_rgba16_fp16");
		copyImageToTensor_rgba16_fp16 <<< grid, block >>> ((half*)tensor.data(), surfInput, tensor.dim().w, tensor.dim().h);
	}
	else
	{
		copyImageToTensor_rgba16_fp32 <<< grid, block >>> ((float*)tensor.data(), surfInput, tensor.dim().w, tensor.dim().h);
	}

	checkCudaErr( cudaGetLastError() );

	checkCudaErr( cudaDeviceSynchronize() );
	checkCudaErr( cudaGetLastError() );

	checkCudaErr( cudaDestroySurfaceObject( surfInput ) );

	checkCudaErr( cudaGraphicsUnmapResources( 1, &input_img_res, 0 ) );

	checkCudaErr( cudaGraphicsUnregisterResource( input_img_res ) );
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copy Tensor to Image
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void copyTensorToImage_fp32_rgba16( const float* tensor, cudaSurfaceObject_t image, glm::uvec4 channels_to_copy, tensor_dimension_cu dim )
{
	int tx = blockIdx.x * blockDim.x + threadIdx.x;
	int ty = blockIdx.y * blockDim.y + threadIdx.y;

	if ( tx >= dim.w || ty >= dim.h )
	{
		return;
	}

	ushort4 color;
	color.x = __half_as_ushort(__float2half(tensor[((dim.h - ty - 1) * dim.w + tx) * dim.c + channels_to_copy[0]]));
	color.y = __half_as_ushort(__float2half(tensor[((dim.h - ty - 1) * dim.w + tx) * dim.c + channels_to_copy[1]]));
	color.z = __half_as_ushort(__float2half(tensor[((dim.h - ty - 1) * dim.w + tx) * dim.c + channels_to_copy[2]]));
	color.w = __half_as_ushort(__float2half(tensor[((dim.h - ty - 1) * dim.w + tx) * dim.c + channels_to_copy[3]]));

	surf2Dwrite( color, image, tx * sizeof(color), ty );
}

__global__ void copyTensorToImage_fp16_rgba16( const half* tensor, cudaSurfaceObject_t image, glm::uvec4 channels_to_copy, tensor_dimension_cu dim )
{
	int tx = blockIdx.x * blockDim.x + threadIdx.x;
	int ty = blockIdx.y * blockDim.y + threadIdx.y;

	if ( tx >= dim.w || ty >= dim.h )
	{
		return;
	}

	ushort4 color;
	color.x = __half_as_ushort(tensor[((dim.h - ty - 1) * dim.w + tx) * dim.c + channels_to_copy[0]]);
	color.y = __half_as_ushort(tensor[((dim.h - ty - 1) * dim.w + tx) * dim.c + channels_to_copy[1]]);
	color.z = __half_as_ushort(tensor[((dim.h - ty - 1) * dim.w + tx) * dim.c + channels_to_copy[2]]);
	color.w = __half_as_ushort(tensor[((dim.h - ty - 1) * dim.w + tx) * dim.c + channels_to_copy[3]]);

	surf2Dwrite( color, image, tx * sizeof(color), ty );
}


void copyTensorToImage( dnn::tensor_view_t& tensor, uint32_t gl_texture_id, glm::uvec4 channels )
{
	// Let's first check the texture has the right properties:
	GLint format;
	glGetTextureLevelParameteriv( GLuint( gl_texture_id ), 0, GL_TEXTURE_INTERNAL_FORMAT, &format );

	if ( format != GL_RGBA16F )
	{
		CUDA_FATAL_ERROR( "Output image does not have 4 channels with 16bit floats per channel!" );
	}
	for ( int i = 0; i < 4; ++i )
	{
		if ( channels[i] >= tensor.dim().c )
		{
			CUDA_FATAL_ERROR( fmt::format("Tensor only has {} channels, can't access channel {}", tensor.dim().c, channels[i]) );
		}
	}

	cudaGraphicsResource* output_framebuffer_res = nullptr;

	checkCudaErr( cudaGraphicsGLRegisterImage( &output_framebuffer_res, gl_texture_id, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsWriteDiscard ) );

	checkCudaErr( cudaGraphicsMapResources( 1, &output_framebuffer_res, 0 ) );

	cudaArray_t d_array;
	checkCudaErr( cudaGraphicsSubResourceGetMappedArray( &d_array, output_framebuffer_res, 0, 0 ) );

	dim3 block = { 16, 16, 1 };
	dim3 grid = { (tensor.dim().w + block.x - 1) / block.x, (tensor.dim().h + block.y - 1) / block.y };

#if COPY_TENSOR_TO_IMAGE_USE_SURFACE
	cudaSurfaceObject_t surfOutput;
	cudaResourceDesc    surfRes;
	memset( &surfRes, 0, sizeof( cudaResourceDesc ) );
	surfRes.resType = cudaResourceTypeArray;
	surfRes.res.array.array = d_array;

	checkCudaErr( cudaCreateSurfaceObject( &surfOutput, &surfRes ) );

	checkCudaErr( cudaGetLastError() );

	if ( tensor.dtype() == DType::Float16 )
	{
		copyTensorToImage_fp16_rgba16 <<< grid, block >>> ((const half*)tensor.data(), surfOutput, channels, tensor.dim()._cuda);
	}
	else
	{
		copyTensorToImage_fp32_rgba16 <<< grid, block >>> ((const float*)tensor.data(), surfOutput, channels, tensor.dim()._cuda);
	}

	checkCudaErr( cudaGetLastError() );

	checkCudaErr( cudaDeviceSynchronize() );
	checkCudaErr( cudaGetLastError() );

	checkCudaErr( cudaDestroySurfaceObject( surfOutput ) );
#else
	copyTensorToImage <<< grid, block >>> (outputs[24]->d_data, output_rgba, outputs[24]->dim.w, outputs[24]->dim.h);

	checkCudaErr( cudaGetLastError() );

	checkCudaErr( cudaMemcpy2DToArray( d_array, 0, 0, output_rgba, outputs[24]->dim.w * sizeof(vec4),
									   outputs[24]->dim.w * sizeof(vec4), outputs[24]->dim.h, cudaMemcpyDeviceToDevice ) );
#endif

	checkCudaErr( cudaGraphicsUnmapResources( 1, &output_framebuffer_res, 0 ) );

	checkCudaErr( cudaGraphicsUnregisterResource( output_framebuffer_res ) );
}

std::vector<float> nchw2nhwc( const std::vector<float>& in, dnn::tensor_dimension dim )
{
	std::vector<float> out;
	out.resize( in.size() );
	for ( size_t n = 0; n < dim.n; ++n )
	{
		for ( size_t c = 0; c < dim.c; ++c )
		{
			for ( size_t y = 0; y < dim.h; ++y )
			{
				for ( size_t x = 0; x < dim.w; ++x )
				{
					out[((n * dim.h + y) * dim.w + x) * dim.c + c] = in[((n * dim.c + c) * dim.h + y) * dim.w + x];
				}
			}
		}
	}
	return out;
};



};  // namespace dnn