# Using the Renderer

## Dependencies
Our setup uses the following, although it might work with different versions:

Cmake 3.22
MS Visual Studio 2019
CUDA 11.4
CuDNN 6.14

## Initial Setup

```
$ mkdir build
$ cd build
$ cmake -G "Visual Studio 16 2019" ..
```

This should have created the file `hair-filtering.sln` which you can now open.

Set the starting project to be `hair-filtering`. Build and run should start the application.

Download hair file models from <www.cemyuksel.com/research/hairmodels>, and open with the File menu.

The real-time evaluation can be found through the `modules` in the menu bar. The network used can be changed with the `model` menu in the bar that appears when rendering with the network.


## Troubleshooting

> Error when trying to use the NN Eval module: `Could not load library cudnn_cnn_infer64_8.dll`

Make sure that CUDA and CuDNN are both installed and that your PATH system variable points to the `bin` directory for them. Otherwise, copying the mentioned dll to the `bin` directory that is created when compiling the application should help solve it.


# Neural Network Training Setup

## Dependencies
You will need python with pytorch installed. Our setup uses:

python 3.7.5
pytorch 1.8.1

## Generating the training data
With the renderer you can generate the training data.

Start with the renderer in the multisample module. You should see a window specific for that purpose. Right now it's set to create 400 different, with 2 sets of samples for each, which takes about 10GB of disk space. You can tweak any of the parameters to change these things.

The "batch number" is to allow for using multiple hairstyles: with batch=1 it is going to create images starting at index 1, if you set batch=2, the first image generated will be at index=number of items (400 by default).

Once you press Generate, you will be able to choose the directory you want the output to go to. A directory structure will be created there with the different sets of images.

The generator will then iterate through all the images to generate, first for the multisample renderer, then for the high resolution renderer; including training and validation sets (if selected).


## Generating cache

To improve and speed up training, we use a cache that holds the already subdivided images.

Create it by opening the `nn/` directory in a terminal and using:

```
$ python hair-filter.py --in-dir=<path-to-the-data-root> --make-cache
```

This will create a couple more directories in the directory structure with the cache data.


## Training

Calling `python hair-filter.py` without parameters will give you a set of options you can use with it.

A call to train a dataset at `train_data` could look like:

```
$ python hair-filter.py --cached --model=medium --epochs=150 --in-dir=train_data --out-dir=train_results
```

In the `train_results` directory the results will appear, including a set of images for each epoch that show the progress of the training with the validation image.



