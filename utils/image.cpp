#include "image.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

bool ImageF::load( std::filesystem::path path )
{
//    unsigned char *data = stbi_load(filename, &x, &y, &n, 0);
//    // ... process data if not NULL ...
//    // ... x = width, y = height, n = # 8-bit components per pixel ...
//    // ... replace '0' with '1'..'4' to force that many components per pixel
//    // ... but 'n' will always be the number that it would have been if you said 0
//    stbi_image_free(data)
	int w,h,n;
	unsigned char* data = stbi_load( path.string().c_str(), &w, &h, &n, 4 );

	if ( !data )
	{
		return false;
	}

	width = w;
	height = h;
	pixels.resize( w * h );

	for ( size_t j = 0; j < h; ++j )
	{
		for ( size_t i = 0; i < w; ++i )
		{
			size_t tgt_idx = j * w + i;
			size_t src_idx = 4*((h - j - 1) * w + i);
			//pixels[i] = glm::vec4( data[4 * l - i * 4 - 4], data[4 * l - i * 4 - 3], data[4 * l - i * 4 - 2], data[4 * l - i * 4 - 1] ) / 255.f;
			pixels[tgt_idx] = glm::vec4( data[src_idx], data[src_idx + 1], data[src_idx + 2], data[src_idx + 3] ) / 255.f;
		}
	}

	return true;
}
