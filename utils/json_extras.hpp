#pragma once
#include "json.hpp"
#include "Log.h"

#include <glm/gtx/quaternion.hpp>
#include <glm/glm.hpp>


namespace glm
{
using json = nlohmann::json;

inline void from_json( const json& j, vec4& a )
{
	DebugAssert( j.size() == 4 );
	a.x = j.at( 0 );
	a.y = j.at( 1 );
	a.z = j.at( 2 );
	a.w = j.at( 3 );
}

inline void to_json( json& j, const vec4& b )
{
	j = json(std::vector<float>{ b.x, b.y, b.z, b.w });
}

inline void from_json( const json& j, vec3& a )
{
	DebugAssert( j.size() == 3 );
	a.x = j.at( 0 );
	a.y = j.at( 1 );
	a.z = j.at( 2 );
}

inline void from_json( const json& j, ivec4& a )
{
	DebugAssert( j.size() == 4 );
	a.x = j.at( 0 );
	a.y = j.at( 1 );
	a.z = j.at( 2 );
	a.w = j.at( 3 );
}

inline void to_json( json& j, const ivec4& b )
{
	j = json(std::vector<int32_t>{ b.x, b.y, b.z, b.w });
}

inline void to_json( json& j, const vec3& b )
{
	j = json(std::vector<float>{ b.x, b.y, b.z });
}

inline void from_json( const json& j, ivec3& a )
{
	DebugAssert( j.size() == 3 );
	a.x = j.at( 0 );
	a.y = j.at( 1 );
	a.z = j.at( 2 );
}

inline void to_json( json& j, const ivec3& b )
{
	j = json(std::vector<int32_t>{ b.x, b.y, b.z });
}

inline void from_json( const json& j, vec2& a )
{
	DebugAssert( j.size() == 2 );
	a.x = j.at( 0 );
	a.y = j.at( 1 );
}

inline void to_json( json& j, const vec2& b )
{
	j = json(std::vector<float>{ b.x, b.y });
}

inline void from_json( const json& j, u16vec2& a )
{
	DebugAssert( j.size() == 2 );
	a.x = j.at( 0 );
	a.y = j.at( 1 );
}

inline void to_json( json& j, const u16vec2& b )
{
	j = json(std::vector<uint16_t>{ b.x, b.y });
}

inline void from_json( const json& j, uvec2& a )
{
	DebugAssert( j.size() == 2 );
	a.x = j.at( 0 );
	a.y = j.at( 1 );
}

inline void to_json( json& j, const uvec2& b )
{
	j = json(std::vector<uint32_t>{ b.x, b.y });
}

inline void from_json( const json& j, ivec2& a )
{
	DebugAssert( j.size() == 2 );
	a.x = j.at( 0 );
	a.y = j.at( 1 );
}

inline void to_json( json& j, const ivec2& b )
{
	j = json(std::vector<int32_t>{ b.x, b.y });
}

inline void from_json( const json& j, quat& a )
{
	DebugAssert( j.size() == 4 );
	a.x = j.at( 0 );
	a.y = j.at( 1 );
	a.z = j.at( 2 );
	a.w = j.at( 3 );
}

inline void to_json( json& j, const quat& b )
{
	j = json(std::vector<float>{ b.x, b.y, b.z, b.w });
}

inline void from_json( const json& js, mat4& a )
{
	DebugAssert( js.size() == 16 );
	for ( length_t i = 0; i < 4; ++i )
	{
		for ( length_t j = 0; j < 4; ++j )
		{
			a[i][j] = js.at( i * 4 + j );
		}
	}
}

inline void to_json( json& j, const mat4& b )
{
	std::vector<float> m;
	for ( length_t i = 0; i < 4; ++i )
	{
		for ( length_t j = 0; j < 4; ++j )
		{
			m.push_back( b[i][j] );
		}
	}
	j = json(m);
}

}   // namespace glm
