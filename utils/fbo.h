#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>

namespace std::filesystem
{
class path;
}

class Framebuffer
{
public:
	enum class TextureFormat
	{
		RGBA16F = 0,
		R32F
	};

	GLuint framebufferId;
	std::vector<GLuint> colorTextureTargets;
	std::vector<TextureFormat> _colorTextureTargetsFormat;
	GLuint depthBuffer;
	int width;
	int height;
	bool isComplete;
	bool hasDepth;
	uint32_t multisampleSamples = 0;

	Framebuffer();
	~Framebuffer();

	// It's either this or ref-counting to avoid freeing a framebufferId that's being used
	Framebuffer( const Framebuffer& ) = delete;
	Framebuffer& operator=( const Framebuffer& ) = delete;

	Framebuffer( Framebuffer&& ) = default;
	Framebuffer& operator=( Framebuffer&& ) = default;


	void bind();

	void clear_color(glm::vec4 rgba = glm::vec4(0, 0, 0, 0));
	void clear_depth(float depth = 1.f);
	void clear(glm::vec4 rgba = glm::vec4(0, 0, 0, 0), float depth = 1.f);
	void clear_colors(const std::vector<glm::vec4>& rgba_per_channel);

	void init( int numberOfColorBuffers = 1, bool includeDepth = false );

	void setMultisample( uint32_t num_samples );

	void resizeColorBufferCount(uint32_t numColorBuffers);

	void setColorBufferFormat( uint32_t buffer_idx, TextureFormat fmt );

	void addDepthBuffer();

	void resize(int w, int h);

	bool checkFramebufferComplete(void);

	bool getColorData( std::vector<float>* data, uint32_t colorBuffer = 0, uint32_t mipmapLevel = 0 );

	bool getColorData( std::vector<uint8_t>* data, uint32_t colorBuffer = 0, uint32_t mipmapLevel = 0 );

	bool saveToFile( const std::filesystem::path& path,
	                 uint32_t colorBuffer = 0,
	                 uint32_t mipmapLevel = 0,
	                 uint32_t num_channels = 4,
	                 glm::uvec4 channels = { 0, 1, 2, 3 } );

	GLuint swapColorTarget( size_t target, GLuint newTarget );

	GLuint swapDepthTarget(GLuint new_target);

	void blit( Framebuffer& to_framebuffer ) const;

	void free();

private:
	void updateFramebuffers();
};


