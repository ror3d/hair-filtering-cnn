#pragma once

#include <glm/glm.hpp>

#include <cmath>



namespace color_helpers
{

template<typename _F = float>
struct color_XYZ_t
{
	_F x = 0, y = 0, z = 0;

	color_XYZ_t() = default;
	color_XYZ_t( const color_XYZ_t& ) = default;
	color_XYZ_t( color_XYZ_t&& ) = default;
	color_XYZ_t(_F x, _F y, _F z) : x(x), y(y), z(z) {}
	explicit color_XYZ_t(const glm::vec3& c) : x(c.x), y(c.y), z(c.z) {}
	operator glm::vec3() const { return glm::vec3( x, y, z ); }

	color_XYZ_t<_F> to_xyz() const
	{
		return *this;
	}

	void from_xyz(const color_XYZ_t<_F>& c)
	{
		x = c.x;
		y = c.y;
		z = c.z;
	}

	color_XYZ_t& operator=( const color_XYZ_t& ) = default;
	color_XYZ_t& operator=( color_XYZ_t&& ) = default;
};

template<typename _F = float>
struct color_RGB_t
{
	_F r = 0, g = 0, b = 0;

	color_RGB_t() = default;
	color_RGB_t( const color_RGB_t& ) = default;
	color_RGB_t( color_RGB_t&& ) = default;
	color_RGB_t(_F r, _F g, _F b) : r(r), g(g), b(b) {}
	explicit color_RGB_t(const glm::vec3& c) : r(c.x), g(c.y), b(c.z) {}
	operator glm::vec3() const { return glm::vec3( r, g, b ); }

	color_XYZ_t<_F> to_xyz() const
	{
		return color_XYZ_t<_F>(((glm::vec3) *this) * glm::mat3( 0.4124564, 0.3575761, 0.1804375,
												    0.2126729, 0.7151522, 0.0721750,
												    0.0193339, 0.1191920, 0.9503041 ));
	}

	void from_xyz(const color_XYZ_t<_F>& c)
	{
		glm::vec3 v = ((glm::vec3)c) * glm::mat3( 3.2404542, -1.5371385, -0.4985314,
												-0.9692660, 1.8760108, 0.0415560,
												0.0556434, -0.2040259, 1.0572252 );
		r = v.x;
		g = v.y;
		b = v.z;
	}

	color_RGB_t& operator=( const color_RGB_t& ) = default;
	color_RGB_t& operator=( color_RGB_t&& ) = default;
};

template<typename _F = float>
struct color_HSV_t
{
	_F h = 0, s = 0, v = 0;

	color_HSV_t() = default;
	color_HSV_t( const color_HSV_t& ) = default;
	color_HSV_t( color_HSV_t&& ) = default;
	color_HSV_t(_F h, _F s, _F v) : h(h), s(s), v(v) {}
	explicit color_HSV_t(const glm::vec3& c) : h(c.x), s(c.y), v(c.z) {}
	explicit operator glm::vec3() const { return glm::vec3( h, s, v ); }

	color_XYZ_t<_F> to_xyz() const
	{
		color_RGB_t<_F> rgb;
		_F C = v * s;
		_F H = 360 * h / 60;
		_F X = C * (1 - glm::abs( glm::mod( H, _F(2) ) - 1 ));
		if ( H <= 1 )
		{
			rgb.r = C;
			rgb.g = X;
			rgb.b = 0;
		}
		else if ( H <= 2 )
		{
			rgb.r = X;
			rgb.g = C;
			rgb.b = 0;
		}
		else if ( H <= 3 )
		{
			rgb.r = 0;
			rgb.g = C;
			rgb.b = X;
		}
		else if ( H <= 4 )
		{
			rgb.r = 0;
			rgb.g = X;
			rgb.b = C;
		}
		else if ( H <= 5 )
		{
			rgb.r = X;
			rgb.g = 0;
			rgb.b = C;
		}
		else if ( H <= 6 )
		{
			rgb.r = C;
			rgb.g = 0;
			rgb.b = X;
		}
		_F m = v - C;

		rgb.r += m;
		rgb.g += m;
		rgb.b += m;

		return rgb.to_xyz();
	}

	void from_xyz(const color_XYZ_t<_F>& c)
	{
		color_RGB_t<_F> rgb;
		rgb.from_xyz( c );
		_F M = glm::max( rgb.r, glm::max( rgb.g, rgb.b ) );
		_F m = glm::min( rgb.r, glm::min( rgb.g, rgb.b ) );
		_F C = M - m;
		_F H = 0;
		if ( C < 0.00001 )
		{
			H = 0; // Undefined
		}
		else if ( M == rgb.r )
		{
			H = glm::modf( (rgb.g - rgb.b) / C, _F(6) );
		}
		else if ( M == rgb.g )
		{
			H = (rgb.b - rgb.r) / C + _F(2);
		}
		else
		{
			H = (rgb.r - rgb.g) / C + _F(4);
		}
		h = (H * 60) / 360.0;
		v = M;
		if ( M < 0.00001 )
		{
			s = 0;
		}
		else
		{
			s = C / v;
		}
	}

	color_HSV_t& operator=( const color_HSV_t& ) = default;
	color_HSV_t& operator=( color_HSV_t&& ) = default;
};

}

