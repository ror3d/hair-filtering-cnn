#ifndef LAB_MATH_UTILS_H
#define LAB_MATH_UTILS_H

/** This file contains utility functions to be used for the labs in computer
 * graphics at chalmers, they are not covered by any particular license...
 */

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include <cmath>

// Sometimes it exists, sometimes not...
#ifndef M_PI
#	define M_PI 3.14159265358979323846f
#endif 



namespace math_helpers {

	void set_rand_seed( uint32_t seed );


	/**
	* Random, uniformly distributed numbers in the interval [0, 1].
	*/
	float randf();

	/**
	* Random, uniformly distributed integers between 0 and UINT32_MAX
	*/
	uint32_t rand();

	/**
	* Random, uniformly distributed 2D points in the [0, 1] square
	*/
	glm::vec2 randf2();

	/**
	* Random, uniformly distributed 3D points in the [0, 1] cube
	*/
	glm::vec3 randf3();

	/**
	* Random, uniformly distributed numbers in the interval [from, to].
	*/
	float uniform_randf(const float from, const float to);


	/**
	* Generate uniform points on a disc
	*/
	void concentricSampleDisk(float *dx, float *dy);

	/**
	* Generate points with a cosine distribution on the hemisphere
	*/
	glm::vec3 cosineSampleHemisphere();

	glm::vec3 sampleSphereSurface();


	glm::vec3 sphericalCoordsToCartesian( glm::vec2 scoords );


	glm::quat qLookAt( glm::vec3 direction, glm::vec3 worldFwd, glm::vec3 headUp );
}
#endif // LAB_MATH_UTILS_H
