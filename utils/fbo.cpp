#include "fbo.h"
#include <cstdint>
#include <filesystem>
#include <utils/gl_helpers.h>
#include <stb/stb_image_write.h>
#include "str.h"
#include "log.h"

Framebuffer::Framebuffer()
    : isComplete(false)
	, framebufferId(0)
	, depthBuffer(0)
	, width(0)
	, height(0)
	, hasDepth(false)
{
}

void Framebuffer::setMultisample( uint32_t num_samples )
{
	// If textures were multisample and we are setting to 1 sample,
	// or they were not multisample and we are making them multisample,
	// we need to delete the old ones because otherwise it will complain about the storage type
	if ( (num_samples >= 1 && multisampleSamples == 0) || (num_samples == 0 && multisampleSamples >= 1) )
	{
		if ( depthBuffer != 0 )
		{
			glDeleteTextures( 1, &depthBuffer );
			depthBuffer = 0;
		}

		for ( auto& c : colorTextureTargets )
		{
			glDeleteTextures( 1, &c );
			c = 0;
		}
	}

	multisampleSamples = num_samples;
	isComplete = false;
	updateFramebuffers();
}

void Framebuffer::blit( Framebuffer& to_framebuffer ) const
{
	GLint program = 0;
	glGetIntegerv( GL_CURRENT_PROGRAM, &program );
	glUseProgram( 0 );

	for ( uint32_t i = 0; i < colorTextureTargets.size() && i < to_framebuffer.colorTextureTargets.size(); ++i )
	{
		glNamedFramebufferReadBuffer( framebufferId, GL_COLOR_ATTACHMENT0 + i );
		glNamedFramebufferDrawBuffer( to_framebuffer.framebufferId, GL_COLOR_ATTACHMENT0 + i );
		glBlitNamedFramebuffer( framebufferId,
								to_framebuffer.framebufferId,
								0,
								0,
								width,
								height,
								0,
								0,
								to_framebuffer.width,
								to_framebuffer.height,
								GL_COLOR_BUFFER_BIT,
								GL_NEAREST );
	}

	std::vector<GLenum> attachments;
	for(size_t i = 0; i < to_framebuffer.colorTextureTargets.size(); i++)
	{
		attachments.push_back( GL_COLOR_ATTACHMENT0 + i );
	}
	glNamedFramebufferDrawBuffers(to_framebuffer.framebufferId, attachments.size(), attachments.data());

	attachments.clear();
	for(size_t i = 0; i < colorTextureTargets.size(); i++)
	{
		attachments.push_back( GL_COLOR_ATTACHMENT0 + i );
	}
	glNamedFramebufferDrawBuffers(framebufferId, attachments.size(), attachments.data());


	glUseProgram( program );
}

void Framebuffer::resizeColorBufferCount( uint32_t numColorBuffers )
{
	if ( colorTextureTargets.size() > numColorBuffers )
	{
		GLenum texture_target = ( multisampleSamples == 0 ) ? GL_TEXTURE_2D : GL_TEXTURE_2D_MULTISAMPLE;
		for ( uint32_t i = colorTextureTargets.size() - 1; i > numColorBuffers; --i )
		{
			glDeleteTextures( 1, &colorTextureTargets[i] );
		}
		if ( framebufferId != 0 )
		{
			glBindFramebuffer(texture_target, framebufferId);

			for ( uint32_t i = colorTextureTargets.size() - 1; i > numColorBuffers; --i )
			{
				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, texture_target, 0, 0);
			}
			glBindFramebuffer(texture_target, 0);
		}
	}
	colorTextureTargets.resize( numColorBuffers, 0 );
	_colorTextureTargetsFormat.resize( numColorBuffers, TextureFormat::RGBA16F );
	isComplete = false;
	updateFramebuffers();
}

void Framebuffer::addDepthBuffer()
{
	if ( hasDepth )
	{
		return;
	}
	hasDepth = true;
	isComplete = false;
	updateFramebuffers();
}

Framebuffer::~Framebuffer()
{
	free();
}

void Framebuffer::free()
{
	if ( framebufferId != 0 )
	{
		glDeleteFramebuffers( 1, &framebufferId );
		framebufferId = 0;
	}

	if ( depthBuffer != 0 )
	{
		glDeleteTextures( 1, &depthBuffer );
		depthBuffer = 0;
	}

	for ( auto c : colorTextureTargets )
	{
		glDeleteTextures( 1, &c );
	}
	colorTextureTargets.clear();
	_colorTextureTargetsFormat.clear();

	colorTextureTargets.clear();
	_colorTextureTargetsFormat.clear();

	width = 0;
	height = 0;
	isComplete = false;
}

void Framebuffer::bind()
{
	glBindFramebuffer( GL_FRAMEBUFFER, framebufferId );
	glViewport( 0, 0, width, height );
}

void Framebuffer::clear_color( glm::vec4 rgba )
{
	for ( size_t i = 0; i < colorTextureTargets.size(); ++i )
	{
		glClearNamedFramebufferfv( framebufferId, GL_COLOR, i, &rgba.r );
	}
}

void Framebuffer::clear_depth( float depth )
{
	glClearNamedFramebufferfv( framebufferId, GL_DEPTH, 0, &depth );
}

void Framebuffer::clear(glm::vec4 rgba, float depth)
{
	clear_color( rgba );
	clear_depth( depth );
}

void Framebuffer::clear_colors(const std::vector<glm::vec4>& rgba_per_channel)
{
	for ( size_t i = 0; i < rgba_per_channel.size(); ++i )
	{
		auto color = rgba_per_channel[i];
		glClearNamedFramebufferfv( framebufferId, GL_COLOR, i, &color.r );
	}
}

void Framebuffer::init( int numberOfColorBuffers, bool includeDepth )
{
	hasDepth = includeDepth;
	colorTextureTargets.resize(numberOfColorBuffers, 0);
	_colorTextureTargetsFormat.resize( numberOfColorBuffers, TextureFormat::RGBA16F );
	isComplete = false;
}


void Framebuffer::resize( int w, int h )
{
	width = w;
	height = h;

	updateFramebuffers();
}

bool Framebuffer::getColorData( std::vector<float>* data, uint32_t colorBuffer, uint32_t mipmapLevel )
{
	if ( multisampleSamples >= 1 )
	{
		LOG_ERROR( "Cannot get pixel data from a multisample framebuffer! You must first resolve it onto a non multisampled framebuffer." );
		return false;
	}
	GLint lwidth, lheight;
	glGetTextureLevelParameteriv( colorTextureTargets[colorBuffer], mipmapLevel, GL_TEXTURE_WIDTH, &lwidth );
	glGetTextureLevelParameteriv( colorTextureTargets[colorBuffer], mipmapLevel, GL_TEXTURE_HEIGHT, &lheight );
	data->resize( size_t(lwidth) * lheight * 4 );
	glGetTextureSubImage( colorTextureTargets[colorBuffer], mipmapLevel, 0, 0, 0, lwidth, lheight, 1, GL_RGBA, GL_FLOAT, data->size() * sizeof( float ), data->data() );
	return true;
}

bool Framebuffer::getColorData( std::vector<uint8_t>* data, uint32_t colorBuffer, uint32_t mipmapLevel )
{
	if ( multisampleSamples >= 1 )
	{
		LOG_ERROR( "Cannot get pixel data from a multisample framebuffer! You must first resolve it onto a non multisampled framebuffer." );
		return false;
	}
	GLint lwidth, lheight;
	glGetTextureLevelParameteriv( colorTextureTargets[colorBuffer], mipmapLevel, GL_TEXTURE_WIDTH, &lwidth );
	glGetTextureLevelParameteriv( colorTextureTargets[colorBuffer], mipmapLevel, GL_TEXTURE_HEIGHT, &lheight );
	data->resize( size_t(lwidth) * lheight * 4 );
	glGetTextureSubImage( colorTextureTargets[colorBuffer], mipmapLevel, 0, 0, 0, lwidth, lheight, 1, GL_RGBA, GL_UNSIGNED_BYTE, data->size(), data->data() );
	return true;
}

void Framebuffer::setColorBufferFormat( uint32_t buffer, TextureFormat fmt )
{
	_colorTextureTargetsFormat[buffer] = fmt;
}

bool Framebuffer::saveToFile(
        const std::filesystem::path& path, uint32_t colorBuffer, uint32_t mipmapLevel, uint32_t num_channels, glm::uvec4 channels )
{
	if ( num_channels == 0 )
	{
		LOG_ERROR( "Can't export 0 channels! Nothing will be exported." );
		return false;
	}
	if ( num_channels > 4 )
	{
		LOG_WARN( "Can't export more than 4 channels at a time! Clamping num_channels to 4." );
		num_channels = 4;
	}
	for ( size_t c = 0; c < num_channels; ++c )
	{
		if ( channels[c] >= 4 )
		{
			LOG_WARN( "Channel index is too high! Must be values from 0 to 3. Clamping to 3." );
			channels[c] = 3;
		}
	}

	std::vector<uint8_t> inv_img;
	getColorData( &inv_img, colorBuffer, mipmapLevel );

	GLint lwidth, lheight;
	glGetTextureLevelParameteriv( colorTextureTargets[colorBuffer], mipmapLevel, GL_TEXTURE_WIDTH, &lwidth );
	glGetTextureLevelParameteriv( colorTextureTargets[colorBuffer], mipmapLevel, GL_TEXTURE_HEIGHT, &lheight );
	std::vector<uint8_t> img( size_t(lwidth) * lheight * num_channels );

	for ( size_t j = 0; j < lheight; ++j )
	{
		for ( size_t i = 0; i < lwidth; ++i )
		{
			size_t tgt = ((lheight - j - 1) * lwidth + i) * num_channels;
			size_t src = (j * lwidth + i) * 4;
			for ( size_t c = 0; c < num_channels; ++c )
			{
				img[tgt + c] = inv_img[src + channels[c]];
			}
		}
	}
	if ( str::tolower(path.extension().string()) == ".png" )
	{
		stbi_write_png( path.string().c_str(), lwidth, lheight, num_channels, img.data(), 0 );
	}
	else
	{
		LOG_ERROR( "Only exporting to PNG is implemented." );
		return false;
	}
	return true;
}

void Framebuffer::updateFramebuffers()
{
	if ( width == 0 || height == 0 )
	{
		return;
	}
	if ( (colorTextureTargets.empty() || colorTextureTargets.size() != _colorTextureTargetsFormat.size())
		 && !hasDepth )
	{
		LOG_ERROR( "Framebuffer not initialized! You need to call init()." );
		return;
	}

	///////////////////////////////////////////////////////////////////////
	// if no texture indices yet, allocate
	///////////////////////////////////////////////////////////////////////
	GLenum texture_target = ( multisampleSamples == 0 ) ? GL_TEXTURE_2D : GL_TEXTURE_2D_MULTISAMPLE;
	for(auto& colorTextureTarget : colorTextureTargets)
	{
		if(colorTextureTarget == 0)
		{
			glCreateTextures(texture_target, 1, &colorTextureTarget);
			if ( multisampleSamples == 0 )
			{
				glBindTexture(texture_target, colorTextureTarget);
				glTexParameteri( texture_target, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
				glTexParameteri( texture_target, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
			}
		}
	}

	if(hasDepth && depthBuffer == 0)
	{
		glCreateTextures(texture_target, 1, &depthBuffer);
		if ( multisampleSamples == 0 )
		{
			glBindTexture(texture_target, depthBuffer);
			glTexParameteri( texture_target, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
			glTexParameteri( texture_target, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
			glTexParameteri( texture_target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
			glTexParameteri( texture_target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
		}
	}

	///////////////////////////////////////////////////////////////////////
	// Allocate / Resize textures
	///////////////////////////////////////////////////////////////////////
	for(size_t i = 0; i < colorTextureTargets.size(); ++i)
	{
		GLuint colorTextureTarget = colorTextureTargets[i];
		GLenum format;
		switch ( _colorTextureTargetsFormat[i] )
		{
			case TextureFormat::R32F:
				format = GL_R32F;
				break;
			default:
				format = GL_RGBA16F;
				break;
		}
		if ( multisampleSamples == 0 )
		{
			glBindTexture(texture_target, colorTextureTarget);
			glTexImage2D(texture_target, 0, format, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
		}
		else
		{
			glBindTexture( texture_target, colorTextureTarget );
			glTexImage2DMultisample( texture_target, multisampleSamples, format, width, height, GL_TRUE );
		}
	}

	if ( hasDepth )
	{
		glBindTexture( texture_target, depthBuffer );
		if ( multisampleSamples == 0 )
		{
			glTexImage2D( texture_target, 0, GL_DEPTH_COMPONENT32F, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT,
						  nullptr );
		}
		else
		{
			glTexImage2DMultisample( texture_target, multisampleSamples, GL_DEPTH_COMPONENT32F, width, height, GL_TRUE );
		}
	}
	glBindTexture( texture_target, 0 );

	///////////////////////////////////////////////////////////////////////
	// Bind textures to framebuffer (if not already done)
	///////////////////////////////////////////////////////////////////////
	if(!isComplete)
	{
		///////////////////////////////////////////////////////////////////////
		// Generate and bind framebuffer
		///////////////////////////////////////////////////////////////////////
		if ( framebufferId == 0 )
		{
			glGenFramebuffers( 1, &framebufferId );
		}
		glBindFramebuffer(GL_FRAMEBUFFER, framebufferId);

		// Bind the color textures as color attachments
		std::vector<GLenum> attachments;
		for(size_t i = 0; i < colorTextureTargets.size(); i++)
		{
			glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, texture_target, colorTextureTargets[i], 0 );
			attachments.push_back( GL_COLOR_ATTACHMENT0 + i );
		}
		if ( attachments.size() > 0 )
		{
			glDrawBuffers( colorTextureTargets.size(), attachments.data() );
		}

		if ( hasDepth )
		{
			// bind the texture as depth attachment (to the currently bound framebuffer)
			glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture_target, depthBuffer, 0 );
		}

		// check if framebuffer is complete
		isComplete = checkFramebufferComplete();

		// bind default framebuffer, just in case.
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

GLuint Framebuffer::swapColorTarget( size_t target_id, GLuint new_target )
{
	GLenum texture_target = ( multisampleSamples == 0 ) ? GL_TEXTURE_2D : GL_TEXTURE_2D_MULTISAMPLE;

	GLuint old = colorTextureTargets[target_id];
	colorTextureTargets[target_id] = new_target;
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferId);
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + target_id, texture_target, new_target, 0 );
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return old;
}

GLuint Framebuffer::swapDepthTarget(GLuint new_target)
{
	GLenum texture_target = ( multisampleSamples == 0 ) ? GL_TEXTURE_2D : GL_TEXTURE_2D_MULTISAMPLE;

	GLuint old = depthBuffer;
	depthBuffer = new_target;
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferId);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture_target, new_target, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	return old;
}

bool Framebuffer::checkFramebufferComplete(void)
{
	// Check that our FBO is correctly set up, this can fail if we have
	// incompatible formats in a buffer, or for example if we specify an
	// invalid drawbuffer, among things.
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferId);
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if(status != GL_FRAMEBUFFER_COMPLETE)
	{
		gl_helpers::fatal_error("Framebuffer not complete");
	}

	return (status == GL_FRAMEBUFFER_COMPLETE);
}

