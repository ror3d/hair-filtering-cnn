#pragma once
#include <vector>
#include <string>

/*
 * Original code by polfosol ( https://stackoverflow.com/users/5358284/polfosol-%E0%B0%A0-%E0%B0%A0 )
 * https://stackoverflow.com/a/37109258/672151
 */
static const char* B64chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static const uint8_t B64index[256] = { 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	                               0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	                               0,  62, 63, 62, 62, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 0,  0,  0,  0,  0,
	                               0,  0,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 16, 17, 18,
	                               19, 20, 21, 22, 23, 24, 25, 0,  0,  0,  0,  63, 0,  26, 27, 28, 29, 30, 31, 32, 33,
	                               34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51 };

static std::string b64encode( const void* data, const size_t& len )
{
	std::string result( ( len + 2 ) / 3 * 4, '=' );
	uint8_t* p = (uint8_t*)data;
	char* str = &result[0];
	size_t j = 0, pad = len % 3;
	const size_t last = len - pad;

	for ( size_t i = 0; i < last; i += 3 )
	{
		int n = int( p[i] ) << 16 | int( p[i + 1] ) << 8 | p[i + 2];
		str[j++] = B64chars[n >> 18];
		str[j++] = B64chars[n >> 12 & 0x3F];
		str[j++] = B64chars[n >> 6 & 0x3F];
		str[j++] = B64chars[n & 0x3F];
	}
	if ( pad )   /// set padding
	{
		int n = --pad ? int( p[last] ) << 8 | p[last + 1] : p[last];
		str[j++] = B64chars[pad ? n >> 10 & 0x3F : n >> 2];
		str[j++] = B64chars[pad ? n >> 4 & 0x03F : n << 4 & 0x3F];
		str[j++] = pad ? B64chars[n << 2 & 0x3F] : '=';
	}
	return result;
}

static std::vector<uint8_t> b64decode( const char* p, size_t len )
{
	if ( len == 0 )
	{
		return {};
	}

	for ( ; len > 0 && (p[len-1] == '\r' || p[len-1] == '\n'); len -= 1 )
	{
	}

	if ( len == 0 )
	{
		return {};
	}

	size_t newlines = 0;
	for ( size_t i = 0; i < len; i += 1 )
	{
		if ( p[i] == '\n' || p[i] == '\r' )
		{
			newlines += 1;
		}
	}
	size_t actual_len = len - newlines;

	size_t pad1 = actual_len % 4 || p[len - 1] == '=';
	size_t pad2 = pad1 && (actual_len % 4 > 2 || p[len - 2] != '=');

	const size_t num_decoded_chars = (( actual_len - pad1 ) / 4) << 2;
	const size_t last_char = num_decoded_chars + newlines;
	std::vector<uint8_t> result( (num_decoded_chars / 4) * 3 + pad1 + pad2, '\0' );
	uint8_t* str = (uint8_t*)&result[0];

	size_t j = 0;
	for ( size_t i = 0; i < last_char; )
	{
		while ( p[i] == '\r' || p[i] == '\n' ) i += 1;
		int a = B64index[p[i]];
		i += 1;
		while ( p[i] == '\r' || p[i] == '\n' ) i += 1;
		int b = B64index[p[i]];
		i += 1;
		while ( p[i] == '\r' || p[i] == '\n' ) i += 1;
		int c = B64index[p[i]];
		i += 1;
		while ( p[i] == '\r' || p[i] == '\n' ) i += 1;
		int d = B64index[p[i]];
		i += 1;

		int n = a << 18 | b << 12 | c << 6 | d;
		str[j++] = n >> 16;
		str[j++] = n >> 8 & 0xFF;
		str[j++] = n & 0xFF;
	}
	if ( pad1 )
	{
		int n = B64index[p[last_char]] << 18 | B64index[p[last_char + 1]] << 12;
		str[j++] = n >> 16;
		if ( pad2 )
		{
			n |= B64index[p[last_char + 2]] << 6;
			str[j++] = n >> 8 & 0xFF;
		}
	}
	return result;
}


static std::vector<float> b64decode_floats( const char* p, size_t len )
{
	auto data = b64decode( p, len );
	std::vector<float> fdata;
	if ( data.size() % sizeof( float ) != 0 )
	{
		LOG_ERROR( "Input data size not congruent with the size of a floating point array!" );
	}
	fdata.resize( data.size() / sizeof( float ) );
	memcpy( fdata.data(), data.data(), data.size() );
	return fdata;
}

static std::vector<float> b64decode_floats( const std::string& b64 )
{
	return b64decode_floats( b64.data(), b64.size() );
}

