#pragma once

#include <string>
#include <vector>
#include <algorithm>


namespace str
{
using std::string;

inline char toupper( char c )
{
	return std::toupper( c );
}

inline char tolower( char c )
{
	return std::tolower( c );
}

inline string tolower( const string& s )
{
	string r(s);
	std::transform( r.begin(), r.end(), r.begin(), (char(*)(char))str::tolower );
	return r;
}

inline string toupper( const string& s )
{
	string r(s);
	std::transform( r.begin(), r.end(), r.begin(), (char(*)(char))str::toupper );
	return r;
}

inline std::vector<string> split( const string& s, const string& sep )
{
	std::vector<string> vec;
	size_t start = 0;
	size_t end = s.find( sep );
	while ( end != s.npos )
	{
		vec.push_back( s.substr( start, end - start ) );
		start = end + sep.length();
		end = s.find( sep, start );
	}
	vec.push_back( s.substr( start ) );

	return vec;
}

// This is not optimized at all and can't handle unicode in the second or third parameter
inline string replace_any( string s, const string& chars_to_replace, char replacement )
{
	for ( size_t i = 0; i < s.length(); ++i )
	{
		for ( size_t j = 0; j < chars_to_replace.length(); ++j )
		{
			if ( s[i] == chars_to_replace[j] )
			{
				s[i] = replacement;
			}
		}
	}
	return s;
}

inline string strip_invalid_filename_chars( const string& s )
{
	string r;
	r.reserve( s.length() );

	for ( size_t i = 0; i < s.length(); ++i )
	{
		char c = s[i];
		if ( (c >= 'a' && c <= 'z')
			 || (c >= 'A' && c <= 'Z')
			 || (c >= '0' && c <= '9')
			 || c == '_'
			 || c == '-'
			 || c == '.'
			 || c == '('
			 || c == ')'
			 || c == '['
			 || c == ']'
			 || c == '+'
			 || c == '*'
			 || c == ' '
			 )
		{
			r.push_back( c );
		}
		else if ( r.length() == 0 || r.back() != '-' )
		{
			r.push_back( '-' );
		}
	}

	return r;
}

}
