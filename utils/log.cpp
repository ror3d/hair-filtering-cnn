#include "log.h"

#ifdef _WIN32
#include <Windows.h>
#else
#include <csignal>
#endif

#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

namespace clog
{

void log_( Level l, const std::string& s )
{
	std::stringstream out_s;

	switch ( l )
	{
		case clog::Level::Error:
			out_s << "[Error] ";
			break;
		case clog::Level::Warning:
			out_s << "[Warning] ";
			break;
		case clog::Level::Info:
			out_s << "[Info] ";
			break;
		case clog::Level::Verbose:
			out_s << "[Verbose] ";
			break;
		default:
			break;
	}

	std::time_t t = std::time( nullptr );
	std::tm* timeinfo = std::localtime( &t );
	out_s << std::put_time( timeinfo, " (%X) " );

	out_s << s << "\n";

#ifdef _WIN32
	OutputDebugStringA( out_s.str().c_str() );
#endif
	std::cerr << out_s.str();
}

}

