#pragma once

#include <string>
#include <fmt/format.h>
#include <fmt/printf.h>

#ifdef _WIN32
extern "C"
{
typedef int BOOL;
__declspec(dllimport) BOOL __stdcall IsDebuggerPresent(void);
__declspec(dllimport) void __stdcall DebugBreak(void);
}
#endif

namespace clog
{

enum class Level { Error, Warning, Info, Verbose };

void log_( Level, const std::string& );

template<typename ... T>
void log( Level l, T&&... t)
{
	std::string s = fmt::format( t... );
	log_( l, s );
}

///////////////////////////////////////////////////////////////////////////////
// Breaking asserts and errors
///////////////////////////////////////////////////////////////////////////////
#define STRINGIFY( x ) STRINGIFY2( x )
#define STRINGIFY2( x ) #x
#ifdef _WIN32
#define DEBUG_BREAK() do { if ( IsDebuggerPresent() ) { DebugBreak(); } } while(0)
#else
#define DEBUG_BREAK() raise(SIGTRAP)
#endif

#define LOG_FATAL(...) do { clog::log(clog::Level::Error, __VA_ARGS__); DEBUG_BREAK(); exit(-1); } while (0)

#define LOG_ERROR(...) do { clog::log(clog::Level::Error, __VA_ARGS__); DEBUG_BREAK(); } while (0)

#define LOG_WARN(...) do { clog::log(clog::Level::Warning, __VA_ARGS__); } while (0)

#define LOG_INFO(...) do { clog::log(clog::Level::Info, __VA_ARGS__); } while (0)

#define LOG_VERBOSE(...) do { clog::log(clog::Level::Verbose, __VA_ARGS__); } while (0)

#define DebugAssert( x )                                                                          \
	do {                                                                                          \
		if ( !( x ) )                                                                             \
		{                                                                                         \
			LOG_ERROR("Assertion failed : {} in {}({})", #x, __FILE__, __LINE__);                 \
			DEBUG_BREAK();                                                                        \
		}                                                                                         \
	} while ( 0 )
#define DebugError( x )                                                                 \
	do {                                                                                \
		LOG_ERROR("Error in {}({}): {}", __FILE__, __LINE__, x);                        \
		DEBUG_BREAK();                                                                  \
	} while ( 0 )


}