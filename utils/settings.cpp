#include "settings.h"
#include <utils/log.h>

#include <utils/json.hpp>
#include <utils/inipp.h>
#include <utils/str.h>

#include <fstream>

chag::Settings& chag::Settings::getGlobalInstance()
{
	static Settings instance;
	return instance;
}

void chag::Settings::bindCommandArgument( const std::string& arg, const std::string& setting )
{
	if ( acceptable_arguments.find( arg ) != acceptable_arguments.end() )
	{
		LOG_ERROR( "Argument {} is already defined.", arg );
		return;
	}
	acceptable_arguments[arg] = argument_t{ false, setting };

	checkForCommandArgument( arg );
}

void chag::Settings::bindCommandArgument( const std::string& arg, const std::string& setting, const std::string& default_value )
{
	if ( acceptable_arguments.find( arg ) != acceptable_arguments.end() )
	{
		LOG_ERROR( "Argument {} is already defined.", arg );
		return;
	}
	acceptable_arguments[arg] = argument_t{ false, setting };

	settings[setting] = setting_t{ {default_value}, false };

	checkForCommandArgument( arg );
}

void chag::Settings::bindCommandFlag( const std::string& arg, const std::string& setting )
{
	if ( acceptable_arguments.find( arg ) != acceptable_arguments.end() )
	{
		LOG_ERROR( "Argument {} is already defined.", arg );
		return;
	}
	acceptable_arguments[arg] = argument_t{ true, setting };

	settings[setting] = setting_t{ {false}, false };

	checkForCommandArgument( arg );
}

void chag::Settings::addCommandArguments( int argc, const char** argv )
{
	for ( size_t i = 0; i < argc; ++i )
	{
		received_arguments.push_back( argv[i] );
	}

	for ( size_t i = 0; i < received_arguments.size(); ++i )
	{
		std::string arg = received_arguments[i];
		if ( acceptable_arguments.find( arg ) == acceptable_arguments.end() )
		{
			continue;
		}

		std::string setting_name = acceptable_arguments[arg].setting;
		setting_t setting;
		setting.persist = false;

		if ( acceptable_arguments[arg].is_flag )
		{
			setting.value = true;
		}
		else
		{
			i += 1;
			if ( i >= received_arguments.size() )
			{
				LOG_FATAL( "Provided argument {} expects an extra value following it!", arg );
			}
			setting.value = received_arguments[i];
		}
		settings[setting_name] = std::move( setting );
	}
}

void chag::Settings::setFile( const std::filesystem::path& file )
{
	settings_file_path = file;
}

void chag::Settings::loadFromFile()
{
	std::ifstream file( settings_file_path );
	if ( !file )
	{
		LOG_WARN( "No settings file present." );
		return;
	}
	if ( settings_file_path.extension() == ".json" )
	{
		using namespace nlohmann;
		json js;
		try
		{
			js = json::parse( file );
		}
		catch ( std::exception& e )
		{
			LOG_ERROR( "Error when loading settings file." );
			//DebugError( e.what() );
			return;
		}

		for ( auto& entry : js.items() )
		{
			setting_t setting;
			setting.persist = true;
			if ( entry.value().is_boolean() )
			{
				setting.value = entry.value().get<bool>();
			}
			else if ( entry.value().is_string() )
			{
				setting.value = entry.value().get<std::string>();
			}
			else
			{
				setting.value = entry.value().dump();
			}
			settings[entry.key()] = setting;
		}
	}
	else if ( settings_file_path.extension() == ".ini" )
	{
		using namespace inipp;
		Ini<char> ini;
		try
		{
			ini.parse( file );
		}
		catch ( std::exception& e )
		{
			LOG_ERROR( "Error when loading settings file." );
			//DebugError( e.what() );
			return;
		}

		for ( auto& [sec, vars] : ini.sections )
		{
			for ( auto& [k, v] : vars )
			{
				setting_t setting;
				setting.persist = true;
				if ( str::tolower( v ) == "true" )
				{
					setting.value = true;
				}
				else if ( str::tolower( v ) == "false" )
				{
					setting.value = false;
				}
				else
				{
					setting.value = v;
				}
				std::string key = k;
				if ( !sec.empty() )
				{
					key = sec + ":" + key;
				}
				settings[key] = setting;
			}
		}
	}
}

void chag::Settings::saveToFile()
{
	std::ofstream file( settings_file_path );
	try
	{
		if ( !file )
		{
			LOG_ERROR( "Failed to open settings file for writing." );
			return;
		}
	}
	catch ( std::exception& e )
	{
		LOG_ERROR( "Error when writing to settings file." );
		//DebugError( e.what() );
		return;
	}

	if ( settings_file_path.extension() == ".json" )
	{
		using namespace nlohmann;
		json js = json::object();

		for ( auto& [k, s] : settings )
		{
			if ( s.persist )
			{
				if ( std::holds_alternative<bool>( s.value ) )
				{
					js[k] = std::get<bool>( s.value );
				}
				else
				{
					js[k] = std::get<std::string>( s.value );
				}
			}
		}
		file << js.dump( 4 ) << std::endl;
	}
	else if ( settings_file_path.extension() == ".ini" )
	{
		using namespace inipp;
		Ini<char> ini;
		for ( auto& [k, s] : settings )
		{
			if ( s.persist )
			{
				auto key = str::split( k, ":" );
				if ( key.size() == 1 )
				{
					ini.sections[""][key[0]] = get( k, "" );
				}
				else
				{
					ini.sections[key[0]][key[1]] = get( k, "" );
				}
			}
		}
		ini.generate( file );
	}
}

std::string chag::Settings::get( const std::string& setting, const std::string& default_value )
{
	if ( settings.find( setting ) == settings.end() )
	{
		return default_value;
	}
	if ( std::holds_alternative<std::string>( settings[setting].value ) )
	{
		return std::get<std::string>( settings[setting].value );
	}
	else
	{
		if ( std::get<bool>( settings[setting].value ) )
		{
			return "true";
		}
		else
		{
			return "false";
		}
	}
}

std::string chag::Settings::get( const char* setting, const std::string& default_value )
{
	return get( std::string( setting ), default_value );
}

std::string chag::Settings::get( const std::string& setting, const char* default_value )
{
	return get( setting, std::string( default_value ) );
}

std::string chag::Settings::get( const char* setting, const char* default_value )
{
	return get( std::string( setting ), std::string( default_value ) );
}

bool chag::Settings::get( const char* setting, bool default_value )
{
	return get( std::string( setting ), default_value );
}

bool chag::Settings::get( const std::string& setting, bool default_value )
{
	if ( settings.find( setting ) == settings.end() )
	{
		return default_value;
	}
	if ( std::holds_alternative<bool>( settings[setting].value ) )
	{
		return std::get<bool>( settings[setting].value );
	}
	else
	{
		return false;
	}
}

void chag::Settings::set( const std::string& setting, bool value, bool persistent )
{
	settings[setting] = setting_t{ {value}, persistent };
}

void chag::Settings::set( const std::string& setting, const std::string& value, bool persistent )
{
	settings[setting] = setting_t{ {value}, persistent };
}

void chag::Settings::set( const std::string& setting, const char* value, bool persistent )
{
	set( setting, std::string( value ), persistent );
}

void chag::Settings::checkForCommandArgument( const std::string& arg )
{
	for ( size_t i = 0; i < received_arguments.size(); ++i )
	{
		if ( received_arguments[i] != arg )
		{
			continue;
		}

		std::string setting_name = acceptable_arguments[arg].setting;
		setting_t setting;
		setting.persist = false;

		if ( acceptable_arguments[arg].is_flag )
		{
			setting.value = true;
		}
		else
		{
			i += 1;
			if ( i >= received_arguments.size() )
			{
				LOG_FATAL( "Provided argument {} expects an extra value following it!", arg );
			}
			setting.value = received_arguments[i];
		}
		settings[setting_name] = std::move( setting );
	}
}




