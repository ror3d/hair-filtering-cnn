#include "math_helpers.h"

#include <cmath>
#include <cstring>
#include <cstdlib>

#include <algorithm>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include <pcg/pcg_basic.h>

namespace math_helpers
{

void set_rand_seed( uint32_t seed )
{
	pcg32_srandom( seed, 0 );
}

uint32_t rand()
{
	return pcg32_random();
}

float uniform_randf(const float from, const float to)
{
	return from + (to - from) * float(pcg32_random()) / float(UINT32_MAX);
}

float randf()
{
	return float(pcg32_random()) / float(UINT32_MAX);
}

glm::vec2 randf2()
{
	return { randf(), randf() };
}

glm::vec3 randf3()
{
	return { randf(), randf(), randf() };
}

///////////////////////////////////////////////////////////////////////////
// Generate uniform points on a disc
///////////////////////////////////////////////////////////////////////////
void concentricSampleDisk(float* dx, float* dy)
{
	float r, theta;
	float u1 = randf();
	float u2 = randf();
	// Map uniform random numbers to $[-1,1]^2$
	float sx = 2 * u1 - 1;
	float sy = 2 * u2 - 1;
	// Map square to $(r,\theta)$
	// Handle degeneracy at the origin
	if(sx == 0.0 && sy == 0.0)
	{
		*dx = 0.0;
		*dy = 0.0;
		return;
	}
	if(sx >= -sy)
	{
		if(sx > sy)
		{ // Handle first region of disk
			r = sx;
			if(sy > 0.0)
				theta = sy / r;
			else
				theta = 8.0f + sy / r;
		}
		else
		{ // Handle second region of disk
			r = sy;
			theta = 2.0f - sx / r;
		}
	}
	else
	{
		if(sx <= sy)
		{ // Handle third region of disk
			r = -sx;
			theta = 4.0f - sy / r;
		}
		else
		{ // Handle fourth region of disk
			r = -sy;
			theta = 6.0f + sx / r;
		}
	}
	theta *= float(M_PI) / 4.0f;
	*dx = r * cosf(theta);
	*dy = r * sinf(theta);
}
///////////////////////////////////////////////////////////////////////////
// Generate points with a cosine distribution on the hemisphere
///////////////////////////////////////////////////////////////////////////
glm::vec3 cosineSampleHemisphere()
{
	glm::vec3 ret;
	concentricSampleDisk(&ret.x, &ret.y);
	ret.z = sqrt(glm::max(0.f, 1.f - ret.x * ret.x - ret.y * ret.y));
	return ret;
}

glm::vec3 sampleSphereSurface()
{
	float theta = randf() * 2 * M_PI;
	float phi = acosf( 2*randf() - 1.f );
	return sphericalCoordsToCartesian( { theta, phi } );
}

glm::vec3 sphericalCoordsToCartesian( glm::vec2 scoords )
{
	return glm::vec3(glm::cos(scoords.x) * glm::sin(scoords.y), glm::sin(scoords.x) * glm::sin(scoords.y), glm::cos(scoords.y));
}

glm::quat qLookAt( glm::vec3 dir, glm::vec3 worldFwd, glm::vec3 up )
{
	dir = normalize( dir );
	using namespace glm;
	quat q;
	if ( abs(1 - abs(dot(dir, up))) < 1e-6 )
	{
		q = rotation( vec3( 0, 1, 0 ), normalize(dir) );
	}
	else
	{
		vec3 wright = normalize( cross( worldFwd, up ) );
		vec3 right = normalize(cross(dir, up));
		if ( abs( dot( wright, right ) + 1 ) < 1e-7 ) // 180deg rotation
		{
			q = angleAxis( glm::pi<float>(), up );
		}
		else
		{
			q = rotation( wright, right );
		}
		vec3 localfwd = rotate( q, worldFwd );
		quat p = rotation( localfwd, dir );
		q = p * q;
	}
	return q;
}

} // namespace labhelper
