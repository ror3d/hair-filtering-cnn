#include <vector>
#include <string>
#include <filesystem>
#include <glm/glm.hpp>


class Image
{
public:
	uint32_t width, height;
	std::vector<glm::u8vec4> data;
};

class ImageF
{
public:
	uint32_t width, height;
	std::vector<glm::vec4> pixels;


	bool load( std::filesystem::path path );
};

