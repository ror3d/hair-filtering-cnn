#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <filesystem>
#include <variant>

using namespace std::string_literals;

namespace chag
{

class Settings
{
	struct setting_t
	{
		std::variant<std::string, bool> value;
		bool persist = true;
	};

	std::unordered_map<std::string, setting_t> settings;

	struct argument_t
	{
		bool is_flag = false;
		std::string setting;
	};

	std::unordered_map<std::string, argument_t> acceptable_arguments;

	std::vector<std::string> received_arguments;

	std::filesystem::path settings_file_path;

public:
	static Settings& getGlobalInstance();

	void bindCommandArgument( const std::string& arg, const std::string& setting );
	void bindCommandArgument( const std::string& arg, const std::string& setting, const std::string& default_value );
	void bindCommandFlag( const std::string& arg, const std::string& setting );
	void addCommandArguments( int argc, const char** argv );

	void setFile( const std::filesystem::path& file );

	void loadFromFile();

	void saveToFile();

	std::string get( const std::string& setting, const std::string& default_value );
	std::string get( const char* setting, const std::string& default_value );
	std::string get( const std::string& setting, const char* default_value );
	std::string get( const char* setting, const char* default_value );
	bool get( const std::string& setting, bool default_value );
	bool get( const char* setting, bool default_value );

	void set( const std::string& setting, const std::string& value, bool persistent = true );
	void set( const std::string& setting, const char* value, bool persistent = true );
	void set( const std::string& setting, bool value, bool persistent = true );

private:

	void checkForCommandArgument( const std::string& arg );
};

}

